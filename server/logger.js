import winston from 'winston';
import path from 'path';

export const emailLogger = winston.loggers.add('email', {
  console: {
    level: 'info',
    colorize: true,
    label: 'Email Sent',
  },
  file: {
    filename: `${path.resolve()}/email.log`,
  },
});

export const userLogger = winston.loggers.add('user', {
  console: {
    level: 'info',
    colorize: true,
    label: 'User',
  },
  file: {
    filename: `${path.resolve()}/user.log`,
  },
});

export const organizationLogger = winston.loggers.add('organization', {
  console: {
    level: 'info',
    colorize: true,
    label: 'Organization',
  },
  file: {
    filename: `${path.resolve()}/organization.log`,
  },
});
