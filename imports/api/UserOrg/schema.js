import { Meteor } from 'meteor/meteor';
import { Class } from 'meteor/jagi:astronomy';

import Organization from '/imports/api/Organization';

import UserPermissionsContract from '/imports/api/helpers/permissions';
export const UserOrgs = new Mongo.Collection('usersOrg');


const UserOrg = Class.create({
  name: 'UserOrg',
  collection: UserOrgs,
  fields: {
    createdAt: Date,
    email: {
      type: String,
      optional: true,
    },
    emails: {
      type: [Object],
      default: () => [{ address: '', verified: true }],
      optional: true,
    },
    name: {
      type: String,
    },
    path: {
      type: String,
      transient: true,
      resolve(doc) {
        return `${UserOrg.path}/${doc._id}`;
      },
    },
    phone: {
      type: String,
      default: '',
    },
    role: {
      type: String,
      default: '',
    },
    type: {
      type: String,
      transient: true,
      resolve(doc) {
          return ((role) => {
              switch (role) {
                  case 'volunteer':
                  case 'organization':
                      return 'Volunteer';
                      break;
                  case 'caseworker':
                      return 'Caseworker';
                      break;
                  case 'administrator':
                      return 'Administrator';
                      break;
                  default:
                      return 'User';
              }
          })(doc.role);
      },
    },
    organizationId: {
      type: String,
      default: '',
    },
    orgName: {
      type: [String],
      default: () => [],
    },
    cpNumber: {
      type: [Number],
      default: () => [],
    },
    location: {
      type: [Number],
      default: () => [],
    },
  },
});

UserOrg.publicFields = {
  _id: 1,
  name: 1,
  organizationId: "",
  email: "",

};

export default UserOrg;
