import { Meteor } from 'meteor/meteor';

import UserOrg from '/imports/api/UserOrg';
import Business from '/imports/api/Organization/Business';

Meteor.publish({
  '/userOrg/list'() {
    console.log("api/User/server/publications:/userOrg/list");

    const user = Meteor.user();
    let orgId =null;
    if (user) {
      orgId = user.profile.organizationId;
    }

    const cursors = [];

    let userCursor = UserOrg.find({}, {sort: {"createdAt": -1}});
    if (user.constructor) {
      console.log("api/User/server/publications:/user/read:constructor.className:" + user.constructor.className);
    } else {
      console.log("api/User/server/publications:/user/read:constructor.className:empty");
    }
    switch (user.constructor.className) {
      case 'Volunteer': {
        if (orgId) {
          let location = Business.find()
              .fetch()
              .filter(organization => organization._id === orgId)
              .map(organization => organization.location);

          let orgs = Business.find()
              .fetch()
              .filter(organization => organization.location === parseInt(location, 0))
              //.map(organization => organization._id);
              .map(organization => organization);

          userCursor = UserOrg.find({"organizationId": {$in: orgs.map(org => org._id)}}, {sort: {"createdAt": -1}});
        }
        break;
      }
      case 'Caseworker': {
        userCursor = {};
        break;
      }
      case 'Administrator': {
        break;
      }
      default:
        break;
    }

    console.log("api/User/server/publications:/userOrg/all:orgId:" + orgId);

    cursors.push(userCursor);

    return cursors;
  },
});
