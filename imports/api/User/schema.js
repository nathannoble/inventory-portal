import { Meteor } from 'meteor/meteor';
import { Class } from 'meteor/jagi:astronomy';

import Organization from '/imports/api/Organization';

import UserPermissionsContract from '/imports/api/helpers/permissions';

const User = Class.create({
  name: 'User',
  collection: Meteor.users,
  // typeField: 'type',
  fields: {
    avatar: {
      type: Object,
      optional: true,
    },
    createdAt: Date,
    email: {
      type: String,
      resolve(doc) {
        //console.log("User/schema.js:email:" + JSON.stringify(doc));
        return doc.emails && doc.emails[0] ? doc.emails[0].address.toLowerCase() : '';
      },
      optional: true,
    },
    emails: {
      type: [Object],
      default: () => [{ address: '', verified: true }],
      optional: true,
    },
    may: {
      transient: true,
      type: UserPermissionsContract,
      default: (user) => {
        let contract = new UserPermissionsContract();
        contract = contract.bind(user);
        return contract;
      },
    },
    name: {
      type: String,
    },
    path: {
      type: String,
      transient: true,
      resolve(doc) {
        return `/user/${doc._id}`;
      },
    },
    phone: {
      type: String,
      default: '',
    },
    profile: {
      type: Object,
      organizationId: String,
      default: doc => ({
        role:
          {
            Volunteer: 'volunteer',
            Caseworker: 'caseworker',
            User: 'user',
            Administrator: 'administrator',
          }[doc.constructor.className] || doc.constructor.className.toLowerCase().replace(/ /g, ''),
      }),
    },
    rawName: {
      // Used for returning the unmodified name
      transient: true,
      type: String,
      resolve(doc) {
        return doc.name;
      },
    },
    services: {
      type: Object,
      default: {},
    },
    status: {
      type: Object,
      optional: true,
    },
    type: {
      type: String,
      transient: true,
      resolve(doc) {
        return ((role) => {
          switch (role) {
            case 'volunteer':
            case 'organization':
              return 'Volunteer';
              break;
            case 'caseworker':
              return 'Caseworker';
              break;
            case 'administrator':
              return 'Administrator';
              break;
            default:
              return 'User';
          }
        })(doc.profile.role);
      },
    },
    updatedAt: {
      type: Date,
      default: new Date(),
    },
    updates: {
      type: [Date],
      default: () => [],
    },
    inactive: {
      type: Boolean,
      optional: true,
    },
    queryStartDate: {
      type: Date,
      default: new Date(),
    },
    queryEndDate: {
      type: Date,
      default: new Date(),
    },
  },
});

User.publicFields = {
  _id: 1,
  name: 1,
  profile: {
    organizationId: "",
  },
  emails: 1,
  may: 1,
  avatar: 1,
  inactive:1,
  queryStartDate:1,
  queryEndDate:1
};

export default User;
