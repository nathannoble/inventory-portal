/* eslint no-param-reassign: 0 */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { emailLogger, userLogger } from '/server/logger';

import Images from '/imports/api/helpers/Images';
import User from '/imports/api/User';
import Caseworker from '/imports/api/User/Caseworker';
import Volunteer, { OrganizationAffiliation } from '/imports/api/User/Volunteer';
import Business from '/imports/api/Organization/Business';
//import School from '/imports/api/Organization/School';

import CC from '/imports/api';

const checkExternalServices = (user, callback) => {
  const userCreatingWithExternalService = user.services && !user.profile.role;
  console.log("api/User/server:events:checkExternalServices? " + userCreatingWithExternalService);
  if (!userCreatingWithExternalService) {
    console.log("api/User/server:events:checkExternalServices:user:" + JSON.stringify(user));
    return callback(user);
  }
  user.emails = [];
  //if (user.services.google) {
  //  user.name = user.name || user.services.google.name;
  //  user.emails.push({
  //    address: user.services.google.email,
  //    verified: user.services.google.verified_email,
  //  });
  //} else if (user.services.twitter) {
  //  user.name = user.name || user.services.twitter.screenName;
  //  user.emails.push({
  //    address: 'user@twitter.com',
  //    verified: true,
  //  });
  //} else if (user.services.facebook) {
  //  user.name = user.name || user.services.facebook.name;
  //  user.emails.push({
  //    address: 'user@facebook.com',
  //    verified: true,
  //  });
  //}
  // This error is meant to be seen by the client
  throw new Meteor.Error(412, 'Role undefined. Please choose a role.', { user });
};

const checkUserOrganization = (user, callback) => {
  if (user.profile.role === 'volunteer' || user.profile.role === 'organization') {
    user = new Volunteer(user);
  } else if (user.profile.role === 'caseworker') {
    user = new Caseworker(user);
  } else {
    user = new User(user);
  }

  if (!user._id) user._id = new Meteor.Collection.ObjectID().toString();

  const userCreatingOrganization = user.profile.organization && !user.profile.organizationId;
  const userAttachingToOrganization = !userCreatingOrganization && user.profile.organizationId;

  if (userCreatingOrganization) {
    const organization = {
      organization: new Business(),
    }[user.profile.role];

    organization.set({
      name: user.profile.organization,
      adminIds: [user._id],
    });

    organization.save((error, organizationId) => {
      if (user.profile.role === 'organization') {
        user.organizations = [new OrganizationAffiliation({ organizationId })];
      }
    });
  } else if (userAttachingToOrganization) {
    if (user.constructor) {
      console.log("api/User/server:events:checkUserOrganization:constructor.className:" + user.constructor.className);
    } else {
      console.log("api/User/server:events:checkUserOrganization:constructor.className:empty");
    }
    //if (user.constructor.className === 'Volunteer') {
    //  const { organizationId } = user.profile;
    //  user.organizations = [new OrganizationAffiliation({ organizationId })];
    //} else if (user.constructor.className === 'Caseworker') {
    //  const { organizationId } = user.profile;
    //  user.organizations = [new OrganizationAffiliation({ organizationId })];
    //}
  }
  console.log("api/User/server:events:checkUserOrganization:user:" + JSON.stringify(user));
  console.log("api/User/server:events:checkUserOrganization:callback:" + JSON.stringify(callback));

  if (callback) return callback(user);
  return user;
};

Meteor.users.before.insert(function(userId, doc) {
  // Hijack the insertion process for users, to make sure,
  // regardless of if via a normal signup or somewhere
  // in the external service login process, that all correct
  // information is attached. Directly inserts after converting
  // to proper role.
  console.log("api/User/server:events:beforeInsert:1:user:" + JSON.stringify(doc));
  doc = checkExternalServices(doc, user => checkUserOrganization(user));
  doc = doc.raw(doc.constructor.getFieldsNames());
  delete doc.may;
  delete doc.status;
  delete doc.type;
  Meteor.users.direct.insert(doc);
  console.log("api/User/server:events:beforeInsert:2:doc:" + JSON.stringify(doc) );
  return false;
});

const UserEvents = {
  afterInsert(event) {
    console.log("api/User/server:events:afterInsert");
    const user = event.currentTarget;
    let imageURL = false;
    //if (user.services.google) {
    //  imageURL = user.services.google.picture;
    //} else if (user.services.twitter) {
    //  imageURL = user.services.twitter.profile_image_url;
    //} else if (user.services.facebook) {
    //  imageURL = `https://graph.facebook.com/${user.services.facebook.id}/picture/?type=large`;
    //}
    if (imageURL) {
      Images.load(imageURL, {}, (error, fileRef) => {
        if (error) {
          console.error(error);
        } else {
          Meteor.users.upsert(
            { _id: user._id },
            {
              $set: {
                avatar: fileRef,
              },
            },
          );
        }
      });
    }
  },
  beforeSave: [CC.events.updateTimestamps],
  beforeUpdate(event) {
    if (Meteor.user().may.update.user(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'Users may only edit their own profile');
    }
  },
  beforeRemove(event) {
    if (Meteor.user().may.delete.user(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to delete this user');
    }
  },
  beforeFind(event) {
    //console.log("api/User/events:event.currentTarget.className:" + event.currentTarget.className);
    // Only finds users in the appropriate role
    const selectors = {
      Volunteer: { $in: ['volunteer', 'organization'] },
      Caseworker: { $in: ['caseworker', 'organization'] },
      Administrator: { $in: ['administrator'] },
    };
    if (selectors[event.currentTarget.className]) {
      event.selector['profile.role'] = selectors[event.currentTarget.className];
    }
  },
};

export default UserEvents;
