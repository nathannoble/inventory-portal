import User from '/imports/api/User';
import UserEvents from '/imports/api/User/server/events';
import '/imports/api/User/methods';
import '/imports/api/User/server/publications';
import '/imports/api/User/server/fixtures';
import '/imports/api/User/server/cron';

User.extend({
  events: UserEvents,
});
