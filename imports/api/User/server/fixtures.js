import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

import Administrator from '../Administrator';

let administrator;

if (Meteor.settings.administrators) {
  Meteor.settings.administrators.forEach((newAdmin) => {
    administrator = Meteor.users.findOne({ emails: { $elemMatch: { address: newAdmin.email } } });
    if (!administrator) {
      administrator = {
        name: newAdmin.name,
        email: newAdmin.email,
        createdAt: new Date(),
        updatedAt: new Date(),
        services: {
          password: {
            bcrypt: '$2a$08$rHstIxyVcP8PRyTX0Umo/ulDtUOYqtoOpkixXuGImtsWUB8iYAj02',
          },
        },
        profile: {
          role: 'administrator',
        },
        emails: [{ address: newAdmin.email, verified: true }],
      };
      Meteor.users.insert(administrator, (error, id) => {
        if (!error) {
          Accounts.setPassword(id, 'password');
          administrator = Administrator.findOne({ _id: id });
          administrator.emails[0].verified = true;
          administrator.save();
        }
      });
    }
  });
}
