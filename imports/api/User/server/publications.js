import { Meteor } from 'meteor/meteor';

import User from '/imports/api/User';
import Administrator from '/imports/api/User/Administrator';
import Caseworker from '/imports/api/User/Caseworker';
import Volunteer from '/imports/api/User/Volunteer';
import Business from '/imports/api/Organization/Business';
import Order from '/imports/api/Order';

Meteor.publish({
  '/user/read'(userId) {
    //console.log("api/User/server/publications:/user/read");
    if (!userId) return this.ready();
    const user = Meteor.user(userId);
    const cursors = [];

    if (!user) return this.ready();

    let userCursor = user.constructor.find({ _id: userId });

    if (user.constructor) {
      console.log("api/User/server/publications:/user/read:constructor.className:" + user.constructor.className);
    } else {
      console.log("api/User/server/publications:/user/read:constructor.className:empty");
    }
    switch (user.constructor.className) {
      case 'Volunteer': {
        userCursor = User.find({ _id: userId });
        break;
      }
      case 'Caseworker': {
        //const affiliations = user.organizations;
        //const businessCursor = Business.find({
        //  _id: {
        //    $in: affiliations.map(({ organizationId }) => organizationId),
        //  },
        //});
        //cursors.push(businessCursor);
        //const requestCursor = Request.find({ requestedId: userId });
        //cursors.push(requestCursor);
        //userCursor = User.find({"profile.role":{ $eq: "caseworker"}});
        userCursor = User.find({ _id: userId });
        break;
      }
      case 'Administrator': {
        userCursor = User.find({ _id: userId });
        break;
      }
      default:
        break;
    }

    cursors.push(userCursor);
    //if (cursors.length > 0) {
    //  console.log("api/User/server/publications:/user/read:cursor:count:" + cursors[0].count());
    //}
    return cursors;
  },
  '/user/basic'() {
    console.log("api/User/server/publications:/user/basic");
    if (!this.userId || Meteor.user().type !== 'Administrator') return this.ready();
    return User.find(
      {},
      {
        fields: {
          _id: 1,
          name: 1,
          createdAt: 1,
          profile: 1,
        },
      },
    );
  },
  '/user/volunteer/list'() {
    console.log("api/User/server/publications:/user/volunteer/list");
    const user = Meteor.user();

    let orgId = "Unknown";
    if (user) {
      orgId = user.profile.organizationId;
    }

    let location = Business.find()
        .fetch()
        .filter(organization => organization._id === orgId)
        .map(organization => organization.location);

    let orgs =  Business.find()
        .fetch()
        .filter(organization => organization.location === parseInt(location,0))
        //.map(organization => organization._id);
        .map(organization => organization);

    return User.find({"profile.organizationId": { $in: orgs.map(org => org._id)}});
  },
  '/user/volunteer/list/pub'() {
    console.log("api/User/server/publications:/user/volunteer/list/pub");
    return Volunteer.find({}, { fields: Volunteer.publicFields });
  },
  '/user/caseworker/list'() {
    console.log("api/User/server/publications:/user/caseworker/list");

    const user = Meteor.user();
    let orgId =null;
    if (user) {
      orgId = user.profile.organizationId;
    }

    console.log("api/User/server/publications:/user/caseworker/list:orgId:" + orgId);
    if (orgId) {
      let location = Business.find()
          .fetch()
          .filter(organization => organization._id === orgId)
          .map(organization => organization.location);

      let orgs = Business.find()
          .fetch()
          .filter(organization => organization.location === parseInt(location, 0))
          //.map(organization => organization._id);
          .map(organization => organization);

      return User.find({"profile.organizationId": {$in: orgs.map(org => org._id)}});
    } else {
      return User.find({}, {sort: {"createdAt": -1}});
    }
  },
  '/user/all/list'() {
    console.log("api/User/server/publications:/user/all/list");
    return Caseworker.find({},{ sort: { 'createdAt': -1 } });
  },
  '/user/caseworker/list/pub'() {
    console.log("api/User/server/publications:/user/caseworker/list/pub");
    return Caseworker.find({}, { fields: Caseworker.publicFields });
  },
  '/user/administrator/list'() {
    console.log("api/User/server/publications:/user/administrator/list");
    const user = Meteor.user();
    let orgId =null;
    if (user) {
      orgId = user.profile.organizationId;
    }

    console.log("api/User/server/publications:/user/administrator/list:orgId:" + orgId);
    if (orgId) {
      let location = Business.find()
          .fetch()
          .filter(organization => organization._id === orgId)
          .map(organization => organization.location);

      let orgs = Business.find()
          .fetch()
          .filter(organization => organization.location === parseInt(location, 0))
          //.map(organization => organization._id);
          .map(organization => organization);

      return User.find({"profile.organizationId": {$in: orgs.map(org => org._id)}});
    } else {
      return User.find({}, {sort: {"createdAt": -1}});
    }
  },
});
