import { Meteor } from 'meteor/meteor';
import Volunteer, { EducationLevel, OrganizationAffiliation } from '/imports/api/User/Volunteer';
import Caseworker, { OrganizationAffiliation as CWOrganizationAffiliation }  from '/imports/api/User/Caseworker';
import { Organizations } from '/imports/api/Organization';

const MigrateUsers = function() {
  // Get the raw data
  Volunteer.find().forEach((volunteer) => {
    const rawUser = Meteor.users.findOne(volunteer._id);
    // First check to make sure the user hasn't been transformed
    if (typeof rawUser.associations !== 'undefined') {
      volunteer.bio =
        ((volunteer) => {
          let bio = volunteer.bio;
          if (volunteer.certifications) bio += `Certifications: ${volunteer.certifications}\r\n`;
          if (volunteer.volunteerExperience) {
            bio += `Volunteer Experience: ${volunteer.volunteerExperience}\r\n`;
          }
          return bio;
        })(rawUser) || '';

      volunteer.requestTypeIds =
        ((associations) => {
          let requestTypeIds = [];
          function conflateRequestTypeIds(opportunities, requestTypeIds) {
            opportunities.forEach((opportunity) => {
              if (!requestTypeIds.includes(opportunity.requestTypeId)) {
                requestTypeIds.push(opportunity.requestTypeId);
              }
            });
            return requestTypeIds;
          }
          if (associations) {
            if (associations.independent && associations.independent.opportunities) {
              requestTypeIds = conflateRequestTypeIds(
                associations.independent.opportunities,
                requestTypeIds,
              );
            }
            if (associations.organizations && associations.organizations.affiliations) {
              associations.organizations.affiliations.forEach((affiliation) => {
                requestTypeIds = conflateRequestTypeIds(affiliation.opportunities, requestTypeIds);
              });
            }
          }
          return requestTypeIds;
        })(rawUser.associations) || [];
      volunteer.organizations =
        (rawUser.associations.organizations.affiliations || []).map((affiliation) => {
          const organization = Organizations.findOne(affiliation.organizationId);
          let departmentId;
          if (organization && organization.departments) {
            organization.departments.forEach(function(department) {
              if (
                affiliation.department === department.id ||
                affiliation.department === department.name
              ) {
                departmentId = department.id;
              }
            });
          }
          return new OrganizationAffiliation({
            departmentId,
            organizationId: affiliation.organizationId,
            titleAtOrganization: affiliation.titleAtOrganization,
          });
        }) || [];
      volunteer.associations = undefined;
      console.log('saving', volunteer);
      volunteer.save();
    }
  });

    // Get the raw data
    Caseworker.find().forEach((caseworker) => {
        const rawUser = Meteor.users.findOne(caseworker._id);
        // First check to make sure the user hasn't been transformed
        if (typeof rawUser.associations !== 'undefined') {
            caseworker.bio =
                ((caseworker) => {
                    let bio = caseworker.bio;
                    if (caseworker.certifications) bio += `Certifications: ${caseworker.certifications}\r\n`;
                    if (caseworker.caseworkerExperience) {
                        bio += `Caseworker Experience: ${caseworker.caseworkerExperience}\r\n`;
                    }
                    return bio;
                })(rawUser) || '';

            caseworker.requestTypeIds =
                ((associations) => {
                    let requestTypeIds = [];
                    function conflateRequestTypeIds(opportunities, requestTypeIds) {
                        opportunities.forEach((opportunity) => {
                            if (!requestTypeIds.includes(opportunity.requestTypeId)) {
                                requestTypeIds.push(opportunity.requestTypeId);
                            }
                        });
                        return requestTypeIds;
                    }
                    if (associations) {
                        if (associations.independent && associations.independent.opportunities) {
                            requestTypeIds = conflateRequestTypeIds(
                                associations.independent.opportunities,
                                requestTypeIds,
                            );
                        }
                        if (associations.organizations && associations.organizations.affiliations) {
                            associations.organizations.affiliations.forEach((affiliation) => {
                                requestTypeIds = conflateRequestTypeIds(affiliation.opportunities, requestTypeIds);
                            });
                        }
                    }
                    return requestTypeIds;
                })(rawUser.associations) || [];
            caseworker.organizations =
                (rawUser.associations.organizations.affiliations || []).map((affiliation) => {
                    const organization = Organizations.findOne(affiliation.organizationId);
                    let departmentId;
                    if (organization && organization.departments) {
                        organization.departments.forEach(function(department) {
                            if (
                                affiliation.department === department.id ||
                                affiliation.department === department.name
                            ) {
                                departmentId = department.id;
                            }
                        });
                    }
                    return new OrganizationAffiliation({
                        departmentId,
                        organizationId: affiliation.organizationId,
                        titleAtOrganization: affiliation.titleAtOrganization,
                    });
                }) || [];
            caseworker.associations = undefined;
            console.log('saving', caseworker);
            caseworker.save();
        }
    });
};

Meteor.methods({
  '/user/migrate'() {
    MigrateUsers();
  },
});

export default MigrateUsers;
