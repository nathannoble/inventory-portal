import {
  CreatePermission,
  ReadPermission,
  DeletePermission,
  UpdatePermission,
} from '/imports/api/helpers/permissions';

CreatePermission.extend({
  helpers: {
    user(user) {
      if (user.type === 'Administrator') return true;
      if (user.type === 'Volunteer') return true;
      return false;
    },
  },
});

ReadPermission.extend({
  helpers: {
    user(user, type = 'user') {
      if (user.type === 'Administrator') return {};
      if (user.type === 'Volunteer') return {};
      if (user.type === 'Caseworker') return {};
      return {
        user: { _id: user._id },
        volunteer: { _id: user._id },
      }[type];
    },
  },
});

UpdatePermission.extend({
  helpers: {
    user(user, userToCheck) {
      if (user.type === 'Administrator') return true;
      if (user._id === userToCheck._id) return true;
      const modifiedFields = userToCheck.getModified();
      if (modifiedFields.includes('organizations')) return true;
      return false;
    },
  },
});

DeletePermission.extend({
  helpers: {
    user(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});
