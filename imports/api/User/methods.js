/* eslint no-console: 0, no-param-reassign: 0 */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';

import { emailLogger, userLogger } from '/server/logger';

import User from '/imports/api/User';
import Caseworker, { OrganizationAffiliation as CWOrganizationAffiliation } from '/imports/api/User/Caseworker';
import Volunteer, { OrganizationAffiliation } from '/imports/api/User/Volunteer';
import Administrator from '/imports/api/User/Administrator';


if (Meteor.isServer) {
  Accounts.onCreateUser((options, user) => {
    if (options.profile) {
      user.profile = options.profile;
      if (options.profile.name) {
        user.name = options.profile.name;
      }
    }
    return user;
  });
}

Meteor.methods({
  '/user/resetPassword'(email) {
    console.log("api/User/methods:/user/resetPassword:email:" + JSON.stringify(email) );
    check(email, String);
    const user = Accounts.findUserByEmail(email);
    //const user = User.findOne({ emails: { $elemMatch: { address: email }}});
    if (user) {
      //throw new Meteor.Error(404, `User with address ${email} WAS found`);
      Accounts.sendResetPasswordEmail(user._id);
    } else {
      throw new Meteor.Error(404, `User with address ${email} not found`);
    }
  },
  '/user/create'(user) {
    // Only to be used
    // when user is logging in via third party
    // or when admin is creating user
    user.save((error) => {
      if (error) {
        console.log("api/User/methods:/user/create:error:" + JSON.stringify(error) );
        throw error;
      } else {
        console.log("api/User/methods:/user/create:success:user:" + JSON.stringify(user) );
        return user;
      }
    });
  },
  '/user/update'(user) {
    check(user, User);
    user.save((error) => {
      if (error) throw error;
    });
  },
  '/user/administrator/update'(administrator) {
    check(administrator, Administrator);
    administrator.save((error) => {
      if (error) throw error;
    });
  },
  'user/volunteer/create'(volunteer) {
    console.log("api/User/methods/volunteer/create:volunteer:" + JSON.stringify(volunteer));
    check(volunteer, Volunteer);
    return volunteer.save((error, volunteerId) => {
      console.log("api/User/methods:/volunteer/create:error:" + JSON.stringify(error) );
      console.log("api/User/methods:/volunteer/create:volunteerId:" + volunteerId );
      if (error) throw error;
      Accounts.sendResetPasswordEmail(volunteerId);
      return volunteerId;
    });
  },
  '/user/volunteer/update'(volunteer) {
    check(volunteer, Volunteer);
    volunteer.save((error) => {
      if (error) throw error;
    });
  },
  '/user/volunteer/delete'(volunteer) {
    check(volunteer, Volunteer);
    if (Meteor.user().type !== 'Administrator') {
      throw new Meteor.Error(401, 'You are not authorized to remove users');
    }
    volunteer.remove((error) => {
      if (error) throw error;
    });
  },
  'user/caseworker/create'(caseworker) {
    console.log("api/User/methods/caseworker/create:caseworker:" + JSON.stringify(caseworker));
    check(caseworker, Caseworker);
    return caseworker.save((error, caseworkerId) => {
      console.log("api/User/methods:/caseworker/create:error:" + JSON.stringify(error) );
      console.log("api/User/methods:/caseworker/create:caseworkerId:" + caseworkerId );
      if (error) throw error;
      Accounts.sendResetPasswordEmail(caseworkerId);
      return caseworkerId;
    });
  },
  '/user/caseworker/update'(caseworker) {
    //console.log("api/User/methods.../user/caseworker/update:caseworker:" + JSON.stringify(caseworker));
    check(caseworker, Caseworker);
    caseworker.save((error) => {
      if (error) throw error;
    });
  },
  '/user/caseworker/delete'(caseworker) {
    check(caseworker, Caseworker);
    if (Meteor.user().type !== 'Administrator') {
      throw new Meteor.Error(401, 'You are not authorized to remove users');
    }
    caseworker.remove((error) => {
      if (error) throw error;
    });
  },
});
