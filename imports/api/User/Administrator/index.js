//import Administrator from './schema';
//import AdministratorHelpers from './helpers';
//
//Administrator.extend({
//  helpers: AdministratorHelpers,
//});
//
//export default Administrator;

import Administrator, {OrganizationAffiliation } from './schema';
import MigrateUsers from '/imports/api/User/migration';
import AdministratorHelpers, { OrganizationAffiliationHelpers } from './helpers';

OrganizationAffiliation.extend({
  helpers: OrganizationAffiliationHelpers,
});

Administrator.extend({
  helpers: AdministratorHelpers,
});

export {
    Administrator as default,
    MigrateUsers,
    OrganizationAffiliation,
};
