import { Meteor } from 'meteor/meteor';

import { Class, Enum } from 'meteor/jagi:astronomy';

import User from '/imports/api/User';

import Business from '/imports/api/Organization/Business';

//import AbstractDuration from '/imports/api/helpers/AbstractDuration';

export const OrganizationAffiliation = Class.create({
  name: 'Administrator Affiliation',
  fields: {
    organizationId: {
      type: String,
      optional: true,
    },

  },
});

const Administrator = User.inherit({
  name: 'Administrator',
  fields: {
    bio: {
      type: String,
      default: '',
    },
  },
});

Administrator.publicFields = Object.assign(
    {
      bio: 1,
    },
    User.publicFields,
);

export default Administrator;
