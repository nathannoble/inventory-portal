/* eslint prefer-destructuring: 0 */
import { Class, Enum } from 'meteor/jagi:astronomy';

import User from '/imports/api/User';

import Business from '/imports/api/Organization/Business';

//import AbstractDuration from '/imports/api/helpers/AbstractDuration';

export const OrganizationAffiliation = Class.create({
  name: 'Caseworker Affiliation',
  fields: {
    //department: {
    //  type: String,
    //  transient: true,
    //  resolve(doc) {
    //    const business = Business.findOne(doc.organizationId);
    //    let name = '';
    //    if (business && business.department(doc.departmentId)) {
    //      name = business.department(doc.departmentId).name;
    //    } else if (doc.department) {
    //      name = doc.department.name;
    //    }
    //    return name;
    //  },
    //},
    organizationId: {
      type: String,
      optional: true,
    },
    //titleAtOrganization: {
    //  type: String,
    //  default: '',
    //},
    //employer: {
    //  type: String,
    //  optional: true,
    //},
  },
});

const Caseworker = User.inherit({
  name: 'Caseworker',
  fields: {
    bio: {
      type: String,
      default: '',
    },
    //employer: {
    //  type: String,
    //  optional: true,
    //},
    //organizations: {
    //  type: [OrganizationAffiliation],
    //  default: () => [],
    //},
  },
});

Caseworker.publicFields = Object.assign(
  {
    bio: 1,
    //organizations: 1,
  },
  User.publicFields,
);

export default Caseworker;
