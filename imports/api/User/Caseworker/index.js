import Caseworker, { OrganizationAffiliation } from './schema';
import MigrateUsers from '/imports/api/User/migration';
import CaseworkerHelpers , { OrganizationAffiliationHelpers } from './helpers';

OrganizationAffiliation.extend({
  helpers: OrganizationAffiliationHelpers,
});

Caseworker.extend({
  helpers: CaseworkerHelpers,
});

export {
  Caseworker as default,
  MigrateUsers,
  OrganizationAffiliation,
};
