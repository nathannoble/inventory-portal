/* eslint global-require: 0 */
import { Meteor } from 'meteor/meteor';
import User from './schema';
import UserHelpers from './helpers';
// import UserEvents from './events';

import './permissions';

User.extend({
  helpers: UserHelpers,
});

User.path = '/user';

Meteor.user = function(userId = false) {
  const user = Meteor.users.findOne(userId || Meteor.userId());
  //console.log("api/User/index:user:" + JSON.stringify(user));
  if (!user) return false;
  let { role } = user.profile;
  if (role === 'organization') role = 'volunteer';
  return {
    caseworker: (_id) => {
      const Caseworker = require('/imports/api/User/Caseworker').default;
      return Caseworker.findOne(_id);
    },
    volunteer: (_id) => {
      const Volunteer = require('/imports/api/User/Volunteer').default;
      return Volunteer.findOne(_id);
    },
    administrator: (_id) => {
      const Administrator = require('/imports/api/User/Administrator').default;
      return Administrator.findOne(_id);
    },
  }[role](user._id);
};

export default User;
