import Organization from '/imports/api/Organization';

import Images from '/imports/api/helpers/Images';

const UserHelpers = {
  administer(organizationId = false) {
    const query = this.type === 'Administrator' ? {} : { adminIds: { $in: [this._id] } };
    if (organizationId) query._id = organizationId;
    const cursor = Organization.find(query);
    if (organizationId) return cursor.count() > 0;
    return cursor;
  },
  formatPhone(format = 'text') {
    const methods = {
      text: (number) => {
        if (number.length === 10) {
          return `(${number.substring(0, 3)}) ${number.substring(3, 6)}-${number.substring(6, 10)}`;
        }
        return number;
      },
      tel: number => number.replace(new RegExp(/\D/, 'g'), ''),
    };
    return methods[!Object.keys(methods).includes(format) ? 'text' : format](this.phone);
  },
  getAvatar() {
    if (process.env.NODE_ENV === 'development') return '';
    if (!this.avatar) return '';
    const avatarCursor = Images.findOne(this.avatar._id);
    if (!avatarCursor) return '';
    return avatarCursor.link();
  },
};

export default UserHelpers;
