/* eslint prefer-destructuring: 0 */
import { Class, Enum } from 'meteor/jagi:astronomy';

import User from '/imports/api/User';

import Business from '/imports/api/Organization/Business';

//import AbstractDuration from '/imports/api/helpers/AbstractDuration';

export const OrganizationAffiliation = Class.create({
  name: 'Volunteer Affiliation',
  fields: {

    organizationId: {
      type: String,
      optional: true,
    },

  },
});

const Volunteer = User.inherit({
  name: 'Volunteer',
  fields: {
    bio: {
      type: String,
      default: '',
    },
  },
});

Volunteer.publicFields = Object.assign(
  {
    bio: 1,
    //organizations: 1,
  },
  User.publicFields,
);

export default Volunteer;
