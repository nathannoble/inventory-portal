import { Meteor } from 'meteor/meteor';
import _ from 'underscore';

import {
    OrganizationAffiliation,
} from '/imports/api/User/Volunteer/schema';

import Files from '/imports/api/helpers/Files';
import OrderType from '/imports/api/OrderType';
import Order, { OrderStatus, OrderFilter } from '/imports/api/Order';

import Organization from '/imports/api/Organization';
import Business from '/imports/api/Organization/Business';

const VolunteerOpportunitiesHelpers = {
    requestType() {
        return RequestType.findOne(this.requestTypeId);
    },
    orderType() {
        return OrderType.findOne(this.orderTypeId);
    },
};

const OrganizationAffiliationHelpers = {
    indexOf(requestTypeId) {
        let index = -1;
        this.opportunities.forEach((opportunity, i) => {
            if (opportunity.requestTypeId === requestTypeId) index = i;
        });
        return index;
    },
    indexOfOrder(orderTypeId) {
        let index = -1;
        this.opportunities.forEach((opportunity, i) => {
            if (opportunity.orderTypeId === orderTypeId) index = i;
        });
        return index;
    },
    organization() {

        const organization = Business.findOne({_id: this.organizationId});
        try {
            if (!organization) {
                //throw new Meteor.Error(404, `Organization with id '${this.organizationId} not found'`);
                return new Business({
                    name:"None",
                    path:"/user/"
                });
            }
        } catch (error) {
            console.error("Volunteer:helpers.js:catch:" + error);
        }
        return organization;
    }
};

const VolunteerAssociationIndependentHelpers = {
    indexOf(requestTypeId) {
        let index = -1;
        this.opportunities.forEach((opportunity, i) => {
            if (opportunity.requestTypeId === requestTypeId) index = i;
        });
        return index;
    },
    indexOfOrder(orderTypeId) {
        let index = -1;
        this.opportunities.forEach((opportunity, i) => {
            if (opportunity.orderTypeId === orderTypeId) index = i;
        });
        return index;
    },
};

const VolunteerHelpers = {
    addOrganization(organizationId, authorizationCode = false, callback = false) {
        if (!this.worksFor(organizationId)) {
            const organization = Organization.findOne({_id: organizationId});
            if (organization.authorizationCode && authorizationCode !== organization.authorizationCode) {
                throw new Meteor.Error(401, 'Not authorized correctly');
            } else {
                this.organizations.push(new OrganizationAffiliation({
                    organizationId,
                }));
                Meteor.call('/user/volunteer/update', this, (error) => {
                    if (error) {
                        callback(error);
                    } else if (callback) {
                        callback();
                    }
                });
            }
        } else {
            throw new Meteor.Error(400, 'User already works for Organization');
        }
    },
    affiliationNames() {
        return this.organizations.map(affiliation => (affiliation.organization() ? affiliation.organization().name : ''));
    },
    affiliationWith(organization) {
        const org =
            typeof organization === 'string' ? Organization.findOne(organization) : organization;
        const affiliation = _.find(
            this.organizations,
            possibleAffiliation => possibleAffiliation.organizationId === org._id,
        );
        return affiliation || new OrganizationAffiliation();
    },
    getImage() {
    },
    getResume() {
        const object = {name: '', link: ''};
        if (!this.resumeObj) return object;
        const resumeCursor = Files.findOne(this.resumeObj._id);
        if (!resumeCursor) return object;
        object.link = resumeCursor.link();
        object.name = resumeCursor.name;
        return object;
    },
    isAvailableFor(requestTypeId, gradeGroups) {
        const self = {
            onBehalfOf: (organizationId) => {
                const onBehalfOf = [];
                if (this.organizations) {
                    this.organizations.forEach((affiliation) => {
                        if (affiliation.availableFor(requestTypeId, gradeGroups)) {
                            if (affiliation.organization()) onBehalfOf.push(affiliation.organization());
                        }
                    });
                }
                return organizationId
                    ? _.map(onBehalfOf, organization => organization._id).indexOf(organizationId) > -1
                    : onBehalfOf;
            },
        };
        return self;
    },
    ordersFor(filter = new OrderFilter()) {
        console.log("api/User/Volunteer/helpers.js:ordersFor");
        const query = {
            $or: [{status: OrderStatus.complete}, {status: OrderStatus.closed}],
            orderedId: this._id,
        };
        if (filter.active()) {
            query.$and = [];
            if (filter.duration) {
                if (filter.duration.start) {
                    query.$and.push({'completedDate.end': {$gte: filter.duration.start}});
                }
                if (filter.duration.end) {
                    query.$and.push({'completedDate.end': {$lte: filter.duration.end}});
                }
            }
            if (filter.status) query.$and.push({status: filter.status});
            if (filter.businessId) query.$and.push({organizationId: filter.businessId});

            if (filter.orderTypeId) {
                query.$and.push({orderTypeId: filter.orderTypeId});
            }
        }
        return Order.find(query);
    },
    resume() {
        let self = false;
        if (this.resumeObj) {
            const resume = Files.findOne({_id: this.resumeObj._id});
            if (resume) {
                self = {
                    URL() {
                        return resume ? resume.url() : false;
                    },
                    fileName() {
                        return resume ? resume.original.name : false;
                    },
                    id() {
                        return resume ? resume._id : false;
                    },
                };
            } else {
                self = false;
            }
        }
        return self;
    },
    worksFor(organizationId) {
        let worksFor = false;
        if (organizationId && this.organizations) {
            this.organizations.forEach((affiliation) => {
                if (affiliation.organizationId === organizationId) worksFor = true;
            });
        } else {
            worksFor = Business.find({
                _id: {$in: _.pluck(this.organizations, 'organizationId')},
            });
        }
        return worksFor;
    },
};

export {
    VolunteerHelpers as default,
    VolunteerOpportunitiesHelpers,
    OrganizationAffiliationHelpers,
    VolunteerAssociationIndependentHelpers,
};
