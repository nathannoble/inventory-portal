import Volunteer, { OrganizationAffiliation } from './schema';
import MigrateUsers from '/imports/api/User/migration';
import VolunteerHelpers, { OrganizationAffiliationHelpers } from './helpers';

OrganizationAffiliation.extend({
  helpers: OrganizationAffiliationHelpers,
});

Volunteer.extend({
  helpers: VolunteerHelpers,
});

export {
  Volunteer as default,
  MigrateUsers,
  OrganizationAffiliation,
};
