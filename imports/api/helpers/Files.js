import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import listify from 'listify';

import S3FilesCollection from './S3FilesCollection';

export const allowedFileExtensions = ['pdf', 'doc', 'docx', 'rtf', 'txt', 'wpf', 'odf'];

const Files = new S3FilesCollection({
  allowClientCode: false,
  collectionName: 'Files',
  debug: false,
  onBeforeUpload(file) {
    if (file.size > 3145728) return 'Please upload file smaller than 3MB';
    const extensionRegExp = new RegExp(allowedFileExtensions.join('|'), 'i');
    if (!extensionRegExp.test(file.extension)) {
      return `Please upload ${listify(allowedFileExtensions, { finalWord: 'or' })}`;
    }
    return true;
  },
  s3path: 'files',
  storagePath: 'assets/app/uploads/uploadedFiles',
});

if (Meteor.isServer) {
  Meteor.methods({
    '/file/delete'(fileId) {
      check(fileId, String);
      return Files.remove({ _id: fileId }, (error) => {
        if (error) throw error;
        return true;
      });
    },
  });
}

export default Files;
