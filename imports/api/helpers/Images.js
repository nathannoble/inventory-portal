import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import listify from 'listify';

import S3FilesCollection from './S3FilesCollection';

export const allowedImageExtensions = ['png', 'jpg', 'jpeg'];

const Images = new S3FilesCollection({
  allowClientCode: false,
  collectionName: 'Images',
  debug: false,
  onBeforeUpload(file) {
    if (file.size > 3145728) return 'Please upload file smaller than 3MB';
    const extensionRegExp = new RegExp(allowedImageExtensions.join('|'), 'i');
    if (!extensionRegExp.test(file.extension)) {
      return `Please Upload ${listify(allowedImageExtensions, { finalWord: 'or' })}`;
    }
    return true;
  },
  s3path: 'images',
  storagePath: 'assets/app/uploads/uploadedFiles',
});

if (Meteor.isServer) {
  Meteor.methods({
    '/image/delete'(imageId) {
      check(imageId, String);
      return Images.remove({ _id: imageId }, (error) => {
        if (error) throw error;
        return true;
      });
    },
  });
}

export default Images;
