/* eslint no-param-reassign: 0, no-restricted-globals: 0 */
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { FilesCollection } from 'meteor/ostrio:files';

import stream from 'stream';

import fs from 'fs';
import S3 from 'aws-sdk/clients/s3';

import _ from 'underscore';

const S3FilesCollection = function({ s3path, storagePath, ...args }) {
  let collection;

  if (Meteor.isServer) {
    const bound = Meteor.bindEnvironment(callback => callback());
    const s3 = new S3({
      secretAccessKey: Meteor.settings.s3.secret,
      accessKeyId: Meteor.settings.s3.key,
      region: Meteor.settings.s3.region,
      httpOptions: {
        timeout: 6000,
        agent: false,
      },
    });

    if (
      !Meteor.settings.s3 ||
      !Meteor.settings.s3.key ||
      !Meteor.settings.s3.secret ||
      !Meteor.settings.s3.bucket ||
      !Meteor.settings.s3.region
    ) {
      throw new Meteor.Error(401, 'Missing Meteor file settings');
    }
    const onAfterUpload = function(fileRef) {
      _.each(fileRef.versions, (vRef, version) => {
        if (s3path && version && fileRef.extension) {
          const filePath = `${s3path}/${Random.id()}-${version}.${fileRef.extension}`;
          s3.putObject(
            {
              StorageClass: 'STANDARD',
              Bucket: Meteor.settings.s3.bucket,
              Key: filePath,
              Body: fs.createReadStream(vRef.path),
              ContentType: vRef.type,
            },
            (error) => {
              bound(() => {
                if (error) {
                  throw error;
                } else {
                  const updateOperation = { $set: {} };
                  updateOperation.$set[`versions.${version}.meta.pipePath`] = filePath;
                  this.collection.update(
                    {
                      _id: fileRef._id,
                    },
                    updateOperation,
                    (updateError) => {
                      if (updateError) {
                        throw updateError;
                      } else {
                        this.unlink(this.collection.findOne(fileRef._id), version);
                      }
                    },
                  );
                }
              });
            },
          );
        }
      });
    };

    const interceptDownload = function(http, fileRef, version) {
      let path;

      if (
        fileRef &&
        fileRef.versions &&
        fileRef.versions[version] &&
        fileRef.versions[version].meta &&
        fileRef.versions[version].meta.pipePath
      ) {
        path = fileRef.versions[version].meta.pipePath;
      }

      if (path) {
        const s3ObjectOptions = {
          Bucket: Meteor.settings.s3.bucket,
          Key: path,
        };

        if (http.request.headers.range) {
          const vRef = fileRef.versions[version];
          const range = _.clone(http.request.headers.range);
          const array = range.split(/bytes=([0-9]*)-([0-9]*)/);
          const start = parseInt(array[1], 10);
          let end = parseInt(array[2], 10);
          if (isNaN(end)) {
            const offset = this.chunkSize - 1;
            end = start + offset;
            if (end >= vRef.size) end = vRef.size - 1;
          }
          http.request.headers.range = `bytes=${start}-${end}`;
          s3ObjectOptions.Range = `bytes=${start}-${end}`;
        }

        const self = this;

        s3.getObject(s3ObjectOptions, function(error) {
          if (error) {
            if (!http.response.finished) http.response.end();
            throw error;
          } else {
            if (http.request.headers.range && this.httpResponse.headers['content-range']) {
              http.request.headers.range = this.httpResponse.headers['content-range']
                .split('/')[0]
                .replace('bytes ', 'bytes=');
            }

            const dataStream = new stream.PassThrough();
            self.serve(http, fileRef, fileRef.versions[version], version, dataStream);
            dataStream.end(this.data.Body);
          }
        });

        return true;
      }
      return false;
    };

    collection = new FilesCollection({
      onAfterUpload,
      interceptDownload,
      storagePath,
      ...args,
    });

    collection.s3path = s3path;

    const orginalRemove = collection.remove;

    collection.remove = function(search) {
      this.collection.find(search).forEach((fileRef) => {
        _.each(fileRef.versions, (vRef) => {
          if (vRef && vRef.meta && vRef.meta.pipePath) {
            s3.deleteObject(
              {
                Bucket: Meteor.settings.s3.bucket,
                Key: vRef.meta.pipePath,
              },
              (error) => {
                bound(() => {
                  if (error) throw error;
                });
              },
            );
          }
        });
      });
      orginalRemove.call(this, search);
    };
  } else if (Meteor.isClient) {
    collection = new FilesCollection({ ...args });
  }

  return collection;
};

export default S3FilesCollection;
