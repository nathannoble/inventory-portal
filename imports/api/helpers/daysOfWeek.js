const daysOfWeek = [
  {
    name: 'Monday',
    abbreviation: 'Mon',
    slug: 'monday',
    letter: 'M',
    letterHTML: 'M',
  },
  {
    name: 'Tuesday',
    abbreviation: 'Tue',
    slug: 'tuesday',
    letter: 'Tu',
    letterHTML: 'T<sup>u</sup>',
  },
  {
    name: 'Wednesday',
    abbreviation: 'Wed',
    slug: 'wednesday',
    letter: 'W',
    letterHTML: 'W',
  },
  {
    name: 'Thursday',
    abbreviation: 'Thu',
    slug: 'thursday',
    letter: 'Th',
    letterHTML: 'T<sup>h</sup>',
  },
  {
    name: 'Friday',
    abbreviation: 'Fri',
    slug: 'friday',
    letter: 'F',
    letterHTML: 'F',
  },
  {
    name: 'Saturday',
    abbreviation: 'Sat',
    slug: 'saturday',
    letter: 'Sa',
    letterHTML: 'S<sup>a</sup>',
  },
  {
    name: 'Sunday',
    abbreviation: 'Sun',
    slug: 'sunday',
    letter: 'Su',
    letterHTML: 'S<sup>u</sup>',
  },
];

export default daysOfWeek;
