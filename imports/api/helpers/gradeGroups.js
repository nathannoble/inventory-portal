import _ from 'underscore';

const gradeGroups = [
  {
    name: 'Pre',
    slug: 'pre-k',
    gradeLevels: [
      {
        num: -1,
        name: 'Pre-school',
        abbreviation: 'Pre',
      },
      {
        num: 0,
        name: 'Kindergarten',
        abbreviation: 'Kind',
      },
    ],
  },
  {
    name: 'K–5',
    slug: 'elementary-school',
    gradeLevels: [
      {
        num: 1,
        name: 'First Grade',
        abbreviation: '1st',
      },
      {
        num: 2,
        name: 'Second Grade',
        abbreviation: '2nd',
      },
      {
        num: 3,
        name: 'Third Grade',
        abbreviation: '3rd',
      },
      {
        num: 4,
        name: 'Fourth Grade',
        abbreviation: '4th',
      },
      {
        num: 5,
        name: 'Fifth Grade',
        abbreviation: '5th',
      },
    ],
  },
  {
    name: '6–8',
    slug: 'middle-school',
    gradeLevels: [
      {
        num: 6,
        name: 'Sixth Grade',
        abbreviation: '6th',
      },
      {
        num: 7,
        name: 'Seventh Grade',
        abbreviation: '7th',
      },
      {
        num: 8,
        name: 'Eighth Grade',
        abbreviation: '8th',
      },
    ],
  },
  {
    name: 'High',
    slug: 'high-school',
    gradeLevels: [
      {
        num: 9,
        name: 'Ninth Grade',
        abbreviation: '9th',
      },
      {
        num: 10,
        name: 'Tenth Grade',
        abbreviation: '10th',
      },
      {
        num: 11,
        name: 'Eleventh Grade',
        abbreviation: '11th',
      },
      {
        num: 12,
        name: 'Twelfth Grade',
        abbreviation: '12th',
      },
    ],
  },
  {
    name: 'College',
    slug: 'college',
    gradeLevels: [
      {
        num: 13,
        name: 'Freshman College',
        abbreviation: 'Fr',
      },
      {
        num: 14,
        name: 'Sophomore College',
        abbreviation: 'So',
      },
      {
        num: 15,
        name: 'Junior College',
        abbreviation: 'Jr',
      },
      {
        num: 16,
        name: 'Senior College',
        abbreviation: 'Sr',
      },
    ],
  },
];

export const gradeGroup = (query) => {
  let group = { name: 'Error', slug: 'error', gradeLevels: [] };
  gradeGroups.forEach((groupToCheck) => {
    if (groupToCheck.name === query || groupToCheck.slug === query) group = groupToCheck;
  });
  return group;
};

export const gradeLevels = _.flatten(gradeGroups.map(group => group.gradeLevels));

// Given any of a grade level's identifiers, returns the larger object
export const gradeLevel = (query) => {
  let level = { num: -99, name: 'Error', abbreviation: 'Error' };
  gradeLevels.forEach((levelToCheck) => {
    if (
      levelToCheck.num === query ||
      levelToCheck.name === query ||
      levelToCheck.abbreviation === query
    ) {
      level = levelToCheck;
    }
  });
  return level;
};

export default gradeGroups;
