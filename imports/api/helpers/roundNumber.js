/* eslint no-restricted-properties: 0, no-mixed-operators: 0 */
// http://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places

const roundNumber = (num, scale) => {
  let number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
  if (num - number > 0) {
    number +=
      Math.floor(2 * Math.round((num - number) * Math.pow(10, scale + 1)) / 10) /
      Math.pow(10, scale);
  }
  return number;
};

export default roundNumber;
