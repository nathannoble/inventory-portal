import { Class } from 'meteor/jagi:astronomy';
import moment from 'moment';

const ConcreteDuration = Class.create({
  name: 'Concrete Duration',
  fields: {
    start: {
      type: Date,
      default: new Date(),
    },
    end: {
      type: Date,
      default: new Date(),
    },
  },
  helpers: {
    asText() {
      const start = moment(this.start);
      const end = moment(this.end);
      let startFormat = 'MMM Do, YYYY [at] h:mm a';
      let endFormat = startFormat;
      let separator = ' to ';
      if (start.year() === end.year()) {
        endFormat = endFormat.replace(', YYYY', '');
        if (start.year() === moment().year()) {
          startFormat = startFormat.replace(', YYYY', '');
        }
        if (start.dayOfYear() === end.dayOfYear()) {
          endFormat = endFormat.replace('MMM Do', '').replace(' [at] ', '');
          startFormat = startFormat.replace(' [at] ', ' ');
          separator = '—';
          if (start.format('A') === end.format('A')) {
            startFormat = startFormat.replace(' a', '');
          }
        }
      }
      return `${moment(start).format(startFormat)}${separator}${moment(end).format(endFormat)}`;
    },
    setDay(date, which = 'both') {
      const dateAsMoment = moment(date);
      if (which === 'both' || which === 'start') {
        this.start = moment(this.start)
          .year(dateAsMoment.year())
          .dayOfYear(dateAsMoment.dayOfYear())
          .toDate();
      }
      if (which === 'both' || which === 'end') {
        this.end = moment(this.end)
          .year(dateAsMoment.year())
          .dayOfYear(dateAsMoment.dayOfYear())
          .toDate();
      }
    },
  },
});

export default ConcreteDuration;
