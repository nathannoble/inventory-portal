/*eslint-disable*/
import { Meteor } from 'meteor/meteor';
import { Class } from 'meteor/jagi:astronomy';
import listify from 'listify';
import moment from 'moment';

import AbstractTimespan from './AbstractTimespan';

const RecurringDuration = Class.create({
  name:
    'Description of how an event recurs. These events may only occur once per day, and their recurrence is defined in terms of day intervals',
  fields: {
    interval: {
      type: Number,
      default: 1,
    },
    intervalType: {
      type: String,
      default: 'week',
    },
    timespan: {
      type: AbstractTimespan,
      default: () => new AbstractTimespan(),
    },
    dateStart: {
      type: Date,
      default: () => new Date(),
    },
    dateEnd: {
      type: Date,
      default: () => new Date(),
    },
    daysOfWeek: {
      type: [String],
      default: () => [],
    },
  },
  helpers: {
    numberOfOccurrences(number = false) {
      let returnVal;
      if (number) {
        if (this.numberOfOccurrences() > number) {
          while (this.numberOfOccurrences() > number) {
            this.dateEnd = moment(this.dateEnd)
              .subtract(1, 'days')
              .toDate();
          }
        } else if (this.numberOfOccurrences() < number) {
          while (this.numberOfOccurrences() < number) {
            this.dateEnd = moment(this.dateEnd)
              .add(1, 'days')
              .toDate();
          }
        }
        returnVal = this;
      } else {
        let numberOfOccurrences = 0;
        const dayMap = [
          'sunday',
          'monday',
          'tuesday',
          'wednesday',
          'thursday',
          'friday',
          'saturday',
        ];
        const normalDays = Math.round(
          moment
            .duration(
              moment(this.dateEnd)
                .day(0)
                .toDate() -
                moment(this.dateStart)
                  .day(6)
                  .toDate(),
            )
            .asDays(),
        );
        numberOfOccurrences += normalDays / 7 * this.daysOfWeek.length;
        for (i = 0; i < 7 - moment(this.dateStart).day(); i++) {
          if (this.daysOfWeek.indexOf(dayMap[dayMap.length - (1 + i)]) > -1) numberOfOccurrences++;
        }
        for (i = 0; i < moment(this.dateEnd).day() + 1; i++) {
          if (this.daysOfWeek.indexOf(dayMap[i]) > -1) numberOfOccurrences++;
        }
        // HACK the Math.round shouldn't be there, but then again moment().asDays() should return a whole number
        returnVal = Math.round(numberOfOccurrences);
      }
      return returnVal;
    },
    description() {
      let description = `Every ${listify(this.daysOfWeek)} during ${format12Hour(
        this.timespan.start,
      )}-${format12Hour(this.timespan.end)}`;
      description += `from ${moment(this.dateStart).format('MMMM Do')} to ${moment(
        this.dateEnd,
      ).format('MMMM Do')}`;
      description += `(${this.numberOfOccurrences()} times)`;
      return description;
    },
  },
});

export default RecurringDuration;
