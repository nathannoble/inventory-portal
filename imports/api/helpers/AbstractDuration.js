import { Class } from 'meteor/jagi:astronomy';
import _ from 'underscore';
import listify from 'listify';

import AbstractTimespan from './AbstractTimespan';

const AbstractDuration = Class.create({
  name: 'Abstract Duration',
  fields: {
    days: {
      type: [String],
      default: () => [],
    },
    times: {
      type: AbstractTimespan,
      default: () => new AbstractTimespan(),
    },
  },
  helpers: {
    asText() {
      const days =
        this.days.length === 7 || this.days.length === 0
          ? 'any day'
          : `on ${listify(this.days.map(day => `${day.charAt(0).toUpperCase() + day.slice(1)}s`), {
            finalWord: 'or',
          })}`;
      return `${this.times.asText()} ${days}`;
    },
    overlaps(prospectiveDuration) {
      let overlaps = false;
      if (_.intersection(this.days, prospectiveDuration.days).length > 0) {
        if (
          this.times.start <= prospectiveDuration.times.start &&
          this.times.end >= prospectiveDuration.times.end
        ) {
          overlaps = true;
        }
      }
      return overlaps;
    },
  },
});

export default AbstractDuration;
