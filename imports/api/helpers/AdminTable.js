import { Random } from 'meteor/random';

export default function AdminTableDefinition() {
  //console.log("api/helpers/AdminTable.js:AdminTableDefinition");

  this.columns = [];

  this.emails = false;

  this.push = (column) => {
    this.columns.push({
      header: column.header || '',
      transform: column.transform || function() {},
      className: column.className || Random.id(),
      export: Object.prototype.hasOwnProperty.call(column, 'export') ? column.export : true,
      visible: Object.prototype.hasOwnProperty.call(column, 'visible') ? column.visible : false,
      sort: Object.prototype.hasOwnProperty.call(column, 'sort') ? column.sort : false,
    });
  };
}
