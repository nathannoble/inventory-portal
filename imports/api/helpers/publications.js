import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { WebApp } from 'meteor/webapp';

import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';

import Organization from '/imports/api/Organization';

import Images from '/imports/api/helpers/Images';
import Files from '/imports/api/helpers/Files';

Meteor.publish('/helpers', function() {
  // Publishes some globals
  const cursors = [];
  const organizationIds = [];
  if (Meteor.user()) {
    cursors.push(User.find({ _id: Meteor.userId() }));
    Meteor.user()
      .administer()
      .forEach(organization => organizationIds.push(organization._id));
  }

  cursors.push(Organization.find({ _id: { $in: organizationIds } }));
  cursors.push(Images.collection.find(
    {},
    {
      fields: {
        name: 1,
        extension: 1,
        _downloadRoute: 1,
        _collectionName: 1,
        'versions.versionName.extension': 1, // <-- Required only for file's version .link(version), and if extension is different from original file
      },
    },
  ));
  cursors.push(Files.collection.find(
    {},
    {
      fields: {
        name: 1,
        extension: 1,
        _downloadRoute: 1,
        _collectionName: 1,
        'versions.versionName.extension': 1, // <-- Required only for file's version .link(version), and if extension is different from original file
      },
    },
  ));

  return cursors;
});

Meteor.publish('/profile', function(userId) {
  check(userId, String);
  const cursors = [];
  cursors.push(User.find({ _id: userId }));
  return cursors;
});

