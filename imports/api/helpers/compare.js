const compare = (a, b) => {
  let comparatively = 0;
  if (a < b) {
    comparatively = -1;
  } else if (a > b) {
    comparatively = 1;
  }
  return comparatively;
};

export default compare;
