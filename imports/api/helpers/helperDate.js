import moment from 'moment';

const helperDate = (date) => {
  const formatString = `MMM D${moment(date).year() !== moment().year() ? ', YYYY' : ''}`;
  return moment(date).format(formatString);
};

const helperDateTime = (date) => {
  const formatString = 'MMM DD YYYY h:mm a';
  return moment(date).format(formatString);
};

export {helperDate as default, helperDateTime};
