import { Class } from 'meteor/jagi:astronomy';

const CreatePermission = Class.create({
  name: 'Create Permissions',
});

// Returns query strings with the documents
// that the attached user is allowed to view
const ReadPermission = Class.create({
  name: 'List Permissions',
});

// Returns boolean if the user is allowed to update
// the passed document
const UpdatePermission = Class.create({
  name: 'Update Permissions',
});

// Returns boolean if the attached user
// may delete the passed search
const DeletePermission = Class.create({
  name: 'Delete Permissions',
});

ReadPermission.extend({
  // All of these arguments have been bound to the appropriate user
  helpers: {},
});

const UserPermissionsContract = Class.create({
  name: 'User Permission Contract',
  fields: {
    create: {
      type: CreatePermission,
      default: () => new CreatePermission(),
    },
    read: {
      type: ReadPermission,
      default: () => new ReadPermission(),
    },
    update: {
      type: UpdatePermission,
      default: () => new UpdatePermission(),
    },
    delete: {
      type: DeletePermission,
      default: () => new DeletePermission(),
    },
  },
  helpers: {
    bind(user) {
      // Loop through each of the methods (CRUD),
      // and get each of their helpers (attached by modules via Permission.extend)
      // and bind the passed user (on user initiation) to them
      this.constructor.getFields().forEach((field) => {
        const helpers = this[field.name].constructor.getHelpers();
        Object.keys(helpers).forEach((helper) => {
          this[field.name][helper] = helpers[helper].bind(this[field.name][helpers], user);
        });
      });
      return this;
    },
  },
});

UserPermissionsContract.extend({
  fields: {},
});

export { CreatePermission, ReadPermission, UpdatePermission, DeletePermission };

export default UserPermissionsContract;

