import { Meteor } from 'meteor/meteor';

import Order from '/imports/api/Order';
import Announcement from '/imports/api/Announcement';
import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import Organization from '/imports/api/Organization';
import Business from '/imports/api/Organization/Business';

// To avoid eval(), this function takes a string and returns the appropriate collection
const collectionByName = (name = '') => {
  let collection;
  switch (name.toLowerCase()) {
    case 'order':
    case 'orders':
      collection = Order;
      break;
    case 'announcement':
    case 'announcements':
      collection = Announcement;
      break;
    case 'user':
    case 'users':
      collection = User;
      break;
    case 'volunteer':
    case 'volunteers':
      collection = Volunteer;
      break;
    case 'caseworker':
    case 'caseworkers':
      collection = Caseworker;
      break;
    case 'organization':
    case 'organizations':
      collection = Organization;
      break;
    case 'business':
    case 'businesses':
      collection = Business;
      break;
    default:
      break;
  }
  if (!collection) {
    throw new Meteor.Error(404, `Collection "${name}" not listed in collectionByName function`);
  }
  return collection;
};

export default collectionByName;
