/* global Assets */
/* eslint no-param-reassign: 0 */
import { Meteor } from 'meteor/meteor';
import { Inky } from 'inky';
import { SSR } from 'meteor/meteorhacks:ssr';
// import { SpacebarsCompiler } from "meteor/spacebars-compiler";
// import { Blaze } from "meteor/blaze";

// export const SSR = {
//   render: (templateName, data) => {
//       const renderFunc = (data)? Blaze.toHTMLWithData : Blaze.toHTML;
//       const template = Blaze.Template[templateName];
//       if (!template) {
//           throw new Error(`Template ${templateName} not found`);
//       }
//       else {
//           return renderFunc(template, data);
//       }
//   },
//   compileTemplate: (name, content) => {
//       const renderfunc = eval(`(function(view) { return ${SpacebarsCompiler.compile(content)}(); })`);
//       const template = new Blaze.Template(name, function() { return renderfunc(this); });
//       Blaze.Template[name] = template;
//       return template;
//   },
// };

// Meteor.defer(() => {
//   SSR.compileTemplate('emails/head', Assets.getTextAsync('emails/head.html'));
//   SSR.compileTemplate('emails/header', Assets.getTextAsync('emails/header.inky'));
//   SSR.compileTemplate('emails/footer', Assets.getTextAsync('emails/footer.inky'));

// });

SSR.compileTemplate('emails/head', Assets.getText('emails/head.html'));
SSR.compileTemplate('emails/header', Assets.getText('emails/header.inky'));
SSR.compileTemplate('emails/footer', Assets.getText('emails/footer.inky'));

const inkyHTML = (assetName, params = {}, wrap = true) => {
  const inky = new Inky({});
  let html = '';
  params.utils = {
    URL: Meteor.absoluteUrl('').replace(/\/$/, ''),
    styles: {
      colors: {
        blue: '#4db6ac',
        blue__dark: '#7986cb',
        teal: '#4db6ac',
        teal__dark: '#2e6f69',
        gray: '#e4e4e4',
        gray__dark: '#484748',
        red: '#f44336',
        green: '#4caf50',
        orange: '#ff9800',
        black: '#212121',
        white: '#fff',
      },
    },
  };
  const styles = [
    `<style>${Assets.getText('assets/css/foundation.css')}</style>`,
    `<style>${Assets.getText('assets/css/foundation-emails.css')}</style>`,
  ];
  if (wrap) {
    html += `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" style="background-color: #4db6ac">
    `;
    html += SSR.render('emails/head', { styles });
    html += '<body>';
  } else {
    styles.forEach((style) => {
      html += style;
    });
  }
  html += '<container style="background-color: #ffffff">';
  html += SSR.render('emails/header', params);
  html += SSR.render(assetName, params);
  html += SSR.render('emails/footer', params);
  html += '</container>';
  if (wrap) html += '</body></html>';
  return inky.releaseTheKraken(html);
};

export default inkyHTML;
