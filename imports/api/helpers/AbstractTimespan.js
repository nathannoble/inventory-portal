import { Class } from 'meteor/jagi:astronomy';
import moment from 'moment';

const AbstractTimespan = Class.create({
  name: 'Abstract Timespan Duration in 24-Hour Hmm format',
  fields: {
    start: {
      type: Number,
      default: 0,
    },
    end: {
      type: Number,
      default: 0,
    },
  },
  helpers: {
    asText() {
      if (this.start === 0 || this.end === 0) return 'any time';
      const start = moment(this.start, 'Hmm').format('h:mm a');
      const end = moment(this.end, 'Hmm').format('h:mm a');
      return `${start}—${end}`;
    },
  },
});

export default AbstractTimespan;
