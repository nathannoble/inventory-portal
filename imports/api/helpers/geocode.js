import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

export default function geocode(address, callback) {

  const apiURL = 'https://maps.googleapis.com/maps/api/geocode/json';
  const key = Meteor.settings.public.googleAPIKey;

  console.log("api/helpers/geocode:address:" + JSON.stringify(address));
  //console.log("api/helpers/geocode:callback:" + callback);
  //console.log("api/helpers/geocode:key:" + key);

  const params = { address, key };
  HTTP.get(apiURL, { params }, (error, result) => {
    console.log("api/helpers/geocode:result:" + JSON.stringify(result));
    console.log("api/helpers/geocode:error:" + JSON.stringify(error));
    if (error) throw error;
    const data = result.data.results[0];
    if (!data || !data.geometry) throw new Meteor.Error(401, 'Invalid data returned from geocoder');
    callback([data.geometry.location.lat, data.geometry.location.lng]);
  });
}
