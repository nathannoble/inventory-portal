/* global Assets */
/* eslint no-param-reassign: 0 */

import { Meteor } from 'meteor/meteor';
import { Email } from 'meteor/email';
import { SSR } from 'meteor/meteorhacks:ssr';
// import { SpacebarsCompiler } from "meteor/spacebars-compiler";
// import { Blaze } from "meteor/blaze";
import { emailLogger } from '/server/logger';
import { check, Match } from 'meteor/check';
import createDOMPurify from 'dompurify';
import { JSDOM } from 'jsdom';
import CSSInliner from 'css-inliner';

//import React from 'react';
//import ReactPDF from  '@react-pdf/renderer';
//import {
//    Document,
//    Page,
//    View,
//    Text,
//    Link,
//    Font,
//    StyleSheet,
//} from '@react-pdf/renderer';


import inkyHTML from './inkyHTML';

// export const SSR = {
//   render: (templateName, data) => {
//       const renderFunc = (data)? Blaze.toHTMLWithData : Blaze.toHTML;
//       const template = Blaze.Template[templateName];
//       if (!template) {
//           throw new Error(`Template ${templateName} not found`);
//       }
//       else {
//           return renderFunc(template, data);
//       }
//   },
//   compileTemplate: (name, content) => {
//       const renderfunc = eval(`(function(view) { return ${SpacebarsCompiler.compile(content)}(); })`);
//       const template = new Blaze.Template(name, function() { return renderfunc(this); });
//       Blaze.Template[name] = template;
//       return template;
//   }
// };

Meteor.methods({
  '/email'(name, to, from, subject, data = {}) {
    from = from || Meteor.settings.email.from;
    check(name, String);
    //console.log("api/helpers/methods../email:to:" + JSON.stringify(to));
    check(to, Match.OneOf(String, [String],[[String]]));
    check(from, String);
    check(subject, String);
    check(data, Object);
    SSR.compileTemplate(`emails/${name}`, Assets.getText(`emails/${name}.inky`));
    const html = inkyHTML(`emails/${name}`, data);
    const text = createDOMPurify(new JSDOM('').window).sanitize(html, { ALLOWED_TAGS: [] });
    const inliner = new CSSInliner();
    inliner.inlineCSSAsync(html)
      .then(function (result) {
        emailLogger.info(`Sending Email "${subject}" to ${to.join(', ')}`);
        if (process.env.NODE_ENV === 'development') {
          emailLogger.info('Redirecting to Nathan');
          to = 'nat.noble@gmail.com';
        }
        Email.send({
          to,
          from,
          subject,
          html: result,
          text,
        });
      });
  },
  '/asset'(name, data = {}) {
    check(name, String);
    check(data, Object);
    if (Meteor.isServer) {
      const asset = Assets.getText(`${name}.inky`);
      if (!asset) throw new Meteor.Error(404, 'Preview unavailable');
      SSR.compileTemplate(name, asset);
      return inkyHTML(name, data, false);
    }
    return '';
  },

  //'/pdf'(pdf, filename) {
  //  filename = `${filename}.pdf`; //${__dirname}/
  //  if (Meteor.isServer) {
  //    console.log("api/helpers/methods../filename:" + filename);
  //    //console.log("api/helpers/methods../pdf:" + JSON.stringify(pdf));
  //
  //    const doc = (
  //        <Document>
  //          <Page size="A4">
  //            <View>
  //              <Text>Test</Text>
  //            </View>
  //          </Page>
  //        </Document>
  //    );
  //
  //    ReactPDF.render(<pdf/>, filename);
  //    return filename;
  //  }
  //  return "Meteor.isServer = false";
  //}

});
