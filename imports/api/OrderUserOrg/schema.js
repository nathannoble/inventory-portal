import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Class, Enum } from 'meteor/jagi:astronomy';

import RecurringDuration from '/imports/api/helpers/RecurringDuration';
import ConcreteDuration from '/imports/api/helpers/ConcreteDuration';

import {ChildSex,OrderStatus} from '/imports/api/Order';
export const OrderUserOrgs = new Mongo.Collection('ordersUserOrg');

const OrderUserOrg = Class.create({
    name: 'OrderUserOrg',
    collection: OrderUserOrgs,
    fields: {
        status: {
            type: OrderStatus,
            default: OrderStatus.active,
        },
        createdAt: {
            type: Date,
            immutable: true,
            default: () => new Date(),
        },
        newChild: Number,
        childName: String,
        childZip: Number,
        childAge: Number,
        childWeight: Number,
        childSex: {
            type: ChildSex,
            default: ChildSex.Male,
        },
        ordererId: String,
        pickupLocation: String,
        pickupDate: {
            type: Date,
            default: () => new Date(),
        },
        itemCategory: {
            type: Number,
            default: 0,
        },
        itemType: {
            type: Number,
            default: 0,
        },
        comments: String,
        items: {
            type: [Object],
            default: () =>
                Object({
                    itemsName: '',
                    itemsCategory: 0,
                    itemsType: 0,
                    itemsSize: 0,
                    shoeSize: {
                        type: Number,
                        default: 0,
                    },
                    pantSize: {
                        type: Number,
                        default: 0,
                    },
                    itemsComment: ''
                }),
        },
        userId: {
            type: [String],
            default: () => [],
        },
        userName: {
            type: [String],
            default: () => [],
        },
        userPhone: {
            type: [String],
            default: () => [],
        },
        userRole: {
            type: [String],
            default: () => [],
        },
        orgId: {
            type: [String],
            default: () => [],
        },
        orgName: {
            type: [String],
            default: () => [],
        },
        cpNumber: {
            type: [Number],
            default: () => [],
        },
        location: {
            type: [Number],
            default: () => [],
        },
        path: {
            type: String,
            transient: true,
            resolve(doc) {
                return `${OrderUserOrg.path}/${doc._id}`;
            },
        },
    },
});

OrderUserOrg.path = '/orderUserOrg';

export { OrderUserOrg as default };

//Example orderUserOrg

//{
//    "_id" : "2AF62JvoZGYQE9Qza",
//    "status" : 1,
//    "createdAt" : ISODate("2019-12-06T21:32:38.718Z"),
//    "newChild" : 0,
//    "childName" : "Mayra/Ada",
//    "childZip" : 97038,
//    "childAge" : 29,
//    "childWeight" : 140,
//    "childSex" : 1,
//    "ordererId" : "coo5KvBSetgxBAoXr",
//    "pickupLocation" : "CCCCHS (180)",
//    "pickupDate" : ISODate("2019-12-11T21:31:06.944Z"),
//    "itemCategory" : 0,
//    "itemType" : 0,
//    "comments" : "",
//    "items" : [ { "itemsName" : "", "itemsComment" : "Mathernity Clothes Pls", "itemsCategory" : 0, "itemsType" : 1, "itemsSize" : 21, "shoeSize" : 0, "pantSize" : 19 } ],
//
//    "userId" : [ "coo5KvBSetgxBAoXr" ],
//    "userName" : [ "Rosa (Rosie) Torres" ],
//    "userPhone" : [ "503-462-3088" ],
//    "userRole" : [ "caseworker" ],
//    "organizationId" : [ "q9SfYcvuYhfmjdoGr" ],
//
//    "orgId" : [ "q9SfYcvuYhfmjdoGr" ],
//    "orgName" : [ "CCCCHS" ],
//    "location" : [ 5 ]
//
//}

// 2 createView steps in mongo to create orderUserOrg
// From orders and users and then ordersUser and organizations

//db.createView(
//    "ordersUserOrg",
//    "ordersUser",
//    [
//        {
//            $lookup: {
//                from: "organizations",
//                localField: "organizationId",
//                foreignField: "_id",
//                as: "org_docs"
//            }
//        },
//        {
//            $project: {
//                "_id": 1,
//                "status":"$status",
//                "createdAt":"$createdAt",
//                "newChild":"$newChild",
//                "childName":"$childName",
//                "childZip":"$childZip",
//                "childAge":"$childAge",
//                "childWeight":"$childWeight",
//                "childSex":"$childSex",
//                "ordererId":"$ordererId",
//                "pickupLocation":"$pickupLocation",
//                "pickupDate":"$pickupDate",
//                "itemCategory":"$itemCategory",
//                "itemType":"$itemType",
//                "comments":"$comments",
//                "items":"$items",
//                "userId": "$userId",
//                "userName": "$userName",
//                "userPhone": "$userPhone",
//                "userRole":  "$userRole",
//                "organizationId":  "$organizationId",
//                "orgId":"$org_docs._id",
//                "orgName": "$org_docs.name",
//                "cpNumber": "$org_docs.cpNumber",
//                "location": "$org_docs.location"
//            }
//        }
//    ])
//
//db.createView(
//    "ordersUser",
//    "orders",
//    [
//        {
//            $lookup: {
//                from: "users",
//                localField: "ordererId",
//                foreignField: "_id",
//                as: "users_docs"
//            }
//        },
//        {
//            $project: {
//                "status":"$status",
//                "createdAt":"$createdAt",
//                "newChild":"$newChild",
//                "childName":"$childName",
//                "childZip":"$childZip",
//                "childAge":"$childAge",
//                "childWeight":"$childWeight",
//                "childSex":"$childSex",
//                "ordererId":"$ordererId",
//                "pickupLocation":"$pickupLocation",
//                "pickupDate":"$pickupDate",
//                "itemCategory":"$itemCategory",
//                "itemType":"$itemType",
//                "comments":"$comments",
//                "items":"$items",
//                "userId": "$users_docs._id",
//                "userName": "$users_docs.name",
//                "userPhone": "$users_docs.phone",
//                "userRole":  "$users_docs.profile.role",
//                "organizationId":  "$users_docs.profile.organizationId"
//            }
//        }
//    ])

//db.createView(
//    "usersOrg",
//    "users",
//    [
//        {
//            $lookup: {
//                from: "organizations",
//                localField: "organizationId",
//                foreignField: "_id",
//                as: "org_docs"
//            }
//        },
//        {
//            $project: {
//                "_id": 1,
//                "name": "$name",
//                "phone": "$phone",
//                "role":  "$profile.role",
//                "organizationId":  "profile.organizationId",
//                "orgId":"$org_docs._id",
//                "orgName": "$org_docs.name",
//                "cpNumber": "$org_docs.cpNumber",
//                "location": "$org_docs.location"
//            }
//        }
//    ])
