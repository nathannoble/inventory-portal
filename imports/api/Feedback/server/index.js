import '../methods';
import './publications';
import FeedbackEvents from './events';

import Feedback, { Feedbacks } from '..';

Feedback.extend({
  events: FeedbackEvents,
});

export { Feedback as default, Feedbacks };
