import { Meteor } from 'meteor/meteor';

import Feedback from '/imports/api/Feedback';

import CC from '/imports/api';

const FeedbackEvents = {
  beforeSave: [CC.events.updateTimestamps],
  afterSave(event) {
    if (event.currentTarget.rating <= 2) {
      Meteor.call('/feedback/lowRatingAlert', event.currentTarget._id);
    }
  },
  beforeUpdate(event) {
    if (Meteor.user().may.update.feedback(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to update this feedback item');
    }
  },
  beforeRemove(event) {
    if (Meteor.user().may.delete.feedback(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'Only the system administrator can remove documents');
    }
  },
};

export default FeedbackEvents;
