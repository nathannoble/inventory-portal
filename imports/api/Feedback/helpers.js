import collectionByName from '/imports/api/helpers/collectionByName';
import User from '/imports/api/User';
import Organization from '/imports/api/Organization';

const FeedbackHelpers = {
  owner() {
    // Organizations or Users may own feedback
    const user = User.findOne({ _id: this.ownerId });
    if (user) return user;
    const organization = Organization.findOne({ _id: this.ownerId });
    if (organization) return organization;
    return false;
  },
  activity() {
    return collectionByName(this.activityType).findOne(this.activityId);
  }
};

export default FeedbackHelpers;
