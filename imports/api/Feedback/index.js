import Feedback, { Feedbacks } from './schema';
import FeedbackHelpers from './helpers';

import './permissions';

Feedback.extend({
  helpers: FeedbackHelpers,
});

export { Feedback as default, Feedbacks };
