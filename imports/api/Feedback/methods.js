import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import Feedback from '/imports/api/Feedback';
import Administrator from '/imports/api/User/Administrator';

Meteor.methods({
  '/feedback/update'(feedback) {
    check(feedback, Feedback);
    if (Meteor.user().may.update.feedback(feedback) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to update this feedback');
    }
    return feedback.save((error) => {
      if (error) throw error;
      return true;
    });
  },
  '/feedback/delete'(feedback) {
    check(feedback, Feedback);
    if (Meteor.user().may.delete.feedback(feedback) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to delete this feedback');
    }
    return feedback.remove((error) => {
      if (error) throw error;
      return true;
    });
  },
  '/feedback/lowRatingAlert'(feedbackId) {
    check(feedbackId, String);
    const feedback = Feedback.findOne(feedbackId);
    const adminEmails = Administrator.find()
      .fetch()
      .map(administrator => administrator.email);
      Meteor.call(
        '/email',
        'request/LowRating',
        adminEmails,
        Meteor.settings.email.from,
        'Low Feedback on Request',
        { feedback },
      );
  }
});
