import User from '/imports/api/User';
import Organization from '/imports/api/Organization';

import { ReadPermission, UpdatePermission, DeletePermission } from '/imports/api/helpers/permissions';

ReadPermission.extend({
  helpers: {
    feedback(user) {
      if (user.type === 'Administrator') return {};
      return { userId: user._id };
    },
  },
});

UpdatePermission.extend({
  helpers: {
    feedback(user, feedback) {
      if (user.type === 'Administrator') return false;
      const allowedIds = [user._id];
      user.administer().forEach(organization => allowedIds.push(organization._id));
      return allowedIds.includes(feedback.ownerId);
    },
  },
});

DeletePermission.extend({
  helpers: {
    feedback(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});
