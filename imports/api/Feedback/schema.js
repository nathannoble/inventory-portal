import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

export const Feedbacks = new Mongo.Collection('feedbacks');

const Feedback = Class.create({
  name: 'Feedback',
  collection: Feedbacks,
  fields: {
    createdAt: {
      type: Date,
      default: new Date(),
    },
    updatedAt: {
      type: Date,
      default: new Date(),
    },
    updates: {
      type: [Date],
      default: () => [],
    },
    activityType: {
      type: String,
      default: 'Request',
    },
    activityId: String,
    rating: {
      type: Number,
      default: 3,
    },
    comment: {
      type: String,
      default: '',
    },
    ownerId: String,
    regardingUserId: {
      description:
        'In case of, e.g. requests, where feedback is about a particular person pertaining to the internship',
      type: String,
      optional: true,
    },
  },
});

export default Feedback;
