import { DocumentType } from './schema';

const DocumentHelpers = {
  type() {
    return DocumentType.findOne(this.documentTypeId);
  },
};

export default DocumentHelpers;
