import Document, { Documents, DocumentType, DocumentTypes } from './schema';
import DocumentHelpers from './helpers';

import './permissions';

Document.extend({
  helpers: DocumentHelpers,
});

export { Document as default, Documents, DocumentType, DocumentTypes };
