import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import Document, { DocumentType } from '/imports/api/Document';

Meteor.publish('/documentType/list', () => DocumentType.find());

Meteor.publish('/document/list', function() {
  if (!this.userId) return this.ready();
  const cursors = [];

  const documentCursor = Document.find(Meteor.user().may.read.document());
  const documentTypeCursor = DocumentType.find();

  cursors.push(documentCursor);
  cursors.push(documentTypeCursor);

  return cursors;
});

Meteor.publish('/document/read', function(documentId) {
  check(documentId, String);
  if (!this.userId) return this.ready();

  const cursors = [];

  const documentCursor = Document.find({ _id: documentId });

  cursors.push(documentCursor);
  cursors.push(DocumentType.find());

  return cursors;
});
