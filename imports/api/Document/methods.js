import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import Document, { DocumentType } from '.';

Meteor.methods({
  '/documentType/create'(documentType) {
    check(documentType, DocumentType);
    if (Meteor.user().may.create.documentType(documentType) !== true) {
      throw new Meteor.Error(401, 'Not authorized to create document type');
    }
    return documentType.save((error, documentTypeId) => {
      if (error) throw error;
      return documentTypeId;
    });
  },
  '/documentType/delete'(documentType) {
    check(documentType, DocumentType);
    if (Meteor.user().may.delete.documentType(documentType) !== true) {
      throw new Meteor.Error(401, 'Not authorized to remove document type');
    }
    return documentType.remove((error) => {
      if (error) throw error;
    });
  },
  '/document/create'(doc) {
    check(doc, Document);
    if (Meteor.user().may.create.document(doc) !== true) {
      throw new Meteor.Error(401, 'Not authorized to remove document');
    }
    return doc.save((error, documentId) => {
      if (error) throw error;
      return documentId;
    });
  },
  '/document/update'(doc) {
    check(doc, Document);
    if (Meteor.user().may.update.document(doc) !== true) {
      throw new Meteor.Error(401, 'Not authorized to update document');
    }
    return doc.save((error) => {
      if (error) throw error;
    });
  },
  '/document/delete'(doc) {
    check(doc, Document);
    if (Meteor.user().may.delete.document(doc) !== true) {
      throw new Meteor.Error(401, 'Not authorized to remove document');
    }
    return doc.remove((error) => {
      if (error) throw error;
    });
  },
});
