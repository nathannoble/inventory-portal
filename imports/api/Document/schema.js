import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

export const Documents = new Mongo.Collection('documents');

export const DocumentTypes = new Mongo.Collection('documentTypes');

export const DocumentType = Class.create({
  name: 'Document Type',
  collection: DocumentTypes,
  fields: {
    name: String,
  },
});

const Document = Class.create({
  name: 'Document',
  collection: Documents,
  fields: {
    name: String,
    title: String,
    documentTypeId: String,
    roles: {
      type: [String],
      optional: true,
    },
    content: String,
    path: {
      type: String,
      transient: true,
      resolve(doc) {
        return `${Document.path}/${doc._id}`;
      },
    },
  },
});

Document.path = '/document';

export default Document;
