import User from '/imports/api/User';

import {
  CreatePermission,
  ReadPermission,
  UpdatePermission,
  DeletePermission,
} from '/imports/api/helpers/permissions';

CreatePermission.extend({
  helpers: {
    document(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
    documentType(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});

ReadPermission.extend({
  helpers: {
    document(user) {
      const roles = [user.type];
      if (user.type === 'Administrator') {
        User.children.forEach(({ className }) => roles.push(className));
      }
      return { roles: { $in: roles } };
    },
    documentType() {
      return {};
    },
  },
});

UpdatePermission.extend({
  helpers: {
    document(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
    documentType(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});

DeletePermission.extend({
  helpers: {
    document(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
    documentType(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});
