import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import Announcement, { AnnouncementStatus } from '/imports/api/Announcement';

Meteor.methods({
  '/announcement/new'(announcement, messageText = '') {
    check(announcement, Announcement);
    check(messageText, String);
    return announcement.save((error) => {
      if (error) throw error;
      return announcement._id;
    });
  },
  '/announcement/delete'(announcement) {
    check(announcement, Announcement);
    if (Meteor.user().may.delete.announcement(announcement) !== true) {
      throw new Meteor.Error(401, 'Not authorized to delete announcement');
    }
    return announcement.remove((error) => {
      if (error) throw error;
    });
  },
  '/announcement/update'(announcement) {
    check(announcement, Announcement);
    return announcement.save((error) => {
      if (error) throw error;
      console.log("api/Announcement/methods:/announcement/update:announcement:" + JSON.stringify(announcement));

      // 0 'pending',
      // 1 'active',
      // 2 'complete',
      // 3 'cancelled',
      // 4 'archived'

    });
  },
});
