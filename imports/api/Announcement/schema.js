import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Class, Enum } from 'meteor/jagi:astronomy';

import RecurringDuration from '/imports/api/helpers/RecurringDuration';
import ConcreteDuration from '/imports/api/helpers/ConcreteDuration';

export const Announcements = new Mongo.Collection('announcements');

export const AnnouncementFilter = Class.create({
    name: 'Filter for Announcements',
    fields: {
        comments: {
            type: String,
            optional: true,
        },
        status: {
            type: Number,
            optional: true,
        },
    },
});

const Announcement = Class.create({
    name: 'Announcement',
    collection: Announcements,
    fields: {
        status: Number,
        createdAt: {
            type: Date,
            immutable: true,
            default: () => new Date(),
        },
        comments: String,
        path: {
            type: String,
            transient: true,
            resolve(doc) {
                return `${Announcement.path}/${doc._id}`;
            },
        },
    },
});

Announcement.path = '/announcement';

export default Announcement;
