import Announcement, { Announcements } from '..';
import '../methods';
import './publications';
import AnnouncementEvents from './events';

Announcement.extend({
  events: AnnouncementEvents,
});

export { Announcement as default, Announcements };
