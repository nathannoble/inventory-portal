import { Meteor } from 'meteor/meteor';
import _ from 'underscore';
import { check } from 'meteor/check';

import Announcement, { AnnouncementStatus } from '/imports/api/Announcement';

import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';

import Business from '/imports/api/Organization/Business';
import Feedback from '/imports/api/Feedback';

Meteor.publish('/announcement/list', function() {
  if (!this.userId) return this.ready();
  const user = Meteor.user(this.userId);
  //console.log("api/Announcement/server/publications:user:" + JSON.stringify(user));
  //console.log("api/Announcement/server/publications:this user:" + JSON.stringify(Meteor.user()));

  const cursors = [];

    const announcementCursor = Announcement.find(
        user.may.read.announcement(),
        {sort: { createdAt: -1 }}
    );

    cursors.push(announcementCursor);
    return cursors;
});

Meteor.publish('/announcement/read', function(announcementId) {
  check(announcementId, String);
  if (!this.userId) return this.ready();

  const announcement = Announcement.findOne(announcementId);
  if (!announcement) return this.ready();

  const currentUser = Meteor.user();
  if (!currentUser.may.update.announcement(announcement)) return this.ready();

  const cursors = [];

  const announcementCursor = Announcement.find({ _id: announcementId });

  cursors.push(announcementCursor);

  if (announcement.status >= 0) { //} AnnouncementStatus.active) {
    const feedbackCursor = Feedback.find({ activityType: 'Announcement', activityId: announcementId });
    cursors.push(feedbackCursor);
  }

  return cursors;
});
