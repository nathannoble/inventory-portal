/* eslint no-param-reassign: 0 */
import { Meteor } from 'meteor/meteor';
import moment from 'moment';

import Announcement, { AnnouncementStatus } from '/imports/api/Announcement';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import Administrator from '/imports/api/User/Administrator';

import CC from '/imports/api';

const AnnouncementEvents = {
  beforeUpdate: [
    CC.events.updateTimestamps,
    function(event) {
      if (this.userId) {
        if (Meteor.user().may.update.announcement(event.currentTarget) !== true) {
          throw new Meteor.Error(403, 'You are not authorized to update this announcement');
        }
      }
    },
    function(event) {
      // Fills out the completedDate field upon the announcement being marked as complete
      const announcement = event.currentTarget;
      if (announcement.status === AnnouncementStatus.complete && !announcement.completedDate) {
        //announcement.completedDate = announcement.announcementedDate;

      }
      // Prevents the announcement being marked as active while being in the past
      //if (
      //  announcement.status === AnnouncementStatus.active &&
      //  moment(announcement.pickupDate).isBefore(new Date())
      //) {
      //  announcement.status = AnnouncementStatus.pending;
      //}
      //announcement.pickupDate = new Date();
    },
  ],
  beforeInsert: [
    function(event) {
      if (Meteor.user().may.create.announcement(event) !== true) {
        throw new Meteor.Error(403, 'You are not authorized to create a announcement for volunteers');
      }
    },
  ],
  afterInsert(event) {
    const announcement = event.currentTarget;
    announcement.path = `${Announcement.path}/${announcement._id}`;
    console.log("api/Announcement/server:AnnouncementEvents:afterInsert:announcement:" + JSON.stringify(announcement));

    // Email admin
    //Meteor.call(
    //    '/email',
    //    'announcement/NotifyNew',
    //    Administrator.find()
    //        .fetch()
    //        .map(administrator => administrator.email),
    //    Meteor.settings.email.from,
    //    'New Announcement made',
    //    {
    //      announcement,
    //    },
    //    (error) => {
    //      console.error(error);
    //    },
    //);

    // Email volunteer
    //Meteor.call(
    //    '/email',
    //    'announcement/NotifyNew',
    //    [announcement.announcementer().email],
    //    Meteor.settings.email.from,
    //    'New Announcement made',
    //    {
    //      announcement,
    //    },
    //    (error) => {
    //      console.error(error);
    //    },
    //);
  },
  afterUpdate(event) {
    const doc = event.currentTarget;
    // const doc = event.currentTarget;
    if (doc.status === AnnouncementStatus.complete) {
      const volunteer = Volunteer.findOne(doc.announcementedId);
      if (volunteer) {
        volunteer.dateOfLastCompletedAnnouncement = new Date();
        Meteor.call('/user/volunteer/update', volunteer);
      }
    }
  },
  beforeRemove(event) {
    if (Meteor.user().may.delete.announcement(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to delete this announcement');
    } else {
      const announcement = event.currentTarget;
      //Conversation.find({
      //  regardingDocumentId: announcement._id,
      //  regardingDocumentType: 'Announcement',
      //}).forEach(function(conversation) {
      //  conversation.remove();
      //});
      //Feedback.find({
      //  activityType: Announcement,
      //  activityId: announcement._id,
      //}).forEach(function(feedback) {
      //  feedback.remove();
      //});
    }
  },
};

export default AnnouncementEvents;
