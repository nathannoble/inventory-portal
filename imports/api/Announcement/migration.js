import { Meteor } from 'meteor/meteor';

import Announcement, { Announcements, AnnouncementGroups } from '/imports/api/Announcement';

const MigrateAnnouncements = function() {
  Announcement.find().forEach(function(announcement) {
    const rawAnnouncement = Announcements.findOne(announcement._id);
    console.log(rawAnnouncement._id);
    // First make sure the announcement hasn't already been transformed
    if (typeof rawAnnouncement.announcementGroupId !== 'undefined') {
      const announcementGroup = AnnouncementGroups.findOne(rawAnnouncement.announcementGroupId);
      announcement.announcementerId = announcementGroup.announcementerId;
      //announcement.announcementedId = rawAnnouncement.volunteerId;
      //announcement.acceptedByAnnouncemented = rawAnnouncement.acceptedByVolunteer;
      //announcement.acceptedByAnnouncementer = rawAnnouncement.acceptedByEducator;
      //announcement.announcementTypeId = announcementGroup.announcementTypeId;
      announcement.announcementedType = 'Volunteer';

      console.log('saving', announcement._id);
      announcement.save();
    }
  });
};

Meteor.methods({
  '/announcement/migrate'() {
    MigrateAnnouncements();
  },
});

export default MigrateAnnouncements;
