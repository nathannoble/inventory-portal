import {
  CreatePermission,
  ReadPermission,
  UpdatePermission,
  DeletePermission,
} from '/imports/api/helpers/permissions';

import Business from '/imports/api/Organization/Business';

CreatePermission.extend({
  helpers: {
    announcement(user) {
      if (user.type === 'Administrator') return true;
      //if (user.type === 'Volunteer') return true;
      //if (user.type === 'Caseworker') return true;
    },
  },
});

ReadPermission.extend({
  helpers: {
    announcement(user) {
      return {
        Administrator: {},
        Volunteer: {},
        Caseworker: {},
      }[user.type];
    },
  },
});

UpdatePermission.extend({
  helpers: {
    announcement(user, announcement) {
      if (user.type === 'Administrator') return true;
      //if (user.type === 'Volunteer') return true;
      //if (user.type === 'Caseworker') return true;
      if (announcement.announcementerId === user._id) return true;
      return false;
    },
  },
});

DeletePermission.extend({
  helpers: {
    announcement(user,announcement) {
      if (user.type === 'Administrator') return true;
      if (announcement.announcementerId === user._id) return true;
      return false;
    },
  },
});
