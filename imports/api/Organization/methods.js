import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

import Organization from '/imports/api/Organization';
import Business from '/imports/api/Organization/Business';
//import School from '/imports/api/Organization/School';

if (Meteor.isServer) {
  Meteor.methods({
    '/organization/update'(organization) {
      check(organization, Organization);
      organization.save((error) => {
        if (error) throw error;
      });
    },
    '/organization/create'(organization) {
      check(organization, Organization);
      if (Meteor.user().may.create.organization(organization.type, organization)) {
        organization.save((error) => {
          if (error) throw error;
        });
        return organization;
      }
      throw new Meteor.Error('You are not authorized to create organizations');
    },
    '/organization/checkName'(name) {
      // Check to see if an organization with the same name has registered already
      check(name, String);
      const response = { code: 200 };
      const existing = Organization.findOne({ name });
      if (existing) {
        response.code = 409;
      }
      return response;
    },
    '/organization/approve'(organization) {
      check(organization, Organization);
      if (Meteor.user().type !== 'Administrator') {
        throw new Meteor.Error(401, 'You are not authorized to approve organization');
      }
      organization.set({ approved: true });
      organization.save((error) => {
        if (error) throw error;
        Meteor.call(
          '/email',
          'organization/approved',
          organization.admins().map(admin => `${admin.name} <${admin.email}>`),
          Meteor.settings.email.from,
          `Your Business, ${organization.name} was Approved for Northwest Children's Outreach`,
          { organization },
          (emailError) => {
            if (emailError) throw emailError;
          },
        );
      });
    },
    '/organization/deny'(organization) {
      check(organization, Organization);
      if (Meteor.user().type !== 'Administrator') {
        throw new Meteor.Error(401, 'You are not authorized to approve organization');
      }
      organization.set({ approved: false });
      organization.save((error) => {
        if (error) throw error;
        Meteor.call(
          '/email',
          'organization/denied',
          organization.admins().map(admin => `${admin.name} <${admin.email}>`),
          Meteor.settings.email.from,
          `Your Business, ${organization.name} was Denied for Northwest Children's Outreach`,
          { organization },
          (emailError) => {
            if (emailError) throw emailError;
          },
        );
      });
    },
    '/organization/business/authorization'(organization, code = null) {
      check(organization, Match.OneOf(Organization, String));
      // If a code is passed, checks to see if it is the organization's code
      // If no code is passed, checks if the organization has an authorization code
      // returns Boolean
      // Server-only
      const business =
        typeof organization === 'string' ? Business.findOne(organization) : organization;
      if (!business) throw new Meteor.Error(404, 'Business not found');
      if (code !== null) {
        if (business.authorizationCode === code || !business.authorizationCode) {
          return true;
        }
        return false;
      }
      return !!business.authorizationCode; // Return true if organization requires code
    },
    '/organization/business/update'(business) {
      check(business, Business);
      business.save((error) => {
        if (error) throw error;
      });
    },
    '/organization/business/delete'(business) {
      check(business, Business);
      if (Meteor.user().may.delete.organization(business)) {
        business.remove((error) => {
          if (error) throw error;
        });
      }
    },
  });
}
