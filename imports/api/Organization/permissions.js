import {
  CreatePermission,
  ReadPermission,
  DeletePermission,
  UpdatePermission,
} from '/imports/api/helpers/permissions';

CreatePermission.extend({
  helpers: {
    organization(user, type = 'organization', organization = false) {
      if (user.type === 'Administrator') return true;
      if(user.type === 'Volunteer' || user.type === "Caseworker") {
        return true;
        if(organization && organization.adminIds.includes(user._id)) return true;
      }
      return false;
    },
  },
});

ReadPermission.extend({
  helpers: {
    organization(user, type = 'organization') {
      if (user.type === 'Volunteer') return {};
      if (user.type === 'Caseworker') return {};
      if (user.type === 'Administrator') return {};
      return {
        $or: [{ approved: true }, { adminIds: { $in: [user._id] } }],
      };
    },
  },
});

UpdatePermission.extend({
  helpers: {
    organization(user, organization) {
      if (user.type === 'Administrator') return true;
      if (organization.adminIds.includes(user._id)) return true;
      return false;
    },
  },
});

DeletePermission.extend({
  helpers: {
    organization(user) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});
