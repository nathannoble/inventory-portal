import { Meteor } from 'meteor/meteor';

import Images from '/imports/api/helpers/Images';

const OrganizationHelpers = {
  addAdmin(userId) {
    if (this.adminIds.indexOf(userId) < 0) {
      this.adminIds.push(userId);
    }
  },
  getAvatar() {
    if (process.env.NODE_ENV === 'development') return '';
    if (!this.avatar) return '';
    const avatarCursor = Images.findOne(this.avatar._id);
    if (!avatarCursor) {
      if (!avatarCursor) return '';
    }
    return avatarCursor.link();
  },
  members() {
    let memberFunc;
    if (this.educators) memberFunc = this.educators();
    else if (this.employees) memberFunc = this.employees();
    return memberFunc;
  },
  removeAdmin(userId) {
    if (this.adminIds.includes(userId)) {
      this.adminIds.splice(this.adminIds.indexOf(userId), 1);
    }
  },
  removeUser(userId) {
    if (this.adminIds.includes(userId)) {
      this.adminIds.splice(this.adminIds.indexOf(userId), 1);
    }
    Meteor.call('/user/update', this, (error) => {
      if (error) throw error;
    });
  },
};

export default OrganizationHelpers;
