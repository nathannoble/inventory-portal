import { Meteor } from 'meteor/meteor';

import CC from '/imports/api';

const OrganizationEvents = {
  beforeSave: [CC.events.updateTimestamps],
  beforeUpdate(event) {
    if (Meteor.user().may.update.organization(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'Users may only edit organizations of which they are an admin');
    }
  },
  beforeRemove() {
    if (Meteor.user().profile.role !== 'administrator') {
      throw new Meteor.Error(403, 'Only the system administrator can remove organizations');
    }
  },
};

export default OrganizationEvents;
