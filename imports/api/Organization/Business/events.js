/* eslint no-param-reassign: 0 */
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';

import { Accounts } from 'meteor/accounts-base';
import { handleError } from '/imports/ui/helpers';

const BusinessEvents = {
  afterInit(event) {
    //const business = event.currentTarget;
    //business.departments.forEach((department, index) => {
    //  if (department) {
    //    business.departments[index].volunteers = department.volunteers.bind(department, business);
    //  }
    //});
  },
  beforeInsert(event) {
    const business = event.currentTarget;
    //if (business.adminIds.length === 0) {
    //  const user = new Volunteer();
    //  user.set({
    //    name: `${business.name} Administrator`,
    //    emails: [{ address: business.email, verified: true }],
    //    organizations: [
    //      {
    //        organizationId: business._id,
    //        titleAtOrganization: 'Administrator',
    //      },
    //    ],
    //  });
    //  user.save(function(error, id) {
    //    business.adminIds = [id];
    //    Accounts.sendResetPasswordEmail(id);
    //  });
    //}
  },

  beforeRemove(event) {
    if (Meteor.user().profile.role === 'administrator') {
      const business = event.currentTarget;
      console.error("api/Org/Business/events.js:beforeRemove:business:" + JSON.stringify(business));
      if (business.adminIds.length === 1) {
        const volunteerId = business.adminIds[0];
        const volunteer = Volunteer.findOne(volunteerId);
        console.error("api/Org/Business/events.js:beforeRemove:volunteer:" + JSON.stringify(volunteer));
        //if (volunteer.organizations.length === 1) {
          Meteor.call('/user/volunteer/delete', volunteer, handleError);
        //}
      }
    }
  },
};

export default BusinessEvents;
