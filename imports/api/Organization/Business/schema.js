import { Class, Enum } from 'meteor/jagi:astronomy';
import Organization from '..';

// List as of 8/7/19

//BEAVERTON WEST
//BEAVERTON EAST
//PORTLAND WEST
//PORTLAND EAST
//SOUTHSOUND
//TUALATIN
//VANCOUVER

export const CareProviderLocation_Old = Enum.create({
  name: 'Care Provider Location Old',
  identifiers: [
    'BEAVERTON WEST', //0
    'BEAVERTON EAST', //1
    'PORTLAND WEST',  //2
    'PORTLAND EAST',  //3
    'SOUTHSOUND',     //4
    'TUALATIN',       //5
    'VANCOUVER'
  ],     //6
});

export const CareProviderLocation = Enum.create({
  name: "Care Provider Location",
  identifiers: Meteor.settings.public.CareProviderLocation
});

export const CareProviderPickupDay_Old = Enum.create({
  name: 'Care Provider Pickup Day Old',
  identifiers: [
    'Sunday', //0
    'Monday', //1
    'Tuesday', //2
    'Wednesday', //3
    'Thursday', //4
    'Friday', //5
    'Saturday' //6
  ],
});

export const CareProviderPickupDay = Enum.create({
  name: "Care Provider Pickup Day",
  identifiers: Meteor.settings.public.CareProviderPickupDay
});

const Business = Organization.inherit({
  name: 'Business',
  fields: {
    address: {
      type: String,
      optional: true,
      resolve(doc) {
        let returnValue = '';
        if (doc.addressParts) {
          returnValue = `${doc.addressParts.street ? `${doc.addressParts.street}, ` : ''}${doc.addressParts.city} ${doc.addressParts
            .state} ${doc.addressParts.zip}`;
        } else if (doc.address) {
          returnValue = doc.address;
        }
        return returnValue;
      },
    },
    addressParts: {
      type: Object,
      default: () =>
        Object({
          street: '',
          city: '',
          state: 'CA',
          zip: '',
        }),
    },
    //departments: {
    //  type: [OrganizationDepartment],
    //  default: () => [],
    //},
    description: {
      type: String,
      default: '',
    },
    email: {
      type: String,
      default: '',
    },
    path: {
      type: String,
      transient: true,
      resolve(doc) {
        return `${Business.path}/${doc._id}`;
      },
    },
    phone: {
      type: String,
      default: '',
    },
    //requestTypeAdmins: {
    //  type: Object, // Keys are requestTypeIds and values are an array of admins
    //  default: () => new Object(),
    //},
    //requestTypeIds: {
    //  type: [String],
    //  default: () => [],
    //},
    cpNumber:  {
      type: Number,
      default: 0,
    },
    location:  {
      type: Number,
      default: 0,
    },
    pickupDay:  {
      type: Number,
      default: 4,
    },
  },
});

Business.publicFields = Object.assign(
  {
    addressParts: 1,
    phone: 1,
    location: 1,
    cpNumber: 1,
    pickupDay:4,
    description: 1,
    departments: 1,
  },
  Organization.publicFields,
);

export default Business;
