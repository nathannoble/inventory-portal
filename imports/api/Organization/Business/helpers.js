/* eslint no-param-reassign: 0 */
import moment from 'moment';

import Volunteer, { OrganizationAffiliation } from '/imports/api/User/Volunteer';

const BusinessHelpers = {
  admins() {
    return Volunteer.find({ _id: { $in: this.adminIds } });
  },
  department(departmentId) {
    let returnVal;
    this.departments.forEach((department) => {
      if (department.id === departmentId) {
        returnVal = department;
      }
    });
    return returnVal;
  },
  employees(map = true) {
    const cursor = Volunteer.find({
      organizations: {
        $elemMatch: { organizationId: this._id },
      },
    });
    if (map) {
      return cursor.map((volunteer) => {
        volunteer.department = ((vol, businessId) => {
          let department = ((affiliations) => {
            let returnVal = '';
            affiliations.forEach((affiliation) => {
              if (affiliation.organizationId === businessId) returnVal = affiliation.department;
            });
            return returnVal;
          })(vol.organizations);
          if (!department) department = '';
          return department;
        })(volunteer, this._id);
        return volunteer;
      });
    }
    return cursor;
  },
  setUserRole(userId, role, adding = true) {
    if (adding) {
      switch (role) {
        case 'admin':
          this.addAdmin(userId);
          break;
        case 'workplaceTourContact':
          this.workplaceTour.addCoordinator(userId);
          break;
        case 'jobShadowingContact':
          this.jobShadowing.addCoordinator(userId);
          break;
        case 'careerDayContact':
          this.careerDay.addCoordinator(userId);
          break;
        default:
          break;
      }
    } else {
      switch (role) {
        case 'admin':
          this.removeAdmin(userId);
          break;
        case 'workplaceTourContact':
          this.workplaceTour.removeCoordinator(userId);
          break;
        case 'jobShadowingContact':
          this.jobShadowing.removeCoordinator(userId);
          break;
        case 'careerDayContact':
          this.careerDay.removeCoordinator(userId);
          break;
        default:
          break;
      }
    }
  },
  volunteerHoursByRange(dateRange) {
    const hours = 0;
    // TODO
    dateRange.forEach();
    return hours;
  },
};

const OrganizationDepartmentHelpers = {
  volunteers(business) {
    // business is bound in Business.afterInit
    business.set(); // To keep eslint happy until I figure out why business is bound
    return Volunteer.find({
      organizations: {
        $elemMatch: {
          departmentId: this.id,
        },
      },
    });
  },
};

export { BusinessHelpers as default, OrganizationDepartmentHelpers };
