import Business, { CareProviderLocation, CareProviderPickupDay } from './schema';
import OrganizationDepartment from './Department';
import BusinessEvents from './events';
import BusinessHelpers, { OrganizationDepartmentHelpers } from './helpers';

import '../migration';

Business.extend({
  events: BusinessEvents,
  helpers: BusinessHelpers,
});

Business.path = '/business';

OrganizationDepartment.extend({
  helpers: OrganizationDepartmentHelpers,
});

export { Business as default, CareProviderLocation, CareProviderPickupDay, OrganizationDepartment };
