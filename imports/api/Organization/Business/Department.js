import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

const OrganizationDepartment = Class.create({
  name: 'OrganizationDepartment',
  fields: {
    name: {
      type: String,
      default: '',
    },
    id: {
      type: String,
      default: () => new Mongo.ObjectID().toHexString(),
    },
  },
});

export default OrganizationDepartment;
