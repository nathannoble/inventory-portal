import Organization, { Organizations } from './schema';
import OrganizationEvents from './events';
import OrganizationHelpers from './helpers';

import './permissions';

Organization.extend({
  events: OrganizationEvents,
  helpers: OrganizationHelpers,
});

Organization.path = '/organization';

export { Organization as default, Organizations };
