import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

import {
  ReadPermission,
  UpdatePermission,
  DeletePermission,
} from '/imports/api/helpers/permissions';

export const Organizations = new Mongo.Collection('organizations');

const Organization = Class.create({
  name: 'Organization',
  collection: Organizations,
  typeField: 'type',
  fields: {
    adminIds: {
      type: [String],
      default: () => [],
    },
    approved: {
      type: Boolean,
      optional: true,
    },
    avatar: {
      type: Object,
      optional: true,
      resolve(doc) {
        return doc.avatar || doc.logo;
      },
    },
    createdAt: Date,
    logo: {
      type: Object,
      optional: true,
    },
    name: String,
    path: {
      type: String,
      transient: true,
      resolve(doc) {
        return `${Organization.path}/${doc._id}`;
      },
    },
    updatedAt: Date,
    updates: {
      type: [Date],
      default: () => [],
    },
  },
  events: {
    beforeFind(event) {
      if (event.currentTarget.className !== 'Organization') {
        event.selector.type = Object({
          Business: 'Business',
        })[event.currentTarget.className];
      }
    },
  },
});

Organization.publicFields = {
  _id: 1,
  createdAt: 1,
  name: 1,
  adminIds: 1,
  avatar: 1,
  type: 1,
};

ReadPermission.extend({
  helpers: {
    organization(user, type = 'organization') {
      if (user.type === 'Administrator') return {};
      return {
        organization: { adminIds: { $in: [user._id] } },
        business: { $and: [{ type: 'Business' }, { adminIds: { $in: [user._id] } }] },
      }[type];
    },
  },
});

UpdatePermission.extend({
  helpers: {
    organization(user, organization) {
      if (user.type === 'Administrator') return true;
      return user.administer(organization._id);
    },
  },
});

DeletePermission.extend({
  helpers: {
    organization(user, organization) {
      if (user.type === 'Administrator') return true;
      return false;
    },
  },
});

export default Organization;
