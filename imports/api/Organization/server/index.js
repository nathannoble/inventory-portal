import { organizationLogger } from '/server/logger';

import { Organizations } from '..';
import '../methods';
import './publications';

//console.log("imports/api/Organization/server/index.js:Organizations:" + Organizations);
  
Organizations.after.insert((userId, doc) => {
  organizationLogger.info(`New Organization Created: ${doc.name}`);
});
