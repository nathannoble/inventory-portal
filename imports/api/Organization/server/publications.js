import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import _ from 'underscore';

import User from '/imports/api/User';
import Caseworker from '/imports/api/User/Caseworker';
import Volunteer from '/imports/api/User/Volunteer';

import Organization from '/imports/api/Organization';
import Business from '/imports/api/Organization/Business';

import Order from '/imports/api/Order';

import Feedback from '/imports/api/Feedback';

Meteor.publish('/organization/business/list', function() {
  //if (Meteor.user() && Meteor.user().type === 'Administrator') {
    const cursors = [];
    const businessCursor = Business.find();
    //const requestCursor = Request.find({
    //  requestedId: {
    //    $in: _.flatten(businessCursor.map(business => business.employees().map(employee => employee._id))),
    //  },
    //});
    cursors.push(businessCursor);
    cursors.push(Volunteer.find());
    //cursors.push(requestCursor);
    return cursors;
  //}
  //return Business.find({ approved: true }, { fields: Business.publicFields });
});

Meteor.publish('/organization/business', function(organizationId) {
  if (!Meteor.user()) return this.ready();
  check(organizationId, String);
  const cursors = [];

  const businessCursor = Business.find({
    $and: [{ _id: organizationId }, Meteor.user().may.read.organization()],
  });
  const volunteerCursor = Business.findOne(organizationId).employees(false);

  cursors.push(businessCursor);
  cursors.push(volunteerCursor);

  return cursors;
});

Meteor.publish('/organization/business/reports', function(organizationId) {
  check(organizationId, String);
  const cursors = [];

  const businessCursor = Business.find({
    $and: [{ _id: organizationId }, Meteor.user().may.read.organization()],
  });

  const requestCursor = Request.find({
    requestedId: {
      $in: _.flatten(businessCursor.map(business => business.employees().map(employee => employee._id))),
    },
  });

  const feedbackCursor = Feedback.find({
    activityId: { $in: requestCursor.map(request => request._id) },
  });

  const userCursor = User.find({
    $or: [
      {
        organizations: {
          $elemMatch: {
            organizationId: { $in: businessCursor.map(business => business._id) },
          },
        },
      },
      {
        _id: {
          $in: requestCursor.map(request => request.requesterId),
        },
      },
    ],
  });

  const organizationCursor = Organization.find({
    _id: {
      $in: _.union(
        businessCursor.map(business => business._id),
      ),
    },
  });

  cursors.push(organizationCursor);
  cursors.push(userCursor);
  cursors.push(requestCursor);
  cursors.push(feedbackCursor);

  return cursors;
});
