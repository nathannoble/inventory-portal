import { Meteor } from 'meteor/meteor';
import Business from '/imports/api/Organization/Business';
import { Organizations } from '/imports/api/Organization';

const MigrateOrganizations = function() {
  Business.find().forEach((organization) => {
    const rawOrganization = Organizations.findOne(organization._id);
    if (typeof rawOrganization.volunteerHoursAvailable !== 'undefined') {
      organization.requestTypeIds =
        ((organization) => {
          const requestTypeIds = [];
          if (organization.jobShadowing.access > 0) {
            requestTypeIds.push(RequestType.findOne({ name: 'Job Shadowing' })._id);
          }
          return requestTypeIds;
        })(rawOrganization) || [];
      organization.save();
    }
  });
};

Meteor.methods({
  '/organization/migrate'() {
    MigrateOrganizations();
  },
});
