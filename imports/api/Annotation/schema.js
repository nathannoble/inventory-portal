import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

export const Annotations = new Mongo.Collection('annotations');

const Annotation = Class.create({
  name: 'Annotation',
  collection: Annotations,
  fields: {
    collectionName: String,
    collectionId: {
      type: String,
      optional: true,
    },
    routes: {
      // A semi-transient list of routes under which the annotation should appear
      type: [String],
      optional: true,
    },
    content: String,
  },
  secured: false,
});

export default Annotation;
