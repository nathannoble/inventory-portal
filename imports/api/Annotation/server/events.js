const AnnotationEvents = {
  beforeSave(event) {
    const routes = [];
    switch (event.currentTarget.collectionName) {
      case 'Request':
        routes.push('requests');
        routes.push('requestList');
        routes.push('requestView');
        break;
      case 'Order':
        routes.push('orders');
        routes.push('orderList');
        routes.push('orderView');
        break;
      case 'Document':
        routes.push();
        break;
      case 'Conversation':
        routes.push();
        break;
      case 'Organization':
        routes.push('organization');
        routes.push('organizationList');
        routes.push('businessEdit');
        routes.push('organizationEdit');
        routes.push('schoolEdit');
        break;
      case 'Search':
        routes.push('searches');
        routes.push('searchList');
        routes.push('searchView');
        break;
      case 'User':
        routes.push('profile');
        routes.push('profileEdit');
        break;
      default:
        break;
    }
    event.currentTarget.set({ routes });
  },
};

export default AnnotationEvents;
