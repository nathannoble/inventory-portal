import Annotation, { Annotations } from '..';
import AnnotationEvents from './events';

Annotation.extend({
  events: AnnotationEvents,
});

export { Annotation as default, Annotations };
