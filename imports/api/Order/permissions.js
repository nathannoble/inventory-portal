import {
  CreatePermission,
  ReadPermission,
  UpdatePermission,
  DeletePermission,
} from '/imports/api/helpers/permissions';

import Business from '/imports/api/Organization/Business';

CreatePermission.extend({
  helpers: {
    order(user) {
      if (user.type === 'Administrator') return true;
      if (user.type === 'Volunteer') return true;
      if (user.type === 'Caseworker') return true;
    },
  },
});

ReadPermission.extend({
  helpers: {
    order(user) {
      return {
        Administrator: {},
        Volunteer: {},
        Caseworker: {},
      }[user.type];
    },
  },
});

UpdatePermission.extend({
  helpers: {
    order(user, order) {
      if (user.type === 'Administrator') return true;
      if (user.type === 'Volunteer') return true;
      //if (user.type === 'Caseworker') return true;
      if (order.ordererId === user._id) return true;
      return false;
    },
  },
});

DeletePermission.extend({
  helpers: {
    order(user,order) {
      if (user.type === 'Administrator') return true;
      if (user.type === 'Volunteer') return true;
      if (order.ordererId === user._id) return true;
      return false;
    },
  },
});
