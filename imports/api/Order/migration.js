import { Meteor } from 'meteor/meteor';

import Order, { Orders, OrderGroups } from '/imports/api/Order';

const MigrateOrders = function() {
  Order.find().forEach(function(order) {
    const rawOrder = Orders.findOne(order._id);
    console.log(rawOrder._id);
    // First make sure the order hasn't already been transformed
    if (typeof rawOrder.orderGroupId !== 'undefined') {
      const orderGroup = OrderGroups.findOne(rawOrder.orderGroupId);
      order.ordererId = orderGroup.ordererId;
      //order.orderedId = rawOrder.volunteerId;
      //order.acceptedByOrdered = rawOrder.acceptedByVolunteer;
      //order.acceptedByOrderer = rawOrder.acceptedByEducator;
      //order.orderTypeId = orderGroup.orderTypeId;
      order.orderedType = 'Volunteer';

      console.log('saving', order._id);
      order.save();
    }
  });
};

Meteor.methods({
  '/order/migrate'() {
    MigrateOrders();
  },
});

export default MigrateOrders;
