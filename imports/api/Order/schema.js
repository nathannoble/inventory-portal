import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Class, Enum } from 'meteor/jagi:astronomy';

import RecurringDuration from '/imports/api/helpers/RecurringDuration';
import ConcreteDuration from '/imports/api/helpers/ConcreteDuration';

export const Orders = new Mongo.Collection('orders');

export const OrderStatus_Old = Enum.create({
    name: 'Order Status Old',
    identifiers: ['New', 'Submitted', 'Locked', 'Completed'],
});

export const OrderStatus = Enum.create({
    name: "OrderStatus",
    identifiers: Meteor.settings.public.OrderStatus
});

export const OrderItemCategory_Old = Enum.create({
    name: 'Category Old',
    identifiers: [
        'Clothing Bag', //0
        'Other Clothing', //1
        'Diapers', //2
        'Wipes', //3
        'Feeding', //4
        'Hygiene Items', //5
        'Bedding', //6
        'Stroller',  //7
        'Toys', //8
        'Equipment' //9
    ],
});

export const OrderItemCategory = Enum.create({
    name: "Category",
    identifiers: Meteor.settings.public.OrderItemCategory
});

export const OrderBagType_Old = Enum.create({
    name: 'Bag Type Old',
    identifiers: [
        'Boy', //0
        'Girl', //1
        'Teen Boy', //2
        'Teen Girl', //3
        'Layette Boy', //4
        'Layette Girl', //5
        'Layette Neutral' //6
    ],
});

export const OrderBagType = Enum.create({
    name: "Bag Type",
    identifiers: Meteor.settings.public.OrderBagType
});

export const OrderClothingType_Old = Enum.create({
    name: 'Clothing Individual Items Old',
    identifiers: {
        "Gloves & Hat": 0,
        "Pajamas - Boys":2,
        "Pajamas - Girls":3,
        "Pants (2 Pair)":4,
        "Shirts (2)":5,
        "Shoes":6,
        "Socks (2 Pair)":7,
        "Jacket":8,
        "Underwear (2 Pair)":9,
        "Maternity": 1
    }
});

export const OrderClothingType = Enum.create({
    name: "Clothing Individual Items",
    identifiers: Meteor.settings.public.OrderClothingType
});

export const OrderDiaperType_Old = Enum.create({
    name: 'Diaper Type Old',
    identifiers: [
        'Preemie - Up to 6lbs',
        'Newborn - 4 to 10lbs',
        'Size 1 - 8 to 14lbs',
        'Size 2 - 12 to 18lbs',
        'Size 3 - 16 to 28lbs',
        'Size 4 - 22 to 37lbs',
        'Size 5 - More than 27lbs',
        'Size 6 - More than 35lbs',
        'Pull-Up Sm',
        'Pull-Up Med',
        'Pull-Up Lg',
        'Depends Sm-Med',
        'Depends Lg- XL',
        'Overnight- Med 45-65 lb',
        'Overnight- Lg 64- 85 lb',
        'Overnight- XL 125 lb',
    ]
});

export const OrderDiaperType = Enum.create({
    name: "Diaper Type",
    identifiers: Meteor.settings.public.OrderDiaperType
});

export const OrderHygieneType_Old = Enum.create({
    name: 'Hygiene Type Old',
    identifiers: [
        'Hygiene Kit - Child', //0
        'Hygiene Kit - Teen Girl', //1
        'Hygiene Kit - Teen Boy', //2
        'Shampoo', //3
        'Baby Shampoo', //4
        'Brush/Comb', //5
        'Deodorant', //6
        'Soap', //7
        'Breast Pads', //8
        'Female Pads', //9
        'Tampons', //10
        'Toothbrush', //11
        'Toothpaste', //11
        'Towel', //12
        'Wash Cloths' //13
    ]
});

export const OrderHygieneType = Enum.create({
    name: "Hygiene Type",
    identifiers: Meteor.settings.public.OrderHygieneType
});

export const OrderBeddingType_Old = Enum.create({
    name: 'Bedding Type Old',
    identifiers: [
        'Blankets: Twin',
        'Blankets: Full',
        'Blankets: Queen',
        'Blankets: King',
        'Crib Mattress',
        'Crib Sheets',
        'Crib Mattress Pad',
        'Sheets: Twin',
        'Sheets: Full',
        'Sheets: Queen',
        'Sheets: King',
        'Baby Blanket',
        'Teen Blanket'
    ]
});

export const OrderBeddingType = Enum.create({
    name: "Bedding Type",
    identifiers: Meteor.settings.public.OrderBeddingType
});


export const OrderEquipmentType_Old = Enum.create({
    name: 'Equipment Type Old',
    identifiers: {
        'Backpack Carrier': 0,
        'Baby Bouncer (0 to 6 months)': 1,
        'Baby Monitor': 2,
        'Bathtub':4,
        'Booster Seat - Table':5,
        'Breast Feeding Pillow':7,
        'Bumbo':8,
        'Changing Pad':9,
        'Changing Pad Cover':10,
        'Crib- Play & Pack':11,
        'Diaper Bag':12,
        'Diaper Genie':13,
        'Front Pack- Infant':14,
        'High Chair':15,
        'Lunch Box':16,
        'Safety Gate':17,
        'Safety Locks':18,
        'School Backpack':19,
        'School Supplies':20,
        'Indoor Swing':21,
        'Tub Seat':22,
        'Potty Seat':23
    }
});

export const OrderEquipmentType = Enum.create({
    name: "Equipment Type",
    identifiers: Meteor.settings.public.OrderEquipmentType
});

export const OrderFeedingType_Old = Enum.create({
    name: 'Feeding Type Old',
    identifiers: [
        "Bib", "Bottle", "Formula", "Plates", "Utensils", "Sippy Cups"
    ]
});

export const OrderFeedingType = Enum.create({
    name: "Feeding Type",
    identifiers: Meteor.settings.public.OrderFeedingType
});

export const OrderToyType_Old = Enum.create({
    name: 'Toy Type Old',
    identifiers: {
        "Birthday Gift":3,
        "Books-4":4,
        "CD's":5,
        "DVD's":6,
        "Games":7,
        "Puzzles":8,
        "Toy- Lg":9,
        "Toy- Sm": 10
    }
});

export const OrderToyType = Enum.create({
    name: "Toy Type",
    identifiers: Meteor.settings.public.OrderToyType
});

export const OrderStrollerType_Old = Enum.create({
    name: 'Stroller Type Old',
    identifiers: [
        "Double", "Full Size", "Umbrella"
    ]
});

export const OrderStrollerType = Enum.create({
    name: "Stroller Type",
    identifiers: Meteor.settings.public.OrderStrollerType
});

export const OrderClothingSize_Old = Enum.create({
    name: 'Clothing Size Old',
    identifiers: [
        'N/A',
        '0-3 mo',
        '3-6 mo',
        '6-9 mo',
        //'9-12 mo',
        '12 mo',
        '18 mo',
        '24 mo',
        'Toddler 2',
        'Toddler 3',
        'Toddler 4',
        'Child 5',
        'Child 6',
        'Child 7',
        'Child 8',
        'Child 10',
        'Child 12',
        'Child 14',
        'Child 16',
        'Child 18',
        'Teen XS',
        'Teen S',
        'Teen M',
        'Teen L',
        'Teen XL',
        'Teen XXL'
    ]
});

export const OrderClothingSize = Enum.create({
    name: "Clothing Size",
    identifiers: Meteor.settings.public.OrderClothingSize
});

export const OrderGirlPantSize_Old = Enum.create({
    name: 'Girl Pant Size Old',
    identifiers: {
        'Infant 0-3':25,
        'Infant 3-6':26,
        'Infant 6-9 mo':0,
        'Infant 12 mo':1,
        'Infant 18 mo':2,
        'Infant 24 mo':3,
        'Toddler 2':4,
        'Toddler 3':5,
        'Toddler 4':6,
        'Child 5':7,
        'Child 6':8,
        'Child 7':9,
        'Child 8':10,
        'Child 10':11,
        'Pre-Teen 12':12,
        'Pre-Teen 14':13,
        'Pre-Teen 16':15,
        'Teen 0':16,
        'Teen 1':17,
        'Teen 3':18,
        'Teen 5':19,
        'Teen 7':20,
        'Teen 9':21,
        'Teen 11':22,
        'Teen 13':23,
        'Teen 15 UP':24
    }
});

export const OrderGirlPantSize = Enum.create({
    name: "Girl Pant Size",
    identifiers: Meteor.settings.public.OrderGirlPantSize
});

export const OrderBoyPantSize_Old = Enum.create({
    name: 'Boy Pant Size Old',
    identifiers: {
        'Infant 0-3':46,
        'Infant 3-6':47,
        'Infant 6-9 mo':0,
        'Infant 12 mo':1,
        'Infant 18 mo':2,
        'Infant 24 mo':3,
        'Toddler 2':4,
        'Toddler 3':5,
        'Toddler 4':6,
        'Child 5':7,
        'Child 6':8,
        'Child 7':9,
        'Child 8':10,
        'Child 10':11,
        'Pre-Teen 12':12,
        'Pre-Teen 14':13,
        'Pre-Teen 16':14,
        '29x30':15, '30x30':16, '30x32':17, '30x34':18, '30x36':19, '31x30':20,
        '32x30':21, '32x32':22, '32x34':23, '32x36':24, '33x30':25,
        '34x30':26, '34x32':27, '34x34':28, '34x36':29, '36x30':30, '36x32':31,
        '36x34':32, '36x36':33, '38x30':34, '38x32':35, '38x34':36,
        '38x36':37, '40x30':38, '40x32':39, '40x34':40, '40x36':41, '42x30':42,
        '42x32':43, '42x34':44, '42x36':45
    }

});

export const OrderBoyPantSize = Enum.create({
    name: "Boy Pant Size",
    identifiers: Meteor.settings.public.OrderBoyPantSize
});

export const OrderBoyPantsWidth = Enum.create({
    name: 'Teen Boy Pants Width',
    identifiers: [
        '28',
        '29',
        '30',
        '31',
        '32',
        '33',
        '34',
        '35',
        '36',
        '38',
        '40',
        '42',
        '44',
        '46'
    ]
});

export const OrderBoyPantsHeight = Enum.create({
    name: 'Teen Boy Pants H',
    identifiers: [
        '30',
        '31',
        '32',
        '33',
        '34',
        '35',
        '36'
    ]
});

export const OrderShoeSize_Old = Enum.create({
    name: 'Shoe Size Old',
    identifiers: [
        'no shoe'
        , 'Infant 1'
        , 'Infant 1.5'
        , 'Infant 2'
        , 'Infant 2.5'
        , 'Infant 3'
        , 'Infant 3.5'
        , 'Infant 4'
        , 'Infant 4.5'
        , 'Infant 5'
        , 'Infant 5.5'
        , 'Toddler 6'
        , 'Toddler 6.5'
        , 'Toddler 7'
        , 'Toddler 7.5'
        , 'Toddler 8'
        , 'Toddler 8.5'
        , 'Child 9'
        , 'Child 9.5'
        , 'Child 10'
        , 'Child 10.5'
        , 'Child 11'
        , 'Child 11.5'
        , 'Child 12'
        , 'Child 12.5'
        , 'Child 13'
        , 'Child 13.5'
        , 'Child 1'
        , 'Child 1.5'
        , 'Child 2'
        , 'Child 2.5'
        , 'Child 3'
        , 'Child 3.5'
        , 'Pre-Teen 4'
        , 'Pre-Teen 4.5'
        , 'Pre-Teen 5'
        , 'Pre-Teen 5.5'
        , 'Pre-Teen 6'
        , 'Pre-Teen 6.5'
        , 'Teen 7'
        , 'Teen 7.5'
        , 'Teen 8'
        , 'Teen 8.5'
        , 'Teen 9'
        , 'Teen 9.5'
        , 'Teen 10'
        , 'Teen 10.5'
        , 'Teen 11'
        , 'Teen 11.5'
        , 'Teen 12'
        , 'Teen 12.5'
        , 'Teen 13'
        , 'Teen 13.5'
        , 'Teen 14'
        , 'Teen 14.5'
        , 'Teen 15'
        , 'Teen 16'
    ]
});

export const OrderShoeSize = Enum.create({
    name: "Shoe Size",
    identifiers: Meteor.settings.public.OrderShoeSize
});

export const ChildStatus_Old = Enum.create({
    name: 'Child Status Old',
    identifiers: ['New', 'Existing'],
});

export const ChildStatus = Enum.create({
    name: "ChildStatus",
    identifiers: Meteor.settings.public.ChildStatus
});

export const ChildAge_Old = Enum.create({
    name: 'Child Age Old',
    identifiers: {
        'unborn':0,
        '1 mo':1,
        '2 mo':2,
        '3 mo':3,
        '4 mo':4,
        '5 mo':5,
        '6 mo':6,
        '7 mo':7,
        '8 mo':8,
        '9 mo':9,
        '10 mo':10,
        '11 mo':11,
        '1':12,
        '2':13,
        '3':14,
        '4':15,
        '5':16,
        '6':17,
        '7':18,
        '8':19,
        '9':20,
        '10':21,
        '11':22,
        '12':23,
        '13':24,
        '14':25,
        '15':26,
        '16':27,
        '17':28,
        '18':29
    },
});

export const ChildAge = Enum.create({
    name: "Child Age",
    identifiers: Meteor.settings.public.ChildAge
});


export const ChildSex_Old = Enum.create({
    name: 'Child Sex Old',
    identifiers: ['Male', 'Female', 'Unborn'],
});

export const ChildSex = Enum.create({
    name: "Child Sex",
    identifiers: Meteor.settings.public.ChildSex
});

export const OrderFilter = Class.create({
    name: 'Filter for Orders',
    fields: {
        name: {
            type: String,
            optional: true,
        },
        status: {
            type: OrderStatus,
            optional: true,
        },
        orderTypeId: {
            type: String,
            optional: true,
        },
    },
});

const Order = Class.create({
    name: 'Order',
    collection: Orders,
    fields: {
        status: {
            type: OrderStatus,
            default: OrderStatus.active,
        },
        createdAt: {
            type: Date,
            immutable: true,
            default: () => new Date(),
        },
        newChild: Number,
        childName: String,
        childZip: Number,
        childAge: Number,
        childWeight: Number,
        childSex: {
            type: ChildSex,
            default: ChildSex.Male,
        },
        //orderTypeData: Object,
        ordererId: String,
        pickupLocation: String,
        pickupDate: {
            type: Date,
            default: () => new Date(),
        },
        itemCategory: {
            type: Number,
            default: 0,
        },
        itemType: {
            type: Number,
            default: 0,
        },
        comments: String,
        items: {
            type: [Object],
            default: () =>
                Object({
                    itemsName: '',
                    itemsCategory: 0,
                    itemsType: 0,
                    itemsSize: 0,
                    shoeSize: {
                        type: Number,
                        default: 0,
                    },
                    pantSize: {
                        type: Number,
                        default: 0,
                    },
                    itemsComment: ''
                }),
        },
        path: {
            type: String,
            transient: true,
            resolve(doc) {
                return `${Order.path}/${doc._id}`;
            },
        },
    },
});

Order.path = '/order';

export default Order;
