import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import Order, { Orders, OrderStatus } from '/imports/api/Order';

Meteor.methods({
  '/order/new'(order, messageText = '') {
    check(order, Order);
    check(messageText, String);
    return order.save((error) => {
      if (error) throw error;

      return order._id;
    });
  },
  '/order/delete'(order) {
    console.log("api/Order/methods:/order/delete:order:" + JSON.stringify(order));
    check(order, Order);
    if (Meteor.user().may.delete.order(order) !== true) {
      throw new Meteor.Error(401, 'Not authorized to delete order');
    }
    return order.remove((error) => {
      if (error) throw error;
    });
  },
  '/order/update'(order) {
    check(order, Order);
    return order.save((error) => {
      if (error) throw error;
      console.log("api/Order/methods:/order/update:order:" + JSON.stringify(order));

      // 0 'pending',
      // 1 'active',
      // 2 'complete',
      // 3 'cancelled',
      // 4 'archived'

    });
  },
  '/order/createOrderBizView'(query) {

      console.log("api/Order/methods:/order/createOrderBizView:query:" + JSON.stringify(query));

      let recentTime = new Date();
      recentTime.setDate(recentTime.getDate() - Meteor.settings.orderHistory);

      if (Meteor.user().type === 'Administrator') {

        const orderAgg = Orders.aggregate(
              //{
              //  $match: query
              //},
              //{
              //  $lookup: {
              //    from: "orders",
              //    localField: "ordererId",
              //    foreignField: "_id",
              //    as: "users"
              //  }
              //},
              //{ $out: "ordersBiz"}
          [
            // ordersUser
            {
              $lookup: {
              from: "users",
              localField: "ordererId",
              foreignField: "_id",
              as: "users_docs"
              }
            },
            {
              $project: {
              "userId": "$users_docs._id",
              "userName": "$users_docs.name",
               organizationId:  "$users_docs.profile.organizationId"
              }
            },
            // ordersUserOrg
            {
              $lookup: {
                from: "organizations",
                localField: "organizationId",
                foreignField: "_id",
                as: "org_docs"
              }
            },
            {
              $project: {
                "_id": 1,
                "userId": "$userId",
                "userName": "$userName",
                "orgId":"$org_docs._id",
                "orgName": "$org_docs.name",
                "location": "$org_docs.location"
              }
            }
          ]);

        //{
        //  $project: {
        //  "users_docs._id": 1,
        //  "users_docs.name": 1,
        //  "users_docs.profile.organizationId": 1}
        //},

        //{
        //  $lookup: {
        //    from: "users",
        //        localField: "profile.organizationId",
        //        foreignField: "_id",
        //        as: "organizations"
        //  }
        //},
        //console.log("api/Order/methods:/order/createOrderBizView:orderAgg:" + JSON.stringify(orderAgg.toArray()));

        return true;
      } else
        throw new Meteor.Error(401, 'Not authorized to see all orders');

  },
});
