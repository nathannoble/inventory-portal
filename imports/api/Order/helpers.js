import moment from 'moment';
import listify from 'listify';
import camelcase from '/imports/api/helpers/camelcase';

//import container from '/imports/ui/helpers/container';

import OrderType from '/imports/api/OrderType';
import Business from '/imports/api/Organization/Business';

import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import Administrator from '/imports/api/User/Administrator';

import Feedback from '/imports/api/Feedback';

export const OrderFilterHelpers = {
  active() {
    return (
      (this.duration && (this.duration.start || this.duration.end)) ||
      this.orderTypeId ||
      this.businessId
    );
  },
  business() {
    //return Business.findOne(this.businessId);
    const organization = Business.findOne(this.businessId);
    try {
      if (!organization) {
        //throw new Meteor.Error(404, `Organization with id '${this.organizationId} not found'`);
        return new Business({ name:"None"});
      }
    } catch (error) {
      console.error("Order:helpers.js:catch:" + error);
    }
    return organization;
  },
};

const OrderHelpers = {
  data() {
    const data = [
      // Example:
      // {
      //  slug: 'equipmentAvailable',
      //  value: 'Projector'
      //  proto: {
      //    name: 'Equipment Available',
      //    inputType: 'text',
      //    required: false
      //  }
      // }
    ];
  },
  feedback(owner = false, createIfNone = true) {
    let feedback;
    if (owner) {
      feedback = Feedback.findOne({
        activityId: this._id,
        ownerId: typeof owner === 'object' ? owner._id : owner,
      });
      if (!feedback && createIfNone) {
        feedback = new Feedback();
        feedback.activityId = this._id;
        feedback.ownerId = owner;
      }
    } else {
      feedback = Feedback.findOne({
        activityId: this._id,
        ownerId: Meteor.userId(),
      });
    }
    return feedback;
  },
  feedbacks() {
    return Feedback.find(Meteor.user().may.read.feedback())
      .fetch()
      .filter(feedback => feedback.activityId === this._id);
  },
  ongoing() {
    return this.orderType().ongoing;
  },
  ordered() {
    if(this.orderedType === 'Caseworker') return Caseworker.findOne(this.orderedId);
    if(this.orderedType === 'Volunteer') return Volunteer.findOne(this.orderedId);
    if(this.orderedType === 'Business') return Business.findOne(this.orderedId);
    return { name: 'Error' };
  },
  orderer() {
    const user = Meteor.user(this.ordererId);
    const user2 = Caseworker.findOne(this.ordererId);
    const user3 = Meteor.users.findOne({_id:this.ordererId});
    
    if (user) {
      //console.log("api/Order/helpers:orderer:" + JSON.stringify((user)));
      if (user.type === 'Volunteer') return Volunteer.findOne(this.ordererId) || new Volunteer({
            name: "Unknown",
            type: "User"
          });
      if (user.type === 'Caseworker') return Caseworker.findOne(this.ordererId) || new Caseworker({
            name: "Unknown",
            type: "User"
          });

      if (user.type === 'Administrator') return Administrator.findOne(this.ordererId) || new Administrator({
            name: "Unknown",
            type: "User"
          });
    } else {

      //console.log("api/Order/helpers:orderer:unknown");
      return new Caseworker({
        name: "Unknown",
        type: "User"
      });

    }
  },
  orderType() {
    //return OrderType.findOne(this.orderTypeId);

    const orderType = OrderType.findOne(this.orderTypeId);
    try {
      if (!orderType) {
        return new OrderType({ name:"None"});
      }
    } catch (error) {
      console.error("Order:helpers.js:catch:" + error);
    }
    return orderType;

  },
  scheduledDate() {
    if (this.status >= 0) { // OrderStatus.pending) {
      const date = this.completedDate || this.orderedDate;
      let start = moment(date.start);
      let end = moment(date.end);
      if (start.format('D') === end.format('D')) {
        if (start.format('A') === end.format('A')) {
          start = start.format('M/D/YY h:mm');
          end = end.format('h:mm A');
        } else {
          start = start.format('M/D/YY h:mm A');
          end = end.format('h:mm A');
        }
      } else {
        start = start.format('M/D/YY h:mm A');
        end = end.format('M/D/YY h:mm A');
      }
      return `${start} — ${end}`;
    }
    return false;
  },
  title() {
    let title = '';
    if (Meteor.userId() === this.ordererId) {
      title += `${this.ordered().name} for ${this.orderType().name}`;
    } else if (Meteor.userId() === this.orderedId) {
      title += `${this.orderer().name} for ${this.orderType().name}`;
    } else {
      title += `${this.orderer().name} ordered ${this.ordered().name} for ${this.orderType()
        .name}`;
    }
    if (this.orderedDate) {
      const thisMoment = moment(this.orderedDate.start);
      let momentFormatString = 'MMM Do';
      if (thisMoment.year() !== moment().year()) momentFormatString += ', YYYY';
      title += ` on ${thisMoment.format(momentFormatString)}`;
    }
    return title;
  },
};

export default OrderHelpers;
