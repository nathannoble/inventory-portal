import Order, { Orders } from '..';
import '../methods';
import './publications';
import OrderEvents from './events';
import './cron';

Order.extend({
  events: OrderEvents,
});

export { Order as default, Orders };
