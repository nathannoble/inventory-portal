import { Meteor } from 'meteor/meteor';
import { SyncedCron } from 'meteor/quave:synced-cron';
//import { SyncedCron } from 'meteor/percolate:synced-cron';

import Order, { OrderStatus } from '..';

if (false) { //Meteor.isServer) {
  SyncedCron.add({
    log: false,
    name: 'Check to see if order dates have passed and mark them as complete',
    schedule: parser => parser.text('every 30 minutes'),
    job() {
      console.log("api/Order/server/cron");
      Order.find({ status: OrderStatus.active })
        .fetch()
        .forEach((order) => {
          // Check if the scheduled date has passed
          if (
            order.orderedDate &&
            order.status < OrderStatus.complete &&
            order.orderedDate.end < new Date()
          ) {
            order.status = OrderStatus.complete;
            order.save();
          }
        });
    },
  });
}
