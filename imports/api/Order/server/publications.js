import { Meteor } from 'meteor/meteor';
import { Promise } from 'meteor/promise';
import { check } from 'meteor/check';

import _ from 'underscore';

import DateTime from 'react-datetime';
import moment from 'moment';

import Order, {  Orders, OrderStatus } from '/imports/api/Order';
//import OrderBiz, { OrdersBiz } from '/imports/api/Order';

import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';

import Business from '/imports/api/Organization/Business';
import Feedback from '/imports/api/Feedback';

Meteor.publish('/order/read', function (orderId) {
    check(orderId, String);
    if (!this.userId) return this.ready();

    const order = Order.findOne(orderId);
    if (!order) return this.ready();

    const currentUser = Meteor.user();
    if (!currentUser.may.update.order(order)) return this.ready();

    const cursors = [];

    const orderCursor = Order.find({_id: orderId});
    const volunteerCursor = Volunteer.find({_id: order.orderedId});
    const caseworkerCursor = Caseworker.find({_id: order.orderedId});

    cursors.push(orderCursor);
    if (currentUser.type === 'Volunteer') {
        const organizationIds = [];
        cursors.push(volunteerCursor);
    } else if (currentUser.type === 'Caseworker') {
        const organizationIds = [];
        cursors.push(caseworkerCursor);
    } else if (currentUser.type === 'Administrator') {
        const organizationIds = [];
        cursors.push(volunteerCursor);
    }

    if (order.status >= 0) { //} OrderStatus.active) {
        const feedbackCursor = Feedback.find({activityType: 'Order', activityId: orderId});
        cursors.push(feedbackCursor);
    }

    return cursors;
});

Meteor.publish('/order/list', function () {
    if (!this.userId) return this.ready();
    const user = Meteor.user(this.userId);
    //console.log("api/Order/server/publications:user:" + JSON.stringify(user));
    console.log("api/Order/server/publications:this user:" + JSON.stringify(Meteor.user()));

    const cursors = [];

    let recentTime = new Date();
    recentTime.setDate(recentTime.getDate() - Meteor.settings.orderHistory);

    if (user.type === 'Volunteer') {

        const location = Business.find()
            .fetch()
            .filter(organization => organization._id === user.profile.organizationId)
            .map(organization => organization.location).toString();


        console.log("api/Order/server/publications:Volunteer:location:" + location);

        const orgs = Business.find()
            .fetch()
            .filter(organization => organization.location === parseInt(location, 0))
            //.map(organization => organization);
            .map(organization => (organization.name + " (" + organization.cpNumber + ")"));

        //console.log("api/Order/server/publications:Volunteer:orgs:" + JSON.stringify(orgs));

        const query = {
            $and:[
                {"pickupDate": {"$gte": recentTime}},
                {pickupLocation: {$in: orgs}},
            ]};

        const orderCursorV = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1}}
        );

        console.log("api/Order/server/publications:Volunteer:orderCursorV.count():" + orderCursorV.count());

        const volunteerCursor = Volunteer.find({
            _id: {$in: orderCursorV.map(order => order.orderedId)}
        });

        //console.log("api/Order/server/publications:Volunteer:volunteerCursor:" + volunteerCursor);

        cursors.push(orderCursorV);
        cursors.push(volunteerCursor);

    } else if (user.type === 'Caseworker') {

        const orderCursorCW = Order.find(
            {ordererId: user._id},
            user.may.read.order(),
            {sort: {'pickupDate': -1}, limit: 100}
        );

        console.log("api/Order/server/publications:Caseworker:orderCursorCW.count():" + orderCursorCW.count());

        const caseworkerCursor = Caseworker.find({
            _id: {$in: orderCursorCW.map(order => order.orderedId)}
        });
        cursors.push(orderCursorCW);
        cursors.push(caseworkerCursor);
    } else if (user.type === 'Administrator') {

        // If admin associated with location, find orders based on that location like a center director
        let query = {
            $and:[
                {"pickupDate": {"$gte": recentTime}},
            ]};
        if (user.profile.organizationId !== null) {
            const location = Business.find()
                .fetch()
                .filter(organization => organization._id === user.profile.organizationId)
                .map(organization => organization.location).toString();


            console.log("api/Order/server/publications:Administrator:location:" + location);

            const orgs = Business.find()
                .fetch()
                .filter(organization => organization.location === parseInt(location, 0))
                .map(organization => (organization.name + " (" + organization.cpNumber + ")"));

            console.log("api/Order/server/publications:Administrator:orgs:" + JSON.stringify(orgs));

            query = {
                $and:[
                    {"pickupDate": {"$gte": recentTime}},
                    {pickupLocation: {$in: orgs}},
                ]};
        }

        const orderCursor = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1}, limit: 100});

        console.log("api/Order/server/publications:Administrator:orderCursor.count():" + orderCursor.count());

        cursors.push(orderCursor);
    }

    return cursors;
});

Meteor.publish('/order/searchByDate', function (startDate,endDate) {
    console.log("api/Order/server/publications:/order/searchByDate:startDate,endDate:" + startDate + ":" + endDate);
    if (!this.userId) return this.ready();
    const user = Meteor.user(this.userId);
    console.log("api/Order/server/publications:this user:" + JSON.stringify(Meteor.user()));

    const cursors = [];

    if (user.type === 'Volunteer') {

        const location = Business.find()
            .fetch()
            .filter(organization => organization._id === user.profile.organizationId)
            .map(organization => organization.location).toString();


        console.log("api/Order/server/publications:Volunteer:location:" + location);

        const orgs = Business.find()
            .fetch()
            .filter(organization => organization.location === parseInt(location, 0))
            .map(organization => (organization.name + " (" + organization.cpNumber + ")"));

        const query = {
            $and:[
                {"pickupDate": {"$gte": startDate}},
                {"pickupDate": {"$lte": endDate}},
                {pickupLocation: {$in: orgs}},
            ]};

        const orderCursorV = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1}}
        );

        console.log("api/Order/server/publications:Volunteer:orderCursorV.count():" + orderCursorV.count());

        const volunteerCursor = Volunteer.find({
            _id: {$in: orderCursorV.map(order => order.orderedId)}
        });

        //console.log("api/Order/server/publications:Volunteer:volunteerCursor:" + volunteerCursor);

        cursors.push(orderCursorV);
        cursors.push(volunteerCursor);

    } else if (user.type === 'Caseworker') {

        const query = {
            $and:[
                {ordererId: user._id},
                {"pickupDate": {"$gte": startDate}},
                {"pickupDate": {"$lte": endDate}}
            ]};
        const orderCursorCW = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1}, limit: 100}
        );

        console.log("api/Order/server/publications:Caseworker:orderCursorCW.count():" + orderCursorCW.count());

        const caseworkerCursor = Caseworker.find({
            _id: {$in: orderCursorCW.map(order => order.orderedId)}
        });
        cursors.push(orderCursorCW);
        cursors.push(caseworkerCursor);
    } else if (user.type === 'Administrator') {

        // If admin associated with location, find orders based on that location like a center director
        let query = {
            $and:[
                {"pickupDate": {"$gte": startDate}},
                {"pickupDate": {"$lte": endDate}},
            ]};
        if (user.profile.organizationId !== null) {
            const location = Business.find()
                .fetch()
                .filter(organization => organization._id === user.profile.organizationId)
                .map(organization => organization.location).toString();


            console.log("api/Order/server/publications:Administrator:location:" + location);

            const orgs = Business.find()
                .fetch()
                .filter(organization => organization.location === parseInt(location, 0))
                .map(organization => (organization.name + " (" + organization.cpNumber + ")"));

            console.log("api/Order/server/publications:Administrator:orgs:" + JSON.stringify(orgs));

            query = {
                $and:[
                    {"pickupDate": {"$gte": startDate}},
                    {"pickupDate": {"$lte": endDate}},
                    {pickupLocation: {$in: orgs}},
                ]};
        }

        const orderCursor = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1}, limit: 100});

        console.log("api/Order/server/publications:Administrator:orderCursor.count():" + orderCursor.count());

        cursors.push(orderCursor);
    }

    return cursors;
});


Meteor.publish('/order/list/all', function () {
    if (!this.userId) return this.ready();
    const user = Meteor.user(this.userId);
    //console.log("api/Order/server/publications:user:" + JSON.stringify(user));
    //console.log("api/Order/server/publications:this user:" + JSON.stringify(Meteor.user()));

    const cursors = [];

    let recentTime = new Date();
    recentTime.setDate(recentTime.getDate() - Meteor.settings.orderHistory);

    if (user.type === 'Volunteer') {

    } else if (user.type === 'Caseworker') {

    } else if (user.type === 'Administrator') {
        let query = {$and:[{"pickupDate": {"$gte": recentTime}}]};

        const orderCursor = Order.find(
            query,
            user.may.read.order(),
            {sort: {'pickupDate': -1},limit: 100});

        console.log("api/Order/server/publications:Administrator:/order/list/all:orderCursor.count:" + orderCursor.count());
        //console.log("api/Order/server/publications:Administrator:/order/list/all:orderCursor(agg):props:" + FindAllProperties(orderAgg));
        //console.log("api/Order/server/publications:Administrator:/order/list/all:orderCursor(agg):methods:" + FindAllMethods(orderAgg));

        cursors.push(orderCursor);
    }

    return cursors;
});