/* eslint no-param-reassign: 0 */
import { Meteor } from 'meteor/meteor';
import moment from 'moment';

import Order, { OrderStatus } from '/imports/api/Order';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import Administrator from '/imports/api/User/Administrator';

import CC from '/imports/api';

const OrderEvents = {
  beforeUpdate: [
    CC.events.updateTimestamps,
    function(event) {
      if (this.userId) {
        if (Meteor.user().may.update.order(event.currentTarget) !== true) {
          throw new Meteor.Error(403, 'You are not authorized to update this order');
        }
      }
    },
    function(event) {
      // Fills out the completedDate field upon the order being marked as complete
      const order = event.currentTarget;
      if (order.status === OrderStatus.complete && !order.completedDate) {
        //order.completedDate = order.orderedDate;
      }
      // Prevents the order being marked as active while being in the past
      //if (
      //  order.status === OrderStatus.active &&
      //  moment(order.pickupDate).isBefore(new Date())
      //) {
      //  order.status = OrderStatus.pending;
      //}
      //order.pickupDate = new Date();
    },
  ],
  beforeInsert: [
    function(event) {
      if (Meteor.user().may.create.order(event) !== true) {
        throw new Meteor.Error(403, 'You are not authorized to create a order for volunteers');
      }
    },
  ],
  afterInsert(event) {
    const order = event.currentTarget;
    order.path = `${Order.path}/${order._id}`;
    console.log("api/Order/server:OrderEvents:afterInsert:order:" + JSON.stringify(order));

    // Email admin
    //Meteor.call(
    //    '/email',
    //    'order/NotifyNew',
    //    Administrator.find()
    //        .fetch()
    //        .map(administrator => administrator.email),
    //    Meteor.settings.email.from,
    //    'New Order made',
    //    {
    //      order,
    //    },
    //    (error) => {
    //      console.error(error);
    //    },
    //);

    // Email volunteer
    //Meteor.call(
    //    '/email',
    //    'order/NotifyNew',
    //    [order.orderer().email],
    //    Meteor.settings.email.from,
    //    'New Order made',
    //    {
    //      order,
    //    },
    //    (error) => {
    //      console.error(error);
    //    },
    //);
  },
  afterUpdate(event) {
    //const doc = event.currentTarget;
    //if (doc.status === OrderStatus.complete) {
    //  const volunteer = Volunteer.findOne(doc.orderedId);
    //  const caseworker = Caseworker.findOne(doc.orderedId);
    //  if (caseworker) {
    //    caseworker.dateOfLastCompletedOrder = new Date();
    //    Meteor.call('/user/caseworker/update', caseworker);
    //  } else {
    //    if (volunteer) {
    //      volunteer.dateOfLastCompletedOrder = new Date();
    //      Meteor.call('/user/volunteer/update', volunteer);
    //    }
    //  }
    //}
  },
  beforeRemove(event) {
    if (Meteor.user().may.delete.order(event.currentTarget) !== true) {
      throw new Meteor.Error(403, 'You are not authorized to delete this order');
    } else {
      const order = event.currentTarget;
      //Conversation.find({
      //  regardingDocumentId: order._id,
      //  regardingDocumentType: 'Order',
      //}).forEach(function(conversation) {
      //  conversation.remove();
      //});
      //Feedback.find({
      //  activityType: Order,
      //  activityId: order._id,
      //}).forEach(function(feedback) {
      //  feedback.remove();
      //});
    }
  },
};

export default OrderEvents;
