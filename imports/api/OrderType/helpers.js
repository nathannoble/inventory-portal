import React from 'react';
import { Label, Select } from '/imports/ui/helpers';
import camelcase from '/imports/api/helpers/camelcase';

const OrderTypeHelpers = {
  slug() {
    return camelcase(this.name);
  },
};

class OrderTypeFieldRenderer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: undefined,
    };
  }

  render() {
    const {
      field: {
        name, inputType, required, multiple, options,
      }, slug,
    } = this.props;
    const props = {
      key: slug,
      name: slug,
      text: name,
      type: inputType,
      required,
    };
    if (inputType === 'select') {
      props.element = Select;
      props.inputProps = { id: slug };
      props.value = this.state.value;
      props.onChange = (selectedOption) => {
        this.setState({ value: selectedOption });
      };
      if (options.length) {
        props.options = options.map(option => Object({ value: option, label: option }));
      }
    }
    return <Label {...props} />;
  }
}

const OrderTypeFieldHelpers = {
  element() {
    return <OrderTypeFieldRenderer field={this} slug={this.slug()} />;
  },
  slug() {
    return camelcase(this.name);
  },
};

export { OrderTypeHelpers as default, OrderTypeFieldHelpers };
