import { Meteor } from 'meteor/meteor';
import OrderType from '/imports/api/OrderType';
import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';
import Order from '/imports/api/Order';
import fixtures from '/imports/api/OrderType/server/fixtures';

function replaceOrderTypeId(array, oldId, newId) {
  if (array.includes(oldId)) {
    array.splice(array.indexOf(oldId), 1, newId);
  }
}

const conflateOngoingOrderTypes = function(orderType, callback) {
  if (orderType.name.includes('ongoing')) {
    const replacementOrderType = (function(name) {
      let baseName = name.replace('(ongoing)', '');
      baseName = baseName.trim();
      return OrderType.findOne({
        name: { $regex: `^(?!.*ongoing).*${baseName.replace(' ', '\\ ')}.*$` },
      });
    }(orderType.name));
    if (replacementOrderType) {
      Volunteer.find().forEach(function(volunteer) {
        replaceOrderTypeId(volunteer.orderTypeIds, orderType._id, replacementOrderType._id);
        volunteer.save();
      });
      Business.find().forEach(function(business) {
        replaceOrderTypeId(business.orderTypeIds, orderType._id, replacementOrderType._id);
        business.save();
      });
      Order.find({ orderTypeId: orderType._id }).forEach(function(order) {
        order.orderTypeId = replacementOrderType._id;
        order.save();
      });
      console.log('REMOVING', orderType.name, replacementOrderType);
      orderType.remove();
    } else {
      orderType.name = orderType.name.replace('(ongoing)', '');
      orderType.name = orderType.name.trim();
      orderType.save(function() {
        callback();
      });
    }
  } else if (orderType.name.includes('one-time')) {
    orderType.name = orderType.name.replace('(one-time)', '');
    orderType.name = orderType.name.trim();
    orderType.save(function() {
      callback();
    });
  } else {
    callback();
  }
};

const MigrateOrderTypes = function() {
  const fixtureNames = {};
  Object.keys(fixtures).forEach(function(fixtureName) {
    fixtureNames[fixtures[fixtureName].name] = fixtureName;
  });
  console.log(fixtureNames, Object.keys(fixtureNames));
  OrderType.find().forEach(function(orderType) {
    console.log('processing', orderType.name);
    // First remove the distinction between ongoing and one-time
    conflateOngoingOrderTypes(orderType, function() {
      console.log(Object.keys(fixtureNames), orderType.name);
      // Then remove all the order types not currently in the fixtures,
      // and remove references to them
      if (Object.keys(fixtureNames).includes(orderType.name)) {
        OrderType.getFieldsNames().forEach(function(field) {
          if (field !== '_id') orderType[field] = fixtures[fixtureNames[orderType.name]][field];
        });
        console.log(orderType);
        orderType.save();
      }
    });
  });
  Object.keys(fixtures).forEach((fixture) => {
    console.log(fixture);
    console.log(OrderType.findOne({ name: fixtures[fixture].name }));
    if (!OrderType.findOne({ name: fixtures[fixture].name })) {
      fixtures[fixture].save();
    }
  });
};

Meteor.methods({
  '/orderType/migrate'() {
    MigrateOrderTypes();
  },
});

export default MigrateOrderTypes;
