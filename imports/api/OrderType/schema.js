import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

export const OrderTypeField = Class.create({
  name: 'Order Type Field',
  fields: {
    name: String,
    inputType: String, // ['text','select','textarea','checkbox','number','radio',]
    required: Boolean,
    multiple: {
      type: Boolean,
      optional: true,
    },
    options: {
      type: [String],
      default: () => [],
    },
  },
});

export const OrderTypes = new Mongo.Collection('orderTypes');

const OrderType = Class.create({
  name: 'Order Type',
  collection: OrderTypes,
  fields: {
    description: {
      type: String,
      default: '',
    },
    fields: [OrderTypeField],
    gradeGroups: [String], // [primarySchool,middleSchool,highSchool,college]
    icon: String,
    name: String,
    orderedEntityTypes: {
      type: [String],
      default: () => [],
    },
  },
});

export default OrderType;
