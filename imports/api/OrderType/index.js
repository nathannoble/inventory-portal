import OrderType, { OrderTypes, OrderTypeField } from './schema';

import OrderTypeHelpers, { OrderTypeFieldHelpers } from './helpers';

OrderType.extend({
  helpers: OrderTypeHelpers,
});

OrderTypeField.extend({
  helpers: OrderTypeFieldHelpers,
});

export { OrderType as default, OrderTypes, OrderTypeField };
