import OrderType, { OrderTypes } from '..';
import '../methods';
import './publications';
import fixtures from './fixtures';
import OrderTypeEvents from './events';

OrderType.extend({
  events: OrderTypeEvents,
});

export { OrderType as default, OrderTypes };
