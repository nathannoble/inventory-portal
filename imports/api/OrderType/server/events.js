import { Meteor } from 'meteor/meteor';
import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';
import Order from '/imports/api/Order';
import OrderType from '/imports/api/OrderType';

const OrderTypeEvents = {
  beforeInsert() {
    if (Meteor.user().profile.role !== 'administrator') {
      throw new Meteor.Error(403, 'Adding Order Types is not permitted at this time');
    }
  },
  beforeUpdate() {
    // if(! Meteor.user().profile.role !== 'administrator') {
    //  throw new Meteor.Error(403, 'Only the system administrator can update order types');
    // }
  },
  beforeRemove(event) {
    if (Meteor.user().profile.role !== 'administrator') {
      throw new Meteor.Error(403, 'Only the system administrator can remove documents');
    } else {
      const orderType = event.currentTarget;
      Volunteer.find().forEach(function(volunteer) {
        if (volunteer.orderTypeIds.includes(orderType._id)) {
          volunteer.orderTypeIds.splice(volunteer.orderTypeIds.indexOf(orderType._id), 1);
        }
        volunteer.save();
      });
      Business.find().forEach(function(business) {
        if (business.orderTypeIds.includes(orderType._id)) {
          business.orderTypeIds.splice(business.orderTypeIds.indexOf(orderType._id), 1);
        }
        business.save();
      });
      Order.find({ orderTypeId: orderType._id }).forEach(function(order) {
        order.remove();
      });
    }
  },
};

export default OrderTypeEvents;
