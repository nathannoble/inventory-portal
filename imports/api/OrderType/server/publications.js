import { OrderTypes } from '/imports/api/OrderType';
import { WebApp } from 'meteor/webapp';

WebApp.connectHandlers.use('/api/v1/OrderType/list', (req, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json' });
  if (req.method === 'GET') {
    const industrySectors = OrderTypes.find().fetch();
    res.end(JSON.stringify(industrySectors));
  }
});
