import OrderType, { OrderTypes, OrderTypeField } from '..';

const guestSpeakerOrderType = new OrderType({
  name: 'Guest Speaker',
  gradeGroups: ['elementary-school', 'middle-school', 'high-school', 'college'],
  orderedEntityTypes: ['user', 'organization'],
  fields: [
    new OrderTypeField({
      name: 'Topic',
      inputType: 'text',
      required: true,
    }),
    new OrderTypeField({
      name: 'Learning Objective',
      inputType: 'text',
      required: true,
    }),
    new OrderTypeField({
      name: 'Number of Students',
      inputType: 'number',
      required: true,
    }),
    new OrderTypeField({
      name: 'Equipment Available',
      inputType: 'text',
      required: false,
    }),
    new OrderTypeField({
      name: 'Grade',
      inputType: 'text',
      required: true,
    }),
  ],
  icon: 'voice',
  description:
    'Share information about your career/job and/or your training/college experience (usually a 1-hour commitment)',
});

const fixtures = {
  guestSpeakerOrderType,
};

export default fixtures;
