import { Mongo } from 'meteor/mongo';
import { Class } from 'meteor/jagi:astronomy';

import User from '/imports/api/User';

export const SearchLogs = new Mongo.Collection('searchLogs');

const SearchLog = Class.create({
  name: 'Search Log',
  collection: SearchLogs,
  fields: {
    time: {
      type: Date,
      default: new Date(),
      immutable: true
    },
    userId: String,
    numberOfResults: Number,
    requestTypeId: String,
    gradeGroupsSelected: {
      type: [String],
      default: () => []
    },
    organizationId: {
      type: String,
      default: ''
    }
  },
  helpers: {
    user() {
      return User.findOne(this.userId);
    },
    requestType () {
      return RequestType.findOne(this.requestTypeId);
    },
  },
  secured: false,
});

export default SearchLog;
