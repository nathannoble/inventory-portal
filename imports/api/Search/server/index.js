import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import SearchLog from '/imports/api/Search';

import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Organization from '/imports/api/Organization';

Meteor.methods({
  '/search/log' (data) {
    const searchLog = new SearchLog(data);
    searchLog.save();
  }
});

Meteor.publish('/searchLogs', function () {
  if(!this.userId) return this.ready();
  if(Meteor.user().type !== 'Administrator') return this.ready();
  const cursors = [];
  const logCursor = SearchLog.find();
  const userCursor = User.find({ _id: { $in: logCursor.map(log => log.userId) } });
  const requestTypeCursor = RequestType.find({ _id: { $in: logCursor.map(log => log.requestTypeId) } });
  //const industrySectorCursor = IndustrySector.find();
  cursors.push(logCursor);
  cursors.push(userCursor);
  cursors.push(requestTypeCursor);
  //cursors.push(industrySectorCursor);
  return cursors;
});

Meteor.publish('/search', function({ requestTypeId }) {
  check(requestTypeId, String);
  if (Meteor.user().type === 'Educator') {
    const cursors = [];
    const volunteerCursor = Volunteer.find({ requestTypeIds: { $in: [requestTypeId] } });
    const organizationIds = [];
    volunteerCursor.forEach(function(volunteer) {
      volunteer.organizations.forEach(function(organization) {
        organizationIds.push(organization.organizationId);
      });
    });
    const organizationCursor = Organization.find({ $or: [
      {requestTypeIds: { $in: [requestTypeId] }},
      {_id: { $in: organizationIds }}
    ] });
    cursors.push(volunteerCursor);
    cursors.push(organizationCursor);
    return cursors;
  }
  return this.ready();
});
