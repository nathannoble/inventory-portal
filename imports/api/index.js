/* eslint no-param-reassign: 0 */
import React from 'react';

// A global helper
const CC = function() {};

CC.mdiIcons = {
  business: 'briefcase',
  create: 'plus',
  dashboard: 'home',
  document: 'file-document-box',
  educator: 'school',
  error: 'stop',
  internship: 'briefcase',
  profile: 'account',
  remove: 'delete',
  school: 'domain',
  student: 'face',
  volunteer: 'walk',
  caseworker: 'face',
  search: 'magnify',
  order: 'checkbox-marked-outline',
  report: 'format-list-bulleted-type'
};

//chart-bar
//chart-histogram

CC.icon = (name) => {
  if (CC.mdiIcons[name]) return <i className={`mdi mdi-${CC.mdiIcons[name]}`} />;
  return <i className={`mdi mdi-${name}`} />;
};

CC.events = {};

CC.events.updateTimestamps = (event) => {
  if (!event.currentTarget.createdAt) event.currentTarget.createdAt = new Date();
  event.currentTarget.updatedAt = new Date();
  if (!event.currentTarget.updates) event.currentTarget.updates = [];
  if (
    event.currentTarget.updates[0] &&
    event.currentTarget.updates[event.currentTarget.updates.length - 1].getTime() <
      new Date().getTime() - 300000
  ) {
    event.currentTarget.updates.push(new Date());
  }
};

export default CC;
