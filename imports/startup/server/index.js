import '/imports/startup/server/api.js';
import '/imports/startup/server/config';

import '/imports/api/OrderType/migration';
import '/imports/api/Order/migration';

import '/imports/api/Announcement/migration';
