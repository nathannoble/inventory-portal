import { SyncedCron } from 'meteor/quave:synced-cron';
//import { SyncedCron } from 'meteor/percolate:synced-cron';

import '/imports/api/helpers/Images';
import '/imports/api/helpers/methods.js';
import '/imports/api/helpers/publications.js';
import '/imports/api/Organization/server';

import '/imports/api/Order/server';
import '/imports/api/OrderType/server';
import '/imports/api/OrderUserOrg/server';

import '/imports/api/Announcement/server';

import '/imports/api/User/server';
import '/imports/api/UserOrg/server';
import '/imports/api/Search/server';

SyncedCron.start();
