/* eslint no-undef: 0, no-multi-str: 0 */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ServiceConfiguration } from 'meteor/service-configuration';
import inkyHTML from '/imports/api/helpers/inkyHTML';
//import { SSR } from 'meteor/meteorhacks:ssr';

Meteor.startup(() => {
  // Amazon
  //const SESAddress = encodeURIComponent(Meteor.settings.SES.address);
  //const SESPassword = encodeURIComponent(Meteor.settings.SES.password);
  //process.env.MAIL_URL = `smtps://${SESAddress}:${SESPassword}@email-smtp.us-west-2.amazonaws.com:465`;

  // Gmail
  const gmailAddress = encodeURIComponent(Meteor.settings.gmail.address);
  const gmailPassword = encodeURIComponent(Meteor.settings.gmail.password);
  process.env.MAIL_URL=`smtps://${gmailAddress}:${gmailPassword}@smtp.gmail.com:465`;
});

Accounts.emailTemplates.siteName = "Northwest Children's Outreach";
Accounts.emailTemplates.from = Meteor.settings.email.from;
Accounts.emailTemplates.resetPassword.subject = function(user) {
  return `[Northwest Children's Outreach] ${user.name}, Reset Your Password`;
};

if (Meteor.settings.oAuth) {
  if (Meteor.settings.oAuth.facebook) {
    ServiceConfiguration.configurations.upsert(
      { service: 'facebook' },
      {
        $set: {
          appId: Meteor.settings.oAuth.facebook.appId,
          secret: Meteor.settings.oAuth.facebook.secret,
        },
      },
    );
  }
  if (Meteor.settings.oAuth.twitter) {
    ServiceConfiguration.configurations.upsert(
      { service: 'twitter' },
      {
        $set: {
          consumerKey: Meteor.settings.oAuth.twitter.consumerKey,
          secret: Meteor.settings.oAuth.twitter.secret,
        },
      },
    );
  }
  if (Meteor.settings.oAuth.google) {
    ServiceConfiguration.configurations.upsert(
      { service: 'google' },
      {
        $set: {
          clientId: Meteor.settings.oAuth.google.clientId,
          secret: Meteor.settings.oAuth.google.secret,
        },
      },
    );
  }
}
