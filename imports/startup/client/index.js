import { Meteor } from 'meteor/meteor';

import Buffer from 'buffer';

if (typeof this.Buffer === 'undefined') {
  this.Buffer = Buffer.Buffer;
}

import React from 'react';
import { render } from 'react-dom';
import $ from 'jquery';

import App from '/imports/ui/layouts/App';
import Preview from '/imports/ui/layouts/Preview';

// First thing on startup is to attach react to the DOM
Meteor.startup(() => {
  global.Buffer = function() {};
  global.Buffer.isBuffer = () => false;

  const reactRoot = document.getElementById('react-root');
  if (window && window.navigator && window.navigator.serviceWorker) {
    navigator.serviceWorker
      .register('/sw.js')
      .then()
      .catch(error => console.error('serviceWorker registration failed: ', error));
  }
  //Meteor.subscribe('/user/administrator/list');
  //Meteor.subscribe('/user/all/list');
  Meteor.subscribe('/helpers', () => {
    if (window.location.pathname.startsWith('/preview')) {
      render(<Preview />, reactRoot);
    } else {
      render(<App />, reactRoot);
    }
  });
});
