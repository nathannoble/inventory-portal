import New from './New.jsx';
import List from './List.jsx';
import View from './View.jsx';

export default { New, List, View };
