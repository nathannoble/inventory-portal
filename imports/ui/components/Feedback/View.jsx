import React from 'react';

import Feedback from '/imports/api/Feedback';

import { handleError, CCButton } from '/imports/ui/helpers';

class FeedbackView extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      consideringRating: this.props.feedback.rating
    };
  }

  render () {
    const feedback = this.props.feedback;
    return <div className="feedback view" onClick={event => event.stopPropagation()}>
      <h2>
        {feedback.ownerId === Meteor.userId()
          ? 'My'
          : feedback.ownerId ? `${feedback.owner().name}'s` : null
        } Feedback
        {Meteor.user().type === 'Administrator' ? <time>{feedback.createdAt.toLocaleString()}</time> : null}
      </h2>
      <label htmlFor="comment">
        <textarea
          defaultValue={feedback.comment}
          placeholder="Make a comment"
          disabled={!Meteor.user().may.update.feedback(feedback)}
          onChange={event => {
            feedback.comment = String(event.target.value);
            Meteor.call('/feedback/update', feedback, handleError);
          }}
        />
      </label>
      <span className="rating">
        {[1, 2, 3, 4, 5].map(
          rating => <i
            key={rating}
            className={`mdi mdi-${this.state.consideringRating >= rating ? 'star' : 'star-outline'}`}
            onMouseEnter={event => {
              if(Meteor.user().may.update.feedback(feedback)) {
                const consideringRating = rating;
                this.setState({ consideringRating });
              }
            }}
            onMouseLeave={event => {
              const consideringRating = feedback.rating;
              this.setState({ consideringRating });
            }}
            onClick={event => {
              if(Meteor.user().may.update.feedback(feedback)) {
                feedback.set({ rating });
                Meteor.call('/feedback/update', feedback, handleError);
              }
            }}
          />
        )}
      </span>
      {Meteor.user().type === 'Administrator'
        ? <CCButton type="remove" onClick={event => {
          event.preventDefault();
          Meteor.call('/feedback/delete', feedback, handleError);
        }} />
        : null
      }
    </div>;
  }
}

export default FeedbackView;
