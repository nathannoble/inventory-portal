import React from 'react';

const HelpButton = () => (
  <a
    href="mailto:website@nwchildrens.org?subject=Question%20About%20Northwest%20Childrens%20Outreach"
    className="button"
    id="helpButton"
    title="Contact Administrator for Help"
  >
    Feedback
  </a>
);

export default HelpButton;
