import React from 'react';
import ReactDOMServer from 'react-dom/server';

import Papa from 'papaparse';
import DOMPurify from 'dompurify';

import DateTime from 'react-datetime';
import moment from 'moment';

import Business from '/imports/api/Organization/Business';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import { Checkbox, Select,  Label, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import saveAs from '/imports/ui/helpers/saveAs';
import Drawer from '/imports/ui/components/helpers/Drawer';

function groupBy(array, f) {
    let groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}

function sum(items, prop) {
    return items.reduce(function (a, b) {
        return a + b[prop];
    }, 0);
};

class CpTotalsOrderAdminTable extends React.Component {
    constructor(props) {
        super(props);

        let startDate;
        let endDate;

        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        const user = Meteor.user();
        
        if (props.match.params.startDate) {
            startDate = new Date(0);
            startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
            endDate = new Date(0);
            endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
        } else if (user.type != 'Caseworker'
            && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
                || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
            startDate = user.queryStartDate;
            endDate = user.queryEndDate;
        } else {
            startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
            endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
        }

        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);

        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        endDate.setMilliseconds(999);

        this.state = {
            columnsVisible: _.uniq(this.props.collection.cpTotalsAdminTable.columns.map(({ className, visible }) => (visible ? className : null))),
            daysUntilPickupLocationDay: daysUntilPickupLocationDay,
            startDate: startDate,
            endDate: endDate,
            dateRange: undefined,
            sort: undefined,
            reverse: false,
        };

    }

    render() {
        const collection = this.props.collection;

        let startDate = new Date();
        let endDate = new Date();

        const results = ((sort) => {

            let query = {
                $and: [
                    {"pickupDate": {"$gte": 0}},
                    {"pickupDate": {"$lte": 0}},
                ]
            };

            if (this.props.startDate) {

                startDate = this.props.startDate;
                endDate = this.props.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                query = {
                    $and: [
                        {"pickupDate": {"$gte": startDate}},
                        {"pickupDate": {"$lte": endDate}},
                    ]
                };
            } else if (this.state.startDate !== undefined &&
                this.state.endDate !== undefined) {

                startDate = this.state.startDate;
                endDate = this.state.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                if (startDate < endDate) {

                    query = {
                        $and: [
                            {"pickupDate": {"$gte": startDate}},
                            {"pickupDate": {"$lte": endDate}},
                        ]
                    };
                    console.log("components/CpTotalsOrderAdminTable.jsx:query:1:" + JSON.stringify(query));

                }

                console.log("components/CpTotalsOrderAdminTable.jsx:manual:startDate:" + startDate);
                console.log("components/CpTotalsOrderAdminTable.jsx:manual:endDate:" + endDate);

            }

            const orders = collection.find(query).fetch();

            orders.forEach((order) => {

                let cpNumber = 0;
                let name = "";
                if (order.pickupLocation.indexOf("(") > 0) {
                    cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
                    name = order.pickupLocation.replace(" " + cpNumber, "");
                    cpNumber = cpNumber.replace("(", "").replace(")", "");
                }
                order.careProvider = name;
                order.cpNumber = cpNumber;

            });

            let cpNumberFreq = groupBy(orders, function (order) {
                return [order.cpNumber];
            });

            if (sort) {
                cpNumberFreq.sort(sort);
                if (this.state.reverse) cpNumberFreq.reverse();
            } else {
                cpNumberFreq.sort((a, b) => compare(a.careProvider, b.careProvider));
            }

            console.log("components/CpTotalsOrderAdminTable.jsx:query:" + JSON.stringify(query));
            console.log("components/CpTotalsOrderAdminTable.jsx:orders.length:" + orders.length);
            console.log("components/CpTotalsOrderAdminTable.jsx:this.state.dateRange:" + this.state.dateRange);
            console.log("components/CpTotalsOrderAdminTable.jsx:startDate:" + startDate);
            console.log("components/CpTotalsOrderAdminTable.jsx:endDate:" + endDate);

            console.log("components/Order/CpTotalsOrderAdminTable.jsx:render:orders.length:" + orders.length);
            console.log("components/Order/CpTotalsOrderAdminTable.jsx:render:cpNumberFreq.length:" + cpNumberFreq.length);
            //console.log("components/Order/CpTotalsOrderAdminTable.jsx:render:orders:" + JSON.stringify(orders));
            //console.log("components/Order/CpTotalsOrderAdminTable.jsx:render:cpNumberFreq:" + JSON.stringify(cpNumberFreq));

            return cpNumberFreq;

        })(this.state.sort);

        return (
            <>
            <div id="reportRoot" className={`adminTable cpTotal`}>
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ text: `Order Totals` }]}/>
                    <Drawer>
                        <div className="adminTableFilter">
                            <Label
                                name="startDate"
                                element={DateTime}
                                text="Pick-up Start Date"
                                onChange={(selectedOption) => {
                                 //event.preventDefault();
                                 console.log("components/CpTotalsOrderAdminTable.jsx:startDate:selectedOption:" + JSON.stringify(selectedOption));
                                 if (typeof selectedOption === "object") {
                                     this.setState({
                                        startDate: selectedOption ? selectedOption.toDate() : undefined,
                                        endDate:undefined,
                                        dateRange: undefined,
                                     });
                                 }
                                 }}
                                timeFormat={false}
                                value={this.state.startDate}
                            />
                            <Label
                                name="endDate"
                                element={DateTime}
                                text="Pick-up End Date"
                                onChange={(selectedOption) => {
                                     //event.preventDefault();
                                     console.log("components/CpTotalsOrderAdminTable.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                                     if (typeof selectedOption === "object") {
                                         this.setState({
                                            endDate: selectedOption ? selectedOption.toDate() : undefined,
                                            dateRange: undefined,
                                          });
                                          
                                        if (this.state.startDate !== undefined && selectedOption.toDate() !== undefined) {

                                            let startDate = this.state.startDate;
                                            let endDate = selectedOption.toDate();

                                            startDate.setHours(0);
                                            startDate.setMinutes(0);
                                            startDate.setSeconds(0);
                                            startDate.setMilliseconds(0);

                                            endDate.setHours(23);
                                            endDate.setMinutes(59);
                                            endDate.setSeconds(59);
                                            endDate.setMilliseconds(999);

                                            if (startDate < endDate) {
                                                console.log("components/CpTotalsOrderAdminTable.jsx:manual:startDate:" + startDate);
                                                console.log("components/CpTotalsOrderAdminTable.jsx:manual:endDate:" + endDate);
                                                this.props.history.push(`/order/cpTotalsReport/${startDate.getTime()}/${endDate.getTime()}`);

                                           }
                                        }
                                     }
                                 }}
                                timeFormat={false}
                                value={this.state.endDate}
                            />
                        </div>
                    </Drawer>
                </SubHeader>
                <div className="headers2">
                    <span>{'CP Totals Report: ' + startDate.toLocaleDateString() + ' - ' + endDate.toLocaleDateString()}</span>
                </div>
                <div className="headers">
                    {collection.cpTotalsAdminTable.columns.map(({ header, className, sort }) =>
                        (this.state.columnsVisible.indexOf(className) >= 0 ? (
                            <span className={className}>
                                {header === "CaseWorker" ? "Users" : header}
                                {sort ? (
                                    <div className="sort">
                                        <i
                                            className={`mdi mdi-arrow-up${this.state.sort === sort && this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: true })}
                                        />
                                        <i
                                            className={`mdi mdi-arrow-down${this.state.sort === sort &&
                                            !this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: false })}
                                        />
                                    </div>
                                ) : null}
                            </span>
                        ) : null))}
                    <span className={'number2'}>Clothing Bags</span>
                    <span className={'number'}>Total Items</span>
                    <span className={'number'}>New Kids</span>
                </div>
                <div className="data">
                    {results.map((datum, index) => (
                        <div className="datum" index={index} key={datum[0]._id} id={datum[0]._id}>
                            <span className={'number'}>{datum[0].cpNumber}</span>
                            <span className={'careProvider'}>{datum[0].careProvider}</span>
                            <span className={'number2'}>{datum.length}</span>
                            <span
                                className={'number2'}>{datum.reduce(((sum2, result) => sum2 + result.items.reduce((n, item) => n + (item.itemsCategory === 0), 0)), 0)}</span>
                            <span
                                className={'number'}>{datum.reduce(((sum2, result) => sum2 + result.items.length), 0)}</span>
                            <span className={'number'}>{datum.length - sum(datum, "newChild")}</span>
                        </div>
                    ))}
                    <div className="datum">
                        <span className={'number'}></span>
                        <span className={'careProvider'}><b>Totals:</b></span>
                        <span className={'number2'}><b>{results.reduce(((sum2, result) => sum2 + result.length), 0)}</b></span>
                        <span
                            className={'number2'}><b>{results.reduce((sum2, result) => sum2 + result.reduce(((sum3, result3) => sum3 + result3.items.reduce((n, item) => n + (item.itemsCategory === 0), 0)), 0), 0)}</b></span>
                        <span
                            className={'number'}><b>{results.reduce((sum2, result) => sum2 + result.reduce(((sum3, result3) => sum3 + result3.items.length), 0), 0)}</b></span>
                        <span
                            className={'number'}><b>{results.reduce(((sum2, result) => sum2 + (result.length - sum(result, "newChild"))), 0)}</b></span>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default container((props, onData) => {
    //console.log("components/CpTotalsOrderAdminTable.jsx" + JSON.stringify(props));
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        onData(null, {});
    }
}, CpTotalsOrderAdminTable);
