import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Papa from 'papaparse';
import DOMPurify from 'dompurify';

import DateTime from 'react-datetime';
import moment from 'moment';

import Business from '/imports/api/Organization/Business';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import { Checkbox, Select,  Label, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import saveAs from '/imports/ui/helpers/saveAs';
import Drawer from '/imports/ui/components/helpers/Drawer';

class UserAdminTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            columnsVisible: _.uniq(this.props.collection.adminTable.columns.map(({ className, visible }) => (visible ? className : null))),
            sort: undefined,
            reverse: false,
            keyword: '',
            pickupLocation: undefined,
            businesses: props.businesses,
        };

    }

    render() {
        const collection = this.props.collection;
        const results = ((sort) => {

            let query = {};

            if (this.state.pickupLocation && this.state.pickupLocation !== null && this.state.pickupLocation!== undefined) {
                query = {"organizationId": {"$eq": this.state.pickupLocation}};
            }

            console.log("components/UserAdminTable.jsx:state:" + JSON.stringify(this.state));
            console.log("components/UserAdminTable.jsx:query:" + JSON.stringify(query));

            const results = collection.find(query).fetch();
            let filteredResults = [];

            console.log("components/UserAdminTable.jsx:results.length:" + results.length);

            results.forEach((result) => {

                const matchesKeyword = this.state.keyword
                    ? ((result, keyword) => {

                    if (result.name.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                        return true;
                    } else {
                        return false;
                    }
                })(result, this.state.keyword)
                    : true;

                if (matchesKeyword) {
                    filteredResults.push(result);
                }
            });

            console.log("components/UserAdminTable.jsx:filteredResults.length:" + filteredResults.length);

            if (sort) {
                filteredResults.sort(sort);
                if (this.state.reverse) filteredResults.reverse();
            } else {
                filteredResults.sort((a, b) => compare(a.name, b.name));
                console.log("components/UserAdminTable.jsx:filteredResults:default sort");
            }
            return filteredResults;

        })(this.state.sort);
        return (
            <div
                className={`adminTable ${collection.className.toLowerCase()}`}>
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ text: `${collection.className  === "Business" ? "Care Provider" : (collection.className  === "UserOrg" ? "User" : (collection.className  === "Volunteer" ? "This Location's User": (collection.className  === "OrderUserOrg") ? "Order" : collection.className))} List` }]}/>
                    <Drawer>
                        {
                            <div className="adminTableFilter">
                                <label htmlFor="keyword">
                                    <input
                                        type="text"
                                        id="keyword"
                                        placeholder="Search Keyword"
                                        value={this.state.keyword}
                                        onChange={(event) => {
                                          console.log("components/UserAdminTable.jsx:render:keyword:event.target.value:" + JSON.stringify(event.target.value));
                                          const keyword = event.target.value;
                                          this.setState({ keyword });
                                        }}
                                    />
                                </label>

                                <Select
                                    id="pickupLocation"
                                    value={this.state.pickupLocation}
                                    placeholder="Select Care Provider"
                                    options={_.uniq(this.state.businesses.map(organization => Object({ value: (organization._id  ), label: (organization.name + " (" + organization.cpNumber + ")") })))}
                                    onChange={(selectedOption) => {
                                    console.log("components/UserAdminTable.jsx:render:pickupLocation:selectedOption:" + JSON.stringify(selectedOption));
                                    this.setState({ pickupLocation: selectedOption ? selectedOption.value : undefined });
                                  }}
                                />
                            </div>}
                        <div className="adminTableFilter">
                            {collection.adminTable.columns.map(({ header, className }) =>
                                (header ? (
                                    <label htmlFor={className}>
                                        {header}
                                        <Checkbox
                                            id={className}
                                            checked={this.state.columnsVisible.indexOf(className) >= 0}
                                            onChange={(event) => {
                                              const columnsVisible = this.state.columnsVisible;
                                              if (event.target.checked) {
                                                columnsVisible.push(className);
                                              } else {
                                                columnsVisible.splice(columnsVisible.indexOf(className), 1);
                                              }
                                              this.setState({ columnsVisible });
                                            }}
                                        />
                                    </label>
                                ) : null))}
                        </div>
                    </Drawer>
                </SubHeader>
                <div className="headers">
                    {collection.adminTable.columns.map(({ header, className, sort }) =>
                        (this.state.columnsVisible.indexOf(className) >= 0 ? (
                            <span className={className}>
                                {header === "CaseWorker" ? "Users" : header}
                                {sort ? (
                                    <div className="sort">
                                        <i
                                            className={`mdi mdi-arrow-up${this.state.sort === sort && this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: true })}
                                        />
                                        <i
                                            className={`mdi mdi-arrow-down${this.state.sort === sort &&
                                            !this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: false })}
                                        />
                                    </div>
                                ) : null}
                            </span>
                        ) : null))}
                </div>
                <div className="data">
                    {results.map((datum, index) => (
                        <div className="datum" index={index} key={datum._id} id={datum._id}>
                            {collection.adminTable.columns.map(({ transform, className }) =>
                                (this.state.columnsVisible.indexOf(className) >= 0 ? (
                                    <span className={className}>{transform(datum)}</span>
                                ) : null))}
                        </div>
                    ))}
                </div>
                <div className="footers">
                    {collection.adminTable.emails ? (
                        <div className="emailActions">
                            <CCButton
                                type="copy"
                                onClick={(event) => {
                                  event.preventDefault();
                                  const input = document.createElement('INPUT');
                                  input.type = 'text';
                                  input.style = 'display: none';
                                  document.body.appendChild(input);
                                  collection.adminTable.emails().forEach((email) => {
                                    input.innerText += `${email.name} <${email.email}>, `;
                                  });
                                  let range;
                                  if (document.body.createTextRange) {
                                    range = document.body.createTextRange();
                                    range.moveToElementText(input);
                                    range.select();
                                  } else if (window.getSelection) {
                                    const selection = window.getSelection();
                                    range = document.createRange();
                                    range.selectNodeContents(input);
                                    selection.removeAllRanges();
                                    selection.addRange(range);
                                  }
                                  try {
                                    document.execCommand('copy');
                                    sAlert.success('Emails copied to clipboard');
                                    if (document.selection) {
                                      document.selection.empty();
                                    } else if (window.getSelection) {
                                      window.getSelection().removeAllRanges();
                                    }
                                  } catch (err) {
                                    window.prompt('Copy to clipboard: Ctrl+C, Enter', input.innerText);
                                  }
                                  document.body.removeChild(input);
                                }}
                            >
                                Copy Emails to Clipboard
                            </CCButton>
                        </div>
                    ) : null}
                    <CCButton
                        type="download"
                        onClick={(event) => {
                          event.preventDefault();
                          const fields = [];
                          const data = [];
                          collection.adminTable.columns.forEach(column =>
                              (column.export && this.state.columnsVisible.indexOf(column.className) >= 0
                                ? fields.push(column.header)
                                : false));
                          results.forEach((datum) => {
                            const row = [];
                            collection.adminTable.columns.forEach((column) => {
                              if (!column.export || this.state.columnsVisible.indexOf(column.className) < 0) {
                                return;
                              }
                              let text = column.transform(datum, true);
                              if (typeof text === 'object') {
                                if (text.props.title)
                                    text = text.props.title.replace('#', '');
                                else {
                                  text = ReactDOMServer.renderToStaticMarkup(text);
                                }
                              }
                              row.push(DOMPurify.sanitize(text, { ALLOWED_TAGS: [] }));
                            });
                            data.push(row);
                          });
                          const link = encodeURI(`data:text/csv;charset=utf-8,${Papa.unparse({ fields, data })}`);
                          saveAs(link, `${collection.className.toLowerCase()}_export.csv`);
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default container((props, onData) => {
    //console.log("components/UserAdminTable.jsx" + JSON.stringify(props));
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        const user = Meteor.user();

        let orgId = "Unknown";
        let location = null;

        if (user) {
            orgId = user.profile.organizationId;

            if (orgId !== "None") {
                let biz = Business.find()
                    .fetch()
                    .filter(organization => organization._id === orgId);

                console.log("components/UserAdminTable.jsx:biz:" + JSON.stringify(biz));
                if (biz.length > 0) {
                    //pickupDay = biz[0].pickupDay;
                    location = parseInt(biz[0].location, 0);
                }
            }
        }

        let businesses = Business.find({},{ sort: { 'name': 1 }})
            .fetch()
            .filter(organization => organization.location === location);

        // If no location exists, return all businesses
        if (location === null) {
            businesses = Business.find({},{ sort: { 'name': 1 }}).fetch()
        }
        console.log("components/UserAdminTable.jsx:businesses.length:" + businesses.length);

        onData(null, {businesses});
    }
}, UserAdminTable);
