import React from 'react';
import moment from 'moment';

import SearchLog from '/imports/api/Search';

import container from '/imports/ui/helpers/container';

const SearchLogView = container((props, onData) => {
  const subscription = Meteor.subscribe('/searchLogs', props);
  if(subscription.ready()) {
    onData(null, props);
  }
}, () => {
  return (
    <div className="adminTable searches">
      <div className="headers">
        <span className="time">Time</span>
        <span className="user">User</span>
        <span className="numberOfResults">Number Of Results</span>
        <span className="requestTypeId">Type</span>
        <span className="gradeGroupsSelected">Grade Groups</span>
      </div>
      <div className="data">
      {SearchLog.find().map(searchLog => (
        <div className="datum" id="{searchLog._id}">
          <span className="time">{moment(searchLog.time).format("MMM Do 'YY")}</span>
          <span className="user">{searchLog.user().name}</span>
          <span className="numberOfResults">{searchLog.numberOfResults}</span>
          <span className="requestTypeId">{searchLog.requestType().name}</span>
          <span className="gradeGroupsSelected">{searchLog.gradeGroupsSelected.join(', ')}</span>
        </div>
      ))}
      </div>
    </div>
  );
});

export default SearchLogView;
