import { Meteor } from 'meteor/meteor';
import React from 'react';
import DateTime from 'react-datetime';
import moment from 'moment';
import _ from 'underscore';
import { withRouter, NavLink } from 'react-router-dom';
import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';

import CC from '/imports/api';
import AbstractDuration from '/imports/api/helpers/AbstractDuration';

import {
  Select,
  DaySelector,
  handleError,
  SubHeader,
  Breadcrumbs,
  Label,
  CCButton,
  Modal,
} from '/imports/ui/helpers';

import Avatar from '/imports/ui/components/Avatar';
import UserCard from '/imports/ui/components/User/Card';
import OrganizationCard from '/imports/ui/components/Organization/Card';
import { DashboardIcon } from '/imports/ui/pages/Dashboard';

import container from '/imports/ui/helpers/container';
import Drawer from '/imports/ui/components/helpers/Drawer';

export const SearchDashboard = () => (
  <div className="search dashboard">
    {RequestType.find({}, { sort: { name: 1 } }).map(requestType => (
      <DashboardIcon
        icon={requestType.icon}
        className={`requestType ${requestType.slug()}`}
        label={requestType.name}
        linkTo={`/search/${requestType._id}`}
      />
    ))}
  </div>
);

const SearchResult = ({ entity, onClick }) => (
  <div className="result" onClick={onClick}>
    <Avatar url={entity.getAvatar()} />
    <span className="name">
      {entity.name}
      <small>
        {entity.type === 'Volunteer' && entity.affiliationNames().length
          ? ` — ${entity.affiliationNames().join(', ')}`
          : null}
      </small>
    </span>
    <CCButton onClick={onClick}>View</CCButton>
  </div>
);

class SearchResults extends React.Component {
  constructor(props) {
    super(props);
    this.sortCriteria = {
      name: {
        sort(a, b) {
          return a.name.localeCompare(b.name);
        },
        text: 'Name',
      },
      // industry: {
      //   sort(a, b) {
      //     return a.name.localeCompare(b.name);
      //   },
      //   text: 'Industry',
      // },
    };
    this.state = {
      requestTypeId: this.props.requestTypeId,
      gradeGroupsSelected: [],
      //industrySectorIds: [],
      textFilter: '',
      pageLength: 10,
      pageOffset: 0,
      organizationId: false,
      sort: {
        criterion: 'name',
        direction: 'descending',
      },
    };
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.requestTypeId !== this.state.requestTypeId) {
      // Reset filters, we're changing request type
      this.setState({
        requestTypeId: nextProps.requestTypeId,
        gradeGroupsSelected: [],
        //industrySectorIds: [],
        organizationId: false,
      });
    }
    return true;
  }
  render() {
    const { requestTypeId = null, onSelect = () => null } = this.props;
    let {
      //industrySectorIds,
      gradeGroupsSelected,
      organizationId,
      pageOffset,
      pageLength,
      textFilter,
      sort,
    } = this.state;
    let results = [];
    const requestType = RequestType.findOne(requestTypeId);
    const businessQuery = { $and: [] };
    const volunteerQuery = { $and: [] };
    businessQuery.$and.push({ requestTypeIds: { $in: [requestTypeId] } });
    volunteerQuery.$and.push({ requestTypeIds: { $in: [requestTypeId] } });
    //if (gradeGroupsSelected.length || industrySectorIds.length) {
    //  if (industrySectorIds.length) {
    //    businessQuery.$and.push({ industrySectorIds: { $in: industrySectorIds } });
    //    volunteerQuery.$and.push({ areasOfExpertiseIds: { $in: industrySectorIds } });
    //  }
    //  if (gradeGroupsSelected.length) {
    //    // businessQuery.$and.push();
    //    volunteerQuery.$and.push({ gradeGroups: { $in: gradeGroupsSelected } });
    //  }
    //}
    if (organizationId) {
      volunteerQuery.$and.push({ 'organizations.organizationId': organizationId });
      businessQuery.$and.push({ _id: 'volunteers-only' });
    } else {
      Business.find(businessQuery).forEach((business) => {
        results.push(business);
      });
    }
    Volunteer.find(volunteerQuery).forEach((volunteer) => {
      results.push(volunteer);
    });
    if (textFilter) {
      results = results.filter(result =>
        result.name.toLowerCase().includes(textFilter.toLowerCase()));
    }
    results.sort(this.sortCriteria[sort.criterion].sort);
    if (sort.direction === 'ascending') results.reverse();
    const totalResultsLength = results.length; // Store for later to determine pager ability
    results = results.slice(pageOffset, pageOffset + pageLength);
    Meteor.call('/search/log', {
      userId: Meteor.userId(),
      numberOfResults: totalResultsLength,
      requestTypeId,
      //industrySectorIds,
      gradeGroupsSelected,
      organizationId: organizationId ? organizationId : '',
    });
    return (
      <div className="searchResults">
        <form className="controls" onSubmit={event => event.preventDefault()}>
          <fieldset className="filter">

            <Label
              name="gradeLevels"
              text="Grades:"
              element={Select}
              multi
              value={gradeGroupsSelected}
              options={gradeGroups
                .filter(gradeGroup => requestType.gradeGroups.includes(gradeGroup.slug))
                .map(gradeGroup => Object({
                  value: gradeGroup.slug,
                  label: gradeGroup.name
                }))}
              onChange={(selectedOptions) => {
                this.setState({ gradeGroupsSelected: selectedOptions.map(option => option.value) });
              }}
            />
          </fieldset>
          <fieldset className="sort">
            {Object.keys(this.sortCriteria).map(criterion => (
              <CCButton
                key={criterion}
                type="sort"
                onClick={(event) => {
                  if (sort.criterion === criterion) {
                    this.setState({
                      sort: {
                        criterion,
                        direction: sort.direction === 'ascending' ? 'descending' : 'ascending',
                      },
                    });
                  } else {
                    this.setState({
                      sort: {
                        criterion,
                        direction: 'descending',
                      },
                    });
                  }
                }}
                className={criterion}
                direction={sort.criterion === criterion ? sort.direction : false}
              >
                {this.sortCriteria[criterion].text}
              </CCButton>
            ))}
          </fieldset>
        </form>
        <ol>
          {results.map(result => (
            <li key={result._id}>
              <SearchResult
                entity={result}
                onClick={() => {
                  onSelect(result);
                }}
              />
            </li>
          ))}
        </ol>
        <div className="pager">
          <Label
            name="pageLength"
            text="Rows per page"
            element={Select}
            value={pageLength}
            clearable={false}
            options={[
              { value: 10, label: '10' },
              { value: 25, label: '25' },
              { value: 100, label: '100' },
            ]}
            onChange={selectedOption => this.setState({ pageLength: selectedOption.value })}
          />
          <span className="pager-label">
            {pageOffset + 1} - {Math.min(pageOffset + pageLength, totalResultsLength)} of{' '}
            {totalResultsLength}
          </span>
          <CCButton
            type="left"
            className="spirit"
            disabled={pageOffset <= 0}
            onClick={(event) => {
              pageOffset = Math.max(pageOffset - pageLength, 0);
              this.setState({ pageOffset });
            }}
          />
          <CCButton
            type="right"
            className="spirit"
            disabled={pageOffset + pageLength >= totalResultsLength}
            onClick={(event) => {
              pageOffset += pageLength;
              this.setState({ pageOffset });
            }}
          />
        </div>
      </div>
    );
  }
}

const SearchResultsContainer = container((props, onData) => {
  const subscription = Meteor.subscribe('/search', props);
  if (subscription.ready()) {
    onData(null, props);
  }
}, SearchResults);

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requestTypeId: props.match.params.requestTypeId,
      viewingUser: false,
      makingRequest: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.requestTypeId !== nextProps.match.params.requestTypeId) {
      // Required because /search/:requestTypeId renders same component
      // and doesn't update state by default
      this.setState({ requestTypeId: nextProps.match.params.requestTypeId });
    }
    return true;
  }

  render() {
    return (
      <div className="search new body">
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[
              {
                link: '/search',
                text: ((state) => {
                  if (state.makingRequest) return `Requesting ${state.makingRequest.name}`;
                  return 'Search';
                })(this.state),
              },
            ]}
          />
        </SubHeader>
        <main>
          {this.state.requestTypeId && !this.state.makingRequest ? (
            <h1>
              <Select
                name="requestType"
                clearable={false}
                value={this.state.requestTypeId}
                options={RequestType.find({ name: { $not: 'Internship' } }).map(requestType =>
                  Object({ value: requestType._id, label: requestType.name }))}
                onChange={(selectedOption) => {
                  this.props.history.push(`/search/${selectedOption.value}`);
                }}
              />
            </h1>
          ) : null}
          {this.state.requestTypeId && !this.state.makingRequest ? (
            <p className="description">
              {RequestType.findOne(this.state.requestTypeId).description}
            </p>
          ) : null}
          {this.state.requestTypeId ? (
            this.state.makingRequest ? (
              <RequestNew
                entity={this.state.makingRequest}
                requestType={RequestType.findOne(this.state.requestTypeId)}
                onCancel={() => this.setState({ makingRequest: false })}
                history={this.props.history}
              />
            ) : (
              <SearchResultsContainer
                requestTypeId={this.state.requestTypeId}
                onSelect={(result) => {
                  if(result.type === 'Business') {
                    this.setState({ viewingBusiness: result });
                  } else if(result.type === 'Volunteer') {
                    this.setState({ viewingUser: result });
                  }
                }}
              />
            )
          ) : (
            <SearchDashboard />
          )}

          {this.state.viewingUser ? (
            <Modal
              onClose={(event) => {
                this.setState({ viewingUser: false });
              }}
              className="search volunteer"
            >
              <UserCard user={this.state.viewingUser} />
              <CCButton
                onClick={(event) => {
                  this.setState({ makingRequest: this.state.viewingUser, viewingUser: false });
                }}
              >
                Request
              </CCButton>
            </Modal>
          ) : this.state.viewingBusiness ? (
            <Modal
              onClose={(event) => {
                this.setState({ viewingBusiness: false });
              }}
              className="search business"
            >
              <OrganizationCard organization={this.state.viewingBusiness} />
              <CCButton
                onClick={(event) => {
                  this.setState({ makingRequest: this.state.viewingBusiness, viewingBusiness: false });
                }}
              >
                Request
              </CCButton>
            </Modal>
          ) : null}

        </main>
      </div>
    );
  }
}

export default Search;
