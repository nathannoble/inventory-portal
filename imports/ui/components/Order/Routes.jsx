import React from 'react';
import { Switch } from 'react-router-dom';

import { OrderStatus } from '/imports/api/Order';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import OrderNew from './New';
import OrderView from './View';
import OrderList from './List';
import OrderAdminTable from './AdminTable';
import CaseworkerOrderAdminTable from './CaseworkerOrderAdminTable';
import OrderGroupList from './GroupList';
import ItemTotalsOrderAdminTable from './ItemTotalsOrderAdminTable';
import ItemTotalsByZipOrderAdminTable from './ItemTotalsByZipOrderAdminTable';
import CbTotalsOrderAdminTable from './CbTotalsOrderAdminTable';
import CbTotalsPantsOrderAdminTable from './CbTotalsPantsOrderAdminTable';
import CbTotalsShoesOrderAdminTable from './CbTotalsShoesOrderAdminTable';
import CpTotalsOrderAdminTable from './CpTotalsOrderAdminTable';
import CenterTotalsOrderAdminTable from './CenterTotalsOrderAdminTable';
import OrderLabels from './Labels';
import PrintLabels from './PrintLabels';

import OrderHelp from './OrderHelp';

const OrderRoutes = props => (
    <Switch className="order">

        <Authenticated
            exact
            name="OrderNew"
            path={`${props.match.path}/new`}
            component={OrderNew}
            roles={['volunteer','caseworker','administrator']}
            {...props}
        />
        <Authenticated
        exact
        name="OrderList"
        path={`${props.match.path}/:startDate/:endDate`}
        component={OrderAdminTable}
        roles={['volunteer', 'administrator']}
        {...props}
        />
        <Authenticated
        exact
        name="OrderList"
        path={`${props.match.path}/:orderStatus(${OrderStatus.getIdentifiers().join('|')})?`}
        component={OrderAdminTable}
        roles={['volunteer', 'administrator']}
        {...props}
        />
        <Authenticated
        exact
        name="OrderHelp"
        path={`${props.match.path}/help`}
        component={OrderHelp}
        roles={['volunteer','caseworker','administrator']}
        {...props}
        />
        <Authenticated
            exact
            name="CaseworkerOrderAdminTable"
            path={`${props.match.path}/cw/:startDate/:endDate`}
            component={CaseworkerOrderAdminTable}
            roles={['caseworker']}
            {...props}
        />
        <Authenticated
        exact
        name="CaseworkerOrderAdminTable"
        path={`${props.match.path}/cw`}
        component={CaseworkerOrderAdminTable}
        roles={['caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="OrderGroupList"
        path={`${props.match.path}/report/:startDate/:endDate`}
        component={OrderGroupList}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
            exact
            name="OrderGroupList"
            path={`${props.match.path}/report`}
            component={OrderGroupList}
            roles={['volunteer','caseworker']}
            {...props}
        />
        <Authenticated
        exact
        name="CpTotalsOrderAdminTable"
        path={`${props.match.path}/cpTotalsReport/:startDate/:endDate`}
        component={CpTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="CpTotalsOrderAdminTable"
        path={`${props.match.path}/cpTotalsReport`}
        component={CpTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="CenterTotalsOrderAdminTable"
        path={`${props.match.path}/centerTotalsReport/:startDate/:endDate`}
        component={CenterTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="CenterTotalsOrderAdminTable"
        path={`${props.match.path}/centerTotalsReport`}
        component={CenterTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="ItemTotalsOrderAdminTable"
        path={`${props.match.path}/itemTotalsReport/:startDate/:endDate`}
        component={ItemTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="ItemTotalsOrderAdminTable"
        path={`${props.match.path}/itemTotalsReport`}
        component={ItemTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="ItemTotalsByZipOrderAdminTable"
        path={`${props.match.path}/itemTotalsByZipReport/:startDate/:endDate`}
        component={ItemTotalsByZipOrderAdminTable}
        roles={['volunteer','administrator']}
        {...props}
        />
        <Authenticated
            exact
            name="ItemTotalsByZipOrderAdminTable"
            path={`${props.match.path}/itemTotalsByZipReport`}
            component={ItemTotalsByZipOrderAdminTable}
            roles={['volunteer','administrator']}
            {...props}
        />
        <Authenticated
                exact
                name="CbTotalsOrderAdminTable"
                path={`${props.match.path}/cbTotalsReport/:startDate/:endDate`}
                component={CbTotalsOrderAdminTable}
                roles={['volunteer','caseworker']}
                {...props}
        />
        <Authenticated
        exact
        name="CbTotalsOrderAdminTable"
        path={`${props.match.path}/cbTotalsReport`}
        component={CbTotalsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
                exact
                name="CbTotalsShoesOrderAdminTable"
                path={`${props.match.path}/cbTotalsShoesReport/:startDate/:endDate`}
                component={CbTotalsShoesOrderAdminTable}
                roles={['volunteer','caseworker']}
                {...props}
        />
        <Authenticated
        exact
        name="CbTotalsShoesOrderAdminTable"
        path={`${props.match.path}/cbTotalsShoesReport`}
        component={CbTotalsShoesOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
                exact
                name="CbTotalsPantsOrderAdminTable"
                path={`${props.match.path}/cbTotalsPantsReport/:startDate/:endDate`}
                component={CbTotalsPantsOrderAdminTable}
                roles={['volunteer','caseworker']}
                {...props}
        />
        <Authenticated
        exact
        name="CbTotalsPantsOrderAdminTable"
        path={`${props.match.path}/cbTotalsPantsReport`}
        component={CbTotalsPantsOrderAdminTable}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
            exact
            name="OrderLabels"
            path={`${props.match.path}/labels/:startDate/:endDate`}
            component={OrderLabels}
            roles={['volunteer','caseworker']}
            {...props}
        />
        <Authenticated
        exact
        name="OrderLabels"
        path={`${props.match.path}/labels`}
        component={OrderLabels}
        roles={['volunteer','caseworker']}
        {...props}
        />
        <Authenticated
        exact
        name="PrintLabels"
        path={`${props.match.path}/printLabels/:startDate/:endDate`}
        component={PrintLabels}
        roles={['volunteer','administrator']}
        {...props}
        />
        <Authenticated
        exact
        name="PrintLabels"
        path={`${props.match.path}/printLabels`}
        component={PrintLabels}
        roles={['volunteer','administrator']}
        {...props}
        />
        <Authenticated
            exact
            name="OrderView"
            path={`${props.match.path}/:orderId`}
            component={OrderView}
            roles={['volunteer','caseworker','administrator']}
            {...props}
        />
        <Authenticated
                exact
                name="OrderView"
                path={`${props.match.path}UserOrg/:orderId`}
                component={OrderView}
                roles={['volunteer','caseworker','administrator']}
                {...props}
        />
    </Switch>
);

export default OrderRoutes;
