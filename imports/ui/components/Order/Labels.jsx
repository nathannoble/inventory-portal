import React from 'react';
import { Link, withRouter,useNavigate } from 'react-router-dom';

import DateTime from 'react-datetime';
import moment from 'moment';
import jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import PropTypes from 'prop-types';

import CC from '/imports/api';

import {
    OrderStatus, OrderFilter, OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType, OrderHygieneType, OrderBeddingType,
    OrderEquipmentType, OrderFeedingType, OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize, OrderBoyPantSize,
    OrderShoeSize, ChildAge, ChildSex
} from '/imports/api/Order';

import OrderUserOrg from '/imports/api/OrderUserOrg';
import Business from '/imports/api/Organization/Business';
import Drawer from '/imports/ui/components/helpers/Drawer';
import container from '/imports/ui/helpers/container';
import {
    SubHeader,
    Breadcrumbs,
    Select,
    Label
} from '/imports/ui/helpers';

class OrderLabelsFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: '',
            pickupLocation: undefined,
            businesses: props.businesses,
            startDate: props.startDate,
            endDate: props.endDate,
            dateRange: undefined,
            runDateQuery: undefined,
            status: undefined
        };

        //console.log("components/Order/Labels.jsx:filter:constructor:state:" + JSON.stringify(this.state));

    }

    componentDidUpdate(nextProps, nextState) {
        console.log("components/Order/Labels.jsx:OrderLabelsFilter:componentDidlUpdate:true");
        if (this.state !== nextState) {
           
            console.log("components/Order/Labels.jsx:OrderLabelsFilter:componentDidlUpdate:state change");
            this.props.onChange(nextState);
        }
    }

    render() {
        return (
            <div className="order list filter">
                <label htmlFor="keyword">
                    <input
                        type="text"
                        id="keyword"
                        placeholder="Search Keyword"
                        value={this.state.keyword}
                        onChange={(event) => {
                            console.log("components/Order/Labels.jsx:render:keyword:event.target.value:" + JSON.stringify(event.target.value));
                            const keyword = event.target.value;
                            this.setState({ keyword });
                        }}
                    />
                </label>

                {Meteor.user().type !== 'Caseworker' ?
                    <Select
                        id="pickupLocation"
                        value={this.state.pickupLocation}
                        placeholder="Select Care Provider"
                        options={_.uniq(this.state.businesses.map(organization => Object({ value: (organization.name + " (" + organization.cpNumber + ")"), label: (organization.name + " (" + organization.cpNumber + ")") })))}
                        onChange={(selectedOption) => {
                            console.log("components/Order/Labels.jsx:render:pickupLocation:selectedOption:" + JSON.stringify(selectedOption));
                            this.setState({ pickupLocation: selectedOption ? selectedOption.value : undefined });
                        }}
                    /> : null}

                <Label
                    name="startDate"
                    element={DateTime}
                    text="Pick-up Start Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("components/Order/Labels.jsx:render:startDate:selectedOption:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                startDate: selectedOption ? selectedOption.toDate() : undefined,
                                endDate: undefined,
                                dateRange: undefined
                            });
                        }
                    }}
                    timeFormat={false}
                    value={this.state.startDate}
                />

                <Label
                    name="endDate"
                    element={DateTime}
                    text="Pick-up End Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("components/Order/Labels.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                endDate: selectedOption ? selectedOption.toDate() : undefined,
                                dateRange: undefined,
                            });

                            if (this.state.startDate !== undefined
                                && selectedOption.toDate() !== undefined) {

                                let startDate = this.state.startDate;
                                let endDate = selectedOption.toDate();

                                startDate.setHours(0);
                                startDate.setMinutes(0);
                                startDate.setSeconds(0);
                                startDate.setMilliseconds(0);

                                endDate.setHours(23);
                                endDate.setMinutes(59);
                                endDate.setSeconds(59);
                                endDate.setMilliseconds(999);

                                if (startDate < endDate) {
                                    this.props.history.push(`/order/labels/${startDate.getTime()}/${endDate.getTime()}`);
                                }
                            }
                        }
                    }}
                    timeFormat={false}
                    value={this.state.endDate}
                />

                <input
                    type="button"
                    value="Reset"
                    onClick={(event) => {
                        event.preventDefault();

                        // This Week (Until this Friday at 5:00 PM)
                        let now = new Date();
                        let nextFriday = 5;
                        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
                        if (daysUntilPickupLocationDay < 0) {
                            daysUntilPickupLocationDay += 7;
                        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
                            daysUntilPickupLocationDay += 7;
                        }

                        console.log("components/Order/Labels.jsx:render:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
                        let startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
                        let endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));

                        startDate.setHours(0);
                        startDate.setMinutes(0);
                        startDate.setSeconds(0);
                        startDate.setMilliseconds(0);

                        endDate.setHours(23);
                        endDate.setMinutes(59);
                        endDate.setSeconds(59);
                        endDate.setMilliseconds(999);

                        this.setState({
                            keyword: '',
                            pickupLocation: undefined,
                            startDate: startDate,
                            endDate: endDate,
                            dateRange: undefined,
                            runDateQuery: undefined,
                            status: undefined,
                        });
                    }}
                />

            </div>
        );
    }
}

const OrderLabelsItem = ({ item, index }) => (
    <li id={item._id}>
        <Link to={`/order/${item._id}`}>
            <header>
                <span className="title">
                    Child: <b>{item.childName}</b><br />
                    Age: <b>
                        {/*ChildAge.getIdentifier(ChildAge.getValues()[item.childAge])*/}
                        {ChildAge.getIdentifier(item.childAge)}
                    </b>&emsp;
                    Wt: <b>{item.childWeight}</b>&emsp;
                    Sex: <b>{ChildSex.getIdentifier(ChildSex.getValues()[item.childSex])}</b>
                </span>
            </header>
            <div className="body">
                <div className="main-fields">
                    <span className="itemCategory">{OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory])}:&nbsp;
                    </span>

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Clothing Bag' ? (
                        <>
                            <span className="itemType">
                                {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType])}
                            </span>
                            <br />
                            <span className="sizeField">
                                <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Boy'
                                    || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Neutral' ? (
                                    <>
                                        <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b></b></span>
                                    </>
                                ) : null}
                                {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Girl' ? (
                                    <>
                                        <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b></b></span>
                                    </>
                                ) : null}
                                {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Boy' || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Boy' ? (
                                    <>
                                        <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                    </>
                                ) : null}
                                {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Girl'
                                    || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Girl' ? (
                                    <>
                                        <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                    </>
                                ) : null}
                            </span>
                        </>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Other Clothing' ? (
                        <>
                            <span className="itemType">
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType])}
                            </span>
                            <br />
                            <span className="sizeField">
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Gloves & Hat' ? (
                                    <>
                                        <span>Shirt: <b>{ }</b>&emsp;</span>
                                        <span>Pant: <b>{ }</b>&emsp;</span>
                                        <span>Shoe: <b>{ }</b></span>
                                    </>
                                ) : null}
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Jacket' ||
                                    OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Shirts (2)' ? (
                                    <>
                                        <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                        <span>Pant: <b>{ }</b>&emsp;</span>
                                        <span>Shoe: <b>{ }</b></span>
                                    </>
                                ) : null}
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Maternity' ||
                                    OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pajamas - Girls' ? (
                                    <>
                                        <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                        <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b>{ }</b></span>
                                    </>
                                ) : null}
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pajamas - Boys' ? (
                                    <>
                                        <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                        <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b>{ }</b></span>
                                    </>
                                ) : null}
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pants (2 Pair)' ||
                                    OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Underwear (2 Pair)' ? (
                                    <>
                                        <span>Shirt: <b>{ }</b>&emsp;</span>
                                        <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                        <span>Shoe: <b>{ }</b></span>
                                    </>
                                ) : null}
                                {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Shoes' ||
                                    OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Socks (2 Pair)' ? (
                                    <>
                                        <span>Shirt: <b>{ }</b>&emsp;</span>
                                        <span>Pant: <b>{ }</b>&emsp;</span>
                                        <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                    </>
                                ) : null}
                            </span>
                        </>

                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Diapers' ? (
                        <>
                            <span className="itemType">
                                {OrderDiaperType.getIdentifier(OrderDiaperType.getValues()[item.itemsType])}
                            </span>
                        </>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Hygiene Items' ? (
                        <span className="itemType">
                            {OrderHygieneType.getIdentifier(OrderHygieneType.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Wipes' ? (
                        <span className="itemType">
                            Size: {OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Bedding' ? (
                        <span className="itemType">
                            {OrderBeddingType.getIdentifier(OrderBeddingType.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Equipment' ? (
                        <span className="itemType">
                            {OrderEquipmentType.getIdentifier(OrderEquipmentType.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Feeding' ? (
                        <span className="itemType">
                            {OrderFeedingType.getIdentifier(OrderFeedingType.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Toys' ? (
                        <span className="itemType">
                            {OrderToyType.getIdentifier(OrderToyType.getValues()[item.itemsType])}
                        </span>
                    ) : null}

                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Stroller' ? (
                        <span className="itemType">
                            {OrderStrollerType.getIdentifier(OrderStrollerType.getValues()[item.itemsType])}
                        </span>
                    ) : null}
                    <span className="itemCommentsLabel"><span>Comments:</span> <div
                        className="itemComments">{item.itemsComment}</div> </span>
                </div>

                {/*Order #: {item._id}*/}
                <span className="valueTop"><b>#{item.cpNumber} - {item.pickupLocation}</b></span>
                {/*<span className="value"></span>*/}

                <span className="label"><b>{item.ordererName}</b></span>
                <span className="value">Zip: <b>{item.childZip}</b></span>

                <span className="label">Phone:<b> {item.phone}</b></span>
                <span className="value">Internal ID: <b>{item.comments}</b></span>

                <div className="print-date">
                    <span>Pickup Date <b>{moment(item.pickupDate).format('MMM Do')}</b></span>
                </div>
                <ul className="chips no-print">
                    {item.status === 1 ? (
                        <li className="paid">
                            <i className="mdi mdi-dollar" />Submitted
                        </li>
                    ) : (
                        <li className="focusId">
                            <i className="mdi" />{OrderStatus.getIdentifier(OrderStatus.getValues()[item.status])}
                        </li>
                    )}
                    <li className="date">Pickup Date {moment(item.pickupDate).format('MMM Do')}</li>
                </ul>
            </div>
        </Link>
    </li>
);

OrderLabelsItem.propTypes = {
    item: PropTypes.object
};

OrderLabelsItem.defaultProps = {
    item: {}
};
class OrderLabels extends React.Component {

    constructor(props) {
        //console.log("components/Order/List.jsx:constructor");
        super(props);

        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            filter: undefined
        };

    }

    componentDidUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            if (this.props.onChange) {
                this.props.onChange(nextState);
                console.log("components/Order/List.jsx:OrderLabels:componentDidUpdate:true");
            }
        } else {
            //console.log("components/Order/List.jsx:OrderLabels:componentDidlUpdate:false");
        }
    }

    render() {

        console.log("components/Order/Labels.jsx:render:this.props.orders.length:" + this.props.orders.length);

        let orders = this.props.orders;

        // If filter defined
        let filteredOrders = [];
        if (this.state.filter) {
            let filter = this.state.filter;
            orders.forEach((order) => {
                const matchesKeyword = filter.keyword
                    ? ((order, keyword) => {

                        if (order.childName.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        if (order.pickupLocation.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        if (order.comments.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        return false;
                    })(order, filter.keyword)
                    : true;
                const matchesPickupLocation = filter.pickupLocation ? order.pickupLocation === filter.pickupLocation : true;
                const matchesStatus = (filter.status || filter.status === 0) ? order.status === filter.status : true;
                        
                if (matchesStatus && matchesKeyword && matchesPickupLocation) {
                    console.log("components/Order/Labels.jsx:render:setState:filteredOrders:push");
                    filteredOrders.push(order);
                }
            });

            if (!this.state.filteredOrders || (filteredOrders.length !== this.state.filteredOrders.length)) {
                console.log("components/Order/Labels.jsx:render:setState:orders");
                this.setState({
                    filteredOrders: filteredOrders
                });
            }

        } else {
            filteredOrders = orders;
        }

        let items = [];

        filteredOrders.forEach((order) => {

            let cpNumber = 0;
            let name = "";
            if (order.pickupLocation.indexOf("(") > 0) {
                cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
                name = order.pickupLocation.replace(" " + cpNumber, "");
                cpNumber = cpNumber.replace("(", "").replace(")", "");
            }

            let phone = order.userPhone[0];
            let ordererName = order.userName[0];

            order.items.forEach((item) => {

                let item2 = item;
                item2.cpNumber = cpNumber;
                item2.phone = phone;
                item2.ordererName = ordererName;
                item2.pickupLocation = name;

                item2._id = order._id;
                item2.childName = order.childName;
                item2.childAge = order.childAge;
                item2.childWeight = order.childWeight;
                item2.childSex = order.childSex;
                item2.childZip = order.childZip;
                item2.comments = order.comments;
                item2.status = order.status;
                item2.pickupDate = order.pickupDate;

                if (item.itemsCategory === undefined) {
                    item.itemsCategory = null;
                }

                items.push(item2);
            });
        });

        items = items.sort(function (a, b) {
            return (new Date(b.pickupDate).setHours(0, 0, 0, 0) - new Date(a.pickupDate).setHours(0, 0, 0, 0))
                || (a.itemsCategory - b.itemsCategory)
                || (a.itemsType - b.itemsType)
                || (a.itemsSize - b.itemsSize)
                || (a.childName.toUpperCase() < b.childName.toUpperCase() ? -1 : (a.childName.toUpperCase() > b.childName.toUpperCase() ? 1 : 0))
                ;
        });

        let groupedItems = [];
        let groupNum = 0;
        let itemNum = 0;
        let groupedItem = [];

        items.forEach((item3) => {
            if (itemNum === 0 && groupNum !== 0) {
                groupedItems.push(groupedItem);
                groupedItem = [];
            }
            groupedItem.push(item3);

            itemNum++;

            if (itemNum === 6) {
                itemNum = 0;
                groupNum++;
            }

            // Add last few items
            if (groupNum * 6 + itemNum === items.length) {
                groupedItems.push(groupedItem);
            }
            //console.log("components/Order/Labels.jsx:groupNum:" + groupNum);
            //console.log("components/Order/Labels.jsx:render:itemNum:" + itemNum);

        });


        //console.log("components/Order/List.jsx:render:props:2" + JSON.stringify(this.state));
        console.log("components/Order/Labels.jsx:render:items.length:" + items.length);
        console.log("components/Order/Labels.jsx:render:groupedItems.length:" + groupedItems.length);


        return (
            <div id="reportRoot" className="order list body" status={this.props.status}>
                <SubHeader className="no-print">
                    <Breadcrumbs className="no-print" breadcrumbs={[{ text: 'All Orders' }]} />
                    <Drawer className="no-print" visible={!Meteor.user()}>
                        <OrderLabelsFilter
                            className="no-print"
                            history={this.props.history}
                            orders={this.props.orders}
                            businesses={this.props.businesses}
                            startDate={this.props.startDate}
                            endDate={this.props.endDate}
                            onChange={(filter) => {
                                if (filter.runDateQuery) {

                                    filter.startDate.setHours(0);
                                    filter.startDate.setMinutes(0);
                                    filter.startDate.setSeconds(0);
                                    filter.startDate.setMilliseconds(0);

                                    filter.endDate.setHours(23);
                                    filter.endDate.setMinutes(59);
                                    filter.endDate.setSeconds(59);
                                    filter.endDate.setMilliseconds(999);

                                    console.log("components/Order/PrintLabels.jsx:runDateQuery");
                                    filter.runDateQuery = undefined;
                                    this.props.history.push(`/order/printLabels/${filter.startDate.getTime()}/${filter.endDate.getTime()}`);

                                } else {
                                    this.setState({ filter: filter });
                                    console.log("components/Order/PrintLabels.jsx:runDateQuery:setState");
                                }
                            }}
                        />
                    </Drawer>

                </SubHeader>
                <ul className="no-print">
                    {Meteor.user() && Meteor.user().may.create.order() && 1 == 2 ? (
                        <>
                            <li className="new no-print">
                                <Link to="/" onClick={(event) => {
                                    event.preventDefault();

                                    let divToPrint = document.getElementById('reportRoot');
                                    var scale = window.devicePixelRatio;

                                    html2canvas.default(divToPrint, {
                                        scrollX: -window.scrollX,
                                        scrollY: 400
                                    }).then(function (canvas) {
                                        // the size of the PDF is a big deal
                                        var pdf = new jsPDF('l', 'mm', [canvas.width * 0.264583, canvas.width * 0.192424]);

                                        for (var i = 0; i <= divToPrint.clientHeight / (612); i++) {

                                            var srcImg = canvas;
                                            var sX = 0;
                                            var sY = 612 * i * scale; // start x pixels down for every new page
                                            var sWidth = canvas.width;
                                            var sHeight = 612 * scale;
                                            var dX = 0;
                                            var dY = 0;
                                            var dWidth = canvas.width;
                                            var dHeight = 612 * scale;

                                            window.onePageCanvas = document.createElement("canvas");
                                            onePageCanvas.setAttribute('width', canvas.width);
                                            onePageCanvas.setAttribute('height', 612 * scale);
                                            var ctx = onePageCanvas.getContext('2d');
                                            ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

                                            var canvasDataURL = onePageCanvas.toDataURL("image/png"); //1.0
                                            var width = onePageCanvas.width;
                                            var height = onePageCanvas.height;

                                            // If we're on anything other than the first page add another page
                                            if (i > 0) {
                                                pdf.addPage(); //2434,1770); //8.5" x 11" in pts (in*72)
                                            }
                                            //! now we declare that we're working on that page
                                            pdf.setPage(i + 1);
                                            //! now we add content to that page!
                                            pdf.addImage(canvasDataURL, 'PNG', 0, 0, null, null, null, 'NONE');

                                        }
                                        pdf.save('Labels.pdf');
                                    });

                                }}

                                >{CC.icon('file-pdf')} Create PDF</Link>
                                <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <Link to="/" onClick={(event) => {
                                    event.preventDefault();
                                    window.print();
                                }}
                                >{CC.icon('printer')} Print</Link>
                            </li>
                        </>
                    ) : null}
                </ul>
                {groupedItems.map((groupItem, index) => (
                    <ul className="page-break" id={"group" + index} key={"group" + index}>
                        {groupItem[0] ? <OrderLabelsItem index={index * 10 + 0} key={groupItem[0]._id + 0} item={groupItem[0]} /> : null}
                        {groupItem[1] ? <OrderLabelsItem index={index * 10 + 1} key={groupItem[1]._id + 1} item={groupItem[1]} /> : null}
                        {groupItem[2] ? <OrderLabelsItem index={index * 10 + 2} key={groupItem[2]._id + 2} item={groupItem[2]} /> : null}
                        {groupItem[3] ? <OrderLabelsItem index={index * 10 + 3} key={groupItem[3]._id + 3} item={groupItem[3]} /> : null}
                        {groupItem[4] ? <OrderLabelsItem index={index * 10 + 4} key={groupItem[4]._id + 4} item={groupItem[4]} /> : null}
                        {groupItem[5] ? <OrderLabelsItem index={index * 10 + 5} key={groupItem[5]._id + 5} item={groupItem[5]} /> : null}
                    </ul>

                ))}
                {/*className="page-break"*/}
            </div>
        );

    }
}

export default container((props, onData) => {

    let startDate;
    let endDate;
    const user = Meteor.user();
    let now = new Date();

    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
    } else if (user.type != 'Caseworker'
        && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
            || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
    } else {
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        console.log("components/Order/Labels.jsx:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const subscription = Meteor.subscribe('/orderUserOrg/searchByDate', startDate, endDate);
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const caseworkerSubscription = Meteor.subscribe('/user/caseworker/list');

    if (volunteerSubscription.ready() &&
        caseworkerSubscription.ready() &&
        businessSubscription.ready() &&
        subscription.ready()
    ) {

        const user = Meteor.user();

        let orgId = "Unknown";
        let pickupDay = 5;
        let location = null;

        if (user) {
            orgId = user.profile.organizationId;

            if (orgId !== "None") {
                let biz = Business.find()
                    .fetch()
                    .filter(organization => organization._id === orgId);


                console.log("components/Order/Labels.jsx:biz:" + JSON.stringify(biz));
                if (biz.length > 0) {
                    pickupDay = biz[0].pickupDay;
                    location = parseInt(biz[0].location, 0);
                }
            }
        }

        const orders = OrderUserOrg.find({}, { sort: { 'pickupDate': -1 } })
            .fetch();

        let businesses = Business.find({}, { sort: { 'name': 1 } })
            .fetch()
            .filter(organization => organization.location === location);

        // If no location exists, return all businesses
        if (location === null) {
            businesses = Business.find({}, { sort: { 'name': 1 } }).fetch()
        }

        console.log("components/Order/Labels.jsx:location:" + location);
        console.log("components/Order/Labels.jsx:orders.length:" + orders.length);
        console.log("components/Order/Labels.jsx:businesses.length:" + businesses.length);
        console.log("components/Order/Labels.jsx:startDate:" + startDate);
        console.log("components/Order/Labels.jsx:endDate:" + endDate);
        onData(null, { orders, businesses, startDate, endDate, history, props });
    }
}, withRouter(OrderLabels));


