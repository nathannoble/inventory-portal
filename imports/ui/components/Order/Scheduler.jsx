import React from 'react';
import DateTime from 'react-datetime';
import moment from 'moment';
import { Bert } from 'meteor/themeteorchef:bert';
import PropTypes from 'prop-types';

import { OrderStatus } from '/imports/api/Order';

import ConcreteDuration from '/imports/api/helpers/ConcreteDuration';
import CC from '/imports/api';

import { handleError, CCButton, Select } from '/imports/ui/helpers';

class ConcreteDurationScheduler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presetRange: !!props.dateRange,
    };
  }
  render() {
    const {
      concreteDuration, isValidDate, onChange, disabled, dateRange, timeRange,
    } = this.props;
    const { presetRange } = this.state;
    return (
      <fieldset className="date">
        {presetRange === false ? null : (
          <Select
            placeholder="Select range of dates"
            value={presetRange}
            options={[
              { value: 1, label: 'One Month' },
              { value: 3, label: 'Three Months' },
              { value: 12, label: 'One Year' },
              { value: 9999, label: 'All Time' },
              { value: false, label: 'Custom Range' },
            ]}
            onChange={({ value }) => {
              this.setState({ presetRange: value });
              if (value) {
                concreteDuration.start = moment()
                  .subtract(value, 'months')
                  .toDate();
                concreteDuration.end = moment().toDate();
                onChange(concreteDuration);
              }
            }}
          />
        )}
        <div style={{ display: presetRange ? 'none' : null }}>
          <DateTime
            input={!!dateRange}
            timeFormat={false}
            isValidDate={isValidDate}
            value={concreteDuration.start}
            onChange={(date) => {
              concreteDuration.setDay(date, dateRange ? 'start' : 'both');
              onChange(concreteDuration);
            }}
          />
          {dateRange ? (
            <DateTime
              input
              timeFormat={false}
              isValidDate={current =>
                current.isAfter(concreteDuration.start) && isValidDate(current)}
              value={concreteDuration.end}
              onChange={(date) => {
                concreteDuration.setDay(date, 'end');
                onChange(concreteDuration);
              }}
            />
          ) : null}
          {timeRange ? (
            <DateTime
              dateFormat={false}
              inputProps={{
                disabled: disabled ? undefined : 'disabled',
              }}
              value={concreteDuration.start}
              onChange={(time) => {
                concreteDuration.start = moment(concreteDuration.start)
                  .hour(time.hour())
                  .minute(time.minute())
                  .toDate();
                onChange(concreteDuration);
              }}
            />
          ) : null}
          {timeRange ? (
            <DateTime
              dateFormat={false}
              inputProps={{
                disabled: disabled ? undefined : 'disabled',
              }}
              value={concreteDuration.end}
              onChange={(time) => {
                concreteDuration.end = moment(concreteDuration.end)
                  .hour(time.hour())
                  .minute(time.minute())
                  .toDate();
                onChange(concreteDuration);
              }}
            />
          ) : null}
        </div>
      </fieldset>
    );
  }
}


ConcreteDurationScheduler.propTypes = {
  concreteDuration: PropTypes.instanceOf(ConcreteDuration),
  isValidDate: PropTypes.func, // A function that, receiving a date, determines if it can be set
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  dateRange: PropTypes.bool,
  timeRange: PropTypes.bool,
};

ConcreteDurationScheduler.defaultProps = {
  concreteDuration: new ConcreteDuration(),
  isValidDate: current => true,
  onChange: concreteDuration => null,
  disabled: false,
  dateRange: false,
  timeRange: true,
};

const OrderScheduler = ({ order }) => (
  <div className="order scheduler">
    <h2>{order.status === OrderStatus.pending ? 'Schedule Date' : 'Scheduled Date'}</h2>
    <span className="empty-warning">{order.orderedDate ? null : 'Please suggest a date'}</span>
    <ConcreteDurationScheduler
      concreteDuration={order.orderedDate || new ConcreteDuration()}
      isValidDate={(current) => {
        if (order.status === OrderStatus.pending) return current.isAfter(moment());
        return false;
      }}
      onChange={(concreteDuration) => {
        order.orderedDate = concreteDuration;
        if (Meteor.user().type === 'Educator') {
          order.acceptedByOrderer = false;
        } else {
          order.acceptedByOrdered = false;
        }
        Meteor.call('/order/update', order, handleError);
      }}
      disabled={order.status === OrderStatus.pending}
    />
    {order.status === OrderStatus.pending ? (
      <div className={`${Meteor.user().type.toLowerCase()} actions`}>
        {Meteor.user().type !== 'Educator' ?
        <CCButton
          type="remove"
          onClick={(event) => {
            event.preventDefault();
            order.status = OrderStatus.cancelled;
            Meteor.call('/order/update', order, handleError);
          }}
        >Decline Order</CCButton>:null}
        {order.orderedDate ? <OrderScheduleButton order={order} /> : null}
      </div>
    ) : null}
  </div>
);
//{Meteor.user().type === 'Educator' ? 'Retract' : 'Decline'} Order  - removed Retract Order from Educator dialog 2/12/19

class OrderScheduleButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirming: false,
    };
    this.confirm = this.confirm.bind(this);
  }

  accept(order) {
    order[
      Meteor.user().type === 'Educator' ? 'acceptedByOrderer' : 'acceptedByOrdered'
    ] = true;
    return order;
  }

  confirm() {
    this.setState({ confirming: true });
    Meteor.setTimeout(() => {
      this.setState({ confirming: false });
    }, 2000);
  }

  render() {
    const { order } = this.props;
    const otherPartyHasAccepted =
      (Meteor.user().type === 'Educator' && order.acceptedByOrdered) ||
      (Meteor.user().type === 'Volunteer' && order.acceptedByOrderer);
    const iHaveAccepted =
      (Meteor.user().type === 'Educator' && order.acceptedByOrderer) ||
      (Meteor.user().type === 'Volunteer' && order.acceptedByOrdered);
    return (
      <CCButton
        className="schedule"
        onClick={(event) => {
          event.preventDefault();
          if (otherPartyHasAccepted) {
            if (this.state.confirming) {
              this.accept(order);
              order.status = OrderStatus.active;
              Meteor.call(
                '/order/update',
                order,
                handleError({
                  callback() {
                    let message = 'Your date has been scheduled!';
                    Bert.alert(message, 'success');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                       let el = document.querySelectorAll('.bert-content')[0];
                       if (el) {
                           el.innerHTML = `<p>${message}</p>`;
                       }
                    }
                  },
                }),
              );
            } else {
              this.confirm();
            }
          } else if (this.state.confirming) {
            this.accept(order);
            console.log(order);
            Meteor.call('/order/update', order, handleError);
          } else {
            this.confirm();
          }
        }}
        disabled={iHaveAccepted}
      >
        {this.state.confirming
          ? 'Are you sure?'
          : otherPartyHasAccepted
            ? 'Confirm Date'
            : iHaveAccepted ? 'Order Sent' : 'Order Date'}
      </CCButton>
    );
  }
}

export { OrderScheduler as default, ConcreteDurationScheduler };

// export default OrderScheduler;
