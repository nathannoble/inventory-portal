import React from 'react';
// import { Link } from 'react-router-dom';
import DateTime from 'react-datetime';
import moment from 'moment';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';
// import {helperDateTime} from '/imports/api/helpers/helperDate';
//import { Bert } from 'meteor/themeteorchef:bert';

import Order, {
    OrderStatus, OrderFilter, OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType, OrderHygieneType, OrderBeddingType,
    OrderEquipmentType, OrderFeedingType, OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize,
    OrderBoyPantSize,
    //OrderBoyPantsWidth, OrderBoyPantsHeight,
    OrderShoeSize, ChildAge, ChildSex, ChildStatus
} from '/imports/api/Order';

import User from '/imports/api/User';

import Business, { CareProviderLocation, CareProviderPickupDay } from '/imports/api/Organization/Business';

import {
    handleError,
    Select,
    SubHeader,
    Breadcrumbs,
    Label,
    CCButton
} from '/imports/ui/helpers';

import checkDate from '../../helpers/checkDate';

class OrderNew extends React.Component {
    constructor(props) {
        super(props);

        var organization = Business.findOne({
            _id: (
                User.findOne(Meteor.userId()) ||
                new User({ name: "Unknown", profile: { organizationId: "Unknown" } })
            ).profile.organizationId
        });

        if (!organization) {
            organization = new Business({
                name: "Unknown",
                pickupDay: 4,
                cpNumber: 0,
                location: 0
            });
        }

        this.dayOfWeekForPickupLocation = parseInt(organization.pickupDay, 0);
        let daysUntilPickupLocationDay = this.dayOfWeekForPickupLocation - (new Date()).getDay();
        if (daysUntilPickupLocationDay <= 7) {
            daysUntilPickupLocationDay += 7;
        }

        let pickupDate = new Date();
        pickupDate.setDate(pickupDate.getDate() + daysUntilPickupLocationDay);

        const now = new Date();

        // 1/3/25 NN

        // Start Ordering: 1/6/25
        // End Ordering: 3/14/25

        // Cutoff	Pickup
        // 1/10	    1/15-17
        // 1/17	    1/22-24
        // 1/24	    1/29-31
        // 1/31	    2/5-7
        // 2/7	    2/12-14
        // 2/14	    2/19-21
        // 2/21	    2/26-28
        // 2/28	    3/5-7
        // 3/7	    3/12-14
        // 3/14	    3/19-21

        // After min date (1/6/25), auto increment date
        if (now > new Date(2025, 0, 6, 8, 0, 0, 0)) {
        
            if (now < new Date(2025, 0, 10, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 0, 15, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 0, 17, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 0, 22, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 0, 24, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 0, 29, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 0, 31, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 1, 5, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 1, 7, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 1, 12, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 1, 14, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 1, 19, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 1, 21, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 1, 26, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 1, 28, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 2, 5, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 2, 7, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 2, 12, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } else if (now < new Date(2025, 2, 14, 17, 0, 0, 0) ) {
                while (pickupDate < new Date(2025, 2, 19, 0, 0, 0, 0)) {
                    pickupDate.setDate(pickupDate.getDate() + 7);
                }
            } 
        }

        this.state = {
            status: 0,
            ordererId: Meteor.userId(),
            childName: undefined,
            childZip: undefined,
            childAge: undefined,
            childWeight: undefined,
            childSex: undefined,
            newChild: undefined,
            pickupLocation: organization.name + " (" + organization.cpNumber + ")",
            location: organization.location,
            pickupDate: pickupDate,
            comments: "",
            items: [],
        };

        console.log('Order/New.jsx:constructor:this.state', JSON.stringify(this.state));
        //comments: (organization.name + " - " + helperDateTime((new Date()))),
        this.createOrder = this.createOrder.bind(this);
    }

    createOrder(event) {
        console.log('Order/New.jsx:createOrder:this', this);

        // var thisUser = User.findOne(Meteor.userId());
        // if (thisUser && thisUser.inactive == true){
        //     let message = 'The current user is inactive and cannot create a new order.';
        //     Bert.alert(message, 'danger');
        //     if (document.querySelectorAll('.bert-content').length > 0) {
        //         let el = document.querySelectorAll('.bert-content')[0];
        //         if (el) {
        //             el.innerHTML = `<p>${message}</p>`;
        //         }
        //     }
        //     return;
        // } 

        event.preventDefault();
        const history = this.props.history;

        const order = new Order({
            ordererId: this.state.ordererId,
            status: 1,
            childName: this.childNameInput.ref.value,
            childZip: parseInt(this.childZipInput.ref.value, 0),
            childAge: this.state.childAge,
            childWeight: parseInt(this.childWeightInput.ref.value, 0),
            childSex: this.state.childSex,
            newChild: this.state.newChild,
            pickupLocation: this.state.pickupLocation,
            pickupDate: this.state.pickupDate,
            comments: this.commentsInput.ref.value,
            items: this.state.items,

        });
        console.log('Order/New.jsx:createOrder:order:', JSON.stringify(order));

        // 1/3/25 NN
        // Distribution Days:  
        const now = new Date();
        let d = this.state.pickupDate;

        if (now > new Date(2025, 2, 14, 17, 0, 0, 0) && Meteor.user().type === 'Caseworker') {
            let message = 'The cutoff date for orders is 3/14/25 at 5:00 PM';
            Bert.alert(message, 'danger');
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }
            return;
        }

        if (now < new Date(2025, 0, 6, 8, 0, 0, 0) && Meteor.user().type === 'Caseworker') {
            let message = 'The start date for orders is 1/6/25 at 8:00 AM';
            Bert.alert(message, 'danger');
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }
            return;
        }

        if (checkDate(d, this.dayOfWeekForPickupLocation) !== true) {
           return;
        }

        if (this.state.items.length === 0) {

            let message = 'You must add at least 1 item to the order to submit it.';
            Bert.alert(message, 'danger');
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }
            return;
        } else {
            Meteor.call(
                '/order/new',
                order,
                handleError({
                    callback(orderId) {
                        let message = 'New order created.';
                        Bert.alert(message, 'success');
                        if (document.querySelectorAll('.bert-content').length > 0) {
                            let el = document.querySelectorAll('.bert-content')[0];
                            if (el) {
                                el.innerHTML = `<p>${message}</p>`;
                            }
                        }
                        history.push(`/order/${orderId}`);
                    },
                })
            );
        }
    }
    render() {
        return (
            <div className="order new body">
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ link: '/order', text: 'Orders' }, { text: 'New' }]}
                    />
                </SubHeader>

                <h2>{(Meteor.user().inactive == true ? 'Current user is inactive' : "New Order")}</h2>

                <form onSubmit={this.createOrder}>
                    <fieldset htmlFor="basic" disabled={!(Meteor.user().type === 'Administrator')}>
                        <h2>Basic Info</h2>
                        <div className="items">
                            <Label
                                name="ordererId"
                                text="Case Worker"
                                type="text"
                                disabled={true}
                                value={
                                    (User.findOne(this.state.ordererId) || new User({
                                        name: "Unknown",
                                        type: "User"
                                    })).name
                                }
                            />
                            {Meteor.user().type !== 'Administrator' ?
                                <Label
                                    type="text"
                                    disabled={true}
                                    value={this.state.pickupLocation}
                                    name="pickupLocation"
                                    text="Care Provider"
                                /> :
                                <Label
                                    name="pickupLocation"
                                    required
                                    text="Care Provider"
                                    placeholder="Care Provider"
                                    value={this.state.pickupLocation}
                                    element={Select}
                                    options={
                                        Business.find({}, { sort: { 'name': 1 } })
                                            .fetch()
                                            .filter(organization => organization.type === 'Business')
                                            .map(organization =>
                                                Object({ value: (organization.name + " (" + organization.cpNumber + ")").toString(), label: (organization.name + " (" + organization.cpNumber + ")").toString() }))
                                    }
                                    onChange={(selectedOption) => {
                                        console.log('Order/New.jsx:pickupLocation:selectedOption:1:', JSON.stringify(this.state));
                                        this.setState({ pickupLocation: selectedOption.label.toString() });
                                        console.log('Order/New.jsx:pickupLocation:selectedOption:2:', JSON.stringify(this.state));
                                    }}
                                />
                            }

                            <Label
                                type="text"
                                disabled={true}
                                value={CareProviderLocation.getIdentifier(this.state.location)}
                                name="location"
                                text="Location"
                            />

                            {Meteor.user().type === 'Caseworker' ?
                                <Label
                                    type="text"
                                    disabled={true}
                                    value={OrderStatus.getIdentifier(this.state.status)}
                                    name="status"
                                    text="Status"
                                /> :

                                <Label
                                    required
                                    name="status"
                                    text="Status"
                                    value={this.state.status}
                                    element={Select}
                                    options={OrderStatus.getIdentifiers().map((status, index) => Object({ value: index, label: status }))}
                                    onChange={({ value }) => {
                                        console.log('Order/New.jsx:status:onChange:value:' + value);
                                        this.setState({ status: value });
                                    }}
                                />}


                        </div>
                    </fieldset>
                    <fieldset htmlFor="child">
                        <h2>Child Info</h2>
                        <div className="items">

                            <Label
                                name="childName"
                                required
                                text="Child Name"
                                placeholder="Child Name"
                                ref={input => (this.childNameInput = input)}
                                type="text"
                            />


                            <Label
                                name="comments"
                                text="Internal ID"
                                placeholder="Internal ID"
                                ref={input => (this.commentsInput = input)}
                                type="text"
                            />

                            <Label
                                name="pickupDate"
                                text="Pickup Date"
                                element={DateTime}
                                /*isValidDate={(date) => {
                                        if (Meteor.user().type === 'Caseworker' ) {
                                        const now = new Date();
                                        let weeksFromNow = new Date();
                                        weeksFromNow.setDate(weeksFromNow.getDate()+30);
                                        return (date.isAfter( now ) && date.isSameOrBefore(weeksFromNow));
                                        } else { return true;}
                                }}*/
                                onChange={(date) => {
                                    if (typeof date === "object") {

                                        if (checkDate(date, this.dayOfWeekForPickupLocation) === true) {
                                            var d = date.toDate();
                                            d.setHours(17);
                                            d.setMinutes(0);
                                            d.setSeconds(0);
                                            d.setMilliseconds(0);
                                            this.setState({ pickupDate: d });
                                            //order.pickupDate = date.toDate();
                                            //Meteor.call('/order/update', order, handleError);
                                        }
                                    }
                                }}
                                timeFormat={false}
                                value={this.state.pickupDate}
                                required
                            />

                            <Label
                                name="newChild"
                                text="Child Status"
                                value={this.state.newChild}
                                element={Select}
                                required
                                options={ChildStatus.getIdentifiers().map((newChild, index) => Object({ value: index, label: newChild }))}
                                onChange={({ value }) => {
                                    console.log('Order/New.jsx:newChild:onChange:value:' + value);
                                    event.preventDefault();
                                    this.setState({ newChild: value });
                                }}
                            />

                            <Label
                                name="childZip"
                                text="Child Zip Code"
                                placeholder="Child Zip Code"
                                ref={input => (this.childZipInput = input)}
                                type="text"
                                size="5"
                                required
                            />

                            <Label
                                name="childAge"
                                required
                                text="Child Age"
                                element={Select}
                                placeholder="Child Age"
                                value={this.state.childAge}
                                options={ChildAge.getIdentifiers().map((childAge, index) => Object({ value: index, label: ChildAge.getIdentifier(index) }))}
                                onChange={(selectedOption) => {
                                    this.setState({ childAge: selectedOption.value });
                                }}
                            />

                            <Label
                                name="childWeight"
                                required
                                text="Child Weight"
                                placeholder="Child Weight"
                                ref={input => (this.childWeightInput = input)}
                                type="number"
                            />

                            <Label
                                name="childSex"
                                required
                                text="Sex"
                                element={Select}
                                value={this.state.childSex}
                                options={ChildSex.getIdentifiers().map((childSex, index) =>
                                    Object({ value: index, label: childSex }))}
                                onChange={(selectedOption) => {
                                    this.setState({ childSex: Number(selectedOption.value) });
                                }}
                            />

                        </div>
                    </fieldset>
                    <fieldset htmlFor="items">
                        <legend>Items</legend>

                        {this.state.items.map((item, index) => (
                            <>
                                <h2>{"Item " + (index + 1) + " " + item.itemsCategory !== undefined ? OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) : null}</h2>
                                <div key={index} className="items">
                                    <Label
                                        name={"itemsCategory" + index}
                                        required
                                        text="Category"
                                        element={Select}
                                        value={item.itemsCategory}
                                        options={OrderItemCategory.getIdentifiers().map((itemsCategory, index) =>
                                            Object({ value: index, label: itemsCategory }))}
                                        onChange={(selectedOption) => {

                                            const newValue = Number(selectedOption.value);
                                            const itemsCategories = this.state.items.map(items => items.itemsCategory);
                                            const clothingBagItems = this.state.items
                                                .filter(items => items.itemsCategory === 0);

                                            console.log('Order/New.jsx:clothing bag item array:' + JSON.stringify(clothingBagItems));
                                            console.log('Order/New.jsx:newValue:' + newValue);
                                            console.log('Order/New.jsx:itemsCategories:' + JSON.stringify(itemsCategories));

                                            let saveItem = true;

                                            // Temporarily restrict all but cb/oc/diapers/wipes
                                            //if (newValue !== 0 && newValue !==1 && newValue !==2 && newValue !== 3 && Meteor.user().type === 'Caseworker') {
                                            //      let message = "Only clothing bags, other clothing, diapers and wipes are permitted (temporarily) because of the Coronavirus";
                                            //      Bert.alert(message, 'danger');
                                            //      if (document.querySelectorAll('.bert-content').length > 0) {
                                            //         let el = document.querySelectorAll('.bert-content')[0];
                                            //         if (el) {
                                            //             el.innerHTML = `<p>${message}</p>`;
                                            //         }
                                            //      }
                                            //
                                            //      saveItem = false;
                                            // } else
                                            // if clothing bag exists already

                                            if (itemsCategories.toString().indexOf("0") >= 0) {

                                                const clothingBagItem = clothingBagItems[0];
                                                // Don't allow more than 1 clothing bag
                                                if (newValue === 0) {
                                                    let message = "A Clothing Bag already exists in this item collection.   No additional 'Clothing Bag' items are permitted.";
                                                    Bert.alert(message, 'danger');
                                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                                        let el = document.querySelectorAll('.bert-content')[0];
                                                        if (el) {
                                                            el.innerHTML = `<p>${message}</p>`;
                                                        }
                                                    }
                                                    saveItem = false;
                                                } else if (newValue === 1) {  // Other Clothing
                                                    let message = "A Clothing Bag already exists in this item collection.  No 'Other Clothing' items are permitted.";
                                                    Bert.alert(message, 'danger');
                                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                                        let el = document.querySelectorAll('.bert-content')[0];
                                                        if (el) {
                                                            el.innerHTML = `<p>${message}</p>`;
                                                        }
                                                    }
                                                    saveItem = false;
                                                } else if (newValue === 2) {  // Diapers
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  No 'Diaper' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 3) {  // Wipes
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  No 'Wipes' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 5) {  // Hygiene Items
                                                    if (clothingBagItem.itemsType === 2 ||
                                                        clothingBagItem.itemsType === 3) {  // Teen Boy and Girl
                                                        let message = "A Teen Boy or Girl Clothing Bag already exists in this item collection.  No 'Hygiene' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 8) {  // Toys
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags

                                                    }
                                                }
                                            }
                                            if (saveItem) {
                                                console.log('Order/New.jsx:saving item');
                                                this.state.items[index].itemsCategory = newValue;
                                                this.setState({ items: this.state.items });
                                            }
                                        }}
                                    />

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Clothing Bag' ? (
                                        <>
                                            <Label
                                                name={"itemsType" + index}
                                                required
                                                text="Bag Type"
                                                element={Select}
                                                value={item.itemsType}
                                                options={OrderBagType.getIdentifiers().map((itemsType, index) => Object({ value: index, label: itemsType }))}
                                                onChange={(selectedOption) => {
                                                    this.state.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }}
                                            />
                                            <Label
                                                name={"itemsSize" + index}
                                                required
                                                text="Shirt Size"
                                                element={Select}
                                                value={item.itemsSize}
                                                options={OrderClothingSize.getIdentifiers().map((itemsSize, index) =>
                                                    Object({ value: index, label: itemsSize }))}
                                                onChange={(selectedOption) => {
                                                    this.state.items[index].itemsSize = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }}
                                            />

                                            {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Boy' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Girl' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Neutral' ? (

                                                <Label
                                                    name={"pantSize" + index}
                                                    required
                                                    text="Pant Size"
                                                    element={Select}
                                                    value={item.pantSize}
                                                    options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                        Object({ value: index, label: pantSize }))}
                                                    onChange={(selectedOption) => {
                                                        this.state.items[index].pantSize = Number(selectedOption.value);
                                                        this.setState({ items: this.state.items });
                                                    }}
                                                />

                                            ) : null}

                                            {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Boy' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Boy' ? (

                                                <>
                                                    <Label
                                                        name={"shoeSize" + index}
                                                        required
                                                        text="Shoe Size"
                                                        element={Select}
                                                        value={item.shoeSize}
                                                        options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                            Object({ value: index, label: shoeSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].shoeSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                    <Label
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                </>
                                            ) : null}

                                            {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Girl' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Girl' ? (

                                                <>
                                                    <Label
                                                        name={"shoeSize" + index}
                                                        required
                                                        text="Shoe Size"
                                                        element={Select}
                                                        value={item.shoeSize}
                                                        options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                            Object({ value: index, label: shoeSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].shoeSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                    <Label
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        options={OrderGirlPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                </>
                                            ) : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Other Clothing' ? (
                                        <>
                                            <Label
                                                name={"itemsType" + index}
                                                required
                                                text="Individual Items"
                                                element={Select}
                                                value={item.itemsType}
                                                options={OrderClothingType.getIdentifiers().map((itemsType, index) =>
                                                    Object({ value: index, label: itemsType }))
                                                    .sort((a, b) => (b.value === 1 ? -1 : (a.label.toUpperCase() > b.label.toUpperCase() ? 1 : 0)))}
                                                onChange={(selectedOption) => {
                                                    this.state.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }}
                                            />
                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Gloves & Hat' ? null : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Jacket' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Shirts (2)' ? (
                                                <Label
                                                    name={"itemsSize" + index}
                                                    required
                                                    text="Shirt Size"
                                                    element={Select}
                                                    value={item.itemsSize}
                                                    options={OrderClothingSize.getIdentifiers().map((itemSize, index) =>
                                                        Object({ value: index, label: itemSize }))}
                                                    onChange={(selectedOption) => {
                                                        this.state.items[index].itemsSize = Number(selectedOption.value);
                                                        this.setState({ items: this.state.items });
                                                    }}
                                                />
                                            ) : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Maternity' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Pajamas - Girls' ? (
                                                <>
                                                    <Label
                                                        name={"itemsSize" + index}
                                                        required
                                                        text="Shirt Size"
                                                        element={Select}
                                                        value={item.itemsSize}
                                                        options={OrderClothingSize.getIdentifiers().map((itemSize, index) =>
                                                            Object({ value: index, label: itemSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].itemsSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                    <Label
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        options={OrderGirlPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                </>
                                            ) : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Pajamas - Boys' ? (
                                                <>
                                                    <Label
                                                        name={"itemsSize" + index}
                                                        required
                                                        text="Shirt Size"
                                                        element={Select}
                                                        value={item.itemsSize}
                                                        options={OrderClothingSize.getIdentifiers().map((itemSize, index) =>
                                                            Object({ value: index, label: itemSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].itemsSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                    <Label
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}
                                                        onChange={(selectedOption) => {
                                                            this.state.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: this.state.items });
                                                        }}
                                                    />
                                                </>
                                            ) : null}


                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Pants (2 Pair)' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Underwear (2 Pair)' ? (
                                                <Label
                                                    name={"pantSize" + index}
                                                    required
                                                    text="Pant Size"
                                                    element={Select}
                                                    value={item.pantSize}
                                                    options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                        Object({ value: index, label: pantSize }))}
                                                    onChange={(selectedOption) => {
                                                        this.state.items[index].pantSize = Number(selectedOption.value);
                                                        this.setState({ items: this.state.items });
                                                    }}
                                                />
                                            ) : null}


                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Shoes' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]) === 'Socks (2 Pair)' ? (
                                                <Label
                                                    name={"shoeSize" + index}
                                                    required
                                                    text="Shoe Size"
                                                    element={Select}
                                                    value={item.shoeSize}
                                                    options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                        Object({ value: index, label: shoeSize }))}
                                                    onChange={(selectedOption) => {
                                                        this.state.items[index].shoeSize = Number(selectedOption.value);
                                                        this.setState({ items: this.state.items });
                                                    }}
                                                />
                                            ) : null}

                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Diapers' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Diaper Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderDiaperType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType })).sort((a, b) => compare(a.label, b.label))}
                                            onChange={(selectedOption) => {
                                                this.state.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: this.state.items });
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Hygiene Items' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Hygiene Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderHygieneType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType }))}
                                            onChange={(selectedOption) => {

                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = this.state.items.map(items => items.itemsCategory);
                                                const clothingBagItems = this.state.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/New.jsx:new type:' + newValue);
                                                let saveItem = true;

                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    const clothingBagItem = clothingBagItems[0];
                                                    // Don't allow more than 1 clothing bag

                                                    if ((clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) && [3, 12].includes(newValue)) {  // Layette bags and hygiene selected types incompatible
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  The selected 'Hygiene' type is not permitted";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }

                                                }
                                                if (saveItem) {
                                                    console.log('Order/New.jsx:saving item');
                                                    this.state.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Bedding' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Bedding Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderBeddingType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType }))}
                                            onChange={(selectedOption) => {
                                                this.state.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: this.state.items });
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Equipment' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Equipment Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderEquipmentType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType })).sort((a, b) => compare(a.label, b.label))}
                                            onChange={(selectedOption) => {
                                                this.state.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: this.state.items });
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Feeding' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Feeding Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderFeedingType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType }))}
                                            onChange={(selectedOption) => {

                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = this.state.items.map(items => items.itemsCategory);
                                                const clothingBagItems = this.state.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/New.jsx:new type:' + newValue);
                                                let saveItem = true;

                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    const clothingBagItem = clothingBagItems[0];
                                                    // Don't allow more than 1 clothing bag

                                                    if ((clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) && [0].includes(newValue)) {  // Layette bags and feeding - bibs incompatible
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  The selected 'Feeding' type is not permitted";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }

                                                }
                                                if (saveItem) {
                                                    console.log('Order/New.jsx:saving item');
                                                    this.state.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Toys' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Toy Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderToyType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType }))}
                                            onChange={(selectedOption) => {
                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = this.state.items.map(items => items.itemsCategory);
                                                const clothingBagItems = this.state.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/New.jsx:new type:' + newValue);
                                                let saveItem = true;
                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    // Don't allow toy types: 4,5,6,7,8,10 if clothing bag exists
                                                    if ([4, 5, 6, 7, 8, 10].includes(newValue)) {
                                                        let message = "A Clothing Bag already exists in this item collection.   The selected 'Toy' type is not permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                }
                                                if (saveItem) {
                                                    console.log('Order/New.jsx:saving toy item');
                                                    this.state.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: this.state.items });
                                                }
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory) === 'Stroller' ? (
                                        <Label
                                            name={"itemsType" + index}
                                            required
                                            text="Stroller Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderStrollerType.getIdentifiers().map((itemType, index) => Object({ value: index, label: itemType }))}
                                            onChange={(selectedOption) => {
                                                this.state.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: this.state.items });
                                            }}
                                        />
                                    ) : null}

                                    <Label
                                        name={"comments" + index}
                                        text="Comments"
                                        value={item.itemsComment}
                                        type="text"
                                        onChange={(event) => {
                                            this.state.items[index].itemsComment = event.target.value;
                                            this.setState({ items: this.state.items });
                                        }}
                                    />

                                    <button
                                        onClick={(event) => {
                                            console.log('Order/New.jsx:help');
                                            event.preventDefault();
                                            let help = "No details for this item.";
                                            let itemsCategory = OrderItemCategory.getIdentifier(this.state.items[index].itemsCategory);
                                            let itemsType = "";

                                            if (itemsCategory === "Clothing Bag") {
                                                itemsType = OrderBagType.getIdentifier(this.state.items[index].itemsType);

                                                if (itemsType.indexOf('Layette') >= 0) {
                                                    help = "<ul>" +
                                                        "<li>Burp Cloths</li>" +
                                                        "<li>Stuffed Animal - age-appropriate</li>" +
                                                        "<li>Toy - child up to 6 months</li>" +
                                                        "<li>Package of size 1 diapers (20 count)</li>" +
                                                        "<li>Diaper wipe package</li>" +
                                                        "<li>Receiving Blankets</li>" +
                                                        "<li>Warm Blanket or quilt</li>" +
                                                        "<li>Small bibs</li>" +
                                                        "<li>Newborn or Small Snap T-shirt</li>" +
                                                        "<li>Medium or Large Snap T-shirt</li>" +
                                                        "<li>Newborn or Small Stretch sleeper</li>" +
                                                        "<li>0-9 month Blanket Sleeper or PJ's (September - January Only)</li>" +
                                                        "<li>Outfits (0-3 month, 3-6 months, 6-9 months)</li>" +
                                                        "<li>Hat</li>" +
                                                        "<li>Socks, booties, shoes, etc.</li>" +
                                                        "<li>Wash cloths</li>" +
                                                        "<li>Towel</li>" +
                                                        "<li>Shampoo bottle / Head-Toe Soap</li>" +
                                                        "<li>Living Water Gospel of John (English or Spanish)</li>" +
                                                        "</ul>"
                                                } else if (itemsType === "Girl") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 onesies (6 to 24 months)</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 dress or skirt with top</li>" +
                                                        "<li>1 pair of pajamas or nightgown</li>" +
                                                        "<li>2 pair of underwear (for ages 3 and older)</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hair accessories</li>" +
                                                        "<li>1 game or puzzle</li>" +
                                                        "<li>2 - 4 books</li>" +
                                                        "<li>2 stuffed animals</li>" +
                                                        "<li>1 - 2 toys</li>" +
                                                        "<li>1 - 2 DVDs, CDs, coloring books, stickers, crafts</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Boy") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 onesies (6 to 24 months)</li>" +
                                                        "<li>2 bottoms 1 pair of pajamas</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>2 pair of underwear (for ages 3 and older)</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 game or puzzle</li>" +

                                                        "<li>2 - 4 books</li>" +
                                                        "<li>2 stuffed animals</li>" +
                                                        "<li>1 - 2 toys</li>" +
                                                        "<li>1 - 2 DVDs, CDs, coloring books, stickers, crafts</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Teen Girl") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 dress or skirt with top</li>" +
                                                        "<li>1 pair of pajamas or nightgown</li>" +
                                                        "<li>2 pair of underwear</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hair accessories</li>" +
                                                        "<li>1 hygiene kit</li>" +
                                                        "<li>2 camisoles</li>" +
                                                        "<li>1 fashion scarf</li>" +
                                                        "<li>1 - 2 books, games activities, or gadgets teen appropriate</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Teen Boy") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 pair of pajamas</li>" +
                                                        "<li>2 pair of underwear</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hygiene kit</li>" +
                                                        "<li>1 - 2 books, games activities, or gadgets teen appropriate</li>" +
                                                        "</ul>";
                                                }
                                            }
                                            else if (itemsCategory === "Hygiene Items") {
                                                itemsType = OrderHygieneType.getIdentifier(this.state.items[index].itemsType);

                                                if (itemsType.indexOf('Hygiene Kit - Child') >= 0) {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "</ul>"
                                                } else if (itemsType === "Hygiene Kit - Teen Boy") {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "<li>deodorant</li>" +

                                                        "<li>Shaving razors</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Hygiene Kit - Teen Girl") {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "<li>deodorant</li>" +
                                                        "<li>razors</li>" +
                                                        "<li>feminine hygiene items</li>" +
                                                        "</ul>";
                                                } else if (itemsType.indexOf('Shampoo') >= 0) {
                                                    help = "Hair shampoo";
                                                } else if (itemsType.indexOf('Baby Shampoo') >= 0) {
                                                    help = "Special hair shampoo designed for babies";
                                                } else if (itemsType.indexOf('Brush/Comb') >= 0) {
                                                    help = "Hair brush or comb";
                                                } else if (itemsType.indexOf('Deodorant') >= 0) {
                                                    help = "Underarm deoderant to assist with body odor and perspiration";
                                                } else if (itemsType.indexOf('Soap') >= 0) {
                                                    help = "Body soap for bathing";
                                                } else if (itemsType.indexOf('Breast Pads') >= 0) {
                                                    help = "Pads for nursing mothers";
                                                } else if (itemsType.indexOf('Female Pads') >= 0) {
                                                    help = "Femine hygiene pads";
                                                } else if (itemsType.indexOf('Tampons') >= 0) {
                                                    help = "Femine hygiene product";
                                                } else if (itemsType.indexOf('Toothbrush') >= 0) {
                                                    help = "For brushing of teeth, matched by age";
                                                } else if (itemsType.indexOf('Toothpaste') >= 0) {
                                                    help = "For brushing of teeth";
                                                } else if (itemsType.indexOf('Towel') >= 0) {
                                                    help = "Larger towels for bathing";
                                                } else if (itemsType.indexOf('Wash Cloths') >= 0) {
                                                    help = "Hand towels";
                                                }

                                            }
                                            else if (itemsCategory === "Other Clothing") {
                                                itemsType = OrderClothingType.getIdentifier(OrderClothingType.getValues()[this.state.items[index].itemsType]);
                                                if (itemsType.indexOf('Gloves & Hat') >= 0) {
                                                    help = "Gloves and hat provided based on age and weight";
                                                } else if (itemsType.indexOf('Maternity') >= 0) {
                                                    help = "Maternity clothing for pregnant mothers. Top and bottoms provided";
                                                } else if (itemsType.indexOf('Pajamas - Boys') >= 0) {
                                                    help = "Sleepwear for boys based on age/sizing";
                                                } else if (itemsType.indexOf('Pajamas - Girls') >= 0) {
                                                    help = "Sleepwear for girls based on age/sizing";
                                                } else if (itemsType.indexOf('Pants (2 Pair)') >= 0) {
                                                    help = "2 pairs of pants";
                                                } else if (itemsType.indexOf('Shirts (2)') >= 0) {
                                                    help = "2 shirts";
                                                } else if (itemsType.indexOf('Shoes') >= 0) {
                                                    help = "2 shoes";
                                                } else if (itemsType.indexOf('Socks (2 Pair)') >= 0) {
                                                    help = "2 pairs of socks";
                                                } else if (itemsType.indexOf('Underwear (2 Pair)') >= 0) {
                                                    help = "2 pairs of underwear";
                                                } else if (itemsType.indexOf('Jacket') >= 0) {
                                                    help = "A coat or jacket based on age/gender/size";
                                                }
                                            }
                                            else if (itemsCategory === "Diapers") {
                                                help = "Package of 20 diapers based on sizing";
                                            }
                                            else if (itemsCategory === "Wipes") {
                                                help = "Package of disposable baby wipes";
                                            }
                                            else if (itemsCategory === "Feeding") {
                                                itemsType = OrderFeedingType.getIdentifier(this.state.items[index].itemsType);
                                                if (itemsType.indexOf('Bib') >= 0) {
                                                    help = "2 bibs to be used for small children to keep clean while eating";
                                                } else if (itemsType.indexOf('Bottle') >= 0) {
                                                    help = "2 Bottle for baby/infant milk or formula";
                                                } else if (itemsType.indexOf('Formula') >= 0) {
                                                    help = "2 containers of baby formula";
                                                } else if (itemsType.indexOf('Plates') >= 0) {
                                                    help = "Plates designed for small children";
                                                } else if (itemsType.indexOf('Utensils') >= 0) {
                                                    help = "Toddler size spoon and fork";
                                                } else if (itemsType.indexOf('Sippy Cups') >= 0) {
                                                    help = "A drinking cup that has a lid and a built-in spout or straw";
                                                }
                                            }
                                            else if (itemsCategory === "Bedding") {
                                                itemsType = OrderBeddingType.getIdentifier(this.state.items[index].itemsType);
                                                if (itemsType.indexOf('Blankets: Twin') >= 0) {
                                                    help = "Blanket for twin size bed";
                                                } else if (itemsType.indexOf('Blankets: Full') >= 0) {
                                                    help = "Blanket for full size bed";
                                                } else if (itemsType.indexOf('Blankets: Queen') >= 0) {
                                                    help = "Blanket for queen size bed";
                                                } else if (itemsType.indexOf('Blankets: King') >= 0) {
                                                    help = "Blanket for king size bed";
                                                } else if (itemsType.indexOf('Crib Bedding Set') >= 0) {
                                                    help = "Contains mattress, mattress pad, and sheets for crib";
                                                } else if (itemsType.indexOf('Crib Mattress') >= 0) {
                                                    help = "Mattress for baby crib";
                                                } else if (itemsType.indexOf('Crib Sheets') >= 0) {
                                                    help = "Sheets for baby crib";
                                                } else if (itemsType.indexOf('Crib Mattress Pad') >= 0) {
                                                    help = "Mattress pad for baby crib to provide protection for mattress";
                                                } else if (itemsType.indexOf('Sheets: Twin') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Sheets: Full') >= 0) {
                                                    help = "Top and bottom sheet for full bed";
                                                } else if (itemsType.indexOf('Sheets: Queen') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Sheets: King') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Baby Blanket') >= 0) {
                                                    help = "Small blanket for baby/infant";
                                                } else if (itemsType.indexOf('Teen Blanket') >= 0) {
                                                    help = "Large blanket for older children";
                                                }
                                            }
                                            else if (itemsCategory === "Stroller") {
                                                itemsType = OrderStrollerType.getIdentifier(this.state.items[index].itemsType);
                                                if (itemsType.indexOf('Double') >= 0) {
                                                    help = "A stroller designed for 2 children";
                                                } else if (itemsType.indexOf('Full Size') >= 0) {
                                                    help = "A full-size stroller with more features and comfort than an umbrella stroller, but is usually more bulky and heavy";
                                                } else if (itemsType.indexOf('Umbrella') >= 0) {
                                                    help = "A lightweight, basic stroller that folds up easily (like an umbrella). Often has a sling type seat. Best for babies that can sit up on their own.";
                                                }
                                            }
                                            else if (itemsCategory === "Toys") {
                                                itemsType = OrderToyType.getIdentifier(this.state.items[index].itemsType);
                                                if (itemsType.indexOf('Bike Boy') >= 0) {
                                                    help = "Boy's bicycle, please add any requests to comments";
                                                } else if (itemsType.indexOf('Bike Girl') >= 0) {
                                                    help = "Girl's bicycle, please add any requests to comments";
                                                } else if (itemsType.indexOf('Bike Helmet') >= 0) {
                                                    help = "Protective helmet for bike riding or other activities";
                                                } else if (itemsType.indexOf('Birthday Gift') >= 0) {
                                                    help = "";
                                                } else if (itemsType.indexOf('Books-4') >= 0) {
                                                    help = "4 books based on age";
                                                } else if (itemsType.indexOf("CD's") >= 0) {
                                                    help = "Music CDs";
                                                } else if (itemsType.indexOf("DVD's") >= 0) {
                                                    help = "DVD movies";
                                                } else if (itemsType.indexOf('Games') >= 0) {
                                                    help = "Various board games selected based on age";
                                                } else if (itemsType.indexOf('Puzzles') >= 0) {
                                                    help = "Various puzzles selected based on age";
                                                } else if (itemsType.indexOf('Toy- Lg') >= 0) {
                                                    help = "Larger toys based on age";
                                                } else if (itemsType.indexOf('Toy- Sm') >= 0) {
                                                    help = "Smaller toys based on age";
                                                }
                                            }
                                            else if (itemsCategory === "Equipment") {
                                                itemsType = OrderEquipmentType.getIdentifier(this.state.items[index].itemsType);
                                                if (itemsType.indexOf('Backpack') >= 0) {
                                                    help = "A (school) bag with shoulder straps that allow it to be carried on one's back";
                                                } else if (itemsType.indexOf('Baby Monitor') >= 0) {
                                                    help = "A radio system used to remotely listen to sounds made by an infant";
                                                } else if (itemsType.indexOf('Bassinette') >= 0) {
                                                    help = "An extra small bed specifically for babies from birth to about four months";
                                                } else if (itemsType.indexOf('Bathtub') >= 0) {
                                                    help = "A large portable container to bathe babies safely. Usually has a contoured seat and a sloped back so the baby lies at an angle. Best for 0-6 months";
                                                } else if (itemsType.indexOf('Booster Seat - Table') >= 0) {
                                                    help = "A reclining chair that bounces/moves up and down. For ages 0-6 months";
                                                } else if (itemsType.indexOf('Breast Feeding Pillow') >= 0) {
                                                    help = "A crescent shaped pillow used to support the baby while breastfeeding";
                                                } else if (itemsType.indexOf('Bumbo') >= 0) {
                                                    help = "A one-piece seat that is made entirely of a low-density foam. It has a deep seat with a high back and sides. For babies that can hold up their head.";
                                                } else if (itemsType.indexOf('Changing Pad') >= 0) {
                                                    help = 'A contoured 3" thick cushion used for changing baby’s diaper. A Portable Changing Pad is just a thin mat that folds compactly';
                                                } else if (itemsType.indexOf('Changing Pad Cover') >= 0) {
                                                    help = "A fitted sheet for the changing pad";
                                                } else if (itemsType.indexOf('Crib- Play & Pack') >= 0) {
                                                    help = 'Portable crib that can fold up, approximately 40" x 28"';
                                                } else if (itemsType.indexOf('Diaper Bag') >= 0) {
                                                    help = "Bag designed to carry diapers, wipes and other items for baby/toddler";
                                                } else if (itemsType.indexOf('Diaper Genie') >= 0) {
                                                    help = "Container for disposable diaper similar to garbage can but with odor control";
                                                } else if (itemsType.indexOf('High Chair') >= 0) {
                                                    help = "A small chair with a tray that is raised high enough for the child to sit at table height";
                                                } else if (itemsType.indexOf('Lunch Box') >= 0) {
                                                    help = "A container to hold a child’s lunch";
                                                } else if (itemsType.indexOf('Potty Seat') >= 0) {
                                                    help = 'A Potty Seat (circle shape) is placed on the regular toilet seat to train a child to use the toilet.';
                                                } else if (itemsType.indexOf('Safety Gate') >= 0) {
                                                    help = "A gate that is placed in a doorway";
                                                } else if (itemsType.indexOf('Safety Locks') >= 0) {
                                                    help = "Devices that keep outlets covered and prevent toddlers from opening drawers and doors";
                                                } else if (itemsType.indexOf('School Supplies') >= 0) {
                                                    help = "Basics items for school such as pen, pencil, crayons, notebook, binder, etc.";
                                                } else if (itemsType.indexOf('Indoor Swing') >= 0) {
                                                    help = "A hanging seat that swings and is suspended between v-shaped poles, usually about 3 feet high. Best for 0-6 months";
                                                } else if (itemsType.indexOf('Tub Seat') >= 0) {
                                                    help = "A soft, mesh sling that can be placed in regular bathtub or large sink to bathe a baby. Best for 0-6 months";
                                                } else if (itemsType === "Front Pack- Infant") {
                                                    help = "<ul>Front Pack - Infant: A device to carry an infant on the body. Mention preferred type in item comments." +
                                                        "<li>A Front Pack is like a reverse backpack with straps over the shoulder and a padded carrying 'pack' that can be placed on your chest.</li>" +
                                                        "<li> A Wrap is a super long piece of stretchy fabric you tie around your shoulders and mid-section to create a carrier.</li>" +
                                                        "<li> A Sling is a wide piece of fabric that goes over one shoulder and across your torso.</li>" +
                                                        "</ul>";
                                                }
                                            }
                                            Bert.defaults.hideDelay = 10000;
                                            Bert.alert(help, 'info', 'fixed-top');
                                            if (document.querySelectorAll('.bert-content').length > 0) {
                                                let el = document.querySelectorAll('.bert-content')[0];
                                                if (el) {
                                                    el.innerHTML = `<p>${help}</p>`;
                                                }
                                            }
                                        }}
                                    >Details
                                    </button>


                                    <CCButton
                                        type="remove"
                                        onClick={(event) => {
                                            event.preventDefault();
                                            this.state.items.splice(index, 1);
                                            this.setState({ items: this.state.items });
                                        }}
                                    />
                                </div>
                            </>
                        ))}

                        <CCButton
                            type="add"
                            onClick={(event) => {
                                event.preventDefault();
                                if (!this.state.items) this.state.items = [];
                                if (
                                    this.state.items.length >= 1 &&
                                    !this.state.items[this.state.items.length - 1]
                                ) {
                                    return;
                                }
                                this.state.items.push({
                                    //itemsCategory:0,
                                    //itemsType:0,
                                    itemsName: "",
                                    itemsComment: ""
                                });
                                this.setState({ items: this.state.items });
                            }}>
                            Add New Item
                        </CCButton>
                    </fieldset>
                    {Meteor.user().inactive == true ? null :
                        <input type="submit" value="Submit Order" />}
                </form>
            </div>
        );
    }
}

export default container((props, onData) => {
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (volunteerSubscription.ready() && businessSubscription.ready()) {

        onData(null, {});
    }
}, OrderNew);
