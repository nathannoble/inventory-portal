import React from 'react';
import { Link as Link } from 'react-router-dom';
import DateTime from 'react-datetime';

import moment from 'moment';
import listify from 'listify';

import PropTypes from 'prop-types';

import CC from '/imports/api';

import Order, { OrderStatus, OrderItemCategory, ChildSex, ChildAge } from '/imports/api/Order';

import Drawer from '/imports/ui/components/helpers/Drawer';

import container from '/imports/ui/helpers/container';
import {
    SubHeader,
    Breadcrumbs,
    Select,
    Label,
    Checkbox,
    CCButton,
    Loader,
    handleError,
} from '/imports/ui/helpers';

class OrderGroupListFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            pickupLocation: undefined,
            pickupDate: undefined,
            status: undefined,
        };
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("components/Order/GroupList.jsx:OrderGroupListFilter:componentWillUpdate:true");
        if (this.state !== nextState) this.props.onChange(nextState);
    }

    render() {
        return (
            <div className="internship list filter">
                <label htmlFor="keyword">
                    <input
                        type="text"
                        id="keyword"
                        placeholder="Search Keyword"
                        value={this.state.keyword}
                        onChange={(event) => {
                          console.log("components/Order/GroupList.jsx:render:keyword:event.target.value:" + JSON.stringify(event.target.value));
                          const keyword = event.target.value;
                          this.setState({ keyword });
                        }}
                    />
                </label>

                <Select
                    id="pickupLocation"
                    value={this.state.pickupLocation}
                    placeholder="Select Care Provider"
                    options={_.uniq(this.props.orders.map(order => order.pickupLocation)).map(pickupLocation => Object({ value: pickupLocation, label: pickupLocation }))}
                    onChange={(selectedOption) => {
                        console.log("components/Order/GroupList.jsx:render:pickupLocation:selectedOption:" + JSON.stringify(selectedOption));
                        this.setState({ pickupLocation: selectedOption ? selectedOption.value : undefined });
                      }}
                />

                <Label
                    id="pickupDate"
                    element={DateTime}
                    text="Pickup Date"
                    onChange={(selectedOption) => {
                     //event.preventDefault();
                     console.log("components/Order/GroupList.jsx:render:pickupDate:selectedOption:" + JSON.stringify(selectedOption));
                     this.setState({ pickupDate: selectedOption ? selectedOption.toDate() : undefined });
                     }}
                    timeFormat={false}
                    value={this.state.pickupDate}
                />

                <Select
                    id="status"
                    value={this.state.status}
                    placeholder="Select Status"
                    options={_.uniq(this.props.orders.map(order => order.status)).map(status => Object({ value: status, label: OrderStatus.getIdentifier(status)}))}
                    onChange={(selectedOption) => {
                        this.setState({ status: selectedOption ? selectedOption.value : undefined });
                    }}
                />

                <input
                    type="button"
                    value="Reset"
                    onClick={(event) => {
                        event.preventDefault();
                        this.setState({
                          keyword: '',
                          pickupLocation: undefined,
                          pickupDate: undefined,
                          status: undefined,
                        });
                    }}
                />

            </div>
        );
    }
}

const OrderGroupListItem = ({ order, index }) => (
    <li className={(index % 2 === 0 && index !== 0) ? "page-break" : null} id={order._id}>
        <Link to={`/order/${order._id}`}>
            <header>
                <h2>
                    <span className="date"></span>{order.childName}
                </h2>
                <span className="date">Pickup Date {moment(order.pickupDate).format('MMM Do')}</span>

            </header>
            <div className="body">

                <span className="label">Child Name</span><span className="value">{order.childName}</span>
                <span className="label">Child Age</span>
                <span className="value">
                    {/*ChildAge.getIdentifier(ChildAge.getValues()[order.childAge])*/}
                    {ChildAge.getIdentifier(order.childAge)}
                </span>
                <span className="label">Child Weight</span><span className="value">{order.childWeight}</span>
                <span className="label">Child Sex</span><span
                className="value">{ChildSex.getIdentifier(order.childSex)}</span>
                <span className="label">Child Zip</span><span className="value">{order.childZip}</span>

                <span className="business">
                  Internal ID: {order.comments}
                </span>

                <span className="business">
                  Items: {listify(order.items.map(item => OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory])))}
                </span>

                <ul className="chips no-print">
                    {order.status === 1 ? (
                        <li className="paid">
                            <i className="mdi mdi-dollar"/>Open
                        </li>
                    ) : (
                        <li className="focusId">
                            <i className="mdi"/>{OrderStatus.getIdentifier(order.status)}
                        </li>
                    )}
                    <li className="focusId">{order.comments}</li>
                    <li className="location">
                        <i className="mdi mdi-map-marker"/>
                        {order.pickupLocation}
                    </li>
                </ul>
            </div>
        </Link>
    </li>
);

OrderGroupListItem.propTypes = {
    order: PropTypes.instanceOf(Order),
};

OrderGroupListItem.defaultProps = {
    order: new Order(),
};

class OrderGroupList extends React.Component {

    constructor(props) {
        console.log("components/Order/List.jsx:constructor:props" + JSON.stringify(props));
        super(props);
        this.state = {
            filteredOrders: this.props.orders,
            //props:this.props
        };
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            if (this.props.onChange) this.props.onChange(nextState);
            console.log("components/Order/List.jsx:OrderGroupList:componentWillUpdate:true");
        } else {
            console.log("components/Order/List.jsx:OrderGroupList:componentWillUpdate:false");
        }
    }

    render() {

        return (
            <div id="reportRoot" className="internship list body" status={this.props.status}>
                <SubHeader className="no-print">
                    <Breadcrumbs className="no-print" breadcrumbs={[{ text: 'All Orders' }]}/>
                    <Drawer className="no-print" visible={!Meteor.user()}>
                        <OrderGroupListFilter
                            className="no-print"
                            orders={this.props.orders}
                            onChange={(filter) => {
                                console.log("components/Order/GroupList.jsx:onChange:event:" + JSON.stringify(filter));
                                const filteredOrders = [];
                                this.props.orders.forEach((order) => {

                                  const matchesKeyword = filter.keyword
                                    ? ((order, keyword) => {
                                        this.setState({
                                          filteredOrders: this.props.orders,
                                        });

                                        if (order.childName.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                                          return true;
                                        }

                                        if (order.pickupLocation.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                                          return true;
                                        }

                                       if (order.comments.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                                          return true;
                                        }

                                        return false;
                                      })(order, filter.keyword)
                                    : true;
                                  const matchesPickupLocation = filter.pickupLocation ? order.pickupLocation === filter.pickupLocation : true;

                                  var orderDay = new Date(order.pickupDate).getDate();
                                  var orderMonth = new Date(order.pickupDate).getMonth();
                                  var orderYear = new Date(filter.pickupDate).getYear();
                                  var filterDay = new Date(filter.pickupDate).getDate();
                                  var filterMonth = new Date(filter.pickupDate).getMonth();
                                  var filterYear = new Date(filter.pickupDate).getYear();

                                  const matchesPickupDate = filter.pickupDate ? (orderDay === filterDay && orderMonth === filterMonth && orderYear === filterYear) : true;
                                  const matchesStatus = filter.status ? order.status === filter.status : true;
                                //console.log("components/Order/GroupList.jsx:onChange:event:matchesPickupDate:" + matchesPickupDate);
                                //console.log("components/Order/GroupList.jsx:onChange:event:matchesStatus:" + matchesStatus);
                                //console.log("components/Order/GroupList.jsx:onChange:event:matchesPickupLocation:" + matchesPickupLocation);
                                //console.log("components/Order/GroupList.jsx:onChange:event:matchesKeyword:" + matchesKeyword);
                                  if (matchesPickupDate && matchesStatus && matchesKeyword && matchesPickupLocation) {
                                    console.log("components/Order/GroupList.jsx:onChange:event:this order matches:" + JSON.stringify(order));
                                    filteredOrders.push(order);
                                  }
                                });

                                //console.log("components/Order/GroupList.jsx:onChange:event:filteredOrders:" + JSON.stringify(filteredOrders));

                                this.setState({ filteredOrders: filteredOrders });
                                //console.log("components/Order/GroupList.jsx:onChange:state:" + JSON.stringify(this.state));
                              }}
                        />
                    </Drawer>

                </SubHeader>
                <ul>
                    {Meteor.user() && Meteor.user().may.create.order() ? (
                        <li className="new no-print">
                            {/*<Link to="/order/new">{CC.icon('create')}New Order</NormalLink>*/}
                            <Link to="/" onClick={(event) => {
                                    event.preventDefault();
                                    window.print();

                          }}
                            >{CC.icon('printer')} Print Report</Link>
                        </li>
                    ) : null}
                </ul>
                <ul>

                    {this.state.filteredOrders.map((order, index) => (
                        <OrderGroupListItem index={index} key={order._id} order={order}/>
                    ))}
                </ul>
            </div>
        );

        //return (doc);
    }
}

export default container((props, onData) => {
    //console.log("components/Order/List.jsx" + JSON.stringify(props));

    const subscription = Meteor.subscribe('/order/list');
    if (subscription.ready()) {
        let query = {};
        const orders = Order.find(query, {sort: {createdAt: -1}}).fetch();
        //console.log("components/Order/List.jsx:status:" + status);
        //console.log("components/Order/List.jsx:orders#:" + orders.length);
        //console.log("components/Order/List.jsx:location.path:" + location.path);
        onData(null, {orders, status, props});
    }
}, OrderGroupList);


