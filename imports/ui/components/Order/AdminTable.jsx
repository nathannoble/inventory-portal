import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import { Link, withRouter } from 'react-router-dom';
import moment from 'moment';
import { Bert } from 'meteor/themeteorchef:bert';

import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import User from '/imports/api/User';

import Order, { OrderStatus, OrderBasicItemCategory, OrderItemCategory } from '/imports/api/Order';
import OrderUserOrg from '/imports/api/OrderUserOrg';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate, {helperDateTime} from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import AdminTable from '/imports/ui/components/AdminTable';

import container from '/imports/ui/helpers/container';
import CC from '/imports/api';

import {handleError,CCButton} from '/imports/ui/helpers';

//console.log('Order/AdminTable.jsx:user:' + Meteor.user());

OrderUserOrg.adminTable = new AdminTableDefinition();

OrderUserOrg.adminTable.push({
    header: 'Child Name',
    sort: (a, b) => {
        a = a.childName.toUpperCase();
        b = b.childName.toUpperCase();
        return compare(a, b);
    },
    transform: (order, willExport = false) => {
        if (willExport) {
            return (
                <a title={order.childName} href={order.path}>
                    {order.childName}
                </a>
            );
        }
        return (
            <Link title={order.childName} to={order.path}>
                {order.childName}
            </Link>
        );
    },
    className: 'childName',
    visible: true,
});

OrderUserOrg.adminTable.push({
    header: 'Created By',
    className: 'caseworkerName',
    sort: (a, b) => {
        a = a.userName[0].toUpperCase();
        b = b.userName[0].toUpperCase();
        return compare(a, b);
    },
    // transform: order => (order.userName[0]),
    transform: (order, willExport = false) => {
        if (Meteor.user().type !== 'Caseworker') {
        if (willExport) {
            return (
                <a title={order.userName[0]} href={`/user/${order.userId[0]}`}>
                    {order.userName[0]}
                </a>
            );
        }
        return (
            <Link title={order.userName[0]} to={`/user/${order.userId[0]}`}>
                {order.userName[0]}
            </Link>
        );
        } else {
            return order.userName[0];
        }
    },
    visible: true,
});

OrderUserOrg.adminTable.push({
    header: 'Care Provider',
    sort: (a, b) => {
        a = a.pickupLocation.toUpperCase();
        b = b.pickupLocation.toUpperCase();
        return compare(a, b);
    },
    transform: order => (order.pickupLocation),
    className: 'careProvider',
    visible: true,
});

OrderUserOrg.adminTable.push({
    header: 'Date Created',
    sort: (a, b) => {
        a = a.createdAt;
        b = b.createdAt;
        return compare(a, b);
    },
    transform: order => helperDate(order.createdAt),
    className: 'createdAt',
    visible: true,
});

OrderUserOrg.adminTable.push({
    header: 'Pickup Date',
    sort: (a, b) => {
        a = a.pickupDate;
        b = b.pickupDate;
        return compare(a, b);
    },
    transform: order => helperDate(order.pickupDate),
    className: 'pickupDate',
    visible: true
});

OrderUserOrg.adminTable.push({
    header: 'Status',
    sort: (a, b) => {
        a = a.status;
        b = b.status;
        return compare(a, b);
    },
    transform: order => (OrderStatus.getIdentifier(order.status)),
    className: 'status',
    visible: true,
});

OrderUserOrg.adminTable.push({
    header: 'Copy',
    transform: order => (
        <CCButton
            type="copy"
            onClick={(event) => {
            event.preventDefault();
                const copiedOrder = new Order({
                        ordererId: order.ordererId,
                        status: order.status,
                        childName: order.childName,
                        childZip: order.childZip,
                        childAge: order.childAge,
                        childWeight: order.childWeight,
                        childSex: order.childSex,
                        newChild: order.newChild,
                        itemCategory: order.itemCategory,
                        itemType: order.itemType,
                        itemSize: order.itemSize,
                        pickupLocation: order.pickupLocation,
                        pickupDate: order.pickupDate,
                        shoeSize: order.shoeSize,
                        comments: order.comments + " - copied on " + helperDateTime(new Date()),
                        items: order.items,
                    });
                console.log('Order/AdminTable.jsx:copyOrder:order:', JSON.stringify(copiedOrder));
                Meteor.call(
                    '/order/new',
                    copiedOrder,
                    handleError({
                        callback(orderId) {
                            //history.push(`/order/${orderId}`);
                            console.log('Order/AdminTable.jsx:createOrder:id:' + orderId);
                        },
                    })
                );
        }}
        />
    ),
    className: 'copy',
    visible: (true)
});

OrderUserOrg.adminTable.push({
        header: 'Delete',
        transform: orderUserOrg => (
            <CCButton
                type="remove"
                onClick={(event) => {
            event.preventDefault();

            const orders = Order.find({_id: orderUserOrg._id}, {sort: {createdAt: -1}}).fetch();
            if (orders.length > 0) {

                let order = orders[0];

                // Edit date
                let daysBeforePickupDate = (new Date(order.pickupDate) - new Date()) / 86400000;
                if (daysBeforePickupDate < 0) {
                    daysBeforePickupDate = 0
                }

                let endEditDate = new Date();
                endEditDate.setDate(endEditDate.getDate() + daysBeforePickupDate - 6);

                const mayEdit = Meteor.user() && Meteor.user().may.delete.order(order); // && mayEditBeforePickupDate
                if (mayEdit) {
                    console.log('Order/AdminTable.jsx:deleteOrder:order');
                    Meteor.call('/order/delete', order, handleError({
                                callback() {
                                    Bert.defaults.hideDelay = 10000;
                                    let message = 'Order deleted.';
                                    Bert.alert(message, 'success');
                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                       let el = document.querySelectorAll('.bert-content')[0];
                                       if (el) {
                                           el.innerHTML = `<p>${message}</p>`;
                                       }
                                    }
                                    console.log('Order/AdminTable.jsx:order deleted');
                                }}));
                } else {
                    console.log('Order/AdminTable.jsx:no permissions to edit order');
                    let message = 'You do not have permissions to delete.  Order is not editable by you.';
                    Bert.alert(message, 'warning');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                       let el = document.querySelectorAll('.bert-content')[0];
                       if (el) {
                           el.innerHTML = `<p>${message}</p>`;
                       }
                    }
                }
            } else {
                console.log('Order/AdminTable.jsx:no order found');
                Bert.defaults.hideDelay = 10000;
                let message = 'Order not found.';
                Bert.alert(message, 'warning');
                if (document.querySelectorAll('.bert-content').length > 0) {
                   let el = document.querySelectorAll('.bert-content')[0];
                   if (el) {
                       el.innerHTML = `<p>${message}</p>`;
                   }
                }
            }
        }}
            />
        ),
        className: 'delete',
        visible: true,
    }
);

const OrderAdminTable = props => (
        <AdminTable collection={OrderUserOrg} {...props} />
);

export default container((props, onData) => {

    console.log("components/Order/AdminTable.jsx:render:props:" + JSON.stringify(props));

    let startDate;
    let endDate;
    const user = Meteor.user();
    let now = new Date();
    
    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
    } else if (user.type != 'Caseworker'
        && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
            || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
    } else {
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        console.log("components/Order/AdminTable.jsx:render:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const orderUserOrgSubscription = Meteor.subscribe('/orderUserOrg/searchByDate', startDate, endDate);
    const orderSubscription = Meteor.subscribe('/order/searchByDate', startDate, endDate);

    const cwSubscription = Meteor.subscribe('/user/caseworker/list');
    const volSubscription = Meteor.subscribe('/user/volunteer/list');

    if (orderSubscription.ready() && orderUserOrgSubscription.ready() && volSubscription.ready() && cwSubscription.ready()) {
         onData(null, {startDate,endDate, history});
    }
}, withRouter(OrderAdminTable));
