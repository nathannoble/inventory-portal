import React from 'react';
import { Link } from 'react-router-dom';
import DateTime from 'react-datetime';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';
import { Bert } from 'meteor/themeteorchef:bert';

import Order, {
    OrderStatus, OrderFilter, OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType, OrderHygieneType, OrderBeddingType,
    OrderEquipmentType, OrderFeedingType, OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize, OrderBoyPantSize,
    OrderShoeSize, ChildAge, ChildSex, ChildStatus
} from '/imports/api/Order';

import Business, { CareProviderLocation, CareProviderPickupDay } from '/imports/api/Organization/Business';

import {
    handleError,
    Select,
    Checkbox,
    SubHeader,
    Breadcrumbs,
    Label,
    CCButton
} from '/imports/ui/helpers';

import CC from '/imports/api';
import checkDate from '../../helpers/checkDate';

class OrderView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { order, mayEdit, history, ordererName, dayOfWeekForPickupLocation, endEditDate, location } = this.props;

        if (mayEdit === undefined) {
            mayEdit = false;
        }

        return (
            <div className="order view body">
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[
                            { link: '/order', text: 'Orders' },
                            {
                                link: `/user/${order.orderer()._id}`,
                                text: `${order.orderer().name}`,
                            },
                            {
                                text: `${order.pickupLocation} - ${order.childName}`,
                            }
                        ]}
                    />
                </SubHeader>
                <h2>{(Meteor.user().inactive == true ? 'Current user is inactive' : (mayEdit ? 'Edit Order ' : 'Editing for this order is currently closed.'))}</h2>
                { /*until ' + ' 5 PM ' + moment(endEditDate).format('MMM Do')*/}
                {/*Care ProviderPickupDay.getIdentifier(dayOfWeekForPickupLocation) + ' before ' + moment(order.pickupDate).format('MMM Do')*/}
                <form>
                    <fieldset htmlFor="basic" disabled={!mayEdit}>
                        <h2>Basic Info</h2>
                        <div className="items">
                            <Label
                                name="ordererId"
                                text="Case Worker"
                                type="text"
                                value={ordererName}
                                readonly={true}
                            />
                            <Label
                                name="pickupLocation"
                                text="Care Provider"
                                type="text"
                                value={order.pickupLocation + " - " + CareProviderLocation.getIdentifier(location)}
                                readonly={true}
                            />

                            <Label
                                name="pickupDay"
                                text="Pickup Day"
                                type="text"
                                value={CareProviderPickupDay.getIdentifier(dayOfWeekForPickupLocation)}
                                readonly={true}
                            />

                            {Meteor.user().type === 'Caseworker' ?
                                <Label
                                    readonly={true}
                                    type="text"
                                    value={OrderStatus.getIdentifier(order.status)}
                                    name="status"
                                    text="Status"
                                /> :
                                <Label
                                    readonly={!mayEdit}
                                    required
                                    name="status"
                                    text="Status"
                                    value={order.status}
                                    element={Select}
                                    options={OrderStatus.getIdentifiers().map((status, index) => Object({ value: index, label: status }))}
                                    onChange={({ value }) => {
                                        order.status = value;
                                        Meteor.call('/order/update', order, handleError);
                                    }}
                                />}

                        </div>
                    </fieldset>
                    <fieldset htmlFor="child" disabled={!mayEdit}>
                        <h2>Child Info</h2>
                        <div className="items">
                            <Label
                                name="childName"
                                required
                                text="Child Name"
                                type="text"
                                value={order.childName}
                                onChange={(event) => {
                                    order.childName = event.target.value;
                                    this.setState({ childName: order.childName });
                                    //Meteor.call('/order/update', order, handleError);
                                }}
                                onBlur={(event) => {
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            <Label
                                name="comments"
                                text="Internal ID"
                                type="text"
                                value={order.comments}
                                onChange={(event) => {
                                    order.comments = event.target.value;
                                    this.setState({ comments: order.comments });
                                }}
                                onBlur={(event) => {
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            {!mayEdit ?
                                <Label
                                    readonly={true}
                                    name="pickupDate"
                                    text="Pickup Date"
                                    timeFormat={false}
                                    value={order.pickupDate}
                                    element={DateTime}
                                    required
                                /> :
                                <Label

                                    name="pickupDate"
                                    text="Pickup Date"
                                    /*isValidDate={(date) => {
                                        if (Meteor.user().type === 'Caseworker' ) {
                                        const now = new Date();
                                        let weeksFromNow = new Date();
                                        weeksFromNow.setDate(weeksFromNow.getDate()+30);
                                        //console.log('Order/View.jsx:isValidDate:'+ date.isAfter( now ));
                                        return (date.isAfter( now ) && date.isSameOrBefore(weeksFromNow));
                                        } else { return true;}
                                    }}*/
                                    timeFormat={false}
                                    value={order.pickupDate}
                                    element={DateTime}
                                    onChange={(date) => {
                                        
                                        if (typeof date === "object") {

                                            if (checkDate(date,dayOfWeekForPickupLocation) === true) {
                                                order.pickupDate = date.toDate();
                                                Meteor.call('/order/update', order, handleError);
                                            }
                                          
                                        }
                                    }}
                                    required
                                />}

                            <Label
                                disabled={!mayEdit}
                                name="newChild"
                                text="Child Status"
                                value={order.newChild}
                                element={Select}
                                required
                                options={ChildStatus.getIdentifiers().map((newChild, index) => Object({ value: index, label: newChild }))}
                                onChange={({ value }) => {
                                    console.log('Order/View.jsx:newChild:onChange:value:' + value);
                                    order.newChild = value;
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            <Label
                                name="childZip"
                                text="Child Zip"
                                type="text"
                                size="5"
                                value={order.childZip}
                                required
                                onChange={(event) => {
                                    order.childZip = parseInt(event.target.value, 0);
                                    console.log('Order/View.jsx:order:' + JSON.stringify(order));
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            <Label
                                disabled={!mayEdit}
                                name="childAge"
                                text="Age"
                                value={order.childAge}
                                element={Select}
                                required
                                options={ChildAge.getIdentifiers().map((childAge, index) => Object({ value: index, label: ChildAge.getIdentifier(index) }))}
                                onChange={(selectedOption) => {
                                    //const newValue = Number(selectedOption.value);
                                    console.log('Order/View.jsx:value:' + JSON.stringify(selectedOption));
                                    console.log('Order/View.jsx:ChildAge:' + JSON.stringify(ChildAge.getIdentifiers()));
                                    order.childAge = selectedOption.value;
                                    console.log('Order/View.jsx:order:' + JSON.stringify(order));
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            <Label
                                name="childWeight"
                                text="Child Weight"
                                type="number"
                                value={order.childWeight}
                                required
                                onChange={(event) => {
                                    order.childWeight = parseInt(event.target.value, 0);
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />

                            <Label
                                disabled={!mayEdit}
                                name="childSex"
                                text="Sex"
                                value={order.childSex}
                                element={Select}
                                required
                                options={ChildSex.getIdentifiers().map((childSex, index) => Object({ value: index, label: childSex }))}
                                onChange={({ value }) => {
                                    order.childSex = value;
                                    Meteor.call('/order/update', order, handleError);
                                }}
                            />
                        </div>
                    </fieldset>
                    <fieldset htmlFor="items" disabled={!mayEdit}>
                        <legend>Items</legend>
                        {order.items.map((item, index) => (
                            <>
                                <h2>{item.itemsCategory !== undefined && item.itemsCategory !== null ? OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) : "Item " + (index + 1)}</h2>
                                <div key={index} className="items">

                                    <Label
                                        disabled={!mayEdit}
                                        name={"itemsCategory" + index}
                                        text="Category"
                                        element={Select}
                                        value={item.itemsCategory}
                                        options={OrderItemCategory.getIdentifiers().map((itemsCategory, index) => Object({ value: index, label: itemsCategory }))}
                                        onChange={(selectedOption) => {

                                            let item = order.items[index];

                                            const newValue = Number(selectedOption.value);
                                            const itemsCategories = order.items.map(items => items.itemsCategory);
                                            const clothingBagItems = order.items
                                                .filter(items => items.itemsCategory === 0);

                                            console.log('Order/View.jsx:clothing bag item array:' + JSON.stringify(clothingBagItems));
                                            console.log('Order/View.jsx:newValue:' + newValue);
                                            console.log('Order/View.jsx:itemsCategories:' + JSON.stringify(itemsCategories));

                                            let saveItem = true;

                                            // Temporarily restrict all but cb/diapers/wipes
                                            //if (newValue !== 0 && newValue !==1 && newValue !==2 && newValue !== 3 && Meteor.user().type === 'Caseworker') {
                                            //    let message = "Only clothing bags,other clothing, diapers and wipes are permitted (temporarily) because of the Coronavirus";
                                            //    Bert.alert(message, 'danger');
                                            //            if (document.querySelectorAll('.bert-content').length > 0) {
                                            //               let el = document.querySelectorAll('.bert-content')[0];
                                            //               if (el) {
                                            //                   el.innerHTML = `<p>${message}</p>`;
                                            //               }
                                            //            }
                                            //    saveItem = false;
                                            //} else
                                            // if clothing bag exists already
                                            if (itemsCategories.toString().indexOf("0") >= 0) {

                                                const clothingBagItem = clothingBagItems[0];
                                                // Don't allow more than 1 clothing bag
                                                if (newValue === 0) {
                                                    let message = "A Clothing Bag already exists in this item collection.   No additional 'Clothing Bag' items are permitted.";
                                                    Bert.alert(message, 'danger');
                                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                                        let el = document.querySelectorAll('.bert-content')[0];
                                                        if (el) {
                                                            el.innerHTML = `<p>${message}</p>`;
                                                        }
                                                    }
                                                    saveItem = false;
                                                } else if (newValue === 1) {  // Other Clothing
                                                    let message = "A Clothing Bag already exists in this item collection.  No 'Other Clothing' items are permitted.";
                                                    Bert.alert(message, 'danger');
                                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                                        let el = document.querySelectorAll('.bert-content')[0];
                                                        if (el) {
                                                            el.innerHTML = `<p>${message}</p>`;
                                                        }
                                                    }
                                                    saveItem = false;
                                                } else if (newValue === 2) {  // Diapers
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  No 'Diaper' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 3) {  // Wipes
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  No 'Wipes' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 5) {  // Hygiene Items
                                                    if (clothingBagItem.itemsType === 2 ||
                                                        clothingBagItem.itemsType === 3) {  // Teen Boy and Girl
                                                        let message = "A Teen Boy or Girl Clothing Bag already exists in this item collection.  No 'Hygiene' items are permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                } else if (newValue === 8) {  // Toys
                                                    if (clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) {  // Layette bags

                                                    }
                                                }
                                            }
                                            if (saveItem) {
                                                console.log('Order/View.jsx:saving item');
                                                if (item !== undefined && item !== '') {
                                                    order.items[index].itemsCategory = newValue;
                                                }
                                                this.setState({ items: order.items });
                                                Meteor.call('/order/update', order, handleError);
                                            }

                                        }}
                                    />

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Clothing Bag' ? (
                                        <>
                                            <Label
                                                disabled={!mayEdit}
                                                name={"itemsType" + index}
                                                required
                                                text="Bag Type"
                                                element={Select}
                                                value={item.itemsType}
                                                onChange={(selectedOption) => {
                                                    order.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }}
                                                options={OrderBagType.getIdentifiers().map((itemsType, index) =>
                                                    Object({ value: index, label: itemsType }))}

                                            />

                                            <Label
                                                disabled={!mayEdit}
                                                name={"itemsSize" + index}
                                                required
                                                text="Shirt Size"
                                                element={Select}
                                                value={item.itemsSize}
                                                onChange={(selectedOption) => {
                                                    order.items[index].itemsSize = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }}
                                                options={OrderClothingSize.getIdentifiers().map((itemsSize, index) =>
                                                    Object({ value: index, label: itemsSize }))}

                                            />

                                            {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Boy' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Girl' ||
                                                OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Neutral' ? (
                                                <Label
                                                    disabled={!mayEdit}
                                                    name={"pantSize" + index}
                                                    required
                                                    text="Pant Size"
                                                    element={Select}
                                                    value={item.pantSize}
                                                    onChange={(selectedOption) => {
                                                        order.items[index].pantSize = Number(selectedOption.value);
                                                        this.setState({ items: order.items });
                                                        Meteor.call('/order/update', order, handleError);
                                                    }}
                                                    options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                        Object({ value: index, label: pantSize }))}


                                                />
                                            ) : null}

                                            {OrderBagType.getIdentifier(order.items[index].itemsType) === 'Boy' ||
                                                OrderBagType.getIdentifier(order.items[index].itemsType) === 'Teen Boy' ? (
                                                <>
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"shoeSize" + index}
                                                        required
                                                        text="Shoe Size"
                                                        element={Select}
                                                        value={item.shoeSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].shoeSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                            Object({ value: index, label: shoeSize }))}


                                                    />
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}

                                                    />
                                                </>
                                            ) : null}

                                            {OrderBagType.getIdentifier(order.items[index].itemsType) === 'Girl' ||
                                                OrderBagType.getIdentifier(order.items[index].itemsType) === 'Teen Girl' ? (
                                                <>
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"shoeSize" + index}
                                                        required
                                                        text="Shoe Size"
                                                        element={Select}
                                                        value={item.shoeSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].shoeSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                            Object({ value: index, label: shoeSize }))}


                                                    />
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderGirlPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}


                                                    />
                                                </>
                                            ) : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Other Clothing' ? (
                                        <>
                                            <Label
                                                disabled={!mayEdit}
                                                name={"itemsType" + index}
                                                text="Individual Items"
                                                element={Select}
                                                value={item.itemsType}
                                                options={OrderClothingType.getIdentifiers().map((itemsType, index) =>
                                                    Object({ value: index, label: itemsType }))}
                                                onChange={(selectedOption) => {
                                                    order.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }}
                                            />

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Gloves & Hat' ? null : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Jacket' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Shirts (2)' ? (
                                                <Label
                                                    disabled={!mayEdit}
                                                    name={"itemsSize" + index}
                                                    text="Shirt Size"
                                                    element={Select}
                                                    value={item.itemsSize}
                                                    options={OrderClothingSize.getIdentifiers().map((itemsSize, index) =>
                                                        Object({ value: index, label: itemsSize }))}
                                                    onChange={(selectedOption) => {
                                                        order.items[index].itemsSize = Number(selectedOption.value);
                                                        this.setState({ items: order.items });
                                                        Meteor.call('/order/update', order, handleError);
                                                    }}
                                                />

                                            ) : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Pajamas - Boys' ? (
                                                <>
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"itemsSize" + index}
                                                        text="Shirt Size"
                                                        element={Select}
                                                        value={item.itemsSize}
                                                        options={OrderClothingSize.getIdentifiers().map((itemsSize, index) =>
                                                            Object({ value: index, label: itemsSize }))}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].itemsSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                    />
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}

                                                    />
                                                </>
                                            ) : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Maternity' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Pajamas - Girls' ? (
                                                <>
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"itemsSize" + index}
                                                        text="Shirt Size"
                                                        element={Select}
                                                        value={item.itemsSize}
                                                        options={OrderClothingSize.getIdentifiers().map((itemsSize, index) =>
                                                            Object({ value: index, label: itemsSize }))}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].itemsSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                    />
                                                    <Label
                                                        disabled={!mayEdit}
                                                        name={"pantSize" + index}
                                                        required
                                                        text="Pant Size"
                                                        element={Select}
                                                        value={item.pantSize}
                                                        onChange={(selectedOption) => {
                                                            order.items[index].pantSize = Number(selectedOption.value);
                                                            this.setState({ items: order.items });
                                                            Meteor.call('/order/update', order, handleError);
                                                        }}
                                                        options={OrderGirlPantSize.getIdentifiers().map((pantSize, index) =>
                                                            Object({ value: index, label: pantSize }))}

                                                    />
                                                </>
                                            ) : null}


                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Pants (2 Pair)' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Underwear (2 Pair)' ? (
                                                <Label
                                                    disabled={!mayEdit}
                                                    name={"pantSize" + index}
                                                    required
                                                    text="Pant Size"
                                                    element={Select}
                                                    value={item.pantSize}
                                                    onChange={(selectedOption) => {
                                                        order.items[index].pantSize = Number(selectedOption.value);
                                                        this.setState({ items: order.items });
                                                        Meteor.call('/order/update', order, handleError);
                                                    }}
                                                    options={OrderBoyPantSize.getIdentifiers().map((pantSize, index) =>
                                                        Object({ value: index, label: pantSize }))}

                                                />
                                            ) : null}

                                            {OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Shoes' ||
                                                OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]) === 'Socks (2 Pair)' ? (
                                                <Label
                                                    disabled={!mayEdit}
                                                    name={"shoeSize" + index}
                                                    required
                                                    text="Shoe Size"
                                                    element={Select}
                                                    value={item.shoeSize}
                                                    onChange={(selectedOption) => {
                                                        order.items[index].shoeSize = Number(selectedOption.value);
                                                        this.setState({ items: order.items });
                                                        Meteor.call('/order/update', order, handleError);
                                                    }}
                                                    options={OrderShoeSize.getIdentifiers().map((shoeSize, index) =>
                                                        Object({ value: index, label: shoeSize }))}


                                                />
                                            ) : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Diapers' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Diaper Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderDiaperType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType })).sort((a, b) => compare(a.label, b.label))}
                                            onChange={(selectedOption) => {
                                                order.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: order.items });
                                                Meteor.call('/order/update', order, handleError);
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Hygiene Items' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Hygiene Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderHygieneType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType }))}
                                            onChange={(selectedOption) => {


                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = order.items.map(items => items.itemsCategory);
                                                const clothingBagItems = order.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/View.jsx:new type:' + newValue);
                                                let saveItem = true;

                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    const clothingBagItem = clothingBagItems[0];
                                                    // Don't allow more than 1 clothing bag

                                                    if ((clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) && [3, 12].includes(newValue)) {  // Layette bags and hygiene selected types incompatible
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  The selected 'Hygiene' type is not permitted";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }

                                                }
                                                if (saveItem) {
                                                    console.log('Order/View.jsx:saving item');
                                                    order.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Bedding' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Bedding Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderBeddingType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType }))}
                                            onChange={(selectedOption) => {
                                                order.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: order.items });
                                                Meteor.call('/order/update', order, handleError);
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Equipment' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Equipment Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderEquipmentType.getIdentifiers()
                                                .map((itemsType, index) =>
                                                    Object({ value: index, label: itemsType })).sort((a, b) => compare(a.label, b.label))}
                                            onChange={(selectedOption) => {
                                                order.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: order.items });
                                                Meteor.call('/order/update', order, handleError);
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Feeding' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Feeding Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderFeedingType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType }))}
                                            onChange={(selectedOption) => {

                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = order.items.map(items => items.itemsCategory);
                                                const clothingBagItems = order.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/View.jsx:new type:' + newValue);
                                                let saveItem = true;

                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    const clothingBagItem = clothingBagItems[0];
                                                    // Don't allow more than 1 clothing bag

                                                    if ((clothingBagItem.itemsType === 4 ||
                                                        clothingBagItem.itemsType === 5 ||
                                                        clothingBagItem.itemsType === 6) && [0].includes(newValue)) {  // Layette bags and feeding - bibs incompatible
                                                        let message = "A Layette Clothing Bag already exists in this item collection.  The selected 'Feeding' type is not permitted";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }

                                                }
                                                if (saveItem) {
                                                    console.log('Order/View.jsx:saving item');
                                                    order.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }

                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Toys' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Toy Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderToyType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType }))}
                                            onChange={(selectedOption) => {

                                                const newValue = Number(selectedOption.value);
                                                const itemsCategories = order.items.map(items => items.itemsCategory);
                                                const clothingBagItems = order.items
                                                    .filter(items => items.itemsCategory === 0);

                                                console.log('Order/View.jsx:new type:' + newValue);
                                                let saveItem = true;
                                                // if clothing bag exists already
                                                if (itemsCategories.toString().indexOf("0") >= 0) {

                                                    // Don't allow toy types: 4,5,6,7,8,10 if clothing bag exists
                                                    if ([4, 5, 6, 7, 8, 10].includes(newValue)) {
                                                        let message = "A Clothing Bag already exists in this item collection.   The selected 'Toy' type is not permitted.";
                                                        Bert.alert(message, 'danger');
                                                        if (document.querySelectorAll('.bert-content').length > 0) {
                                                            let el = document.querySelectorAll('.bert-content')[0];
                                                            if (el) {
                                                                el.innerHTML = `<p>${message}</p>`;
                                                            }
                                                        }
                                                        saveItem = false;
                                                    }
                                                }
                                                if (saveItem) {
                                                    console.log('Order/View.jsx:saving toy item');
                                                    order.items[index].itemsType = Number(selectedOption.value);
                                                    this.setState({ items: order.items });
                                                    Meteor.call('/order/update', order, handleError);
                                                }
                                            }}
                                        />
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(order.items[index].itemsCategory) === 'Stroller' ? (
                                        <Label
                                            disabled={!mayEdit}
                                            name={"itemsType" + index}
                                            text="Stroller Type"
                                            element={Select}
                                            value={item.itemsType}
                                            options={OrderStrollerType.getIdentifiers().map((itemsType, index) =>
                                                Object({ value: index, label: itemsType }))}
                                            onChange={(selectedOption) => {
                                                order.items[index].itemsType = Number(selectedOption.value);
                                                this.setState({ items: order.items });
                                                Meteor.call('/order/update', order, handleError);
                                            }}
                                        />
                                    ) : null}

                                    <Label
                                        name={"itemsComment" + index}
                                        text="Comments"
                                        value={item.itemsComment}
                                        type="text"
                                        onChange={(event) => {
                                            order.items[index].itemsComment = event.target.value;
                                            this.setState({ items: order.items });
                                            //Meteor.call('/order/update', order, handleError);
                                        }}
                                    />

                                    <button
                                        onClick={(event) => {
                                            console.log('Order/View.jsx:help');
                                            event.preventDefault();
                                            let help = "No details for this item.";
                                            let itemsCategory = OrderItemCategory.getIdentifier(order.items[index].itemsCategory);
                                            let itemsType = "";

                                            if (itemsCategory === "Clothing Bag") {
                                                itemsType = OrderBagType.getIdentifier(order.items[index].itemsType);

                                                if (itemsType.indexOf('Layette') >= 0) {
                                                    help = "<ul>" +
                                                        "<li>Burp Cloths</li>" +
                                                        "<li>Stuffed Animal - age-appropriate</li>" +
                                                        "<li>Toy - child up to 6 months</li>" +
                                                        "<li>Package of size 1 diapers (20 count)</li>" +
                                                        "<li>Diaper wipe package</li>" +
                                                        "<li>Receiving Blankets</li>" +
                                                        "<li>Warm Blanket or quilt</li>" +
                                                        "<li>Small bibs</li>" +
                                                        "<li>Newborn or Small Snap T-shirt</li>" +
                                                        "<li>Medium or Large Snap T-shirt</li>" +
                                                        "<li>Newborn or Small Stretch sleeper</li>" +
                                                        "<li>0-9 month Blanket Sleeper or PJ's (September - January Only)</li>" +
                                                        "<li>Outfits (0-3 month, 3-6 months, 6-9 months)</li>" +
                                                        "<li>Hat</li>" +
                                                        "<li>Socks, booties, shoes, etc.</li>" +
                                                        "<li>Wash cloths</li>" +
                                                        "<li>Towel</li>" +
                                                        "<li>Shampoo bottle / Head-Toe Soap</li>" +
                                                        "<li>Living Water Gospel of John (English or Spanish)</li>" +
                                                        "</ul>"
                                                } else if (itemsType === "Girl") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 onesies (6 to 24 months)</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 dress or skirt with top</li>" +
                                                        "<li>1 pair of pajamas or nightgown</li>" +
                                                        "<li>2 pair of underwear (for ages 3 and older)</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hair accessories</li>" +
                                                        "<li>1 game or puzzle</li>" +
                                                        "<li>2 - 4 books</li>" +
                                                        "<li>2 stuffed animals</li>" +
                                                        "<li>1 - 2 toys</li>" +
                                                        "<li>1 - 2 DVDs, CDs, coloring books, stickers, crafts</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Boy") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 onesies (6 to 24 months)</li>" +
                                                        "<li>2 bottoms 1 pair of pajamas</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>2 pair of underwear (for ages 3 and older)</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 game or puzzle</li>" +

                                                        "<li>2 - 4 books</li>" +
                                                        "<li>2 stuffed animals</li>" +
                                                        "<li>1 - 2 toys</li>" +
                                                        "<li>1 - 2 DVDs, CDs, coloring books, stickers, crafts</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Teen Girl") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 dress or skirt with top</li>" +
                                                        "<li>1 pair of pajamas or nightgown</li>" +
                                                        "<li>2 pair of underwear</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hair accessories</li>" +
                                                        "<li>1 hygiene kit</li>" +
                                                        "<li>2 camisoles</li>" +
                                                        "<li>1 fashion scarf</li>" +
                                                        "<li>1 - 2 books, games activities, or gadgets teen appropriate</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Teen Boy") {
                                                    help = "<ul>" +
                                                        "<li>4 shirts</li>" +
                                                        "<li>2 bottoms</li>" +
                                                        "<li>1 swimsuit - SUMMER ONLY</li>" +
                                                        "<li>1 pair of pajamas</li>" +
                                                        "<li>2 pair of underwear</li>" +
                                                        "<li>2 pair of socks</li>" +
                                                        "<li>2 pair of shoes (1 athletic, 1 casual)</li>" +
                                                        "<li>1 jacket</li>" +
                                                        "<li>1 sweatshirt or hoodie</li>" +
                                                        "<li>1 hat</li>" +
                                                        "<li>1 hygiene kit</li>" +
                                                        "<li>1 - 2 books, games activities, or gadgets teen appropriate</li>" +
                                                        "</ul>";
                                                }
                                            }
                                            else if (itemsCategory === "Hygiene Items") {
                                                itemsType = OrderHygieneType.getIdentifier(order.items[index].itemsType);

                                                if (itemsType.indexOf('Hygiene Kit - Child') >= 0) {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "</ul>"
                                                } else if (itemsType === "Hygiene Kit - Teen Boy") {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "<li>deodorant</li>" +

                                                        "<li>Shaving razors</li>" +
                                                        "</ul>";
                                                } else if (itemsType === "Hygiene Kit - Teen Girl") {
                                                    help = "<ul>" +
                                                        "<li>Soap</li>" +
                                                        "<li>Shampoo</li>" +
                                                        "<li>Toothpaste and toothbrush</li>" +
                                                        "<li>washcloth</li>" +
                                                        "<li>brush or comb</li>" +
                                                        "<li>deodorant</li>" +
                                                        "<li>razors</li>" +
                                                        "<li>feminine hygiene items</li>" +
                                                        "</ul>";
                                                } else if (itemsType.indexOf('Shampoo') >= 0) {
                                                    help = "Hair shampoo";
                                                } else if (itemsType.indexOf('Baby Shampoo') >= 0) {
                                                    help = "Special hair shampoo designed for babies";
                                                } else if (itemsType.indexOf('Brush/Comb') >= 0) {
                                                    help = "Hair brush or comb";
                                                } else if (itemsType.indexOf('Deodorant') >= 0) {
                                                    help = "Underarm deoderant to assist with body odor and perspiration";
                                                } else if (itemsType.indexOf('Soap') >= 0) {
                                                    help = "Body soap for bathing";
                                                } else if (itemsType.indexOf('Breast Pads') >= 0) {
                                                    help = "Pads for nursing mothers";
                                                } else if (itemsType.indexOf('Female Pads') >= 0) {
                                                    help = "Femine hygiene pads";
                                                } else if (itemsType.indexOf('Tampons') >= 0) {
                                                    help = "Femine hygiene product";
                                                } else if (itemsType.indexOf('Toothbrush') >= 0) {
                                                    help = "For brushing of teeth, matched by age";
                                                } else if (itemsType.indexOf('Toothpaste') >= 0) {
                                                    help = "For brushing of teeth";
                                                } else if (itemsType.indexOf('Towel') >= 0) {
                                                    help = "Larger towels for bathing";
                                                } else if (itemsType.indexOf('Wash Cloths') >= 0) {
                                                    help = "Hand towels";
                                                }

                                            }
                                            else if (itemsCategory === "Other Clothing") {
                                                itemsType = OrderClothingType.getIdentifier(OrderClothingType.getValues()[order.items[index].itemsType]);
                                                if (itemsType.indexOf('Gloves & Hat') >= 0) {
                                                    help = "Gloves and hat provided based on age and weight";
                                                } else if (itemsType.indexOf('Maternity') >= 0) {
                                                    help = "Maternity clothing for pregnant mothers. Top and bottoms provided";
                                                } else if (itemsType.indexOf('Pajamas - Boys') >= 0) {
                                                    help = "Sleepwear for boys based on age/sizing";
                                                } else if (itemsType.indexOf('Pajamas - Girls') >= 0) {
                                                    help = "Sleepwear for girls based on age/sizing";
                                                } else if (itemsType.indexOf('Pants (2 Pair)') >= 0) {
                                                    help = "2 pairs of pants";
                                                } else if (itemsType.indexOf('Shirts (2)') >= 0) {
                                                    help = "2 shirts";
                                                } else if (itemsType.indexOf('Shoes') >= 0) {
                                                    help = "2 shoes";
                                                } else if (itemsType.indexOf('Socks (2 Pair)') >= 0) {
                                                    help = "2 pairs of socks";
                                                } else if (itemsType.indexOf('Underwear (2 Pair)') >= 0) {
                                                    help = "2 pairs of underwear";
                                                } else if (itemsType.indexOf('Jacket') >= 0) {
                                                    help = "A coat or jacket based on age/gender/size";
                                                }
                                            }
                                            else if (itemsCategory === "Diapers") {
                                                help = "Package of 20 diapers based on sizing";
                                            }
                                            else if (itemsCategory === "Wipes") {
                                                help = "Package of disposable baby wipes";
                                            }
                                            else if (itemsCategory === "Feeding") {
                                                itemsType = OrderFeedingType.getIdentifier(order.items[index].itemsType);
                                                if (itemsType.indexOf('Bib') >= 0) {
                                                    help = "2 bibs to be used for small children to keep clean while eating";
                                                } else if (itemsType.indexOf('Bottle') >= 0) {
                                                    help = "2 Bottle for baby/infant milk or formula";
                                                } else if (itemsType.indexOf('Formula') >= 0) {
                                                    help = "2 containers of baby formula";
                                                } else if (itemsType.indexOf('Plates') >= 0) {
                                                    help = "Plates designed for small children";
                                                } else if (itemsType.indexOf('Utensils') >= 0) {
                                                    help = "Toddler size spoon and fork";
                                                } else if (itemsType.indexOf('Sippy Cups') >= 0) {
                                                    help = "A drinking cup that has a lid and a built-in spout or straw";
                                                }
                                            }
                                            else if (itemsCategory === "Bedding") {
                                                itemsType = OrderBeddingType.getIdentifier(order.items[index].itemsType);
                                                if (itemsType.indexOf('Blankets: Twin') >= 0) {
                                                    help = "Blanket for twin size bed";
                                                } else if (itemsType.indexOf('Blankets: Full') >= 0) {
                                                    help = "Blanket for full size bed";
                                                } else if (itemsType.indexOf('Blankets: Queen') >= 0) {
                                                    help = "Blanket for queen size bed";
                                                } else if (itemsType.indexOf('Blankets: King') >= 0) {
                                                    help = "Blanket for king size bed";
                                                } else if (itemsType.indexOf('Crib Bedding Set') >= 0) {
                                                    help = "Contains mattress, mattress pad, and sheets for crib";
                                                } else if (itemsType.indexOf('Crib Mattress') >= 0) {
                                                    help = "Mattress for baby crib";
                                                } else if (itemsType.indexOf('Crib Sheets') >= 0) {
                                                    help = "Sheets for baby crib";
                                                } else if (itemsType.indexOf('Crib Mattress Pad') >= 0) {
                                                    help = "Mattress pad for baby crib to provide protection for mattress";
                                                } else if (itemsType.indexOf('Sheets: Twin') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Sheets: Full') >= 0) {
                                                    help = "Top and bottom sheet for full bed";
                                                } else if (itemsType.indexOf('Sheets: Queen') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Sheets: King') >= 0) {
                                                    help = "Top and bottom sheet for twin bed";
                                                } else if (itemsType.indexOf('Baby Blanket') >= 0) {
                                                    help = "Small blanket for baby/infant";
                                                } else if (itemsType.indexOf('Teen Blanket') >= 0) {
                                                    help = "Large blanket for older children";
                                                }
                                            }
                                            else if (itemsCategory === "Stroller") {
                                                itemsType = OrderStrollerType.getIdentifier(order.items[index].itemsType);
                                                if (itemsType.indexOf('Double') >= 0) {
                                                    help = "A stroller designed for 2 children";
                                                } else if (itemsType.indexOf('Full Size') >= 0) {
                                                    help = "A full-size stroller with more features and comfort than an umbrella stroller, but is usually more bulky and heavy";
                                                } else if (itemsType.indexOf('Umbrella') >= 0) {
                                                    help = "A lightweight, basic stroller that folds up easily (like an umbrella). Often has a sling type seat. Best for babies that can sit up on their own.";
                                                }
                                            }
                                            else if (itemsCategory === "Toys") {
                                                itemsType = OrderToyType.getIdentifier(order.items[index].itemsType);
                                                if (itemsType.indexOf('Bike Boy') >= 0) {
                                                    help = "Boy's bicycle, please add any requests to comments";
                                                } else if (itemsType.indexOf('Bike Girl') >= 0) {
                                                    help = "Girl's bicycle, please add any requests to comments";
                                                } else if (itemsType.indexOf('Bike Helmet') >= 0) {
                                                    help = "Protective helmet for bike riding or other activities";
                                                } else if (itemsType.indexOf('Birthday Gift') >= 0) {
                                                    help = "";
                                                } else if (itemsType.indexOf('Books-4') >= 0) {
                                                    help = "4 books based on age";
                                                } else if (itemsType.indexOf("CD's") >= 0) {
                                                    help = "Music CDs";
                                                } else if (itemsType.indexOf("DVD's") >= 0) {
                                                    help = "DVD movies";
                                                } else if (itemsType.indexOf('Games') >= 0) {
                                                    help = "Various board games selected based on age";
                                                } else if (itemsType.indexOf('Puzzles') >= 0) {
                                                    help = "Various puzzles selected based on age";
                                                } else if (itemsType.indexOf('Toy- Lg') >= 0) {
                                                    help = "Larger toys based on age";
                                                } else if (itemsType.indexOf('Toy- Sm') >= 0) {
                                                    help = "Smaller toys based on age";
                                                }
                                            }
                                            else if (itemsCategory === "Equipment") {
                                                itemsType = OrderEquipmentType.getIdentifier(order.items[index].itemsType);
                                                if (itemsType.indexOf('Backpack') >= 0) {
                                                    help = "A (school) bag with shoulder straps that allow it to be carried on one's back";
                                                } else if (itemsType.indexOf('Baby Monitor') >= 0) {
                                                    help = "A radio system used to remotely listen to sounds made by an infant";
                                                } else if (itemsType.indexOf('Bassinette') >= 0) {
                                                    help = "An extra small bed specifically for babies from birth to about four months";
                                                } else if (itemsType.indexOf('Bathtub') >= 0) {
                                                    help = "A large portable container to bathe babies safely. Usually has a contoured seat and a sloped back so the baby lies at an angle. Best for 0-6 months";
                                                } else if (itemsType.indexOf('Booster Seat - Table') >= 0) {
                                                    help = "A reclining chair that bounces/moves up and down. For ages 0-6 months";
                                                } else if (itemsType.indexOf('Breast Feeding Pillow') >= 0) {
                                                    help = "A crescent shaped pillow used to support the baby while breastfeeding";
                                                } else if (itemsType.indexOf('Bumbo') >= 0) {
                                                    help = "A one-piece seat that is made entirely of a low-density foam. It has a deep seat with a high back and sides. For babies that can hold up their head.";
                                                } else if (itemsType.indexOf('Changing Pad') >= 0) {
                                                    help = 'A contoured 3" thick cushion used for changing baby’s diaper. A Portable Changing Pad is just a thin mat that folds compactly';
                                                } else if (itemsType.indexOf('Changing Pad Cover') >= 0) {
                                                    help = "A fitted sheet for the changing pad";
                                                } else if (itemsType.indexOf('Crib- Play & Pack') >= 0) {
                                                    help = 'Portable crib that can fold up, approximately 40" x 28"';
                                                } else if (itemsType.indexOf('Diaper Bag') >= 0) {
                                                    help = "Bag designed to carry diapers, wipes and other items for baby/toddler";
                                                } else if (itemsType.indexOf('Diaper Genie') >= 0) {
                                                    help = "Container for disposable diaper similar to garbage can but with odor control";
                                                } else if (itemsType.indexOf('High Chair') >= 0) {
                                                    help = "A small chair with a tray that is raised high enough for the child to sit at table height";
                                                } else if (itemsType.indexOf('Lunch Box') >= 0) {
                                                    help = "A container to hold a child’s lunch";
                                                } else if (itemsType.indexOf('Potty Seat') >= 0) {
                                                    help = 'A Potty Seat (circle shape) is placed on the regular toilet seat to train a child to use the toilet.';
                                                } else if (itemsType.indexOf('Safety Gate') >= 0) {
                                                    help = "A gate that is placed in a doorway";
                                                } else if (itemsType.indexOf('Safety Locks') >= 0) {
                                                    help = "Devices that keep outlets covered and prevent toddlers from opening drawers and doors";
                                                } else if (itemsType.indexOf('School Supplies') >= 0) {
                                                    help = "Basics items for school such as pen, pencil, crayons, notebook, binder, etc.";
                                                } else if (itemsType.indexOf('Indoor Swing') >= 0) {
                                                    help = "A hanging seat that swings and is suspended between v-shaped poles, usually about 3 feet high. Best for 0-6 months";
                                                } else if (itemsType.indexOf('Tub Seat') >= 0) {
                                                    help = "A soft, mesh sling that can be placed in regular bathtub or large sink to bathe a baby. Best for 0-6 months";
                                                } else if (itemsType === "Front Pack- Infant") {
                                                    help = "<ul>Front Pack - Infant: A device to carry an infant on the body. Mention preferred type in item comments." +
                                                        "<li>A Front Pack is like a reverse backpack with straps over the shoulder and a padded carrying 'pack' that can be placed on your chest.</li>" +
                                                        "<li> A Wrap is a super long piece of stretchy fabric you tie around your shoulders and mid-section to create a carrier.</li>" +
                                                        "<li> A Sling is a wide piece of fabric that goes over one shoulder and across your torso.</li>" +
                                                        "</ul>";
                                                }
                                            }
                                            Bert.defaults.hideDelay = 10000;
                                            Bert.alert(help, 'info', 'fixed-top');
                                            if (document.querySelectorAll('.bert-content').length > 0) {
                                                let el = document.querySelectorAll('.bert-content')[0];
                                                if (el) {
                                                    el.innerHTML = `<p>${help}</p>`;
                                                }
                                            }
                                        }}
                                    >Details
                                    </button>

                                    <CCButton
                                        disabled={!mayEdit}
                                        type="remove"
                                        onClick={(event) => {
                                            event.preventDefault();
                                            order.items.splice(index, 1);
                                            this.setState({ items: order.items });
                                            Meteor.call('/order/update', order, handleError);
                                        }}
                                    />
                                </div>
                            </>
                        ))}
                        <CCButton
                            disabled={!mayEdit}
                            type="add"
                            onClick={(event) => {
                                event.preventDefault();
                                if (!order.items) order.items = [];
                                if (
                                    order.items.length >= 1 && !order.items[order.items.length - 1]
                                ) {
                                    return;
                                }
                                order.items.push({
                                    "name": "",
                                    "itemsCategory": null,
                                    "itemsType": null,
                                    "itemsSize": null,
                                    "itemsComment": ""
                                });
                                Meteor.call('/order/update', order, handleError);
                            }}>
                            Add New Item
                        </CCButton>
                    </fieldset>
                    <div className="actions">
                        <Link
                            to="/"
                            onClick={(event) => {
                                event.preventDefault();
                                this.setState({ saved: true });
                                let message = 'Order saved.';
                                Bert.alert(message, 'success');
                                if (document.querySelectorAll('.bert-content').length > 0) {
                                    let el = document.querySelectorAll('.bert-content')[0];
                                    if (el) {
                                        el.innerHTML = `<p>${message}</p>`;
                                    }
                                }

                                Meteor.call('/order/update', order, handleError);
                            }}
                        >{CC.icon('check')} Saved</Link>
                        {/*<Link
                             to="/"
                             onClick={(event) => {
                             event.preventDefault();

                             const copiedOrder = new Order({
                             ordererId: order.ordererId,
                             status: order.status,
                             childName: order.childName,
                             childZip: order.childZip,
                             childAge: order.childAge,
                             childWeight: order.childWeight,
                             childSex: order.childSex,
                             //itemCategory: order.itemCategory,
                             //itemType: order.itemType,
                             //itemSize: order.itemSize,
                             pickupLocation: order.pickupLocation,
                             pickupDate: order.pickupDate,
                             shoeSize: order.shoeSize,
                             comments: order.comments + " - copied on " + helperDateTime(new Date()),
                             items: order.items,
                             });

                             console.log('Order/View.jsx:copyOrder:order:', JSON.stringify(copiedOrder));
                             Meteor.call(
                             '/order/new',
                             copiedOrder,
                             handleError({
                             callback(orderId) {
                             console.log('Order/AdminTable.jsx:copiedOrder:id:' + orderId);
                             history.push(`/order/${orderId}`);
                             },
                             })
                             );
                             }}
                             >{CC.icon('content-copy')} Copy This Order</Link>*/}
                        <Link
                            to="/order/new"
                        >{CC.icon('create')} Add a New Order</Link>
                        {/*<DashboardLink />*/}
                    </div>
                </form>
            </div>
        );
    }
}

export default container((props, onData) => {
    const orderId = props.match.params.orderId;
    const adminSubscription = Meteor.subscribe('/user/administrator/list');
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const caseworkerSubscription = Meteor.subscribe('/user/caseworker/list');
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    const subscription = Meteor.subscribe('/order/read', orderId);

    if (volunteerSubscription.ready() &&
        caseworkerSubscription.ready() &&
        adminSubscription.ready() &&
        businessSubscription.ready() &&
        subscription.ready()) {
        const order = Order.findOne(orderId);

        if (!order) {

            let message = 'No order found for ID:' + orderId + '. Redirecting to order list...';
            Bert.alert(message, 'danger');
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }
            if (Meteor.user().type === 'Caseworker') {
                props.history.push(`/order/cw`);
            } else {
                props.history.push(`/order`);
            }
        }

        let now = new Date();

        let daysBeforePickupDate = (new Date(order.pickupDate) - now) / 86400000;
        if (daysBeforePickupDate < 0) {
            daysBeforePickupDate = 0;
        }

        console.log('Order/View.jsx:render:pickupLocation:', order.pickupLocation);

        let dayOfWeekForPickupLocation =
            Business.find()
                .fetch()
                .filter(organization => organization.name === order.pickupLocation)
                .map(organization => organization.pickupDay);

        //console.log('Order/View.jsx:dayOfWeekForPickupLocation:1:', dayOfWeekForPickupLocation);
        dayOfWeekForPickupLocation = parseInt(dayOfWeekForPickupLocation, 0);

        //console.log('Order/View.jsx:dayOfWeekForPickupLocation:', dayOfWeekForPickupLocation);

        let cpNumber = 0;
        let location = 0;
        if (order.pickupLocation.indexOf("(") > 0) {
            cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
            cpNumber = cpNumber.replace("(", "").replace(")", "");
            console.log('Order/View.jsx:render:cpNumber:', cpNumber);
            let org = Business.find()
                .fetch()
                .filter(organization => organization.cpNumber === parseInt(cpNumber, 0))
                .map(organization => organization);

            dayOfWeekForPickupLocation = parseInt(org.map(organization => organization.pickupDay), 0);
            location = parseInt(org.map(organization => organization.location), 0);
            console.log('Order/View.jsx:org:', JSON.stringify(org));
            //console.log('Order/View.jsx:dayOfWeekForPickupLocation:2:', dayOfWeekForPickupLocation);
        }

        if (dayOfWeekForPickupLocation === 3) { // Wednesday
            daysBeforePickupDate = daysBeforePickupDate + 2;
        } else if (dayOfWeekForPickupLocation === 4) { // Thursday
            daysBeforePickupDate++;
        }

        let endEditDate = new Date(now.getTime() + 86400000 * (daysBeforePickupDate - 7));
        endEditDate.setHours(17);
        endEditDate.setMinutes(0);

        let diff = endEditDate.getTime() - now.getTime();

        //console.log('Order/View.jsx:render:order:', JSON.stringify(order));
        console.log('Order/View.jsx:render:order.pickupDate:', order.pickupDate);
        console.log('Order/View.jsx:render:dayOfWeekForPickupLocation:', dayOfWeekForPickupLocation);
        console.log('Order/View.jsx:render:daysBeforePickupDate:', daysBeforePickupDate);
        //console.log('Order/View.jsx:render:daysUntilPickupLocationDay:', daysUntilPickupLocationDay);
        console.log('Order/View.jsx:render:now:', now);
        console.log('Order/View.jsx:render:endEditDate:', endEditDate);
        //console.log('Order/View.jsx:render:endEditDateFuture:', endEditDateFuture);
        console.log('Order/View.jsx:render:diff:', diff);

        const mayEditBeforePickupDate = diff > 0 || (Meteor.user().type !== 'Caseworker');

        const ordererName = order.orderer().name;

        console.log('Order/View.jsx:render:order.orderer():', JSON.stringify(order.orderer()));
        let mayEdit = Meteor.user() && Meteor.user().may.update.order(order) && mayEditBeforePickupDate && Meteor.user().inactive != true;

        if (checkDate(order.pickupDate,dayOfWeekForPickupLocation) === true) {
            mayEdit = true;
        } else {
            mayEdit = false;
        }

        const history = props.history;

        onData(null, { order, mayEdit, history, ordererName, dayOfWeekForPickupLocation, endEditDate, location });
    }
}, OrderView);