import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import { Link, withRouter } from 'react-router-dom';


import moment from 'moment';
import { Bert } from 'meteor/themeteorchef:bert';

import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import User from '/imports/api/User';

import Order, { ChildAge, OrderStatus, OrderBasicItemCategory, OrderItemCategory } from '/imports/api/Order';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate, { helperDateTime } from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import CpTotalsAdminTable from '/imports/ui/components/CpTotalsOrderAdminTable.jsx';

import container from '/imports/ui/helpers/container';
import CC from '/imports/api';

import { handleError, CCButton } from '/imports/ui/helpers';

//console.log('Order/CpTotalsOrderAdminTable.jsx');

Order.cpTotalsAdminTable = new AdminTableDefinition();

Order.cpTotalsAdminTable.push({
    header: 'CP Number',
    sort: (a, b) => {
        a = parseInt(a[0].cpNumber, 0);
        b = parseInt(b[0].cpNumber, 0);
        return compare(a, b);
    },
    transform: order => (order.cpNumber),
    className: 'number',
    visible: true,
});

Order.cpTotalsAdminTable.push({
    header: 'Care Provider',
    sort: (a, b) => {
        a = a.careProvider;
        b = b.careProvider;
        return compare(a, b);
    },
    transform: order => (order.careProvider),
    className: 'careProvider',
    visible: true,
});

Order.cpTotalsAdminTable.push({
    header: 'Count',
    sort: (a, b) => {
        a = a.length;
        b = b.length;
        return compare(a, b);
    },
    transform: order => (order.length),
    className: 'number2',
    visible: true,
});

const CpTotalsOrderAdminTable = props =>
    <CpTotalsAdminTable collection={Order} {...props} />
    ;

export default container((props, onData) => {
    //const subscription = Meteor.subscribe('/order/list');

    let startDate;
    let endDate;
    const user = Meteor.user();
    let now = new Date();

    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
    } else if (user.type != 'Caseworker'
        && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
            || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
    } else {
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const subscription = Meteor.subscribe('/order/searchByDate', startDate, endDate);
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const caseworkerSubscription = Meteor.subscribe('/user/caseworker/list');
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (volunteerSubscription.ready() &&
        caseworkerSubscription.ready() &&
        businessSubscription.ready() &&
        subscription.ready()) {

        let orgId = "Unknown";
        let pickupDay = 5;
        let location = null;

        if (user) {
            orgId = user.profile.organizationId;

            if (orgId !== "None") {
                let biz = Business.find()
                    .fetch()
                    .filter(organization => organization._id === orgId);


                console.log("components/Order/CpTotalsOrderAdminTable.jsx:biz:" + JSON.stringify(biz));
                if (biz.length > 0) {
                    pickupDay = biz[0].pickupDay;
                    location = parseInt(biz[0].location, 0);
                }
            }
        }

        let businesses = Business.find({}, { sort: { 'name': 1 } })
            .fetch()
            .filter(organization => organization.location === location);

        // If no location exists, return all businesses
        if (location === null) {
            businesses = Business.find({}, { sort: { 'name': 1 } }).fetch()
        }

        console.log("components/Order/CpTotalsOrderAdminTable.jsx:location:" + location);
        //console.log("components/Order/CpTotalsOrderAdminTable.jsx:orders.length:" + orders.length);
        console.log("components/Order/CpTotalsOrderAdminTable.jsx:businesses.length:" + businesses.length);
        onData(null, { businesses, startDate, endDate, history, props });
    }
}, withRouter(CpTotalsOrderAdminTable));