import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import PropTypes from 'prop-types';

import CC from '/imports/api';

import Order, { OrderStatus, OrderItemType, OrderItemSize, ChildSex } from '/imports/api/Order';

import Drawer from '/imports/ui/components/helpers/Drawer';
import container from '/imports/ui/helpers/container';
import BasicGoogleMap from '/imports/ui/components/helpers/BasicGoogleMap';

import OrderAdminTable from './AdminTable';
//import CaseworkerOrderAdminTable from './CaseworkerAdminTable';

import {
    SubHeader,
    Breadcrumbs,
    Select,
    Checkbox,
} from '/imports/ui/helpers';

class OrderListFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            focusId: undefined,
            city: undefined,
            active: false,
        };
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state !== nextState) this.props.onChange(nextState);
    }

    render() {
        return (
            <div className="internship list filter">
                <label htmlFor="keyword">
                    <input
                        type="text"
                        id="keyword"
                        placeholder="Search Keyword"
                        value={this.state.keyword}
                        onChange={(event) => {
                        const keyword = event.target.value;
                        this.setState({ keyword });
                    }}
                    />
                </label>

                {Meteor.user().type !== 'Caseworker' ?
                <Select
                    id="pickupLocation"
                    value={this.state.pickupLocation}
                    placeholder="Select Care Provider"
                    options={_.uniq(this.props.orders.map(order => order.pickupLocation)).map(pickupLocation => Object({ value: pickupLocation, label: pickupLocation }))}
                    onChange={(selectedOption) => {
                    this.setState({ city: selectedOption ? selectedOption.value : undefined });
                  }}
                /> : null}

                <Select
                    id="itemType"
                    value={this.state.itemType}
                    placeholder="Select Item Type"
                    options={_.uniq(this.props.orders.map(order => order.itemType)).map(itemType => Object({ value: itemType, label: itemType }))}
                    onChange={(selectedOption) => {
                    this.setState({ city: selectedOption ? selectedOption.value : undefined });
                  }}
                />

                <label htmlFor="active">
                    Active
                    <Checkbox
                        id="active"
                        checked={this.state.active}
                        onChange={(event) => {
              event.preventDefault();
              this.setState({ active: event.target.checked });
            }}
                    />
                </label>

                <input
                    type="button"
                    value="Reset"
                    onClick={(event) => {
            event.preventDefault();
            this.setState({
              keyword: '',
              focusId: undefined,
              city: undefined,
              active: false,
            });
          }}
                />
            </div>
        );
    }
}

const OrderListItem = ({ order }) => (
    <li id={order._id} >
        <Link to={`/order/${order._id}`}>
            <header>
                <h2>
                    Child Name {order.childName}
                </h2>
                <span className="date">Pickup Date {moment(order.pickupDate).format('MMM Do')}</span>

            </header>
            <div className="body">
        <span className="business">
          {order.itemType}
        </span>
                <span className="description">{order.pickupLocation.substr(0, 300)}…</span>
                <ul className="chips">
                    {order.paid ? (
                        <li className="paid">
                            <i className="mdi mdi-dollar"/>Active
                        </li>
                    ) : null}
                </ul>
                <button className="spirit">Visit</button>
            </div>
        </Link>
    </li>
);

OrderListItem.propTypes = {
    order: PropTypes.instanceOf(Order),
};

OrderListItem.defaultProps = {
    order: new Order(),
};

class OrderList extends React.Component {

    constructor(props) {
        //console.log("components/Order/List.jsx:constructor:props" + JSON.stringify(props));
        super(props);
        this.state = {
            filteredOrders: this.props.orders,
            props: this.props
        };
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            this.props.onChange(nextState);
            //console.log("components/Order/List.jsx:componentWillUpdate:true");
        } else {
            //console.log("components/Order/List.jsx:componentWillUpdate:false");
        }
    }

    render() {
        return <OrderAdminTable />;

        //if (Meteor.user().type !== 'Caseworker') {
        //    console.log("components/Order/List.jsx:user type:" + Meteor.user().type);
        //    console.log("components/Order/List.jsx:OrderAdminTable");
        //    return <OrderAdminTable />;
        //} else {
        //    console.log("components/Order/List.jsx:user type:" + Meteor.user().type);
        //    console.log("components/Order/List.jsx:CaseworkerOrderAdminTable");
        //    return <CaseworkerOrderAdminTable />;
        //}
        this.state = {
            filteredOrders: this.props.orders,
            status: this.props.status,
        };
        return (

            <div className="internship list body" status={this.props.status}>
                <SubHeader>
                    <Breadcrumbs breadcrumbs={[{ text: 'All Orders' }]}/>
                    <Drawer visible={!Meteor.user()}>
                        <OrderListFilter
                            orders={this.props.orders}
                            onChange={(filter) => {
                //console.log("components/Order/List.jsx:onChange:event:" + JSON.stringify(filter));
                const filteredOrders = [];
                this.props.orders.forEach((order) => {
                  const matchesFocus = filter.focusId
                    ? order.focusId === filter.focusId
                    : true;
                  const matchesActive = filter.active ? order.active : true;
                  const matchesKeyword = filter.keyword
                    ? ((order, keyword) => {
                        this.setState({
                          filteredOrders: this.props.orders,
                        });
                        if (order.title.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                          return true;
                        }
                        if (order.address.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                          return true;
                        }
                        if (
                          order.requirements.toLowerCase().indexOf(keyword.toLowerCase()) >= 0
                        ) {
                          return true;
                        }
                        if (
                          order.description.toLowerCase().indexOf(keyword.toLowerCase()) >= 0
                        ) {
                          return true;
                        }
                        if (
                          order.business() &&
                          order
                            .business()
                            .name.toLowerCase()
                            .indexOf(keyword.toLowerCase()) >= 0
                        ) {
                          return true;
                        }
                        return false;
                      })(order, filter.keyword)
                    : true;
                  const matchesCity = filter.city
                    ? order.addressParts.city === filter.city
                    : true;
                  if (matchesFocus && matchesActive && matchesKeyword && matchesCity) {
                    filteredOrders.push(order);
                  }
                });
                this.setState({ filteredOrders });
              }}
                        />
                    </Drawer>
                </SubHeader>
                <ul>
                    {Meteor.user() && Meteor.user().may.create.order() ? (
                        <li className="new">
                            <Link to="/order/new">{CC.icon('create')}New Order</Link>
                        </li>
                    ) : null}
                    {this.state.filteredOrders.map(order => (
                        <OrderListItem key={order._id} order={order}/>
                    ))}
                </ul>
                <BasicGoogleMap
                    containerElement={<div className="map" style={{ height: '500px' }} />}
                    mapElement={<div style={{ height: '500px' }} />}
                    markers={this.state.filteredOrders.map(order =>
            Object({
              latLng: {
                lat: order.locationLatLng[0],
                lng: order.locationLatLng[1],
              },
              _id: order._id,
              onClick: () => {
                const element = document.getElementById(order._id);
                const y =
                  element.offsetTop -
                  document.getElementById('subHeader').offsetHeight -
                  parseFloat(getComputedStyle(document.documentElement).fontSize);
                $('.target').removeClass('target');
                $(element).addClass('target');
                this.props.history.replace(`#${order._id}`);
                if (window.scroll) {
                  window.scroll({
                    top: y,
                    left: 0,
                    behavior: 'smooth',
                  });
                } else {
                  window.scrollTo(0, y);
                }
              },
            }))}
                />
            </div>
        );
    }
}

export default container((props, onData) => {
    //console.log("components/Order/List.jsx");

    const organizationId = ((user) => {
        if (!user) return undefined;
        if (props.match.params.organizationId) return props.match.params.organizationId;
        if (user.type === 'Administrator') return user.administer().map(organization => organization._id);
        if (user.type === 'Caseworker') return undefined;
        if (user.type === 'Volunteer') return undefined;
    })(Meteor.user());

    //const subscription = Meteor.subscribe('/order/list');
    //if (subscription.ready()) {
        const query = {};
        const status = props.match.params.status;
        //if (status) {
        //
        //    const filter = order => order.status === OrderStatus[status];
        //    const map = ({ orderId }) => orderId;
        //    const ids = Meteor.user()
        //        .orders.filter(filter)
        //        .map(map);
        //    query._id = {$in: ids};
        //}
        //let limit = 1;
        //if (Meteor.user().type !== 'Caseworker') {
        //    limit = 1;
        //}
        const orders = [];

        //Order.find(query,{sort: {'pickupDate': -1}, limit: limit}).fetch();
        //console.log("components/Order/List.jsx:orders.length:" + orders.length);
        //console.log("components/Order/List.jsx:user.type:" + Meteor.user().type);
        onData(null, {orders, status, props});
    //}
}, OrderList);


