import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import PropTypes from 'prop-types';

import CC from '/imports/api';

import Order, { OrderStatus, OrderItemType, OrderItemSize, ChildSex } from '/imports/api/Order';

import Drawer from '/imports/ui/components/helpers/Drawer';
import container from '/imports/ui/helpers/container';
import BasicGoogleMap from '/imports/ui/components/helpers/BasicGoogleMap';

import CaseworkerOrderAdminTable from './CaseworkerOrderAdminTable';

import {
    SubHeader,
    Breadcrumbs,
    Select,
    Checkbox,
} from '/imports/ui/helpers';


class CaseworkerOrderList extends React.Component {

    constructor(props) {
        //console.log("components/Order/List.jsx:constructor:props" + JSON.stringify(props));
        super(props);
        this.state = {
            filteredOrders: this.props.orders,
            props: this.props
        };
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            this.props.onChange(nextState);
            //console.log("components/Order/List.jsx:componentWillUpdate:true");
        } else {
            //console.log("components/Order/List.jsx:componentWillUpdate:false");
        }
    }

    render() {
        return <CaseworkerOrderAdminTable />;
    }
}

export default container((props, onData) => {
    console.log("components/Order/CaseworkerList.jsx");

    const subscription = Meteor.subscribe('/order/list');
    //const volSubscription = Meteor.subscribe('/user/volunteer/list');
    if (subscription.ready()) { //} && volSubscription.ready()) {
        const query = {};
        //if (organizationId) query.businessId = { $in: organizationId };
        const status = props.match.params.status;
        if (status) {

            const filter = order => order.status === OrderStatus[status];
            const map = ({ orderId }) => orderId;
            const ids = Meteor.user()
                .orders.filter(filter)
                .map(map);
            query._id = {$in: ids};
        }
        const orders = Order.find(query, {sort: {createdAt: -1}}).fetch();
        console.log("components/Order/CaseworkerList.jsx:orders.length:" + orders.length);
        onData(null, {orders, status, props});
    }
}, CaseworkerOrderList);


