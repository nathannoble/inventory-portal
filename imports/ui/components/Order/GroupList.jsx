import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import DateTime from 'react-datetime';
import moment from 'moment';

import listify from 'listify';

import PropTypes from 'prop-types';

import CC from '/imports/api';

import {
    OrderStatus, OrderFilter, OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType, OrderHygieneType, OrderBeddingType,
    OrderEquipmentType, OrderFeedingType, OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize, OrderBoyPantSize,
    OrderShoeSize, ChildAge, ChildSex
} from '/imports/api/Order';

import OrderUserOrg from '/imports/api/OrderUserOrg';

import User from '/imports/api/User';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import Administrator from '/imports/api/User/Administrator';
import Business from '/imports/api/Organization/Business';

import Drawer from '/imports/ui/components/helpers/Drawer';

import container from '/imports/ui/helpers/container';
import {
    SubHeader,
    Breadcrumbs,
    Select,
    Label,
} from '/imports/ui/helpers';

class GroupListFilter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            keyword: '',
            pickupLocation: undefined,
            businesses: props.businesses,
            startDate: props.startDate,
            endDate: props.endDate,
            dateRange: undefined,
            runDateQuery: undefined,
            status: undefined,
        };
    }

    componentDidUpdate(nextProps, nextState) {
        //console.log("components/Order/GroupList.jsx:GroupListFilter:componentDidUpdate:true");
        if (this.state !== nextState) this.props.onChange(nextState);
    }

    render() {
        return (
            <div className="order grouplist filter">
                <label htmlFor="keyword">
                    <input
                        type="text"
                        id="keyword"
                        name="keyword"
                        placeholder="Search Keyword"
                        value={this.state.keyword}
                        onChange={(event) => {
                            console.log("components/Order/GroupList.jsx:render:keyword:event.target.value:" + JSON.stringify(event.target.value));
                            const keyword = event.target.value;
                            this.setState({ keyword });
                        }}
                    />
                </label>

                {Meteor.user().type !== 'Caseworker' ?
                    <Select
                        id="pickupLocation"
                        name="pickupLocation"
                        value={this.state.pickupLocation}
                        placeholder="Select Care Provider"
                        options={_.uniq(this.state.businesses.map(organization => Object({ value: (organization.name), label: (organization.name + " (" + organization.cpNumber + ")") })))}
                        onChange={(selectedOption) => {
                            console.log("components/Order/GroupList.jsx:render:pickupLocation:selectedOption:" + JSON.stringify(selectedOption));
                            this.setState({ pickupLocation: selectedOption ? selectedOption.value : undefined });
                        }}
                    /> : null}

                <Label
                    id="startDate"
                    name="startDate"
                    element={DateTime}
                    text="Pick-up Start Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("components/Order/GroupList.jsx:render:startDate:selectedOption:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                startDate: selectedOption ? selectedOption.toDate() : undefined,
                                endDate: undefined,
                                dateRange: undefined
                            });
                        }
                    }}
                    timeFormat={false}
                    value={this.state.startDate}
                />

                <Label
                    name="endDate"
                    element={DateTime}
                    text="Pick-up End Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("components/Order/GroupList.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                endDate: selectedOption ? selectedOption.toDate() : undefined,
                                dateRange: undefined,
                            });

                            if (this.state.startDate !== undefined
                                && selectedOption.toDate() !== undefined) {

                                let startDate = this.state.startDate;
                                let endDate = selectedOption.toDate();

                                startDate.setHours(0);
                                startDate.setMinutes(0);
                                startDate.setSeconds(0);
                                startDate.setMilliseconds(0);

                                endDate.setHours(23);
                                endDate.setMinutes(59);
                                endDate.setSeconds(59);
                                endDate.setMilliseconds(999);

                                if (startDate < endDate) {
                                    this.props.history.push(`/order/report/${startDate.getTime()}/${endDate.getTime()}`);
                                }
                            }

                        }
                    }}
                    timeFormat={false}
                    value={this.state.endDate}
                />

                {/* <Select
                    id="status"
                    value={this.state.status}
                    placeholder="Select Status"
                    options={OrderStatus.getIdentifiers().map((status, index) => Object({ value: index, label: status }))}
                    onChange={(selectedOption) => {
                        this.setState({ status: selectedOption ? selectedOption.value : undefined });
                    }}
                />*/}

                <input
                    type="button"
                    value="Reset"
                    onClick={(event) => {
                        event.preventDefault();

                        // This Week (Until this Friday at 5:00 PM)
                        let now = new Date();
                        let nextFriday = 5;
                        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
                        if (daysUntilPickupLocationDay < 0) {
                            daysUntilPickupLocationDay += 7;
                        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
                            daysUntilPickupLocationDay += 7;
                        }

                        console.log("components/Order/Labels.jsx:render:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);

                        let startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
                        let endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));

                        startDate.setHours(0);
                        startDate.setMinutes(0);
                        startDate.setSeconds(0);
                        startDate.setMilliseconds(0);

                        endDate.setHours(23);
                        endDate.setMinutes(59);
                        endDate.setSeconds(59);
                        endDate.setMilliseconds(999);

                        this.setState({
                            keyword: '',
                            pickupLocation: undefined,
                            startDate: startDate,
                            endDate: endDate,
                            dateRange: undefined,
                            runDateQuery: undefined,
                            status: undefined,
                        });
                    }}
                />

            </div>
        );
    }
};

const GroupListItem = ({ order, index }) => (
    order.items.map((item, itemIndex) => {

        //#[CP#] - [Care Provider Name], [Case Worker Name] [Case Worker Phone]
        //Child: [child Name] Age: [age] Wt: [weight] Sex: [sex] ID: [internal id] Zip: [zip code]
        //[item] [item type] [sizing/options]
        //[comments]

        return (
            <div id={order._id + itemIndex} key={order._id + itemIndex} className={item.newCareProvider ? 'page-break-before' : null}>
                <Link to={`/order/${order._id}`}>
                    <span>#{order.cpNumber} - {order.pickupLocation}, {order.ordererName} {order.phone} (Pickup: {moment(order.pickupDate).format('MMM Do')})</span><br />
                    <span>Child: <b>{order.childName}</b>
                        Age: <b>
                            {/*ChildAge.getIdentifier(ChildAge.getValues()[order.childAge])*/}
                            {ChildAge.getIdentifier(order.childAge)}
                        </b>
                        Wt: <b>{order.childWeight}</b> Sex: <b>{ChildSex.getIdentifier(order.childSex)}</b>
                        &nbsp;ID: <b>{order.comments}</b> Zip: <b>{order.childZip}</b></span><br />
                    <span>
                        <span><b>{OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory])}</b>:&nbsp;
                        </span>

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Clothing Bag' ? (
                            <>
                                <span >
                                    <b>{OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType])}</b>&nbsp;
                                </span>
                                <span >
                                    <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                    {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Boy'
                                        || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Neutral' ? (
                                        <>
                                            <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b></b></span>
                                        </>
                                    ) : null}
                                    {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Girl' ? (
                                        <>
                                            <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b></b></span>
                                        </>
                                    ) : null}
                                    {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Boy' || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Boy' ? (
                                        <>
                                            <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                        </>
                                    ) : null}
                                    {OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Girl'
                                        || OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Girl' ? (
                                        <>
                                            <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                        </>
                                    ) : null}
                                </span>
                            </>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Other Clothing' ? (
                            <>
                                <span >
                                    <b>{OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType])}</b>&nbsp;
                                </span>
                                <span >
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Gloves & Hat' ? (
                                        <>
                                            <span>Shirt: <b>{ }</b>&emsp;</span>
                                            <span>Pant: <b>{ }</b>&emsp;</span>
                                            <span>Shoe: <b>{ }</b></span>
                                        </>
                                    ) : null}
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Jacket' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Shirts (2)' ? (
                                        <>
                                            <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                            <span>Pant: <b>{ }</b>&emsp;</span>
                                            <span>Shoe: <b>{ }</b></span>
                                        </>
                                    ) : null}
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Maternity' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pajamas - Girls' ? (
                                        <>
                                            <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                            <span>Pant: <b>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b>{ }</b></span>
                                        </>
                                    ) : null}
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pajamas - Boys' ? (
                                        <>
                                            <span>Shirt: <b>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsSize])}</b>&emsp;</span>
                                            <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b>{ }</b></span>
                                        </>
                                    ) : null}
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Pants (2 Pair)' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Underwear (2 Pair)' ? (
                                        <>
                                            <span>Shirt: <b>{ }</b>&emsp;</span>
                                            <span>Pant: <b>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item.pantSize])}</b>&emsp;</span>
                                            <span>Shoe: <b>{ }</b></span>
                                        </>
                                    ) : null}
                                    {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Shoes' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item.itemsType]) === 'Socks (2 Pair)' ? (
                                        <>
                                            <span>Shirt: <b>{ }</b>&emsp;</span>
                                            <span>Pant: <b>{ }</b>&emsp;</span>
                                            <span>Shoe: <b>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item.shoeSize])}</b></span>
                                        </>
                                    ) : null}
                                </span>
                            </>

                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Diapers' ? (
                            <>
                                <span >
                                    <b>{OrderDiaperType.getIdentifier(OrderDiaperType.getValues()[item.itemsType])}</b>&nbsp;
                                </span>
                            </>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Hygiene Items' ? (
                            <span >
                                <b>{OrderHygieneType.getIdentifier(OrderHygieneType.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Wipes' ? (
                            <span  >
                                <b>Size: {OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Bedding' ? (
                            <span >
                                <b>{OrderBeddingType.getIdentifier(OrderBeddingType.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Equipment' ? (
                            <span  >
                                <b>
                                    {OrderEquipmentType.getIdentifier(OrderEquipmentType.getValues()[item.itemsType])}
                                </b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Feeding' ? (
                            <span  >
                                <b>{OrderFeedingType.getIdentifier(OrderFeedingType.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Toys' ? (
                            <span  >
                                <b>{OrderToyType.getIdentifier(OrderToyType.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}

                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item.itemsCategory]) === 'Stroller' ? (
                            <span  >
                                <b>{OrderStrollerType.getIdentifier(OrderStrollerType.getValues()[item.itemsType])}</b>&nbsp;
                            </span>
                        ) : null}
                        <br /><span className="itemCommentsLabel"><span>Comments:</span> <span>{item.itemsComment}</span>
                        </span>
                    </span>
                </Link>
            </div>
        );
    }));

GroupListItem.propTypes = {
    order: PropTypes.instanceOf(OrderUserOrg),
};

GroupListItem.defaultProps = {
    order: new OrderUserOrg(),
};

class GroupList extends React.Component {

    constructor(props) {
        //console.log("components/Order/GroupList.jsx:constructor" );
        super(props);

        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            filter: undefined
        };
    }

    componentDidUpdate(nextProps, nextState) {
        if (this.state !== nextState) {
            if (this.props.onChange) this.props.onChange(nextState);
            //console.log("components/Order/List.jsx:GroupList:componentDidUpdate:true");
        } else {
            //console.log("components/Order/List.jsx:GroupList:componentDidUpdate:false");
        }
    }

    render() {

        let orders = this.props.orders;

        // If filter defined
        let filteredOrders = [];
        if (this.state.filter) {
            let filter = this.state.filter;
            orders.forEach((order) => {
                const matchesKeyword = filter.keyword
                    ? ((order, keyword) => {
                        // this.setState({
                        //     filteredOrders: orders,
                        // });

                        if (order.childName.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        if (order.pickupLocation.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        if (order.comments.toLowerCase().indexOf(keyword.toLowerCase()) >= 0) {
                            return true;
                        }

                        return false;
                    })(order, filter.keyword)
                    : true;
                const matchesPickupLocation = filter.pickupLocation ? order.pickupLocation === filter.pickupLocation : true;
                const matchesStatus = (filter.status || filter.status === 0) ? order.status === filter.status : true;

                if (matchesStatus && matchesKeyword && matchesPickupLocation) {
                    filteredOrders.push(order);
                }
            });

            if (!this.state.filteredOrders || (filteredOrders.length !== this.state.filteredOrders.length)) {
                console.log("components/Order/GroupList.jsx:render:setState:orders");
                this.setState({
                    filteredOrders: filteredOrders,
                });
            }

        } else {
            filteredOrders = orders;
        }

        filteredOrders.forEach((order) => {

            let cpNumber = 0;
            let name = "";
            if (order.pickupLocation.indexOf("(") > 0) {
                cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
                name = order.pickupLocation.replace(" " + cpNumber, "");
                cpNumber = cpNumber.replace("(", "").replace(")", "");
                order.cpNumber = cpNumber;
                order.pickupLocation = name;
            }

            let phone = order.userPhone[0];
            let ordererName = order.userName[0];

            order.ordererName = ordererName;
            order.phone = phone;
        });

        filteredOrders = filteredOrders.sort(function (a, b) {
            return (
                (new Date(b.pickupDate).setHours(0, 0, 0, 0) - new Date(a.pickupDate).setHours(0, 0, 0, 0))
                || (parseInt(a.cpNumber, 0) - parseInt(b.cpNumber, 0))
                || (a.ordererName && b.ordererName && a.ordererName.toUpperCase() < b.ordererName.toUpperCase() ? -1 : (a.ordererName && b.ordererName && a.ordererName.toUpperCase() > b.ordererName.toUpperCase() ? 1 : 0))
                || (a.childName && b.childName && a.childName.toUpperCase() < b.childName.toUpperCase() ? -1 : (a.childName && b.childName && a.childName.toUpperCase() > b.childName.toUpperCase() ? 1 : 0))
            );
        });

        // Place flag when starting a new Care Provider
        var cpNumberPrev = 0;
        var cpNumberThis = 0;
        filteredOrders.forEach((order) => {
            order.items.forEach((item) => {
                cpNumberThis = order.cpNumber;
                if (cpNumberThis !== cpNumberPrev && cpNumberPrev !== 0) {
                    //console.log ("NEW:" + JSON.stringify(item));
                    item.newCareProvider = true;
                } else {
                    //console.log ("NOT NEW:" + JSON.stringify(item));
                    item.newCareProvider = false;
                }
                cpNumberPrev = cpNumberThis;
            });
        });

        //console.log("components/Order/GroupList.jsx:constructor:filteredOrders:" + JSON.stringify(filteredOrders));

        return (
            <div id="reportRoot" className="order grouplist body" status={this.props.status}>
                <SubHeader className="no-print">
                    <Breadcrumbs className="no-print" breadcrumbs={[{ text: 'All Orders' }]} />
                    <Drawer className="no-print" visible={!Meteor.user()}>
                        <GroupListFilter
                            className="no-print"
                            history={this.props.history}
                            orders={this.props.orders}
                            businesses={this.props.businesses}
                            startDate={this.props.startDate}
                            endDate={this.props.endDate}
                            onChange={(filter) => {
                                if (filter.runDateQuery) {

                                    filter.startDate.setHours(0);
                                    filter.startDate.setMinutes(0);
                                    filter.startDate.setSeconds(0);
                                    filter.startDate.setMilliseconds(0);

                                    filter.endDate.setHours(23);
                                    filter.endDate.setMinutes(59);
                                    filter.endDate.setSeconds(59);
                                    filter.endDate.setMilliseconds(999);

                                    console.log("components/Order/PrintLabels.jsx:runDateQuery");
                                    filter.runDateQuery = undefined;
                                    this.props.history.push(`/order/printLabels/${filter.startDate.getTime()}/${filter.endDate.getTime()}`);

                                } else {
                                    this.setState({ filter: filter });
                                }
                            }}
                        />
                    </Drawer>

                </SubHeader>
                <ul className="no-print">
                    {Meteor.user() && Meteor.user().may.create.order() ? (
                        <li className="new no-print">
                            <Link to="/" onClick={(event) => {
                                event.preventDefault();
                                window.print();

                            }}
                            >{CC.icon('printer')} Print</Link>
                        </li>
                    ) : null}
                </ul>
                {/* <ul>
                    <li>
                        <h4>Orders Report:&nbsp;{this.state.startDate.toLocaleDateString() + ' - ' + this.state.endDate.toLocaleDateString()}</h4>
                    </li>
                </ul>*/}
                <div className="ul">
                    {filteredOrders.map((order, index) => (
                        <GroupListItem index={index} key={order._id} order={order} />
                    ))}
                </div>
            </div>
        );
    }
}

export default container((props, onData) => {

    let startDate;
    let endDate;
    const user = Meteor.user();
    let now = new Date();

    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
    } else if (user.type != 'Caseworker'
        && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
            || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
    } else {
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        console.log("components/Order/GroupList.jsx:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const subscription = Meteor.subscribe('/orderUserOrg/searchByDate', startDate, endDate);
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const caseworkerSubscription = Meteor.subscribe('/user/caseworker/list');
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (volunteerSubscription.ready() &&
        caseworkerSubscription.ready() &&
        businessSubscription.ready() &&
        subscription.ready()) {

        const user = Meteor.user();

        let orgId = "Unknown";
        let location = null;

        if (user) {
            orgId = user.profile.organizationId;

            if (orgId !== "None") {
                let biz = Business.find()
                    .fetch()
                    .filter(organization => organization._id === orgId);

                console.log("components/Order/GroupList.jsx:biz:" + JSON.stringify(biz));
                if (biz.length > 0) {
                    location = parseInt(biz[0].location, 0);
                }
            }
        }

        const orders = OrderUserOrg.find({}, { sort: { 'pickupDate': -1 } })
            .fetch();

        let businesses = Business.find({}, { sort: { 'name': 1 } })
            .fetch()
            .filter(organization => organization.location === location);

        // If no location exists, return all businesses
        if (location === null) {
            businesses = Business.find({}, { sort: { 'name': 1 } }).fetch()
        }

        console.log("components/Order/GroupList.jsx:location:" + location);
        console.log("components/Order/GroupList.jsx:orders.length:" + orders.length);
        console.log("components/Order/GroupList.jsx:businesses.length:" + businesses.length);
        onData(null, { orders, businesses, startDate, endDate, history, props });
    }
}, withRouter(GroupList));