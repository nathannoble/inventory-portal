import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import { Link, withRouter } from 'react-router-dom';

import moment from 'moment';
import { Bert } from 'meteor/themeteorchef:bert';

import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import User from '/imports/api/User';

import Order, { ChildAge,OrderStatus, OrderBasicItemCategory, OrderItemCategory } from '/imports/api/Order';
import OrderUserOrg from '/imports/api/OrderUserOrg';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate, {helperDateTime} from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import CWAdminTable from '/imports/ui/components/CWAdminTable';

import container from '/imports/ui/helpers/container';
import CC from '/imports/api';

import {handleError,CCButton} from '/imports/ui/helpers';

//console.log('Order/CaseworkerOrderAdminTable.jsx');

OrderUserOrg.caseWorkerAdminTable = new AdminTableDefinition();

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Child Name',
    sort: (a, b) => {
        a = a.childName;
        b = b.childName;
        return compare(a, b);
    },
    //transform: order => (order.childName),
    transform: (order, willExport = false) => {
        if (willExport) {
            return (
                <a title={order.childName} href={order.path}>
                    {order.childName}
                </a>
            );
        }
        return (
            <Link title={order.childName} to={order.path}>
                {order.childName}
            </Link>
        );
    },
    className: 'childName',
    visible: true,
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Internal ID',
    sort: (a, b) => {
        a = a.comments;
        b = b.comments;
        return compare(a, b);
    },
    transform: order => (order.comments),
    className: 'status',
    visible: true,
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Child Age',
    sort: (a, b) => {
        a = a.childAge;
        b = b.childAge;
        return compare(a, b);
    },
    //transform: order => (ChildAge.getIdentifier(ChildAge.getValues()[order.childAge])),
    transform: order => (ChildAge.getIdentifier(order.childAge)),
    className: 'status',
    visible: true,
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Date Created',
    sort: (a, b) => {
        a = a.createdAt;
        b = b.createdAt;
        return compare(a, b);
    },
    transform: order => helperDate(order.createdAt),
    className: 'createdAt',
    visible: true,
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Pickup Date',
    sort: (a, b) => {
        a = a.pickupDate;
        b = b.pickupDate;
        return compare(a, b);
    },
    transform: order => helperDate(order.pickupDate),
    className: 'pickupDate',
    visible: true
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Status',
    sort: (a, b) => {
        a = a.status;
        b = b.status;
        return compare(a, b);
    },
    transform: order => (OrderStatus.getIdentifier(order.status)),
    className: 'status',
    visible: true,
});

OrderUserOrg.caseWorkerAdminTable.push({
    header: 'Delete',
    transform: orderUserOrg => (
        <CCButton
            type="remove"
            onClick={(event) => {
            event.preventDefault();
            console.log('Order/CaseworkerOrderAdminTable.jsx:deleteOrder');
            const orders = Order.find({_id: orderUserOrg._id}, {sort: {createdAt: -1}}).fetch();
            if (orders.length > 0) {

                let order = orders[0];

                // Edit date
                let daysBeforePickupDate = (new Date(order.pickupDate) - new Date()) / 86400000;
                if (daysBeforePickupDate < 0) {
                    daysBeforePickupDate = 0
                }

                let dayOfWeekForPickupLocation = 4;  // Assume it's Friday
                let daysUntilPickupLocationDay = dayOfWeekForPickupLocation - (new Date()).getDay();

                if (daysUntilPickupLocationDay <= 0) {
                    daysUntilPickupLocationDay += 7;
                }

                let endEditDate = new Date();
                endEditDate.setDate(endEditDate.getDate() + daysBeforePickupDate - 6);

                const mayEditBeforePickupDate = daysBeforePickupDate >= daysUntilPickupLocationDay;
                const mayEdit = Meteor.user() && Meteor.user().may.delete.order(order) && mayEditBeforePickupDate;
                if (mayEdit) {
                    console.log('Order/CaseworkerOrderAdminTable.jsx:deleteOrder:order:' + JSON.stringify(order));
                    Meteor.call('/order/delete', order, handleError({
                                callback() {
                                    Bert.defaults.hideDelay = 10000;
                                    let message = 'Order deleted.';
                                    Bert.alert(message, 'success');
                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                       let el = document.querySelectorAll('.bert-content')[0];
                                       if (el) {
                                           el.innerHTML = `<p>${message}</p>`;
                                       }
                                    }
                                    console.log('Order/CaseworkerOrderAdminTable.jsx:order deleted');
                                }}));
                } else {
                    console.log('Order/CaseworkerOrderAdminTable.jsx:no permissions to edit order');
                    let message = 'You do not have permissions to delete.  Order is not editable by you or its edit date has past.';
                    Bert.alert(message, 'danger');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                       let el = document.querySelectorAll('.bert-content')[0];
                       if (el) {
                           el.innerHTML = `<p>${message}</p>`;
                       }
                    }

                }
        } else {
                console.log('Order/CaseworkerOrderAdminTable.jsx:no order found');
                Bert.defaults.hideDelay = 10000;
                let message = 'Order not found.';
                Bert.alert(message, 'warning');
                if (document.querySelectorAll('.bert-content').length > 0) {
                   let el = document.querySelectorAll('.bert-content')[0];
                   if (el) {
                       el.innerHTML = `<p>${message}</p>`;
                   }
                }
            }
        }}
        />
    ),
    className: 'delete',
    visible: true,
}
);

const CaseworkerOrderAdminTable = props =>
<CWAdminTable collection={OrderUserOrg} {...props} />
;

export default container((props, onData) => {

    let startDate;
    let endDate;

    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate,0)/1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate,0)/1000);
    } else {
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        console.log("components/Order/CaseworkerOrderAdminTable.jsx:render:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const orderUserOrgSubscription = Meteor.subscribe('/orderUserOrg/searchByDate', startDate, endDate);
    const orderSubscription = Meteor.subscribe('/order/searchByDate', startDate, endDate);
    const cwSubscription = Meteor.subscribe('/user/caseworker/list');
    const volSubscription = Meteor.subscribe('/user/volunteer/list');
    if (orderSubscription.ready() && orderUserOrgSubscription.ready() && volSubscription.ready() && cwSubscription.ready()) {
    onData(null, {startDate,endDate, history});
}
}, withRouter(CaseworkerOrderAdminTable));
