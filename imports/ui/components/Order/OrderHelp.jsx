import React from 'react';
import { Link as Link } from 'react-router-dom';

import PropTypes from 'prop-types';

import CC from '/imports/api';

import {
    SubHeader,
    Breadcrumbs,
    Select,
    Label,
    Checkbox,
    CCButton,
    Loader,
    handleError,
} from '/imports/ui/helpers';


class OrderHelp extends React.Component {

    constructor(props) {
        console.log("components/Order/OrderHelp.jsx:constructor:props" + JSON.stringify(props));
        super(props);
        this.state = {
            filteredOrders: this.props.orders,
            //props:this.props
        };
    }

    render() {
        return (
            <div className="adminTable order">
                <SubHeader className="no-print">
                    <Breadcrumbs className="no-print" breadcrumbs={[{ text: 'Help' }]}/>
                </SubHeader>
                <div className="help">
                    <h3><strong>Help/Resources for Ordering from NCO</strong></h3>
                    <p>These documents will be updated as changes to the system are made. Please also check the Announcements at the top of the dashboard for further instructions.</p>
                    <p><a href="https://docs.google.com/document/d/1FLExhpMEZ32R01QURGI6rvuCiadG3RqzR8BYhkDUDss/edit?usp=sharing" target="_blank" rel="noopener">Ordering Dates/Email Updates</a> (Google Doc) Updated Regularly</p>
                    <p><a href="https://nwchildrens.org/order-instructions" target="_blank" rel="noopener">Ordering Instructions</a> (Google Doc) Updated 10/30/19</p>
                    <p><a href="https://nwchildrens.org/equipment-descriptions" target="_blank" rel="noopener">Equipment Descriptions</a> (Google Doc) Updated 10/31/19</p>
                    <p>If you can not find what you are looking for, please email <a href="mailto:website@nwchildrens.org" target="_blank" rel="noopener">website@nwchildrens.org</a></p>
                </div>
            </div>


        );
    }
}

export default OrderHelp;


