import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import { Link, withRouter } from 'react-router-dom';

import moment from 'moment';
import { Bert } from 'meteor/themeteorchef:bert';

import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';
import Caseworker from '/imports/api/User/Caseworker';
import User from '/imports/api/User';

import { ChildAge, OrderStatus, OrderBasicItemCategory, OrderItemCategory } from '/imports/api/Order';
import OrderUserOrg from '/imports/api/OrderUserOrg';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate, { helperDateTime } from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import CenterTotalsAdminTable from '/imports/ui/components/CenterTotalsOrderAdminTable.jsx';

import container from '/imports/ui/helpers/container';
import CC from '/imports/api';

import { handleError, CCButton } from '/imports/ui/helpers';

//console.log('Order/CenterTotalsOrderAdminTable.jsx');

OrderUserOrg.centerTotalsAdminTable = new AdminTableDefinition();

//Order.centerTotalsAdminTable.push({
//    header: 'CP Number',
//    sort: (a, b) => {
//        a = a[0].cpNumber;
//        b = b[0].cpNumber;
//        return compare(a, b);
//    },
//    transform: order => (order[0].cpNumber),
//    className: 'status',
//    visible: true,
//});

OrderUserOrg.centerTotalsAdminTable.push({
    header: 'Location',
    sort: (a, b) => {
        a = a[0].location[0];
        b = b[0].location[0];
        return compare(a, b);
    },
    transform: order => (order[0].location[0]),
    className: 'status',
    visible: true,
});

OrderUserOrg.centerTotalsAdminTable.push({
    header: 'Count',
    sort: (a, b) => {
        a = a.length;
        b = b.length;
        return compare(a, b);
    },
    transform: order => (order.length),
    className: 'status',
    visible: true,
});

const CenterTotalsOrderAdminTable = props =>
    <CenterTotalsAdminTable collection={OrderUserOrg} {...props} />
    ;

export default container((props, onData) => {

    let startDate;
    let endDate;
    const user = Meteor.user();
    let now = new Date();

    if (props.match.params.startDate) {
        startDate = new Date(0);
        startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
        endDate = new Date(0);
        endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
    } else if (user.type != 'Caseworker'
        && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
            || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    const subscription = Meteor.subscribe('/orderUserOrg/searchAllByDate', startDate, endDate);
    const volunteerSubscription = Meteor.subscribe('/user/volunteer/list');
    const caseworkerSubscription = Meteor.subscribe('/user/caseworker/list');
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (volunteerSubscription.ready() &&
        caseworkerSubscription.ready() &&
        businessSubscription.ready() &&
        subscription.ready()) {

        onData(null, { startDate, endDate, history, props });
    }
}, withRouter(CenterTotalsOrderAdminTable));