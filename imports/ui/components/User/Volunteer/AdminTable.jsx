import React from 'react';
import _ from 'underscore';
import { Link } from 'react-router-dom';
import listify from 'listify';

import UserOrg from '/imports/api/UserOrg';
import Caseworker from '/imports/api/User/Caseworker';
import Business, { CareProviderLocation} from '/imports/api/Organization/Business';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import UserAdminTable from '/imports/ui/components/UserAdminTable';

import container from '/imports/ui/helpers/container';

UserOrg.adminTable = new AdminTableDefinition();

UserOrg.adminTable.push({
  transform: userOrg => <Avatar user={userOrg} />,
  className: 'avatar',
  export: false,
  visible: false,
});

UserOrg.adminTable.push({
  header: 'Name',
  className: 'name',
  sort: (a, b) => {
    a = a.name;
    b = b.name;
    return compare(a, b);
  },
  transform: (userOrg, willExport = false) => {
    if (willExport) {
      return (
        <a title={userOrg.name} href={`/user/${userOrg._id}`}>
          {userOrg.name ? userOrg.name : "No Name"}
        </a>
      );
    }
    return (
      <Link title={userOrg.name} to={`/user/${userOrg._id}`}>
        {userOrg.name ? userOrg.name : "No Name"}
      </Link>
    );
  },
  visible: true,
});

UserOrg.adminTable.push({
  header: 'Registered',
  className: 'createdAt',
  sort: (a, b) => {
    a = a.createdAt;
    b = b.createdAt;
    return compare(a, b);
  },
  transform: userOrg => helperDate(userOrg.createdAt),
  visible: true,
});

UserOrg.adminTable.push({
  header: 'Care Provider ',
  className: 'affiliations',
  transform: userOrg => (userOrg.orgName[0] + " (" + userOrg.cpNumber[0] + ")"),
  visible: true,
});

UserOrg.adminTable.push({
  header: 'Location ',
  className: 'affiliations',
  transform: userOrg => CareProviderLocation.getIdentifier(userOrg.location[0] ),
  visible: true,
});

UserOrg.adminTable.push({
  header: 'Type',
  className: 'affiliations',
  sort: (a, b) => {
    a = a.type;
    b = b.type;
    return compare(a, b)
  },
  transform: userOrg => userOrg.type === 'Volunteer' ? 'Center Director': userOrg.type ,
  visible: true,
});


UserOrg.adminTable.push({
  header: 'Email',
  className: 'email',
  transform: userOrg => <a href={`mailto:${userOrg.emails[0].address}`}>{userOrg.emails[0].address}</a>,
  visible: true,
});

const VolunteerAdminTable = props => <UserAdminTable collection={UserOrg} {...props} />;

export default container((props, onData) => {
  const userOrgSubscription = Meteor.subscribe('/userOrg/list');
  if (userOrgSubscription.ready()) {
    onData(null, {});
  }
}, VolunteerAdminTable);
