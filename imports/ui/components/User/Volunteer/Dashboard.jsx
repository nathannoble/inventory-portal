import React from 'react';
import CC from '/imports/api';

import { DashboardIcon } from '/imports/ui/pages/Dashboard';

import NewOrders from '/imports/ui/pages/Dashboard/NewOrders';
import Announcements from '/imports/ui/pages/Dashboard/Announcements';
import MyDates from '/imports/ui/pages/Dashboard/MyDates';

const DashboardVolunteer = ({ volunteer }) => (
    <div className="volunteer dashboard">
        {/*<DashboardIcon
         icon="account"
         label="My Profile"
         linkTo={`/user/${volunteer._id}`}
         className="profile"
         />*/}
        <Announcements />
        <DashboardIcon icon="clipboard-check" label="Orders" linkTo="/order" className="order"/>
        <DashboardIcon
            icon={CC.mdiIcons.create}
            label="New Order"
            linkTo="/order/new"
            className="order"
        />
        <MyDates user={volunteer}/>
        <NewOrders />

    </div>
);

export default DashboardVolunteer;
