import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import VolunteerNew from './New';
import VolunteerView from './View';
import VolunteerAdminTable from './AdminTable';
import VolunteerList from './List';

const VolunteerRoutes = props => (
  <Switch className="volunteer">
    <Authenticated exact name="VolunteerNew" path={`${props.match.path}/new`} component={VolunteerNew} {...props} />
    <Authenticated exact name="VolunteerList" path={`${props.match.path}/allmyusers`} component={VolunteerAdminTable}  {...props} />
    <Authenticated exact name="VolunteerList" path={`${props.match.path}`} component={VolunteerAdminTable}  {...props} />
    <Authenticated exact name="VolunteerView" path={`${props.match.path}/:volunteerId`} component={VolunteerView} {...props} />
   </Switch>
);

export default VolunteerRoutes;
