import React from 'react';
import { Link } from 'react-router-dom';
import DateTime from 'react-datetime';

import { Bert } from 'meteor/themeteorchef:bert';

import container from '/imports/ui/helpers/container';
import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';

import CC from '/imports/api';

import {
    handleError,
    Select,
    Checkbox,
    SubHeader,
    Breadcrumbs,
    Label,
} from '/imports/ui/helpers';

class VolunteerNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      emails: [
        {
          address: "",
          verified: true,
        },
      ],
      profile: {
        organizationId:(Meteor.user()
            .administer()
            .count() > 0 && Meteor.user().type !== 'Administrator'
            ? Meteor.user()
            .administer()
            .fetch()[0]._id
            : undefined),
      },
    };
  }

  render() {
    return (
        <div className="volunteer user new">
          {Meteor.user().may.create.user() ? (
          <form onSubmit={(event) => {
          event.preventDefault();
          //const formInputs = event.target.getElementsByTagName('input');
          //const name = formInputs.name.value;
          //const email = formInputs.email.value;
          //const businessId = formInputs.businessId.value;

          const history = this.props.history;
          const name = this.nameInput.ref.value;
          const volunteer = new Volunteer({
            name,
            emails: [
              {
                address: this.emailInput.ref.value,
                verified: true,
              },
            ],
            profile: {
              role:"volunteer",
              organizationId:this.state.organizationId
            },
          });

          console.log("Volunteer/New:pre create:" + JSON.stringify(volunteer));

          Meteor.call(
            'user/volunteer/create',
            volunteer,
            handleError({
              callback: (volunteerId) => {
                console.log(JSON.stringify("Volunteer/New:post create:volunteerId:" + volunteerId));
                history.push(`/user/volunteer/${volunteerId}`);
              },
            }),
          );
        }}>

            <Label name="name" required text="Name" type="text"
                   placeholder="New Center Director's Name"
                   ref={input => (this.nameInput = input)}/>
            <Label name="email" required text="Email" type="email"
                   placeholder="newuser@domain.com"
                   ref={input => (this.emailInput = input)}/>
            <Label
                required
                name="businessId"
                text="Care Provider"
                value={this.state.organizationId}
                element={Select}
                options={
                       Business.find()
                      .fetch()
                      .filter(organization => organization.type === 'Business')
                      .map(organization =>
                        Object({ value: organization._id, label: organization.name }))
                }
                onChange={(selectedOption) => {
                 this.setState({organizationId: selectedOption.value});
                 console.log(JSON.stringify(this.state));
                 }}
            />

            <label>
              <input type="submit" value="Add Center Director" />
            </label>
          </form>
          ) : null}
        </div>
    );
  }
}

export default container((props, onData) => {
  const businessSubscription = Meteor.subscribe('/organization/business/list');
  if (businessSubscription.ready()) {
    onData(null, {});
  }
}, VolunteerNew);