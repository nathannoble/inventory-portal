import React from 'react';
import listify from 'listify';

const VolunteerCardFields = ({ volunteer }) => (
  <div>
    <span className="bio">
      <span className="label">Bio</span>
      <span className="value">{volunteer.bio}</span>
    </span>
    {volunteer.worksFor().map((organization) => {
      const affiliation = volunteer.affiliationWith(organization);
      return (
        <span className="organization" key={organization._id}>
          <span className="label">{affiliation.titleAtOrganization || 'works'} at</span>
          <span className="value">
            {organization.name}
            <br />
            {affiliation.department ? ` in the ${affiliation.department} department` : null}
          </span>
        </span>
      );
    })}
  </div>
);

export default VolunteerCardFields;
