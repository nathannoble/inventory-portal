import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import PropTypes from 'prop-types';

import Business from '/imports/api/Organization/Business';
import Volunteer, { OrganizationAffiliation } from '/imports/api/User/Volunteer';

import container from '/imports/ui/helpers/container';
import { handleError, CCButton, TypeaheadResults, Select, Checkbox } from '/imports/ui/helpers';
import Avatar from '/imports/ui/components/Avatar';
import AssociateOrganization from '/imports/ui/components/User/Volunteer/Associate';
import OpportunitiesTable from '../OpportunitiesTable';

class VolunteerOrganizationAffiliationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationMatches: [],
      organizationSelected: false,
      codeRequired: false,
    };
  }

  render() {
    const affiliation = this.props.affiliation;
    const onChange = this.props.onChange;
    const onRemove = this.props.onRemove;
    const organization = affiliation.organization();

    return (
      <div className="organization__affiliation">
        {organization ? (
          <h3>
            <Avatar organization={organization} />
            {organization.name}
          </h3>
        ) : null}
        {organization ? (
          <section className="organization">
            {organization.departments && organization.departments.length ? (
              <label htmlFor="department">
                Department
                <Select
                  className="organization__department"
                  value={affiliation.departmentId}
                  options={organization.departments.map(department =>
                    Object({ value: department.id, label: department.name }))}
                  onChange={(selectedOption) => {
                    affiliation.department = selectedOption.value;
                    onChange(affiliation);
                  }}
                />
              </label>
            ) : null}

            <label htmlFor="titleAtOrganization">
              Title At Organization
              <input
                type="text"
                name="titleAtOrganization"
                defaultValue={affiliation.titleAtOrganization}
                placeholder="Title at organization"
                onChange={(event) => {
                  affiliation.titleAtOrganization = String(event.target.value);
                  onChange(affiliation);
                }}
              />
            </label>

            <label htmlFor="opportunities">
              <h4>Volunteer Opportunities</h4>
              <OpportunitiesTable affiliation={affiliation} onChange={onChange} />
            </label>
          </section>
        ) : (
          <section className="organization">
            <AssociateOrganization
              onChoose={(chosenOrganization) => {
                affiliation.organizationId = chosenOrganization._id;
                onChange(affiliation);
              }}
            />
          </section>
        )}
        <CCButton
          type="remove"
          onClick={(event) => {
            event.preventDefault();
            onRemove();
          }}
        />
      </div>
    );
  }
}

VolunteerOrganizationAffiliationComponent.propTypes = {
  affiliation: PropTypes.instanceOf(OrganizationAffiliation),
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
};

const VolunteerOrganizationAffiliation = container((props, onData) => {
  const subscription = Meteor.subscribe('/organization/business/list');
  if (subscription.ready()) {
    onData(null, {});
  }
}, VolunteerOrganizationAffiliationComponent);

const VolunteerViewOrganizationOpportunities = ({ volunteer }) => (
  <form className="volunteer organization opportunities">
    <h2>I am interested in volunteering on behalf of my employer or organization</h2>
    {volunteer.organizations.map((affiliation, index) => (
      <VolunteerOrganizationAffiliation
        key={index}
        affiliation={affiliation}
        onChange={(affiliation) => {
          volunteer.organizations[index] = affiliation;
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
        onRemove={() => {
          volunteer.organizations.splice(index, 1);
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
      />
    ))}
    <CCButton
      type="add"
      onClick={(event) => {
        event.preventDefault();
        volunteer.organizations.push(new OrganizationAffiliation());
        Meteor.call('/user/volunteer/update', volunteer, handleError);
      }}
    >
      Add New Organization
    </CCButton>
  </form>
);

export { VolunteerViewOrganizationOpportunities, VolunteerOrganizationAffiliation };
