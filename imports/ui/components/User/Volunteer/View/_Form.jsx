import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';

import CC from '/imports/api';
import container from '/imports/ui/helpers/container';

import Business from '/imports/api/Organization/Business';

//import Files from '/imports/api/helpers/Files';

import {
    CCButton,
    PasswordChanger,
    handleError,
    Select,
    Label,
    Checkbox,
} from '/imports/ui/helpers';

const VolunteerViewForm = ({ volunteer }) => (
    <form className="volunteer user">
        <h2>Profile Information</h2>

        <Label
            name="name"
            required
            text="Name"
            type="text"
            placeholder="Your Name"
            defaultValue={volunteer.name}
            onChange={(event) => {
        volunteer.name = String(event.target.value);
        Meteor.call('/user/volunteer/update', volunteer, handleError);
      }}
        />

        <PasswordChanger user={volunteer}/>

        <Label
            name="email"
            required
            text="Email"
            type="email"
            placeholder="your@email.com"
            defaultValue={volunteer.emails[0].address}
            onChange={(event) => {
        volunteer.emails[0].address = String(event.target.value);
        Meteor.call(
          '/user/volunteer/update',
          volunteer,
          handleError({ confirmationText: 'Email Updated' }),
        );
      }}
        />

        <Label
            name="phone"
            text="Phone Number"
            type="tel"
            placeholder="(530) 123-4567"
            defaultValue={volunteer.phone}
            onChange={(event) => {
        volunteer.phone = String(event.target.value);
        Meteor.call('/user/volunteer/update', volunteer, handleError);
      }}
        />

        <Label
            name="businessId"
            text="Care Provider"
            value={volunteer.profile.organizationId}
            element={Select}
            options={
               Business.find({},{ sort: { 'name': 1 }})
               .fetch()
               .filter(organization => organization.type === 'Business')
               .map(organization =>
                Object({ value: organization._id, label:(organization.name + " (" + organization.cpNumber + ")" ).toString()}))
            }
            onChange={(event) => {
            volunteer.profile.organizationId = event.value;
            Meteor.call('/user/volunteer/update', volunteer, handleError);
          }}
        />

        <Label
              name="active"
              text="Status"
              value={volunteer.inactive ? (volunteer.inactive == true ? "False" : "True") : "True"  }
              element={Select}
              required
              options={[
                { value: "True", label: 'Active' },
                { value: "False", label: 'Inactive' }
              ]}
              onChange={({ value }) => {
                  console.log('User/Volunteer/View/_Form.jsx:inactive:onChange:value:' + value);
                  if (value == "False")
                      volunteer.inactive = true;
                  else
                      volunteer.inactive = false; 
                  Meteor.call('/user/volunteer/update', volunteer, handleError);
              }}
          />

          <Label
            name="bio"
            text="Bio"
            element="textarea"
            defaultValue={volunteer.bio}
            onChange={(event) => {
        volunteer.bio = String(event.target.value);
        Meteor.call('/user/volunteer/update', volunteer, handleError);
      }}
        />


    </form>
);

//export default VolunteerViewForm;
export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (businessSubscription.ready()) {
        onData(null, {});
    }

}, VolunteerViewForm);
