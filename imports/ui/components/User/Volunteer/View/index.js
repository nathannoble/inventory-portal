import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import { Bert } from 'meteor/themeteorchef:bert';

import Volunteer from '/imports/api/User/Volunteer';
import { OrderStatus, OrderFilter } from '/imports/api/Order';
import VolunteerViewForm from '/imports/ui/components/User/Volunteer/View/_Form';
// import VolunteerViewAvailability from '/imports/ui/components/User/Volunteer/View/_Availability';
// import { VolunteerViewOrganizationOpportunities } from '/imports/ui/components/User/Volunteer/View/_Opportunities';
// import VolunteerAffiliation, {
//   VolunteerAffiliationAdd,
// } from '/imports/ui/components/User/Volunteer/View/_Affiliation';

import AvatarUpload from '/imports/ui/components/AvatarUpload';

import container from '/imports/ui/helpers/container';

// import ChangeUserType from '/imports/ui/components/User/ChangeUserType';
import { handleError, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import Drawer from '/imports/ui/components/helpers/Drawer';
import MyOrders from '../../../../pages/Dashboard/MyOrders';

const VolunteerRequestList = ({ volunteer }) => (
  <div className="requests">
    <h2>Requests</h2>
    <ul>
      {volunteer.requestsFor(new RequestFilter({ status: RequestStatus.complete })).map(request => (
        <li key={request._id}>
          <Link to={request.path}>{request.title()}</Link>
        </li>
      ))}
    </ul>
  </div>
);

const VolunteerOrderList = ({ volunteer }) => (
    <div className="orders">
        <h2>Orders</h2>
        <ul>
            {volunteer.ordersFor(new OrderFilter({ status: OrderStatus.complete })).map(order => (
                <li key={order._id}>
                    <Link to={order.path}>{order.title()}</Link>
                </li>
            ))}
        </ul>
    </div>
);

const VolunteerView = container(
  (props, onData) => {
    // If volunteer has an ID, and this form is being rendered on /user/volunteer/new,
    // redirect to the proper page.
    // Done upon creation of a user

    if (
      props.volunteer &&
      props.volunteer._id &&
      props.location &&
      props.location.pathname === '/user/volunteer/new'
    ) {
      props.history.push(`/user/volunteer${props.volunteer._id}`);
    }
    let { volunteer } = props;
    const volunteerId = props.match ? props.match.params.volunteerId : volunteer._id;
    const subscription = Meteor.subscribe('/user/read', volunteerId);
    if (subscription.ready() || volunteer) {
      if (!volunteer) volunteer = Volunteer.findOne(volunteerId);
      if (volunteer) {
        onData(null, { volunteer });
      } else {
            //console.log("Volunteer/View:props:" + JSON.stringify(props));
            let message = 'Center Director not found.  Redirecting to Center Director List.';
            Bert.alert(message);
            if (document.querySelectorAll('.bert-content').length > 0) {
              let el = document.querySelectorAll('.bert-content')[0];
              if (el) {
                  el.innerHTML = `<p>${message}</p>`;
              }
            }
            props.history.push(`/user/volunteer`);
      }
    }
  },
  ({ volunteer }) => (
    <div className="volunteer user view">
      {Meteor.user().type === 'Administrator' ? (
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[
              { text: 'Users' },
              { link: '/user/volunteer', text: 'Center Directors' },
              { text: volunteer.name },
            ]}
          />
          <Drawer>
            {/*<ChangeUserType user={volunteer} />*/}
            <CCButton
              type="remove"
              onClick={(event) => {
                event.preventDefault();
                //Meteor.call('/user/volunteer/delete', volunteer, handleError);
                Meteor.call('/user/volunteer/delete', volunteer, handleError({
                  callback: () => {
                    //location.path = "/";
                    //history.push(`/user/volunteer/${volunteerId}`);
                  },
                }));
              }}
            >
              Delete User
            </CCButton>
          </Drawer>
        </SubHeader>
      ) : null}
      <VolunteerViewForm volunteer={volunteer} />
     
      <div className="sidebar">
        <AvatarUpload
          entity={volunteer}
          onUpload={(volunteer) => {
            Meteor.call('/user/volunteer/update', volunteer, handleError);
          }}
          onRemove={(volunteer, callback) => {
            Meteor.call('/user/volunteer/update', volunteer, handleError({ callback }));
          }}
        />
         <MyOrders user={volunteer}/>
      </div>
    </div>
  ),
);

export default VolunteerView;
