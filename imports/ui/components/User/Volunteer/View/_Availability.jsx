import { Meteor } from 'meteor/meteor';
import React from 'react';
import DateTime from 'react-datetime';
import moment from 'moment';

import AbstractDuration from '/imports/api/helpers/AbstractDuration';

import { DaySelector, CCButton, handleError } from '/imports/ui/helpers';

const VolunteerAvailabilitySlot = ({ slot, index, onRemove, onChange }) => <div className="availability">
  <DateTime inputProps={{placeholder: 'Start of Availability'}} defaultValue={moment(slot.times.start, 'Hmm')} dateFormat={false} onChange={time => {
    slot.times.start = Number(time.format('Hmm'));
    onChange(slot);
  }}/>
  <DateTime inputProps={{placeholder: 'End of Availability'}} defaultValue={moment(slot.times.end, 'Hmm')} dateFormat={false} onChange={time => {
    slot.times.end = Number(time.format('Hmm'));
    onChange(slot);
  }}/>
  <DaySelector selected={slot.days} name="availability" prefix={`availability-${index}-`} onChange={days => {
    slot.days = days;
    onChange(slot);
  }} />
  <CCButton type="remove" onClick={event => {
    event.preventDefault();
    onRemove();
  }} />
</div>

const VolunteerViewAvailability = ({ volunteer }) => <form className="volunteer availability">
  <h2>Your General Availability</h2>
  {volunteer.availability && volunteer.availability.length
    ? volunteer.availability.map(
      (availabilitySlot, index) => <VolunteerAvailabilitySlot
        key={index}
        index={index}
        slot={availabilitySlot}
        onRemove={() => {
          volunteer.availability.splice(index, 1);
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
        onChange={slot => {
          volunteer.availability[index] = slot;
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
      />)
      : null
    }
    <CCButton type="add" onClick={event => {
      event.preventDefault();
      if(volunteer.availability.length) {
        volunteer.availability.push(new AbstractDuration);
      } else {
        volunteer.availability = [new AbstractDuration];
      }
      Meteor.call('/user/volunteer/update', volunteer, handleError);
    }}>{volunteer.availability.length ? null : 'Add Availability Slot'}</CCButton>
  </form>;

  export default VolunteerViewAvailability;
