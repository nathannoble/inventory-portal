import React from 'react';

import AssociateOrganization from '/imports/ui/components/User/Volunteer/Associate';

const SignupVolunteer = ({ onChooseOrganization }) => (
  <fieldset name="volunteer">
    <label htmlFor="organizationSearch">
      Enter the Care Provider you are associated with.
    </label>
    <AssociateOrganization onChoose={onChooseOrganization} />
  </fieldset>
);

export default SignupVolunteer;
