import { Meteor } from 'meteor/meteor';
import React from 'react';
import { NavLink, Link } from 'react-router-dom';

import CC from '/imports/api';

import { DashboardLink } from '/imports/ui/components/helpers/Navigation';

const NavigationVolunteer = ({ volunteer, messageCount }) => (
    <ul className="navigation navigation--volunteer">
        <li>
            <DashboardLink />
        </li>
        <li>
            <NavLink to={`/user/${volunteer._id}`}>{CC.icon('profile')}{Meteor.user().name}</NavLink>
        </li>
        <li>
            <NavLink to="/user/volunteer/allmyusers">{CC.icon('student')}My Users</NavLink>
        </li>
        <li>
            <NavLink to="/order">{CC.icon('order')}Orders</NavLink>
        </li>
        <li>
            <NavLink to="/order/labels">{CC.icon('file-pdf-box')}Order Labels</NavLink>
        </li>
        <li>
            <NavLink to="/order/report">{CC.icon('report')}Reports</NavLink>
            <ul>
                <li>
                    <NavLink to="/order/report">Orders Report</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cpTotalsReport">CP Totals Report</NavLink>
                </li>
                <li>
                    <NavLink to="/order/centerTotalsReport">Center Totals Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/itemTotalsReport">Item Totals Report</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsReport">CB (Shirt Size) Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsShoesReport">CB (Shoe Size) Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsPantsReport">CB (Pant Size) Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/printLabels">Print Labels</NavLink>
                </li>
            </ul>
        </li>
        <li>
            <NavLink to="/order/help">{CC.icon('help')}Help</NavLink>
        </li>
    </ul>
);

export default NavigationVolunteer;
