import React from 'react';
import PropTypes from 'prop-types';

import OrderType from '/imports/api/OrderType';

import { VolunteerOpportunities, OrganizationAffiliation } from '/imports/api/User/Volunteer';

import Checkbox from '/imports/ui/components/helpers/Checkbox';

const OpportunityTableRow = ({ organizationId, opportunities, onChange }) => (
  <tr
    id={opportunities.requestTypeId}
    className={`opportunityType ${opportunities.gradeGroups.length === 0 ? 'empty' : ''}`}
  >
    <td className="name">{opportunities.requestType().name}</td>
    {gradeGroups.map(gradeGroup => (
      <td key={gradeGroup.slug}>
        <Checkbox
          name={gradeGroup.slug}
          id={`${organizationId}-${opportunities.requestTypeId}-${gradeGroup.slug}`}
          checked={opportunities.gradeGroups.includes(gradeGroup.slug)}
          onChange={(event) => {
            if (event.target.checked) {
              opportunities.gradeGroups.push(event.target.name);
            } else {
              opportunities.gradeGroups.splice(
                opportunities.gradeGroups.indexOf(event.target.name),
                1,
              );
            }
            onChange(opportunities);
          }}
        />
      </td>
    ))}
  </tr>
);

const OpportunitiesTable = ({ affiliation, onChange, disabled }) => (
  <table className={`opportunitiesTable${disabled ? ' disabled' : ''}`}>
    <colgroup>
      <col style={{ width: '5rem' }} />
      {gradeGroups.map((gradeGroup, index) => <col key={index} />)}
    </colgroup>
    <thead>
      <tr>
        <th>&nbsp;</th>
        {gradeGroups.map(gradeGroup => <th key={gradeGroup.slug}>{gradeGroup.name}</th>)}
      </tr>
    </thead>
    <tbody>
      <tr className="row--subHeader">
        <td colSpan={gradeGroups.length + 1}>
          <strong>One-Time</strong>
        </td>
      </tr>
      {RequestType.find({ ongoing: false })
        .fetch()
        .map((requestType) => {
          const opportunities =
            affiliation.indexOf(requestType._id) >= 0
              ? affiliation.opportunities[affiliation.indexOf(requestType._id)]
              : new VolunteerOpportunities({ requestTypeId: requestType._id });
          return (
            <OpportunityTableRow
              key={requestType._id}
              organizationId={affiliation.organizationId}
              opportunities={opportunities}
              onChange={(opportunities) => {
                if (affiliation.indexOf(requestType._id) >= 0) {
                  affiliation.opportunities[affiliation.indexOf(requestType._id)] = opportunities;
                } else {
                  affiliation.opportunities.push(opportunities);
                }
                onChange(affiliation);
              }}
            />
          );
        })}

      <tr className="row--subHeader">
        <td colSpan={gradeGroups.length + 1}>
          <strong>Ongoing</strong>
          <i>(Requires a commitment for one semester or a full school year)</i>
        </td>
      </tr>
      {RequestType.find({ ongoing: true })
        .fetch()
        .map((requestType) => {
          const opportunities =
            affiliation.indexOf(requestType._id) >= 0
              ? affiliation.opportunities[affiliation.indexOf(requestType._id)]
              : new VolunteerOpportunities({ requestTypeId: requestType._id });
          return (
            <OpportunityTableRow
              key={requestType._id}
              organizationId={affiliation.organizationId}
              opportunities={opportunities}
              onChange={(opportunities) => {
                if (affiliation.indexOf(requestType._id) >= 0) {
                  affiliation.opportunities[affiliation.indexOf(requestType._id)] = opportunities;
                } else {
                  affiliation.opportunities.push(opportunities);
                }
                onChange(affiliation);
              }}
            />
          );
        })}
    </tbody>
  </table>
);

OpportunitiesTable.propTypes = {
  affiliation: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.instanceOf(OrganizationAffiliation),
  ]).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

OpportunitiesTable.defaultProps = {
  affiliation: new OrganizationAffiliation(),
  onChange: () => null,
  disabled: false,
};

export default OpportunitiesTable;
