import { Meteor } from 'meteor/meteor';
import React from 'react';

import Business from '/imports/api/Organization/Business';

import container from '/imports/ui/helpers/container';
import { handleError, TypeaheadResults } from '/imports/ui/helpers';

class AssociateOrganization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      organizationMatches: [],
      organizationSelected: false,
      codeRequired: false,
    };
    this.maxResults = 5;
    this.typeaheadTimeout = 4000;
    this.onKeyUpHandler = this.onKeyUpHandler.bind(this);
    this.onSelectHandler = this.onSelectHandler.bind(this);
  }

  onKeyUpHandler(event) {
    if (this.props.onChange) this.props.onChange(event);
    this.setState({ organizationSelected: false });
    const organizationMatches = Business.find({}, { sort: { name: 1 } })
      .fetch()
      .filter(business => business.name.toLowerCase().includes(event.target.value.toLowerCase()))
      .slice(0, this.maxResults);
    this.setState({ organizationMatches });
    Meteor.setTimeout(() => {
      this.setState({ organizationMatches: [] });
    }, this.typeaheadTimeout);
  }

  onSelectHandler(organizationId) {
    const organizationSelected = Business.findOne(organizationId);
    this.setState({ organizationSelected });
    Meteor.call(
      '/organization/business/authorization',
      organizationId,
      handleError({
        callback: (hasCode) => {
          if (hasCode) {
            this.organizationSearch.value = organizationSelected.name;
            this.setState({
              codeRequired: true,
              organizationMatches: [],
            });
          } else {
            this.props.onChoose(organizationSelected);
          }
        },
      }),
    );
  }

  render() {
    const {
      onChange, value, disabled, label,
    } = this.props;
    return (
      <div style={{ position: 'relative' }} className="associateOrganization">
        {label ? <span className="label">{label}</span> : null}
        <input
          disabled={disabled}
          defaultValue={value || ''}
          type="text"
          id="organizationSearch"
          name="organizationSearch"
          placeholder="Choose Care Provider"
          ref={input => (this.organizationSearch = input)}
          onKeyUp={this.onKeyUpHandler}
        />
        <TypeaheadResults
          results={this.state.organizationMatches}
          onSelect={this.onSelectHandler}
        />
        {this.state.codeRequired && this.state.organizationSelected ? (
          <input
            type="text"
            id="authorizationCode"
            placeholder="Authorization Code"
            onKeyUp={(event) => {
              Meteor.call(
                '/organization/business/authorization',
                this.state.organizationSelected._id,
                event.target.value,
                handleError({
                  callback: (codeMatches) => {
                    if (codeMatches) onChoose(this.state.organizationSelected);
                  },
                }),
              );
            }}
          />
        ) : null}
      </div>
    );
  }
}

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/organization/business/list');
  if (subscription.ready()) {
    onData(null, {});
  }
}, AssociateOrganization);
