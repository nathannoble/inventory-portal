import { Meteor } from 'meteor/meteor';
import React from 'react';

import container from '/imports/ui/helpers/container';

import CaseworkerView from '/imports/ui/components/User/Caseworker/View';
import VolunteerView from '/imports/ui/components/User/Volunteer/View';
import AdministratorView from '/imports/ui/components/User/Administrator/View';

const UserProfileSwitcher = container(
  (props, onData) => {
    const { userId } = props.match.params;
    const subscription = Meteor.subscribe('/user/read', userId);
    if (subscription.ready()) {
      const user = Meteor.user(userId);
      if (user) {
        onData(null, { user });
      } else {
          let message = 'User not found.  Redirecting to User List.';
          Bert.alert(message);
          if (document.querySelectorAll('.bert-content').length > 0) {
              let el = document.querySelectorAll('.bert-content')[0];
              if (el) {
                  el.innerHTML = `<p>${message}</p>`;
              }
          }
          //onData(new Meteor.Error(404, 'User not found', 'It may have been deleted'));
          props.history.push(`/user/caseworker`);
      }
    }
  },
  ({ user }) =>
    Object({
      Caseworker: <CaseworkerView caseworker={user} />,
      Volunteer: <VolunteerView volunteer={user} />,
      Administrator: <AdministratorView administrator={user} />,
    })[user.type],
);

export default UserProfileSwitcher;
