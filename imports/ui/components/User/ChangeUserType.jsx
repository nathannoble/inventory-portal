import { Meteor } from 'meteor/meteor';
import React from 'react';

import handleError from '/imports/ui/helpers/handleError';
import { Select } from '/imports/ui/helpers';

const ChangeUserType = ({ user }) => (
  <Select
    value={user.profile.role}
    options={[
      { value: 'volunteer', label: 'Volunteer' },
      { value: 'organization', label: 'Organization' },
      { value: 'caseworker', label: 'Caseworker' },
      { value: 'administrator', label: 'Administrator' },
    ]}
    onChange={(selectedOption) => {
      user.profile.role = selectedOption.value;
      Meteor.call('/user/update', user, handleError);
    }}
  />
);

export default ChangeUserType;
