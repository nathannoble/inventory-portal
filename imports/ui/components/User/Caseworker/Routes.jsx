import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import CaseworkerNew from './New';
import CaseworkerView from './View';
import CaseworkerAdminTable from './AdminTable';

const CaseworkerRoutes = props => (
  <Switch className="caseworker">
    <Authenticated exact name="CaseworkerNew" path={`${props.match.path}/new`} component={CaseworkerNew} {...props} />
    <Authenticated name="CaseworkerList" path={`/user/caseworker`} component={CaseworkerAdminTable}  {...props} />
      {/*roles={['administrator, volunteer']}*/}
    <Authenticated exact name="CaseworkerView" path={`${props.match.path}/:caseworkerId`} component={CaseworkerView} {...props} />
  </Switch>
);

export default CaseworkerRoutes;
