import React from 'react';
import listify from 'listify';

const CaseworkerCardFields = ({ Caseworker }) => (
  <div>
    <span className="bio">
      <span className="label">Bio</span>
      <span className="value">{Caseworker.bio}</span>
    </span>
    {Caseworker.worksFor().map((organization) => {
      const affiliation = Caseworker.affiliationWith(organization);
      return (
        <span className="organization" key={organization._id}>
          <span className="label">{affiliation.titleAtOrganization || 'works'} at</span>
          <span className="value">
            {organization.name}
            <br />
            {affiliation.department ? ` in the ${affiliation.department} department` : null}
          </span>
        </span>
      );
    })}
  </div>
);

export default CaseworkerCardFields;
