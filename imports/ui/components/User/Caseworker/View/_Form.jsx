import { Meteor } from 'meteor/meteor';
import React from 'react';

import CC from '/imports/api';

import container from '/imports/ui/helpers/container';

import Business, { CareProviderLocation, CareProviderPickupDay} from '/imports/api/Organization/Business';

import Files from '/imports/api/helpers/Files';

import {
    CCButton,
    PasswordChanger,
    handleError,
    Select,
    Label,
    Checkbox,
} from '/imports/ui/helpers';

const CaseworkerViewForm = ({ caseworker }) => (
    <form className="caseworker user">
        <h2>Profile Information</h2>

        {Meteor.user().type === 'Caseworker' ?
            <Label
                readOnly={true}
                name="name"
                required
                text="Name"
                type="text"
                placeholder="Your Name"
                value={caseworker.name}
            /> :
        <Label
            name="name"
            required
            text="Name"
            type="text"
            placeholder="Your Name"
            defaultValue={caseworker.name}
            onChange={(event) => {
        caseworker.name = String(event.target.value);
        Meteor.call('/user/caseworker/update', caseworker, handleError);
      }}
        />}

        <PasswordChanger user={caseworker}/>

        {Meteor.user().type === 'Caseworker' ?
            <Label
                readOnly={true}
                name="email"
                required
                text="Email"
                type="email"
                placeholder="your@email.com"
                value={caseworker.emails[0].address}
            /> :
        <Label
            name="email"
            required
            text="Email"
            type="email"
            placeholder="your@email.com"
            defaultValue={caseworker.emails[0].address}
            onChange={(event) => {
        caseworker.emails[0].address = String(event.target.value);
        Meteor.call(
          '/user/caseworker/update',
          caseworker,
          handleError({ confirmationText: 'Email Updated' }),
        );
      }}
        />}

        <Label
            name="phone"
            text="Phone Number"
            type="tel"
            placeholder="(530) 123-4567"
            defaultValue={caseworker.phone}
            onChange={(event) => {
        caseworker.phone = String(event.target.value);
        Meteor.call('/user/caseworker/update', caseworker, handleError);
      }}
        />

        {Meteor.user().type === 'Caseworker' ?
        <Label
            readOnly={true}
            name="businessId"
            text="Care Provider"
            value={
                  Business.find()
                  .fetch()
                  .filter(organization => organization._id === caseworker.profile.organizationId)
                  .map(organization => organization.name)
            }
        /> :
        <Label
            name="businessId"
            value={caseworker.profile.organizationId}
            text="Care Provider"
            options={
                   Business.find({},{ sort: { 'name': 1 }})
                   .fetch()
                   .filter(organization => organization.type === 'Business')
                   .map(organization =>
                    Object({ value: organization._id, label:(organization.name + " (" + organization.cpNumber + ")" ).toString()}))
                }
            element={Select}
            onChange={(event) => {
            caseworker.profile.organizationId = event.value;
            Meteor.call('/user/caseworker/update', caseworker, handleError);
          }}

        />}

        {/* <Label
            readOnly={true}
            name="cpNumber"
            text="CP Number"
            value={
                  Business.find()
                  .fetch()
                  .filter(organization => organization._id === caseworker.profile.organizationId)
                  .map(organization => organization.cpNumber)
            }
        />*/}
        {Meteor.user().type === 'Caseworker' ?
        
        <Label
            readOnly={true}
            name="active"
            text="Status"
            value={caseworker.inactive ? (caseworker.inactive == true ? "Inactive" : "Active") : "Active" }
        />
        :
         <Label
            name="active"
            text="Status"
            value={caseworker.inactive ? (caseworker.inactive == true ? "False" : "True") : "True"  }
            element={Select}
            required
            options={[
                { value: "True", label: 'Active' },
                { value: "False", label: 'Inactive' }
              ]}
            onChange={({ value }) => {
                console.log('User/Caseworker/View/_Form.jsx:inactive:onChange:value:' + value);
                if (value == "False")
                    caseworker.inactive = true;
                else
                    caseworker.inactive = false; 
                Meteor.call('/user/caseworker/update', caseworker, handleError);
            }}
        />}

        <Label
            readOnly={true}
            name="location"
            text="Location"
            placeholder="Location"
            value={
                  Business.find()
                  .fetch()
                  .filter(organization => organization._id === caseworker.profile.organizationId)
                  .map(organization => CareProviderLocation.getIdentifier(organization.location))
            }
        />

        <Label
            readOnly={true}
            name="pickupDay"
            text="Pickup Day"
            value={
                  Business.find()
                  .fetch()
                  .filter(organization => organization._id === caseworker.profile.organizationId)
                  .map(organization => CareProviderPickupDay.getIdentifier(organization.pickupDay))
            }
        />

        <Label
            name="bio"
            text="Bio"
            element="textarea"
            defaultValue={caseworker.bio}
            onChange={(event) => {
        caseworker.bio = String(event.target.value);
        Meteor.call('/user/caseworker/update', caseworker, handleError);
      }}
        />


        {Meteor.user().type === 'Caseworker' ?
            <div style={{ padding: '1rem' }}>
            *If you need to change your name or email, please contact your Center Director or send an email to <a href="mailto:website@nwchildrens.org" target="_blank" rel="noopener">website@nwchildrens.org</a>.
            </div>
            : null}
        {/*{Meteor.user().type === 'Administrator' ? (
        <CCButton
            type="remove"
            onClick={(event) => {
                event.preventDefault();
                Meteor.call('/user/caseworker/delete', caseworker, handleError({
                    callback() {
                      history.push('/user/caseworker');
                    },
                  }));
              }}
        >
            Delete User
        </CCButton>
        ) : null}*/}

    </form>
);

//export default CaseworkerViewForm;
export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (businessSubscription.ready()) {
        onData(null, {});
    }

}, CaseworkerViewForm);
