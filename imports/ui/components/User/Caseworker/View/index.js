import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';

import Caseworker from '/imports/api/User/Caseworker';

import { OrderStatus, OrderFilter } from '/imports/api/Order';

import CaseworkerViewForm from '/imports/ui/components/User/Caseworker/View/_Form';
// import CaseworkerViewAvailability from '/imports/ui/components/User/Caseworker/View/_Availability';
// import CaseworkerAffiliation, {
//   CaseworkerAffiliationAdd,
// } from '/imports/ui/components/User/Caseworker/View/_Affiliation';

import AvatarUpload from '/imports/ui/components/AvatarUpload';

import container from '/imports/ui/helpers/container';

// import ChangeUserType from '/imports/ui/components/User/ChangeUserType';
import { handleError, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import Drawer from '/imports/ui/components/helpers/Drawer';
import MyOrders from '../../../../pages/Dashboard/MyOrders';

const CaseworkerOrderList = ({ caseworker }) => (
    <div className="orders">
        <h2>Orders</h2>
        <ul>
            {Caseworker.ordersFor(new OrderFilter({ status: OrderStatus.complete })).map(order => (
                <li key={order._id}>
                    <Link to={order.path}>{order.title()}</Link>
                </li>
            ))}
        </ul>
    </div>
);

const CaseworkerView = container(
  (props, onData) => {
    // If Caseworker has an ID, and this form is being rendered on /user/caseworker/new,
    // redirect to the proper page.
    // Done upon creation of a user
    if (
      props.caseworker &&
      props.caseworker._id &&
      props.location &&
      props.location.pathname === '/user/caseworker/new'
    ) {
      props.history.push(`/user/caseworker${props.caseworker._id}`);
    }
    let { caseworker } = props;
    const caseworkerId = props.match ? props.match.params.caseworkerId : caseworker._id;
    const subscription = Meteor.subscribe('/user/read', caseworkerId);
    if (subscription.ready() || caseworker) {
      if (!caseworker) caseworker = Caseworker.findOne(caseworkerId);
      if (caseworker) {
        onData(null, { caseworker });
      } else {
          let message = 'Case Worker not found.  Redirecting to User List.';
          Bert.alert(message);
          if (document.querySelectorAll('.bert-content').length > 0) {
              let el = document.querySelectorAll('.bert-content')[0];
              if (el) {
                  el.innerHTML = `<p>${message}</p>`;
              }
          }
          props.history.push(`/user/caseworker`);
      }
    }
  },
  ({ caseworker }) => (
    <div className="caseworker user view">
      {Meteor.user().type === 'Administrator' ? (
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[
              { text: 'Users' },
              { link: '/user/caseworker', text: 'Case Workers' },
              { text: caseworker.name },
            ]}
          />
          <Drawer>
            {/*<ChangeUserType user={caseworker} />*/}
            <CCButton
              type="remove"
              onClick={(event) => {
                event.preventDefault();
                Meteor.call('/user/caseworker/delete', caseworker, handleError);
              }}
            >
              Delete User
            </CCButton>
          </Drawer>
        </SubHeader>
      ) : null}
      <CaseworkerViewForm caseworker={caseworker} />
      <div className="sidebar">
        <AvatarUpload
          entity={caseworker}
          onUpload={(caseworker) => {
            Meteor.call('/user/caseworker/update', caseworker, handleError);
          }}
          onRemove={(caseworker, callback) => {
            Meteor.call('/user/caseworker/update', caseworker, handleError({ callback }));
          }}
        />
         <MyOrders user={caseworker}/>
      </div>
    </div>
  ),
);

export default CaseworkerView;
