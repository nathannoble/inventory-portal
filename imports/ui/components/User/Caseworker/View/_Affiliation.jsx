import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import { Bert } from 'meteor/themeteorchef:bert';

import Business from '/imports/api/Organization/Business';

import { OrganizationAffiliation } from '/imports/api/User/Caseworker';

import AssociateOrganization from '/imports/ui/components/User/Caseworker/Associate';

import { Label, Select, CCButton, handleError } from '/imports/ui/helpers';

const CaseworkerAffiliation = ({ affiliation, onChange, onRemove }) => (
  <div className="affiliation">
    <h2>
      {Meteor.user().may.update.organization(affiliation.organization()) ? (
        <Link to={affiliation.organization().path}>{affiliation.organization().name}</Link>
      ) : (
        affiliation.organization().name
      )}
    </h2>

    <Label
      name="titleAtOrganization"
      text="Title at Organization"
      placeholder="Employee"
      defaultValue={affiliation.titleAtOrganization}
      onChange={(event) => {
        affiliation.titleAtOrganization = event.target.value;
        onChange(affiliation);
      }}
    />
    <Label
      name="departmentId"
      element={Select}
      text="Department"
      placeholder="Choose Department"
      options={affiliation
        .organization()
        .departments.map(department => Object({ value: department.id, label: department.name }))}
      value={affiliation.departmentId}
      onChange={function({ value }) {
        affiliation.departmentId = value;
        onChange(affiliation);
      }}
    />
    <CCButton
      type="remove"
      onClick={function(event) {
        event.preventDefault();
        onRemove();
      }}
    />
  </div>
);

class CaseworkerAffiliationAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdding: false,
    };
  }

  render() {
    const component = this;
    const { Caseworker } = this.props;
    if (this.state.isAdding) {
      return (
        <form
          className="affiliation new"
          onSubmit={(event) => {
            event.preventDefault();
            const organizationName = Array.from(event.target.getElementsByTagName('input')).reduce(function (accumulator, input) {
              return accumulator + (input.type === 'text' ? input.value : '');
            }, '');
            let advancingBlocked = false;
            Meteor.call(
              '/organization/checkName',
              organizationName,
              handleError({
                calback: (response) => {
                  if(response.code !== 200) advancingBlocked = true;
                  if(response.code === 409) {
                    let message = 'An organization with this name has already been entered. Try selecting its name in the dropdown';
                    Bert.alert(message, 'danger');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                       let el = document.querySelectorAll('.bert-content')[0];
                       if (el) {
                           el.innerHTML = `<p>${message}</p>`;
                       }
                    }
                  }
                }
              })
            );
            if(!advancingBlocked) {
              const business = new Business({
                adminIds: [caseworker._id],
                name: organizationName
              });
              Meteor.call(
                '/organization/create',
                business,
                handleError({
                  callback: (response) => {
                    const affiliation = new OrganizationAffiliation();
                    affiliation.organizationId = response._id;
                    Caseworker.organizations.push(affiliation);
                    Meteor.call(
                      '/user/Caseworker/update',
                      Caseworker,
                      handleError({
                        callback() {
                          component.setState({ isAdding: false });
                        }
                      })
                    );
                  }
                })
              );
            }
          }}
        >
          <h2>Add Organization</h2>
          <AssociateOrganization
            onChoose={function(organization) {
              //const affiliation = new OrganizationAffiliation();
              //affiliation.organizationId = organization._id;
              //Caseworker.organizations.push(affiliation);
              Caseworker.employer = organization._id;
              Meteor.call(
                '/user/Caseworker/update',
                Caseworker,
                handleError({
                  callback() {
                    component.setState({ isAdding: false });
                  },
                }),
              );
            }}
          />
          <label>
            <input
              type="button"
              value="Cancel"
              onClick={function(event) {
                event.preventDefault();
                component.setState({ isAdding: false });
              }}
            />
            <input
              type="submit"
              value="Add"
            />
          </label>
        </form>
      );
    }
    return (
      <CCButton
        type="create"
        className="organization"
        onClick={(event) => {
          event.preventDefault();
          this.setState({ isAdding: true });
        }}
      >
        Add Organization
      </CCButton>
    );
  }
}

export { CaseworkerAffiliation as default, CaseworkerAffiliationAdd };
