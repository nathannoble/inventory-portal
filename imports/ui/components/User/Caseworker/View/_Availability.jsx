import { Meteor } from 'meteor/meteor';
import React from 'react';
import DateTime from 'react-datetime';
import moment from 'moment';

import AbstractDuration from '/imports/api/helpers/AbstractDuration';

import { DaySelector, CCButton, handleError } from '/imports/ui/helpers';

const CaseworkerAvailabilitySlot = ({ slot, index, onRemove, onChange }) => <div className="availability">
  <DateTime inputProps={{placeholder: 'Start of Availability'}} defaultValue={moment(slot.times.start, 'Hmm')} dateFormat={false} onChange={time => {
    slot.times.start = Number(time.format('Hmm'));
    onChange(slot);
  }}/>
  <DateTime inputProps={{placeholder: 'End of Availability'}} defaultValue={moment(slot.times.end, 'Hmm')} dateFormat={false} onChange={time => {
    slot.times.end = Number(time.format('Hmm'));
    onChange(slot);
  }}/>
  <DaySelector selected={slot.days} name="availability" prefix={`availability-${index}-`} onChange={days => {
    slot.days = days;
    onChange(slot);
  }} />
  <CCButton type="remove" onClick={event => {
    event.preventDefault();
    onRemove();
  }} />
</div>

const CaseworkerViewAvailability = ({ Caseworker }) => <form className="Caseworker availability">
  <h2>Your General Availability</h2>
  {Caseworker.availability && Caseworker.availability.length
    ? Caseworker.availability.map(
      (availabilitySlot, index) => <CaseworkerAvailabilitySlot
        key={index}
        index={index}
        slot={availabilitySlot}
        onRemove={() => {
          Caseworker.availability.splice(index, 1);
          Meteor.call('/user/Caseworker/update', Caseworker, handleError);
        }}
        onChange={slot => {
          Caseworker.availability[index] = slot;
          Meteor.call('/user/Caseworker/update', Caseworker, handleError);
        }}
      />)
      : null
    }
    <CCButton type="add" onClick={event => {
      event.preventDefault();
      if(Caseworker.availability.length) {
        Caseworker.availability.push(new AbstractDuration);
      } else {
        Caseworker.availability = [new AbstractDuration];
      }
      Meteor.call('/user/Caseworker/update', Caseworker, handleError);
    }}>{Caseworker.availability.length ? null : 'Add Availability Slot'}</CCButton>
  </form>;

  export default CaseworkerViewAvailability;
