import { Meteor } from 'meteor/meteor';
import React from 'react';
import { NavLink, Link } from 'react-router-dom';

import CC from '/imports/api';

import { DashboardLink } from '/imports/ui/components/helpers/Navigation';

const NavigationCaseworker = ({ caseworker, messageCount }) => (
    <ul className="navigation navigation--Caseworker">
        <li>
            <DashboardLink />
        </li>
        <li>
            <NavLink to={`/user/${caseworker._id}`}>{CC.icon('profile')}{Meteor.user().name}</NavLink>
        </li>
        <li>
            <NavLink to="/order/cw">{CC.icon('order')}Orders</NavLink>
        </li>
        <li>
            <NavLink to="/order/report">{CC.icon('report')}Reports</NavLink>
            <ul>
                <li>
                    <NavLink to="/order/report">Orders Report</NavLink>
                </li>
                <li>
                 <NavLink to="/order/cpTotalsReport">CP Totals Report</NavLink>
                 </li>
                <li>
                    <NavLink to="/order/itemTotalsReport">Item Totals Report</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsReport">CB (Shirt Size) Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsShoesReport">CB (Shoe Size) Rpt</NavLink>
                </li>
                <li>
                    <NavLink to="/order/cbTotalsPantsReport">CB (Pant Size) Rpt</NavLink>
                </li>
            </ul>
        </li>
        <li>
            <NavLink to="/order/help">{CC.icon('help')}Help</NavLink>
        </li>
    </ul>
);

export default NavigationCaseworker;
