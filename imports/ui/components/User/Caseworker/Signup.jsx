import React from 'react';

import AssociateOrganization from '/imports/ui/components/User/Caseworker/Associate';

const SignupCaseworker = ({ onChooseOrganization }) => (
  <fieldset name="Caseworker">
    <label htmlFor="organizationSearch">
      Enter the Care Provider you are associated with.
    </label>
    <AssociateOrganization onChoose={onChooseOrganization} />
  </fieldset>
);

export default SignupCaseworker;
