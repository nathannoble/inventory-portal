import React from 'react';
import { Link } from 'react-router-dom';
import DateTime from 'react-datetime';

import { Bert } from 'meteor/themeteorchef:bert';

import container from '/imports/ui/helpers/container';
import Business from '/imports/api/Organization/Business';
import Caseworker from '/imports/api/User/Caseworker';

import CC from '/imports/api';

import {
    handleError,
    Select,
    Checkbox,
    SubHeader,
    Breadcrumbs,
    Label,
} from '/imports/ui/helpers';

class CaseworkerNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      emails: [
        {
          address: "",
          verified: true,
        },
      ],
      profile: {
        organizationId:(Meteor.user()
            .administer()
            .count() > 0 && Meteor.user().type !== 'Administrator'
            ? Meteor.user()
            .administer()
            .fetch()[0]._id
            : undefined),
      },
    };
  }

  render() {
    return (
        <div className="caseworker user new">
          {Meteor.user().may.create.user() ? (
              <form onSubmit={(event) => {
          event.preventDefault();

          const history = this.props.history;
          const name = this.nameInput.ref.value;
          const caseworker = new Caseworker({
            name,
            phone: this.phoneInput.ref.value,
            emails: [
              {
                address: this.emailInput.ref.value,
                verified: true,
              },
            ],
            profile: {
              role:"caseworker",
              organizationId:this.state.organizationId
            },
          });

          console.log("Caseworker/New:pre create:" + JSON.stringify(caseworker));

          Meteor.call(
            'user/caseworker/create',
            caseworker,
            handleError({
              callback: (caseworkerId) => {
                console.log(JSON.stringify("Caseworker/New:post create:caseworkerId:" + caseworkerId));
                history.push(`/user/caseworker/${caseworkerId}`);
              },
            }),
          );
        }}>

                <Label name="name" required text="Name" type="text"
                       placeholder="New Caseworker's Name"
                       ref={input => (this.nameInput = input)}/>
                <Label name="email"
                       required text="Email"
                       type="email"
                       placeholder="newuser@domain.com"
                       ref={input => (this.emailInput = input)}/>
                <Label
                      name="phone"
                      text="Phone Number"
                      type="tel"
                      placeholder="(530) 123-4567"
                      ref={input => (this.phoneInput = input)}/>

                <Label
                    required
                    name="businessId"
                    text="Care Provider"
                    value={this.state.organizationId}
                    element={Select}
                    options={
                       Business.find({},{ sort: { 'name': 1 }})
                      .fetch()
                      .filter(organization => organization.type === 'Business')
                      .map(organization =>
                        Object({ value: organization._id, label:(organization.name + " (" + organization.cpNumber + ")" ).toString()}))
                }
                    onChange={(selectedOption) => {
                 this.setState({organizationId: selectedOption.value});
                 console.log(JSON.stringify(this.state));
                 }}
                />

                <label>
                  <input type="submit" value="Add Caseworker" />
                </label>
              </form>
          ) : null}
        </div>
    );
  }
}

export default container((props, onData) => {
  const businessSubscription = Meteor.subscribe('/organization/business/list');
  if (businessSubscription.ready()) {
    onData(null, {});
  }
}, CaseworkerNew);