import React from 'react';
import CC from '/imports/api';

import { DashboardIcon } from '/imports/ui/pages/Dashboard';
import NewOrders from '/imports/ui/pages/Dashboard/NewOrders';
import Announcements from '/imports/ui/pages/Dashboard/Announcements';

const DashboardCaseworker = ({ caseworker }) => (
  <div className="Caseworker dashboard">
    <Announcements />
    <DashboardIcon
    icon="clipboard-check"
    label="Past Orders"
    linkTo="/order/cw"
    className="order"
    />
    <DashboardIcon
    icon={CC.mdiIcons.create}
    label="Add New Order"
    linkTo="/order/new"
    className="order"
    />
    <NewOrders />
  </div>
);

export default DashboardCaseworker;
