import React from 'react';
import { Redirect, Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import AdministratorRoutes from '/imports/ui/components/User/Administrator/Routes';
import CaseworkerRoutes from '/imports/ui/components/User/Caseworker/Routes';
import VolunteerRoutes from '/imports/ui/components/User/Volunteer/Routes';

import UserProfileSwitcher from '/imports/ui/components/User';

const UserRoutes = (props) => {
  const roles = [
    'volunteer',
    'caseworker',
    'administrator',
  ];
  let role = props.match.params.userType;
  if (roles.includes(role) || roles.includes(props.match.params.userId)) {
    if (roles.includes(props.match.params.userId)) role = props.match.params.userId;
    return {
      volunteer: <VolunteerRoutes {...props} />,
      caseworker: <CaseworkerRoutes {...props} />,
      administrator: <AdministratorRoutes {...props} />,
      }[role];
  }
  return <UserProfileSwitcher {...props} />;
};

export default UserRoutes;
