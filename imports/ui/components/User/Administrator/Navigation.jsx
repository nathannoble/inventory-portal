import { Meteor } from 'meteor/meteor';
import React from 'react';
import { NavLink, Link } from 'react-router-dom';

import CC from '/imports/api';

import { DashboardLink } from '/imports/ui/components/helpers/Navigation';

// import { Label } from '/imports/ui/helpers';

import DateTime from 'react-datetime';

import container from '/imports/ui/helpers/container';

const NavigationAdministrator = container(
    (props, onData) => {
        //console.log("components/User/Administrator/Navigation.jsx" + JSON.stringify(props));

        let startDate;
        let endDate;

        // let now = new Date();

        // // This Week (Until this Friday at 5:00 PM)
        // let nextFriday = 5;
        // let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        // if (daysUntilPickupLocationDay < 0) {
        //     daysUntilPickupLocationDay += 7;
        // } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
        //     daysUntilPickupLocationDay += 7;
        // }

        // if (props.startDate) {
        //     startDate = new Date(0);
        //     startDate.setUTCSeconds(parseInt(props.startDate, 0) / 1000);
        //     endDate = new Date(0);
        //     endDate.setUTCSeconds(parseInt(props.endDate, 0) / 1000);
        // } else {
        //     startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        //     endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
        // }

        // startDate.setHours(0);
        // startDate.setMinutes(0);
        // startDate.setSeconds(0);
        // startDate.setMilliseconds(0);

        // endDate.setHours(23);
        // endDate.setMinutes(59);
        // endDate.setSeconds(59);
        // endDate.setMilliseconds(999);

        // this.state = {
        //     startDate: startDate,
        //     endDate: endDate
        // };

        onData(null, { startDate, endDate });

    },
    ({ administrator, startDate, endDate }) => (
        <ul className="navigation navigation--administrator">
            <li>
                <DashboardLink />
            </li>
            <li>
                <NavLink to={`/user/${administrator._id}`}>{CC.icon('profile')}{Meteor.user().name}</NavLink>
            </li>
            <li>
                <NavLink to="/business">{CC.icon('business')}Care Providers</NavLink>
            </li>
            <li>
                <NavLink to="/administrator/allusers">{CC.icon('student')}All Users</NavLink>
            </li>
            <li>
                <NavLink to="/order">{CC.icon('order')}Orders</NavLink>
            </li>
            <li>
                <NavLink to="/order/labels">{CC.icon('file-pdf-box')}Order Labels</NavLink>
            </li>
            {/* <li><span>Choose Dates</span>
            <ul>
                
            </ul>
        </li> */}
            <li>
                <NavLink to="/order/report">{CC.icon('report')}Reports</NavLink>
                <ul>
                    <li>
                        <NavLink to="/order/report">Orders Report</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/cpTotalsReport">CP Totals Report</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/centerTotalsReport">Center Totals Rpt</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/itemTotalsReport">Item Totals Report</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/itemTotalsByZipReport">Item Totals By Zip</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/cbTotalsReport">CB (Shirt Size) Rpt</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/cbTotalsShoesReport">CB (Shoe Size) Rpt</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/cbTotalsPantsReport">CB (Pant Size) Rpt</NavLink>
                    </li>
                    <li>
                        <NavLink to="/order/printLabels">Print Labels</NavLink>
                    </li>
                </ul>
            </li>
            <li>
                <NavLink to="/order/help">{CC.icon('help')}Help</NavLink>
            </li>
        </ul>
    ),
);

export default NavigationAdministrator;
// export default container((props, onData) => {
//     onData(null, { startDate, endDate });
// }, NavigationAdministrator);

