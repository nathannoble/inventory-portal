import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import { Bert } from 'meteor/themeteorchef:bert';

import Administrator from '/imports/api/User/Administrator';

// import { OrderStatus, OrderFilter } from '/imports/api/Order';

import AdministratorViewForm from '/imports/ui/components/User/Administrator/View/_Form';

import AvatarUpload from '/imports/ui/components/AvatarUpload';

import container from '/imports/ui/helpers/container';

// import ChangeUserType from '/imports/ui/components/User/ChangeUserType';
import { handleError, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
// import Drawer from '/imports/ui/components/helpers/Drawer';

import MyOrders from '../../../../pages/Dashboard/MyOrders';

const AdministratorView = container(
  (props, onData) => {
    // If administrator has an ID, and this form is being rendered on /user/administrator/new,
    // redirect to the proper page.
    // Done upon creation of a user

    if (
      props.administrator &&
      props.administrator._id &&
      props.location &&
      props.location.pathname === '/user/administrator/new'
    ) {
      props.history.push(`/user/administrator${props.administrator._id}`);
    }
    let { administrator } = props;
    const administratorId = props.match ? props.match.params.administratorId : administrator._id;
    const subscription = Meteor.subscribe('/user/read', administratorId);
    if (subscription.ready() || administrator) {
      if (!administrator) administrator = Administrator.findOne(administratorId);
      if (administrator) {
           //console.log("AdministratorView:yes:props:" + JSON.stringify(props));
        onData(null, { administrator });
      } else {
            //console.log("AdministratorView:no:props:" + JSON.stringify(props));
            let message = 'Administrator not found.  Redirecting to User List.';
            Bert.alert(message);
            if (document.querySelectorAll('.bert-content').length > 0) {
              let el = document.querySelectorAll('.bert-content')[0];
              if (el) {
                  el.innerHTML = `<p>${message}</p>`;
              }
            }
            props.history.push(`/user/administrator`);
      }
    }
  },
  ({ administrator }) => (
    <div className="volunteer user view">
      {Meteor.user().type === 'Administrator' ? (
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[
              { text: 'Users' },
              { link: '/user/administrator', text: 'Administrators' },
              { text: administrator.name },
            ]}
          />
          {/*<Drawer>

            <CCButton
              type="remove"
              onClick={(event) => {
                event.preventDefault();
                Meteor.call('/user/administrator/delete', administrator, handleError({
                  callback: () => {},
                }));
              }}
            >
              Delete User
            </CCButton>
          </Drawer>*/}
        </SubHeader>
      ) : null}
      <AdministratorViewForm administrator={administrator} />
      <div className="sidebar">
        <AvatarUpload
          entity={administrator}
          onUpload={(administrator) => {
            Meteor.call('/user/administrator/update', administrator, handleError);
          }}
          onRemove={(administrator, callback) => {
            Meteor.call('/user/administrator/update', administrator, handleError({ callback }));
          }}
        />
        <MyOrders user={administrator}/>
      </div>
    </div>
  ),
);

export default AdministratorView;
