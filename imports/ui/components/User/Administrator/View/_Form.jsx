import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';

import CC from '/imports/api';
import container from '/imports/ui/helpers/container';

import Business from '/imports/api/Organization/Business';

import {
    CCButton,
    PasswordChanger,
    handleError,
    Select,
    Label,
    Checkbox,
} from '/imports/ui/helpers';



// React Select doc - https://react-select.com

const AdministratorViewForm = ({ administrator }) => (
    <form className="administrator user">
        <h2>Profile Information</h2>

        <Label
            name="name"
            required
            text="Name"
            type="text"
            placeholder="Your Name"
            defaultValue={administrator.name}
            onChange={(event) => {
        administrator.name = String(event.target.value);
        Meteor.call('/user/administrator/update', administrator, handleError);
      }}
        />

        <PasswordChanger user={administrator}/>

        <Label
            name="email"
            required
            text="Email"
            type="email"
            placeholder="your@email.com"
            defaultValue={administrator.emails[0].address}
            onChange={(event) => {
        administrator.emails[0].address = String(event.target.value);
        Meteor.call(
          '/user/administrator/update',
          administrator,
          handleError({ confirmationText: 'Email Updated' }),
        );
      }}
        />

        <Label
            name="phone"
            text="Phone Number"
            type="tel"
            placeholder="(530) 123-4567"
            defaultValue={administrator.phone}
            onChange={(event) => {
        administrator.phone = String(event.target.value);
        Meteor.call('/user/administrator/update', administrator, handleError);
      }}
        />

            <label htmlFor="businessId">
                Care Provider
                <Select
                    text="All Locations"
                    id="businessId"
                    name="businessId"
                    placeholder="All Locations"
                    value={administrator.profile.organizationId}
                    options={
                       Business.find({},{ sort: { 'name': 1 }})
                       .fetch()
                       .filter(organization => organization.type === 'Business')
                       .map(organization => Object({ value: organization._id, label:(organization.name + " (" + organization.cpNumber + ")" ).toString()}))
                    }
                    onChange={(selection) => {
                        console.log('User/Administrator/View/_Form.jsx:onChange:selection:' + JSON.stringify(selection));

                        let oldValue = administrator.profile.organizationId;

                        if (selection && selection.value) {
                            administrator.profile.organizationId = selection.value;
                        } else {
                            administrator.profile.organizationId = null;
                        }
                        if (administrator.profile.organizationId !== oldValue) {
                            Meteor.call('/user/administrator/update', administrator, handleError);
                        }
                    }}
                >
                    <option default>Select2</option>
                </Select>
            </label>

        <Label
            name="active"
            text="Status"
            value={administrator.inactive ? (administrator.inactive == true ? "False" : "True") : "True"  }
            element={Select}
            required
            options={[
                { value: "True", label: 'Active' },
                { value: "False", label: 'Inactive' }
              ]}
            onChange={({ value }) => {
                console.log('User/Administrator/View/_Form.jsx:inactive:onChange:value:' + value);
                if (value == "False")
                    administrator.inactive = true;
                else
                    administrator.inactive = false; 
                Meteor.call('/user/administrator/update', administrator, handleError);
            }}
        />

        <Label
            name="bio"
            text="Bio"
            element="textarea"
            defaultValue={administrator.bio}
            onChange={(event) => {
                administrator.bio = String(event.target.value);
                Meteor.call('/user/administrator/update', administrator, handleError);
            }}
        />

    </form>
);

export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');

    if (businessSubscription.ready()) {
        onData(null, {});
    }

}, AdministratorViewForm);
