import React from 'react';
import listify from 'listify';

const AdministratorCardFields = ({ administrator }) => (
  <div>
    <span className="bio">
      <span className="label">Bio</span>
      <span className="value">{administrator.bio}</span>
    </span>
    {administrator.worksFor().map((organization) => {
      const affiliation = administrator.affiliationWith(organization);
      return (
        <span className="organization" key={organization._id}>
          <span className="label">{affiliation.titleAtOrganization || 'works'} at</span>
          <span className="value">
            {organization.name}
            <br />
            {affiliation.department ? ` in the ${affiliation.department} department` : null}
          </span>
        </span>
      );
    })}
  </div>
);

export default AdministratorCardFields;
