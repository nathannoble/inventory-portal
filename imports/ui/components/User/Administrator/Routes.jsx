import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import AdministratorNew from './New';
import AdministratorView from './View';
import AdministratorAdminTable from './AdminTable';
import AdministratorList from './List';

import CaseworkerAdminTable from '../Caseworker/AdminTable';

const AdministratorRoutes = props => (
	<Switch className="administrator">
		<Authenticated exact name="AdministratorNew" path={`${props.match.path}/new`} component={AdministratorNew} {...props} />
		<Authenticated exact name="AdministratorList" path={`${props.match.path}/allusers`} component={CaseworkerAdminTable}  {...props} />
		<Authenticated exact name="AdministratorList" path={`${props.match.path}`} component={AdministratorAdminTable} {...props} />
		<Authenticated exact name="AdministratorView" path={`${props.match.path}/:administratorId`} component={AdministratorView} {...props} />
	</Switch>
);

export default AdministratorRoutes;
