import React from 'react';
import _ from 'underscore';
import { Link } from 'react-router-dom';
import listify from 'listify';

import Administrator from '/imports/api/User/Administrator';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import AdminTable from '/imports/ui/components/AdminTable';

import container from '/imports/ui/helpers/container';

Administrator.adminTable = new AdminTableDefinition();

Administrator.adminTable.push({
  transform: administrator => <Avatar user={administrator} />,
  className: 'avatar',
  export: false,
  visible: true,
});

Administrator.adminTable.push({
  header: 'Name',
  className: 'name',
  sort: (a, b) => {
    a = a.name;
    b = b.name;
    return compare(a, b);
  },
  transform: (administrator, willExport = false) => {
    if (willExport) {
      return (
        <a title={administrator.name} href={`/user/${administrator._id}`}>
          {administrator.name}
        </a>
      );
    }
    //console.log("ui/components/User/Administrator/AdminTable.jsx:administrator:" + JSON.stringify(administrator));
    return (
      <Link title={administrator.name} to={`/user/${administrator._id}`}>
        {administrator.name}
      </Link>
    );
  },
  visible: true,
});

Administrator.adminTable.push({
  header: 'Registered',
  className: 'createdAt',
  sort: (a, b) => {
    a = a.createdAt;
    b = b.createdAt;
    return compare(a, b);
  },
  transform: administrator => helperDate(administrator.createdAt),
  visible: true,
});

Administrator.adminTable.push({
  header: 'Care Provider',
  className: 'affiliations',
  sort: (a, b) => {
    a = a.profile.organizationId;
    b = b.profile.organizationId;
    return compare(a, b);
  },
  transform: (administrator) => {
    return (
        Meteor.user()
            .administer()
            .fetch()
            .filter(organization => organization._id === administrator.profile.organizationId)
            .map(organization => organization.name)
    );
  },
  visible: true,
});

Administrator.adminTable.push({
  header: 'Type',
  className: 'affiliations',
  sort: (a, b) => {
    a = a.type;
    b = b.type;
    return compare(a, b)
  },
  transform: administrator => administrator.type === 'Volunteer' ? 'Center Director': administrator.type ,
  visible: true,
});

Administrator.adminTable.push({
  header: 'Email',
  className: 'email',
  transform: administrator => <a href={`mailto:${administrator.email}`}>{administrator.email}</a>,
  visible: true,
});

const AdministratorAdminTable = props => <AdminTable collection={Administrator} {...props} />;

export default container((props, onData) => {
  const administratorSubscription = Meteor.subscribe('/user/administrator/list');
  const businessSubscription = Meteor.subscribe('/organization/business/list');
  if (administratorSubscription.ready() && businessSubscription.ready()) {
    //const administrators = Administrator.find({profile: {role: "administrator" }}, {sort: {createdAt: -1}}).fetch();

    onData(null, {});
  }
}, AdministratorAdminTable);
