import React from 'react';

import CC from '/imports/api';

import { DashboardIcon } from '/imports/ui/pages/Dashboard';

import User from '/imports/api/User';
import Business from '/imports/api/Organization/Business';

import BusinessApprovalQueue from '/imports/ui/pages/Dashboard/BusinessApprovalQueue';
import SearchLog from '/imports/ui/pages/Dashboard/SearchLog';

import NewUsers from '/imports/ui/pages/Dashboard/NewUsers';
import MyUser from '/imports/ui/pages/Dashboard/MyUser';

import Announcements from '/imports/ui/pages/Dashboard/Announcements';
import MyDates from '/imports/ui/pages/Dashboard/MyDates';

const DashboardAdministrator = ({ administrator }) => (
    <div className="administrator dashboard">
        {/*<MyUser />*/}
        <Announcements />
        <DashboardIcon
            icon={CC.mdiIcons.business}
            label={`${Business.find().count()} Care Providers`}
            linkTo="/business"
            className="internship"
        />
        <DashboardIcon
            icon={CC.mdiIcons.create}
            label="New Care Provider"
            linkTo="/business/new"
            className="internship"
        />
        <DashboardIcon
            icon={CC.mdiIcons.volunteer}
            label={`${User.find({
                $or: [{ 'profile.role': 'organization' }, { 'profile.role': 'volunteer' }],
            }).count()} Center Directors`}
            linkTo="/user/volunteer"
            className="student"
        />
        <DashboardIcon
            icon={CC.mdiIcons.create}
            label="New Center Director"
            linkTo="/user/volunteer/new"
            className="student"
        />
        <DashboardIcon
            icon={CC.mdiIcons.create}
            label="New Case Worker"
            linkTo="/user/caseworker/new"
            className="educator"
        />
        <DashboardIcon icon="clipboard-check" label="Orders" linkTo="/order" className="order" />
        <DashboardIcon
            icon={CC.mdiIcons.create}
            label="New Order"
            linkTo="/order/new"
            className="order"
        />
        <MyDates user={administrator}/>
        <NewUsers />

        {/* <NewOrders />*/}
    </div>
);

export default DashboardAdministrator;
