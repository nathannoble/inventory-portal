import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Helmet } from 'react-helmet';

const Head = () => (
    <Helmet>
        <meta name="application-name" content="Northwest Children's Outreach" />
        <meta name="description" content="Reaching out to Meet the Need" />
        <meta name="google" content="nositelinkssearchbox" />
        <meta name="generator" content="meteor" />
        <meta httpEquiv="Content-Language" content="en_US" />
        <link rel="author" href="http://northwestchildrensoutreach.org/" />
        <link rel="license" href="https://www.gnu.org/licenses/gpl.html" />
        {process.env.NODE_ENV === 'development' ? <script src="http://localhost:8097" /> : null}

        <link
         rel="apple-touch-icon"
         sizes="180x180"
         href={Meteor.absoluteUrl('img/favicon/apple-touch-icon.png')}
         />
         <link
         rel="icon"
         type="image/png"
         sizes="32x32"
         href={Meteor.absoluteUrl('img/favicon/favicon-32x32.png')}
         />
         <link
         rel="icon"
         type="image/png"
         sizes="16x16"
         href={Meteor.absoluteUrl('img/favicon/favicon-16x16.png')}
         />
         {/*<link
         rel="mask-icon"
         href={Meteor.absoluteUrl('img/favicon/safari-pinned-tab.svg')}
         color="#ff9800"
         />*/}

        <meta name="theme-color" content="#ff9800" />

        <meta property="fb:app_id" content="1362376433801579" />
        <meta property="og:url" content={Meteor.absoluteUrl()} />
        <meta property="og:type" content="application" />
        <meta property="og:title" content="Northwest Children's Outreach" />
        <meta property="og:image" content={Meteor.absoluteUrl('img/favicon/og_image.png')} />
        <meta property="og:description" content="Reaching out to Meet the Need" />
        <meta property="og:site_name" content="Northwest Children's Outreach" />
        <meta property="og:locale" content="en_US" />
        <meta property="article:author" content="http://northwestchildrensoutreach.org/" />

        {/*   <meta name="twitter:card" content="summary" />
         <meta name="twitter:creator" content="@tylershuster" />
         <meta name="twitter:url" content={Meteor.absoluteUrl()} />
         <meta name="twitter:title" content="Northwest Children's Outreach" />
         <meta name="twitter:description" content="Connecting students, teachers, and businesses" />
         <meta name="twitter:image" content={Meteor.absoluteUrl('img/favicon/og_image.png')} />
         */}

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="#ff9800" />
        <meta name="apple-mobile-web-app-title" content="Northwest Children's Outreach" />
    </Helmet>
);

export default Head;
