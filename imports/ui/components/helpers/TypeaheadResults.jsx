import React from 'react';
import PropTypes from 'prop-types';

const TypeaheadResults = ({ results, onSelect }) => (
  <ul className="list list--typeahead">
    {results.map(result => (
      <li
        key={result._id}
        onClick={(event) => {
          onSelect(event.target.dataset.id);
        }}
        data-id={result._id}
      >
        {result.name}
      </li>
    ))}
  </ul>
);

TypeaheadResults.propTypes = {
  results: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.name,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default TypeaheadResults;
