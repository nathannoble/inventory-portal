import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';

import CC from '/imports/api';

class CCButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isConfirming: false,
    };

    this.icons = {
      close: CC.icon('close'),
      remove: CC.icon('remove'),
      add: CC.icon('create'),
      copy: CC.icon('content-copy'),
      up: CC.icon('arrow-up'),
      right: CC.icon('arrow-right'),
      left: CC.icon('arrow-left'),
      down: CC.icon('arrow-down'),
      download: CC.icon('download'),
      save: CC.icon('check'),
      view: CC.icon('eye'),
      edit: CC.icon('pencil'),
      print: CC.icon('printer'),
      help: CC.icon('help'),
      pdf: CC.icon('file-pdf-box')
    };

    this.onClickHandler = this.onClickHandler.bind(this);
  }

  onClickHandler(event) {
    event.stopPropagation();
    if (this.props.type === 'remove' && !this.state.isConfirming) {
      event.preventDefault();
      this.setState({ isConfirming: true });
      Meteor.setTimeout(() => {
        this.setState({ isConfirming: false });
      }, 2000);
    } else {
      this.props.onClick(event);
    }
  }

  render() {

    //console.log("CCButton.jsx:render:this props:" + JSON.stringify(this.props));

    if (this.props.type === "pdf") { // || this.props.type === "print") {
      if (location.pathname.indexOf('/order/labels') >= 0) { // && Meteor.user().type === 'Administrator' ) {
        console.log("CCButton.jsx:render:pdf on labels page");
      } else if (location.pathname.indexOf('/order/cpTotalsReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on cpTotalsReport page");
      } else if (location.pathname.indexOf('/order/centerTotalsReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on centerTotalsReport page");
      } else if (location.pathname.indexOf('/order/itemTotalsReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on itemTotalsReport page");
      } else if (location.pathname.indexOf('/order/itemTotalsByZipReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on itemTotalsByZipReport page");
      } else if (location.pathname.indexOf('/order/cbTotalsReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on cbTotalsReport page");
      } else if (location.pathname.indexOf('/order/cbTotalsShoesReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on cbTotalsShoesReport page");
      } else if (location.pathname.indexOf('/order/cbTotalsPantsReport') >= 0 ) {
        console.log("CCButton.jsx:render:pdf on cbTotalsPantsReport page");
      } else {
        return null;
      }
    }

    const {
      type, className, children, confirmMessage, text, disabled,
    } = this.props;

    const icon = ((type) => {
      if (!type) return null;
      if (type === 'sort') {
        return (({ direction }) => {
          if (!direction) return <i className="mdi mdi-sort" />;
          return direction === 'ascending' ? (
            <i className="mdi mdi-sort-ascending" />
          ) : (
            <i className="mdi mdi-sort-descending" />
          );
        })(this.props);
      }
      return this.icons[type];
    })(type);

    return (
      <button
        className={`${icon ? 'has-icon' : ''}${disabled ? ' disabled' : ''} ${type ||
          ''} ${className}`}
        onClick={this.onClickHandler}
      >
        {this.state.isConfirming ? confirmMessage || 'Are you sure?' : children}
        {text || null}
        {icon}
      </button>
    );
  }
}

CCButton.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node,
  confirmMessage: PropTypes.string,
  text: PropTypes.string,
};

CCButton.defaultProps = {
  className: '',
  onClick: event => event.preventDefault(),
  children: '',
  text: '',
};

export default CCButton;
