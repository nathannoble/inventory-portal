import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

const Modal = ({
  children, onClose, duration, header, className,
}) => {
  const modal = document.getElementById('modalWrapper');
  return ReactDOM.createPortal(
    <div
      className={`modal ${className}`}
      style={{ animationDuration: `${duration}ms` }}
      onClick={(event) => {
        $(event.target).addClass('hiding');
        Meteor.setTimeout(() => {
          if (onClose) onClose(event);
        }, duration);
      }}
    >
      <i className="mdi mdi-close" />
      <div
        className="content"
        onClick={event => event.stopPropagation()}
        role="textbox"
        onKeyDown={event => event.stopPropagation()}
      >
        {header ? <span className="header">{header}</span> : null}
        {children}
      </div>
    </div>,
    modal,
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  onClose: PropTypes.func.isRequired,
  duration: PropTypes.number,
  header: PropTypes.string,
  className: PropTypes.string,
};

Modal.defaultProps = {
  duration: 500,
  header: false,
  className: '',
};

export default Modal;
