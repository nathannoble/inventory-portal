// An extension of the React route component for checking that the user is logged in
import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { Session } from 'meteor/session';
import { Bert } from 'meteor/themeteorchef:bert';

import CC from '/imports/api';

import handleError from '/imports/ui/helpers/handleError';
import { WrappedRoute } from '/imports/ui/helpers/errorBoundary';

const Authenticated = ({
  loggingIn, authenticated, component, roles, ...rest
}) => (
  <WrappedRoute
    {...rest}
    render={(props) => {
      if (loggingIn) return <div>Logging In</div>;

      const user = Meteor.user();
      if (!user) return <Redirect to="/login" />;

      if (roles && roles.indexOf(user.type.toLowerCase()) < 0 && user.type !== 'Administrator') {
 return (
   <div className="forbidden unauthorized">
     <div>
       <h2>Unauthorized</h2>
       <p>You are not authorized to view this page. Authorized roles: {roles.join(', ')}</p>
     </div>
   </div>
        );
}

      return React.createElement(component, { ...props, loggingIn, authenticated });
    }}
  />
);

Authenticated.propTypes = {
  loggingIn: PropTypes.bool,
  authenticated: PropTypes.bool,
  component: PropTypes.func,
};

export default Authenticated;
