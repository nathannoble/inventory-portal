import React from 'react';
import PropTypes from 'prop-types';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import handleError from '/imports/ui/helpers/handleError';

class PasswordChanger extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isChanging: false,
            passwordsMatch: false,
        };
    }

    handleError(error) {
        if (error.error === 403) {
            this.oldPasswordInput.focus();
            this.oldPasswordInput.selectionStart = 0;
            this.oldPasswordInput.selectionEnd = this.oldPasswordInput.value.length;

            let message = 'Incorrect current password entered';
            Bert.alert(message);
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }
        } else {
            handleError(error);
        }
    }

    render() {
        const user = this.props.user;
        return (
            <label htmlFor="password">
                Password
                <input
                    ref={input => (this.newPasswordInput = input)}
                    type="text"
                    name="newPassword"
                    placeholder="Change Password"
                    autoComplete="off"
                    onKeyUp={event => this.setState({ isChanging: !!event.target.value })}
                />
                {this.state.isChanging ? (
                    <input
                        ref={input => (this.confirmationInput = input)}
                        type="text"
                        name="confirmation"
                        placeholder="Confirm new password"
                        autoComplete="off"
                        onKeyUp={event =>
              this.setState({
                passwordsMatch: this.confirmationInput.value === this.newPasswordInput.value,
              })}
                    />
                ) : null}
                {this.state.passwordsMatch ? (
                    <input
                        ref={input => (this.oldPasswordInput = input)}
                        type="text"
                        placeholder="Enter current password to change"
                        onKeyUp={(event) => {
                          if (event.keyCode === 13) {
                            Accounts.changePassword(
                              this.oldPasswordInput.value,
                              this.newPasswordInput.value,
                              (error) => {
                                if (error) {
                                  this.handleError(error);
                                } else {
                                  this.setState({
                                    isChanging: false,
                                    passwordsMatch: false,
                                  });
                                  this.newPasswordInput.value = '';

                                  let message = 'Password changed';
                                  Bert.alert(message);
                                   if (document.querySelectorAll('.bert-content').length > 0) {
                                       let el = document.querySelectorAll('.bert-content')[0];
                                       if (el) {
                                           el.innerHTML = `<p>${message}</p>`;
                                       }
                                   }
                                }
                              },
                            );
                          }
                    }}
                    />
                ) : null}
            </label>
        );
    }
}

export default PasswordChanger;
