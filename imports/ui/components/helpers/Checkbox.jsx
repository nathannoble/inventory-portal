import React from 'react';
import PropTypes from 'prop-types';

const Checkbox = ({
  id, className, checked, label, ...props
}) => (
  <label htmlFor={id} className={`checkbox ${className} ${checked ? 'checked' : 'unchecked'}`}>
    {label || (checked ? (
      <i className="mdi mdi-checkbox-marked-outline" />
    ) : (
      <i className="mdi mdi-checkbox-blank-outline" />
    ))}
    <input type="checkbox" id={id} checked={checked} {...props} />
  </label>
);

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
  checked: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
};

Checkbox.defaultProps = {
  className: '',
  checked: false,
  label: '',
};

export default Checkbox;
