import React from 'react';
import { withGoogleMap, GoogleMap, Marker, InfoWindow, withScriptjs } from 'react-google-maps';
import Script from 'react-load-script';

const googleMapURL = `https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=${Meteor
  .settings.public.googleAPIKey}`;

const AsyncGoogleMap = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    defaultCenter={{ lat: 40.58654, lng: -122.391675 }}
    defaultZoom={13}
    googleMapURL={googleMapURL}
    onZoomChanged={function() {
        const maxZoom = 15;
        if (this.zoom > maxZoom) {
          this.setZoom(Math.min(this.zoom, maxZoom));
        }
      }}
    ref={(map) => {
        if (map) {
          const bounds = new google.maps.LatLngBounds();
          props.markers.forEach(marker => bounds.extend(marker.latLng));
          map.fitBounds(bounds);
        }
      }}
  >
    {props.markers.map(marker => (
      <Marker key={marker._id} onClick={marker.onClick} position={marker.latLng} />
      ))}
  </GoogleMap>
)));

const BasicGoogleMap = props => (
  <AsyncGoogleMap googleMapURL={googleMapURL} loadingElement={<div>Loading</div>} {...props} />
);

export default BasicGoogleMap;
