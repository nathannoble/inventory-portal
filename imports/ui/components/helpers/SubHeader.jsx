import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class SubHeader extends React.Component {
  constructor(props) {
    super(props);
    this.portal = document.getElementById('subHeader');
  }

  render() {
    const { children } = this.props;
    return this.portal ? ReactDOM.createPortal(children, this.portal) : null;
  }
}

SubHeader.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SubHeader;
