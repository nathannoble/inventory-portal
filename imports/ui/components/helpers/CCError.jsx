import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';

const CCError = ({ error }) => {
  console.error(error);
  return (
    <div className="ccerror">
      <h1>{error.reason || (error.message === "[timeout]" ? "Waiting for this page to load...": error.message) || `Error ${error.error}`}</h1>
      {error.details ? <p>{error.details}</p> : null}
    </div>
  );
};

CCError.propTypes = {
  error: PropTypes.instanceOf(Meteor.Error),
};

CCError.defaultProps = {
  error: new Meteor.Error(500),
};

export default CCError;
