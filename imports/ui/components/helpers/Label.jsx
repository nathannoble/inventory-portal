import React from 'react';
import PropTypes from 'prop-types';

class Label extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      name, required, text, element, className, readOnly, ref, ...props
    } = this.props;
    if (
      required &&
      ((props.hasOwnProperty('defaultValue') && !props.defaultValue) ||
        (props.hasOwnProperty('value') && !props.value))
    ) {
      className += ' invalid';
    }
    if (readOnly) {
      return (
        <label htmlFor={name}>
          <span>{text}</span>
          <span className="value">{props.value || props.defaultValue}</span>
        </label>
      );
    }
    return (
      <label htmlFor={name}>
        <span>{text}</span>
        {required ? <span className="required">*</span> : null}
        {React.createElement(element, {
          id: name,
          className,
          required: !!required,
          ref: element => (this.ref = element),
          ...props,
        })}
      </label>
    );
  }
}

Label.propTypes = {
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  text: PropTypes.string,
  element: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element,
    PropTypes.string,
    PropTypes.func,
  ]),
  className: PropTypes.string,
};

Label.defaultProps = {
  required: false,
  text: '',
  element: 'input',
  className: '',
};

export default Label;
