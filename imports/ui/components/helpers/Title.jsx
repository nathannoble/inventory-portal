import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

const Title = () => (
  <Helmet>
    <title>{getTitle() !== "" ? `${getTitle()} - ` : ''}Northwest Children's Outreach</title>
  </Helmet>
);


function getTitle() {

    const children = location.pathname;
    console.log("components/helpers/Title.jsx:getTitle:children:" + children);

    if (children.indexOf("/orderUserOrg") > -1) {
        return "Order";
    } else if (children.indexOf("/user") > -1 ||
               children.indexOf("/administrator") > -1 ||
               children.indexOf("/volunteer") > -1 ||
               children.indexOf("/caseworker") > -1) {
        if (children.indexOf("/administrator/allusers") > -1) {
            return "All Users";
        } else if (children.indexOf("/user/volunteer/new") > -1) {
            return "New Center Director";
        } else if (children.indexOf("/user/volunteer") > -1) {
            return "User List";
        } else if (children.indexOf("/user/caseworker/new") > -1) {
            return "New Caseworker";
        } else if (children.indexOf("/user/") > -1) {
            return "User";
        } else {
            return "User List";
        }
    } else if (children.indexOf("/business") > -1) {

        if (children.indexOf("/business/new") > -1) {
            return "New Care Provider";
        } else if (children.indexOf("/business") > -1) {
            return "Care Provider List";
        } else {
            return "Care Provider";
        }
    } else {
        switch (children) {
            case "/order":
                return "Order List";
            case "/order/list":
                return "Order List";
            case "/order/itemTotalsByZipReport":
                return "Item Totals By Zip Code";
            case "/order/cpTotalsReport":
                return "Care Provider Totals";
            case "/order/centerTotalsReport":
                return "Center Totals";
            case "/order/itemTotalsReport":
                return "Item Totals";
            case "/order/cbTotalsReport":
                return "Clothing Bag Totals (By Shirt Size)";
            case "/order/cbTotalsShoesReport":
                return "Clothing Bag Totals (By Shoe Size)";
            case "/order/cbTotalsPantsReport":
                return "Clothing Bag Totals (By Pant Size)";
            case "/order/labels":
                return "Order Labels";

            case "/order/new":
                return "New Order";
            case "/order/printLabels":
                return "Print Order";
            case "/order/help":
                return "Order Help";
            case "/order/report":
                return "Order Report";
            case "/order/cw":
                return "Order List";
            default :
                return "";
        }
    }
}

Title.propTypes = {
  children: PropTypes.string,
};

Title.defaultProps = {
  children: '',
};

export default Title;
