import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link, Redirect, withRouter, NavLink } from 'react-router-dom';

import CC from '/imports/api';

import container from '/imports/ui/helpers/container';
import { handleError, Select } from '/imports/ui/helpers';

import NavigationVolunteer from '/imports/ui/components/User/Volunteer/Navigation';
import NavigationCaseworker from '/imports/ui/components/User/Caseworker/Navigation';
import NavigationAdministrator from '/imports/ui/components/User/Administrator/Navigation';

export const DashboardLink = () => (
  <NavLink
    to="/"
    isActive={(match, location) => {
      if (!match) return false;
      return location.pathname === '/';
    }}
  >
    {CC.icon('dashboard')}
    Dashboard
  </NavLink>
);

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
      height: 0,
      active: false,
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    if (!Meteor.user()) {
      return null;
    }

    const { history, messageCount, ...props } = this.props;
    //console.log("components/helpers/Navigation:location:" + JSON.stringify(location));
    //console.log("components/helpers/Navigation:history:" + JSON.stringify(history));
    //console.log("components/helpers/Navigation:messageCount:" + JSON.stringify(messageCount));
    //console.log("components/helpers/Navigation:this.props.children:" + JSON.stringify(this.props.children));
    return (
      <nav className={this.state.active ? 'active' : 'inactive'}>
        <div className="overlay" onClick={event => this.setState({ active: false })} />
        {this.state.width < 768 ? (
          <button id="nav-toggle" onClick={event => this.setState({ active: !this.state.active })}>
            {this.state.active ? <i className="mdi mdi-close" /> : <i className="mdi mdi-menu" />}
          </button>
        ) : null}
        <div className="nav-wrapper" onClick={event => this.setState({ active: false })}>
          {
            {
              Volunteer: (
                <NavigationVolunteer volunteer={Meteor.user()} messageCount={messageCount} />
              ),
              Caseworker: (
                <NavigationCaseworker caseworker={Meteor.user()} messageCount={messageCount}/>
              ),
              Administrator: <NavigationAdministrator administrator={Meteor.user()} />,
            }[Meteor.user().type]
          }
          {/*<button>{Meteor.user().name}</button>*/}
          <button
            id="logout"
            onClick={(event) => {
              event.preventDefault();
              Meteor.logout(handleError({
                  callback() {
                    window.location = Meteor.absoluteUrl('login', {
                      secure: process.env.NODE_ENV !== 'development',
                    });
                  },
                }));
            }}
          >
            Logout
          </button>
        </div>
      </nav>
    );
  }
}

export default withRouter(Navigation);

