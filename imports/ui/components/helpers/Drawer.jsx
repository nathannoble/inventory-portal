import React from 'react';

import CCButton from '/imports/ui/components/helpers/CCButton';

class Drawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: Object.prototype.hasOwnProperty.call(this.props, 'visible')
        ? this.props.visible
        : false,
    };
  }

  render() {
    return (
      <div
        className={`drawer ${this.state.visible ? 'visible' : 'hidden'} ${this.props.className ||
          ''}`}
      >
        {Object.prototype.hasOwnProperty.call(this.props, 'visible') &&
        this.props.visible ? null : (
          <CCButton
            type="up"
            className={`toggle ${this.state.visible ? 'up' : 'down'}`}
            onClick={(event) => {
              event.preventDefault();
              this.setState({ visible: !this.state.visible });
            }}
          />
        )}
        <div className="content">{this.props.children}</div>
      </div>
    );
  }
}

export default Drawer;
