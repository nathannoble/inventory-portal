import React from 'react';
import PropTypes from 'prop-types';

import Images from '/imports/api/helpers/Images';
import handleError from '/imports/ui/helpers/handleError';

const ImageUploader = ({ id, onUpload }) => (
  <input
    id={id}
    type="file"
    onChange={(event) => {
      if (event.currentTarget.files && event.currentTarget.files[0]) {
        const upload = Images.insert(
          {
            file: event.currentTarget.files[0],
            streams: 'dynamic',
            chunkSize: 'dynamic',
          },
          false,
        );

        upload.on('end', handleError({ callback: onUpload }));

        upload.start();
      }
    }}
  />
);

ImageUploader.propTypes = {
  id: PropTypes.string.isRequired,
  onUpload: PropTypes.func.isRequired,
};

export default ImageUploader;
