import React from 'react';
import PropTypes from 'prop-types';

import daysOfWeek from '/imports/api/helpers/daysOfWeek';

import Checkbox from '/imports/ui/components/helpers/Checkbox';

const DaySelector = ({
  selected, name, disabled, prefix, legendText, onChange,
}) => (
  <fieldset className={`daySelector ${name} ${disabled ? 'disabled' : null}`}>
    <div>
      {legendText ? <legend>{legendText}</legend> : null}
      {daysOfWeek.map(day => (
        <Checkbox
          key={day.slug}
          name={name}
          value={day.slug}
          id={prefix + day.slug}
          checked={selected.includes(day.slug)}
          disabled={disabled}
          onClick={(event) => {
            if (disabled) event.preventDefault();
          }}
          onChange={(event) => {
            if (disabled) {
              event.preventDefault();
            } else {
              if (event.target.checked) {
                selected.push(event.target.value);
              } else {
                selected.splice(selected.indexOf(event.target.value), 1);
              }
              onChange(selected);
            }
          }}
          label={day.abbreviation.charAt(0)}
        />
      ))}
    </div>
  </fieldset>
);

DaySelector.propTypes = {
  selected: PropTypes.array,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  prefix: PropTypes.string,
  legendText: PropTypes.node,
  onChange: PropTypes.func.isRequired,
};

DaySelector.defaultProps = {
  selected: [],
  name: '',
  disabled: false,
  prefix: '',
  legendText: '',
};

export default DaySelector;
