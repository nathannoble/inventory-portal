import React from 'react';

import CCError from '/imports/ui/components/helpers/CCError';

class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasTimedOut: false,
      timeTilTimeout: 10,
    };

    this.timeout = undefined;
    this.interval = undefined;

    this.intervalFunc = this.intervalFunc.bind(this);
    this.startCountdown = this.startCountdown.bind(this);
    this.clearCountdown = this.clearCountdown.bind(this);
  }

  clearCountdown(timeout, interval) {
    Meteor.clearTimeout(timeout);
    Meteor.clearInterval(interval);
  }

  startCountdown() {
    this.timeout = Meteor.setTimeout(() => {
      this.setState({ hasTimedOut: true });
      this.interval = Meteor.setInterval(this.intervalFunc, 1000);
    }, 3000);
    Meteor.setTimeout(() => {
      Meteor.clearInterval(this.interval);
    }, 10000);
  }

  intervalFunc() {
    if (this.state.timeTilTimeout <= 0) {
      this.clearCountdown(this.timeout, this.interval);
    } else {
      const timeTilTimeout = this.state.timeTilTimeout - 1;
      this.setState({ timeTilTimeout });
    }
  }

  componentWillUnmount() {
    this.clearCountdown(this.timeout, this.interval);
  }

  render() {
    this.startCountdown();

    const { text } = this.props;

    return this.state.hasTimedOut ? (
      <CCError error={new Meteor.Error('timeout')} />
    ) : (
      <div className="loader">
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          width="40px"
          height="40px"
          viewBox="0 0 50 50"
          style={{ enableBackground: 'new 0 0 50 50' }}
        >
          <path d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z">
            <animateTransform
              attributeType="xml"
              attributeName="transform"
              type="rotate"
              from="0 25 25"
              to="360 25 25"
              dur="0.6s"
              repeatCount="indefinite"
            />
          </path>
        </svg>
      </div>
    );
  }
}

export default Loader;
