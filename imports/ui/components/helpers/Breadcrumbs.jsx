import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Breadcrumbs = ({ breadcrumbs }) => (
  <ul className="breadcrumbs">
    {breadcrumbs.map((breadcrumb, index) => (
      <li key={index}>
        {breadcrumb.hasOwnProperty('link') ? (
          <Link to={breadcrumb.link}>{breadcrumb.text}</Link>
        ) : (
          breadcrumb.text
        )}
      </li>
    ))}
  </ul>
);

Breadcrumbs.propTypes = {
  breadcrumbs: PropTypes.arrayOf(PropTypes.shape({
    link: PropTypes.string,
    text: PropTypes.string,
  })).isRequired,
};

export default Breadcrumbs;
