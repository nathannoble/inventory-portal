import { Meteor } from 'meteor/meteor';
import React from 'react';

import { CCButton, handleError, ImageUploader } from '/imports/ui/helpers';

const AvatarUpload = ({ entity, onUpload, onRemove, label = 'Upload Avatar' }) => <div className="entity avatarUploader">
  <label htmlFor="avatar" style={{backgroundImage: `url(${entity.getAvatar()})`}}>
    <ImageUploader id="avatar" onUpload={fileObj => {
      if(entity.avatar) Meteor.call('/image/delete', entity.avatar._id, handleError);
      entity.avatar = fileObj;
      onUpload(entity)
    }} />
    <i className="mdi mdi-camera"></i>
    {entity.avatar
      ? null
      : <span>{label}</span>
    }
  </label>
  {entity.avatar
    ? <CCButton onClick={event => {
      event.preventDefault();
      const imageId = entity.avatar._id;
      delete entity.avatar;
      onRemove(entity, () => {
        Meteor.call('/image/delete', imageId, handleError);
      });
    }} type="remove"/>
    : null
  }
</div>;

export default AvatarUpload;
