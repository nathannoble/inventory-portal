import React from 'react';
import { Link as Link } from 'react-router-dom';

import jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

//import CC from '/imports/api';

import {
    CCButton
} from '/imports/ui/helpers';


const PrintButton = () => (
    <CCButton
        className="no-print"
        type="print"
        onClick={(event) => {
            event.preventDefault();
            window.print();
        }}
    />

);


export default PrintButton;
