import React from 'react';

import CC from '/imports/api';
import Organization from '/imports/api/Organization';

const Avatar = ({ user, organization, url }) => {
  url = url || '';
  if (user && !url) {
    if (typeof user === 'string') user = Meteor.user(user);
    if (user.avatar) url = user.getAvatar();
  } else if (organization && !url) {
    if (typeof organization === 'string') organization = Organization.findOne(organization);
    if (organization.avatar) url = organization.getAvatar();
  }
  return (
    <figure className="avatar" style={{ backgroundImage: `url(${url})` }}>
      {url ? '' : CC.icon(organization ? 'business' : 'profile')}
    </figure>
  );
};

export default Avatar;
