import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Papa from 'papaparse';
import DOMPurify from 'dompurify';

import DateTime from 'react-datetime';
import moment from 'moment';

import Business from '/imports/api/Organization/Business';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import { Checkbox, Select,  Label, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import saveAs from '/imports/ui/helpers/saveAs';
import Drawer from '/imports/ui/components/helpers/Drawer';

import { OrderStatus,  OrderFilter,OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType,OrderHygieneType,OrderBeddingType,
    OrderEquipmentType, OrderFeedingType,OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize,OrderBoyPantSize,
    OrderShoeSize, ChildAge, ChildSex } from '/imports/api/Order';

function groupBy(array, f) {
    let groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}

function sum(items, prop) {
    return items.reduce(function (a, b) {
        return a + b[prop];
    }, 0);
};

class CbTotalsPantsOrderAdminTable extends React.Component {
    constructor(props) {
        super(props);

        let startDate;
        let endDate;

        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }
        const user = Meteor.user();
        
        if (props.match.params.startDate) {
            startDate = new Date(0);
            startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
            endDate = new Date(0);
            endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
        } else if (user.type != 'Caseworker'
            && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
                || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
            startDate = user.queryStartDate;
            endDate = user.queryEndDate;
        } else {
            startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
            endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
        }

        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);

        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        endDate.setMilliseconds(999);

        this.state = {
            columnsVisible: _.uniq(this.props.collection.cbTotalsPantsAdminTable.columns.map(({ className, visible }) => (visible ? className : null))),
            daysUntilPickupLocationDay: daysUntilPickupLocationDay,
            startDate: startDate,
            endDate: endDate,
            dateRange: undefined,
            sort: undefined,
            reverse: false,
        };

    }

    render() {
        const collection = this.props.collection;

        let startDate = new Date();
        let endDate = new Date();

        const results = ((sort) => {

            let query = {
                $and: [
                    {"pickupDate": {"$gte": 0}},
                    {"pickupDate": {"$lte": 0}},
                ]
            };

            if (this.props.startDate) {

                startDate = this.props.startDate;
                endDate = this.props.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                query = {
                    $and: [
                        {"pickupDate": {"$gte": startDate}},
                        {"pickupDate": {"$lte": endDate}},
                    ]
                };
            }  else if (this.state.startDate !== undefined &&
                this.state.endDate !== undefined ) {

                startDate = this.state.startDate;
                endDate = this.state.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                if (startDate < endDate) {

                    query = {
                        $and: [
                            {"pickupDate": {"$gte": startDate}},
                            {"pickupDate": {"$lte": endDate}},
                            {"itemCategory": 0}
                        ]
                    };
                    console.log("components/CbTotalsPantsOrderAdminTable.jsx:query:1:" + JSON.stringify(query));

                }

                console.log("components/CbTotalsPantsOrderAdminTable.jsx:manual:startDate:" + startDate);
                console.log("components/CbTotalsPantsOrderAdminTable.jsx:manual:endDate:" + endDate);

            }

            const orders = collection.find(query).fetch();

            // Make filteredItems from filteredOrders
            let filteredItems = [];
            orders.forEach((order) => {

                let cpNumber = 0;
                let name = "";
                if (order.pickupLocation.indexOf("(") > 0) {
                    cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
                    name = order.pickupLocation.replace(" " + cpNumber,"");
                    cpNumber = cpNumber.replace("(", "").replace(")", "");
                }

                let phone = order.userPhone[0];
                let ordererName = order.userName[0];

                order.items.forEach((item) => {

                    let item2 = item;
                    item2.cpNumber = cpNumber;
                    item2.phone = phone;
                    item2.ordererName = ordererName;
                    item2.pickupLocation = name;

                    item2._id = order._id;
                    item2.childName = order.childName;
                    item2.childAge = order.childAge;
                    item2.childWeight = order.childWeight;
                    item2.childSex = order.childSex;
                    item2.childZip = order.childZip;
                    item2.comments = order.comments;
                    item2.pickupDate = order.pickupDate;
                    item2.status = order.status;

                    //if (item2.itemsCategory !== undefined && item2.itemsCategory !== null) {
                    if (item2.itemsCategory === 0) {
                        filteredItems.push(item2);
                    }
                });
            });

            let itemsCategoryFreq = groupBy(filteredItems, function (item) {
                return [item.itemsCategory, item.itemsType];
            });

            if (sort) {
                itemsCategoryFreq.sort(sort);
                if (this.state.reverse) itemsCategoryFreq.reverse();
            } else {
                itemsCategoryFreq = itemsCategoryFreq.sort(function (a, b) {
                    return (b.length - a.length);
                });
            }

            return itemsCategoryFreq;

        })(this.state.sort);

        return (
            <>
            <div id="reportRoot" className={`adminTable cbTotal`}>
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ text: `Order Totals` }]}/>
                    <Drawer>
                        <div className="adminTableFilter">
                            <Label
                                name="startDate"
                                element={DateTime}
                                text="Pick-up Start Date"
                                onChange={(selectedOption) => {
                                 //event.preventDefault();
                                 console.log("components/CbTotalsPantsOrderAdminTable.jsx:startDate:selectedOption:" + JSON.stringify(selectedOption));
                                 if (typeof selectedOption === "object") {
                                     this.setState({
                                        startDate: selectedOption ? selectedOption.toDate() : undefined,
                                        endDate:undefined,
                                        dateRange: undefined,
                                     });
                                 }
                                 }}
                                timeFormat={false}
                                value={this.state.startDate}
                            />
                            <Label
                                name="endDate"
                                element={DateTime}
                                text="Pick-up End Date"
                                onChange={(selectedOption) => {
                                     //event.preventDefault();
                                     console.log("components/CbTotalsPantsOrderAdminTable.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                                     if (typeof selectedOption === "object") {
                                         this.setState({
                                            endDate: selectedOption ? selectedOption.toDate() : undefined,
                                            dateRange: undefined,
                                          });

                                           if (this.state.startDate !== undefined
                                          && selectedOption.toDate() !== undefined) {

                                            let startDate = this.state.startDate;
                                            let endDate = selectedOption.toDate();

                                            startDate.setHours(0);
                                            startDate.setMinutes(0);
                                            startDate.setSeconds(0);
                                            startDate.setMilliseconds(0);

                                            endDate.setHours(23);
                                            endDate.setMinutes(59);
                                            endDate.setSeconds(59);
                                            endDate.setMilliseconds(999);

                                            if (startDate < endDate) {
                                                this.props.history.push(`/order/cbTotalsPantsReport/${startDate.getTime()}/${endDate.getTime()}`);
                                           }

                                     }}
                                 }}
                                timeFormat={false}
                                value={this.state.endDate}
                            />
                        </div>
                    </Drawer>
                </SubHeader>
                <div className="headers2">
                    <span>{'CB Totals (By Pant Size) Report: ' + startDate.toLocaleDateString() + ' - ' + endDate.toLocaleDateString()}</span>
                </div>
                <div className="headers">
                    {collection.cbTotalsPantsAdminTable.columns.map(({ header, className, sort }) =>
                        (this.state.columnsVisible.indexOf(className) >= 0 ? (
                            <span className={className}>
                                {header === "CaseWorker" ? "Users" : header}
                                {sort ? (
                                    <div className="sort">
                                        <i
                                            className={`mdi mdi-arrow-up${this.state.sort === sort && this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: true })}
                                        />
                                        <i
                                            className={`mdi mdi-arrow-down${this.state.sort === sort &&
                                            !this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: false })}
                                        />
                                    </div>
                                ) : null}
                            </span>
                        ) : null))}
                </div>
                <div className="data">
                    {results.map((item, index) => (
                        (groupBy(item, function (item2) {return [item2.pantSize]})).sort(function (a, b) {return (parseInt(b.length,0) - parseInt(a.length,0))}).map((item3, index3) => (
                        <>
                        {index3 === 0 ?
                        <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                            <span className={'text'}><b>
                                {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                &nbsp;:&nbsp;{OrderBagType.getIdentifier(OrderBagType.getValues()[item[0].itemsType])}
                            </b></span>
                            <span className={'text'}>&nbsp;</span>
                            <span className={'number2'}>&nbsp;</span>
                            <span className={'number2'}><b>{item.length}</b></span>
                        </div>
                        : null}
                        <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                            <span className={'text'}></span>
                            {
                             OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Girl' ||
                             OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Teen Girl' ||
                             OrderBagType.getIdentifier(OrderBagType.getValues()[item.itemsType]) === 'Layette Girl' ?
                                <span
                                    className={'text'}>{OrderGirlPantSize.getIdentifier(OrderGirlPantSize.getValues()[item3[0].pantSize])} </span> :
                                <span
                                    className={'text'}>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item3[0].pantSize])} </span>
                            }
                            <span className={'number2'}>{item3.length}</span>
                            <span className={'number2'}></span>
                        </div>
                        </>
                    )
                    )))}
                    <div className="datum-heading">
                        <span className={'text'}><b>Totals:</b></span>
                        <span className={'text'}>&nbsp;</span>
                        <span className={'number2'}>&nbsp;</span>
                        <span className={'number2'}><b>{results.reduce(((sum2, result) => sum2 + result.length), 0)}</b></span>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default container((props, onData) => {
    //console.log("components/CbTotalsPantsOrderAdminTable.jsx" + JSON.stringify(props));
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        onData(null, {});
    }
}, CbTotalsPantsOrderAdminTable);
