import React from 'react';
import { Link as Link } from 'react-router-dom';

//import { Bert } from 'meteor/themeteorchef:bert';

import jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import {
    CCButton
} from '/imports/ui/helpers';

const PDFButton = () => (

    <CCButton
        className="no-print"
        type="pdf"
        id="pdfButton"
        onClick={(event) => {
        event.preventDefault();

        if (location.pathname.indexOf('/order/labels') >= 0) {

            let divToPrint = document.getElementById('reportRoot');
            let count = Math.floor(divToPrint.getElementsByClassName("page-break").length);
            console.log("componentsPDFButton.jsx:Labels:total page count:" + count);

            Bert.defaults.hideDelay = 1000000;
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
                el.innerHTML = '<p>Generating PDF: page 1 of ' + count  + '</p>';
            }
            Bert.alert('Generating PDF: page 1 of ' + count  , 'info', 'fixed-top');
            html2canvasOnePage(null, 0, count,'Labels');
        } else if (location.pathname.indexOf('/order/cpTotalsReport') >= 0) {

            // cpTotalsOrderReport
            console.log("componentsPDFButton.jsx:cpTotalsOrderReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;
            let message = 'Generating PDF...';
            Bert.alert(message, 'info', 'fixed-top');
            if (document.querySelectorAll('.bert-content').length > 0) {
                let el = document.querySelectorAll('.bert-content')[0];
                if (el) {
                    el.innerHTML = `<p>${message}</p>`;
                }
            }

            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff"
                }).then(function(canvas) {

                    // the size of the PDF is a big deal
                    //let pdf = new jsPDF('l', 'mm', [canvas.width*0.264583,canvas.width*0.192424]);
                    let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);


                    // pixel/canvas w - 791, 2434
                    // pixel/canvas h - 612, 1170, 1535
                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {

                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);

                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        // document.body.appendChild(canvas);
                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);
                        //let width         = onePageCanvas.width;
                        //let height        = onePageCanvas.clientHeight;

                        if (i > 0) {
                            pdf.addPage(); //2434,1770); //8.5" x 11" in pts (in*72)
                        }
                        //pdf.setPage(i+1);
                        pdf.addImage(canvasDataURL, 'jpeg', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>CP Totals Report PDF Generated.</p>';
                    }
                    Bert.alert('CP Totals Report PDF Generated.', 'success');
                    pdf.save( 'CPTotalsOrderReport.pdf');
            });

        } else if (location.pathname.indexOf('/order/centerTotalsReport') >= 0) {

            // centerTotalsOrderReport
            console.log("componentsPDFButton.jsx:centerTotalsOrderReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;

            let message = 'Generating PDF...';
            Bert.alert(message,'info', 'fixed-top');
             if (document.querySelectorAll('.bert-content').length > 0) {
                 let el = document.querySelectorAll('.bert-content')[0];
                 if (el) {
                     el.innerHTML = `<p>${message}</p>`;
                 }
             }

            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff"
                }).then(function(canvas) {

                    let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]); //3704
                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {

                        let srcImg  = canvas;
                        let sX      = 0; //canvas.width * .04;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);

                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage(); //2434,1770); //8.5" x 11" in pts (in*72)
                        }
                        //pdf.setPage(i+1);
                        pdf.addImage(canvasDataURL, 'jpeg', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>CP Totals Report PDF Generated.</p>';
                    }
                    Bert.alert('Center Totals Report PDF Generated.', 'success');
                    pdf.save( 'CenterTotalsOrderReport.pdf');
            });

        } else if (location.pathname.indexOf('/order/itemTotalsReport') >= 0) {

            // itemTotalsReport
            console.log("componentsPDFButton.jsx:itemTotalsReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;
            
            let message = 'Generating PDF...';
            Bert.alert(message, 'info', 'fixed-top');
            if (document.querySelectorAll('.bert-content').length > 0) {
                 let el = document.querySelectorAll('.bert-content')[0];
                 if (el) {
                     el.innerHTML = `<p>${message}</p>`;
                 }
            }
            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418, //2.2388
                backgroundColor: "#ffffff"
                }).then(function(canvas) {

                   //let pdf = new jsPDF('l', 'mm', [canvas.width*0.264583,canvas.width*0.192424]);
                   let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);

                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {
                        
                        console.log("componentsPDFButton.jsx:itemTotalsByZipReport:canvas:height:" + canvas.height);
                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale; //* 0.94;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale; //* 0.94;;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);
                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage();
                        }
                        pdf.addImage(canvasDataURL, 'jpeg', 15, 15);

                    }

                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Item Totals Report PDF Generated.</p>';
                    }
                    Bert.alert('Item Totals Report PDF Generated.', 'success');
                    pdf.save( 'ItemTotalsOrderReport.pdf');
            });

        } else if (location.pathname.indexOf('/order/itemTotalsByZipReport') >= 0) {

            itemTotalsByZipReport
            console.log("componentsPDFButton.jsx:itemTotalsByZipReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;

            let message = 'Generating PDF...';
            Bert.alert(message, 'info', 'fixed-top');
            if (document.querySelectorAll('.bert-content').length > 0) {
               let el = document.querySelectorAll('.bert-content')[0];
               if (el) {
                   el.innerHTML = `<p>${message}</p>`;
               }
            }
            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff"
                }).then(function(canvas) {

                   //let pdf = new jsPDF('l', 'mm', [canvas.width*0.264583,canvas.width*0.192424]);
                   let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);

                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {
                        console.log("componentsPDFButton.jsx:itemTotalsByZipReport:canvas:height:" + canvas.height);
                        //canvas.height = 14000;

                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);
                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage();
                        }
                        pdf.addImage(canvasDataURL, 'jpeg', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Item Totals By Zip Report PDF Generated.</p>';
                    }
                    Bert.alert('Item Totals By Zip Report PDF Generated.', 'success');
                    pdf.save( 'ItemTotalsByZipOrderReport.pdf');
            });

            // let divToPrint = document.getElementById('reportRoot');
            // let count = Math.floor(divToPrint.getElementsByClassName("page-break").length);
            // console.log("componentsPDFButton.jsx:itemTotalsByZipReport:total page count:" + count);

            // Bert.defaults.hideDelay = 1000000;
            // let el = document.querySelectorAll('.bert-content')[0];
            // if (el) {
            //     el.innerHTML = '<p>Generating PDF: page 1 of ' + count  + '</p>';
            // }
            // Bert.alert('Generating PDF: page 1 of ' + count  , 'info', 'fixed-top');
            // html2canvasOnePage(null, 0, count,'Labels');

        } else if (location.pathname.indexOf('/order/cbTotalsReport') >= 0) {

            // cbTotalsReport (Clothing Bags)
            console.log("componentsPDFButton.jsx:cbTotalsReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;

            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff",
                //removeContainer: true
                }).then(function(canvas) {
                   let message = 'Generating PDF...';
                   Bert.alert(message, 'info', 'fixed-top');
                   if (document.querySelectorAll('.bert-content').length > 0) {
                      let el = document.querySelectorAll('.bert-content')[0];
                      if (el) {
                          el.innerHTML = `<p>${message}</p>`;
                      }
                   }
                   //let pdf = new jsPDF('l', 'mm', [canvas.width*0.264583,canvas.width*0.192424]);
                   let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);

                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {

                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);

                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        //ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage();
                        }
                        pdf.addImage(canvasDataURL, 'PNG', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Clothing Bag Totals Report (By Shirt Size) PDF Generated.</p>';
                    }
                    Bert.alert('Clothing Bag Totals Report (By Shirt Size) PDF Generated.', 'success');
                    pdf.save( 'CBTotalsOrderReport.pdf');
            });

        } else if (location.pathname.indexOf('/order/cbTotalsShoesReport') >= 0) {

            // cbTotalsShoesReport (Clothing Bags with Shoes frequency)
            console.log("componentsPDFButton.jsx:cbTotalsShoesReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;

            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff",
                //removeContainer: true
                }).then(function(canvas) {
                    let message = 'Generating PDF...';
                    Bert.alert(message, 'info', 'fixed-top');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                      let el = document.querySelectorAll('.bert-content')[0];
                      if (el) {
                          el.innerHTML = `<p>${message}</p>`;
                      }
                    }
                    let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);

                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {

                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);

                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        //ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage();
                        }
                        pdf.addImage(canvasDataURL, 'PNG', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Clothing Bag Totals Report (By Shoe Size) PDF Generated.</p>';
                    }
                    Bert.alert('Clothing Bag Totals Report (By Shoe Size) PDF Generated.', 'success');
                    pdf.save( 'CBTotalsShoesOrderReport.pdf');
            });

        } else if (location.pathname.indexOf('/order/cbTotalsPantsReport') >= 0) {

            // cbTotalsPantsReport (Clothing Bags with Pants frequency)
            console.log("componentsPDFButton.jsx:cbTotalsPantsReport");
            let divToPrint = document.getElementById('reportRoot');
            let scale = 2; //window.devicePixelRatio;
            Bert.defaults.hideDelay = 100000;

            html2canvas.default(divToPrint, {
                scrollX: -window.scrollX,
                scrollY: -window.scrollY,
                scale:2.2418,
                backgroundColor: "#ffffff",
                //removeContainer: true
                }).then(function(canvas) {
                    let message = 'Generating PDF...';
                    Bert.alert(message, 'info', 'fixed-top');
                    if (document.querySelectorAll('.bert-content').length > 0) {
                      let el = document.querySelectorAll('.bert-content')[0];
                      if (el) {
                          el.innerHTML = `<p>${message}</p>`;
                      }
                    }
                    let pdf = new jsPDF('p', 'mm', [canvas.width *.2694,canvas.width*0.3704]);

                    for (let i = 0; i <= divToPrint.clientHeight/(1535); i++) {

                        let srcImg  = canvas;
                        let sX      = 0;
                        let sY      = 1535*i*scale; // start x pixels down for every new page
                        let sWidth  = canvas.width * 0.94;
                        let sHeight = 1535*scale;
                        let dX      = 0; //canvas.width * .04;
                        let dY      = 0; //canvas.height * .04;
                        let dWidth  = canvas.width * 0.94;
                        let dHeight = 1535*scale;

                        window.onePageCanvas = document.createElement("canvas");
                        onePageCanvas.setAttribute('width', canvas.width);
                        onePageCanvas.setAttribute('height', canvas.height);

                        let ctx = onePageCanvas.getContext('2d');
                        ctx.width = canvas.width;
                        ctx.height = canvas.height;
                        ctx.fillStyle = "white";
                        //ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.fillRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
                        //ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

                        let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);

                        if (i > 0) {
                            pdf.addPage();
                        }
                        pdf.addImage(canvasDataURL, 'PNG', 15, 15);

                    }
                    Bert.defaults.hideDelay = 10000;
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Clothing Bag Totals Report (By Pant Size) PDF Generated.</p>';
                    }
                    Bert.alert('Clothing Bag Totals Report (By Pant Size) PDF Generated.', 'success');
                    pdf.save( 'CBTotalsPantsOrderReport.pdf');
            });

        }
    }}
    />
);

function html2canvasOnePageCallback(pdf, pageNum, count, name) {
    //console.log("componentsPDFButton.jsx:html2canvasOnePageCallback");
    html2canvasOnePage(pdf, pageNum, count, name);
}

function html2canvasOnePage(pdf, pageNum, count, name) {

    //let scale = window.devicePixelRatio;
    //console.log("componentsPDFButton.jsx:html2canvasOnePage:scale:" +scale);
    console.log("componentsPDFButton.jsx:html2canvasOnePage:page started:pageNum:" + pageNum);
    let group = document.getElementById('group' + pageNum);

    if (group === null || group === undefined) {
        console.log("componentsPDFButton.jsx:document finished out of loop:total pages:" + pageNum + 1);
        Bert.defaults.hideDelay = 10000;
        Bert.alert(`${name} PDF Generated.`, 'success');
        if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
                el.innerHTML = `<p>${name} PDF Generated</p>`;
            }
        }
        pdf.save(`${name}.pdf`);
    } else {
        html2canvas.default(group, {
            scrollX: -window.scrollX,
            scrollY: -window.scrollY,
            //scale: 1, //scale,
            //allowTaint: true,
            //removeContainer: true,
            //useCORS: true,
            backgroundColor: "#ffffff"
        }).then(function (canvas) {

            if (pdf === null) {
                // The approach below doubles the canvas size when the scale doubles (i.e. scale = 2 on MacBook Pro Retina)
                pdf = new jsPDF('p', 'mm', [canvas.width * .2694, canvas.width * 0.3704]);
                //pdf = new jsPDF('p', 'px');
                //pdf = new jsPDF('p', 'mm', [canvas.width*0.192424*scale *.7,canvas.width*0.264583*scale *.7]);

            }

            let srcImg = canvas;
            //let sX = 0;
            //let sY = 0; // start x pixels down for every new page
            //let sWidth = canvas.width;
            //let sHeight = canvas.height;
            let dX = canvas.width * .015;
            let dY = 0; //canvas.height *.075;
            let dWidth = canvas.width; ///(scale);
            let dHeight = canvas.height; ///(scale);

            window.onePageCanvas = document.createElement("canvas");
            onePageCanvas.setAttribute('width', canvas.width);
            onePageCanvas.setAttribute('height', canvas.height);

            let ctx = onePageCanvas.getContext('2d');
            ctx.width = canvas.width;
            ctx.height = canvas.height;
            //ctx.scale(scale,scale);
            //ctx.mozImageSmoothingEnabled = false;
            //ctx.webkitImageSmoothingEnabled = false;
            //ctx.msImageSmoothingEnabled = false;
            //ctx.imageSmoothingEnabled = false;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            //ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);
            ctx.drawImage(srcImg, dX, dY, dWidth, dHeight);

            let canvasDataURL = onePageCanvas.toDataURL("image/jpeg", 1.0);
            //let width = onePageCanvas.width;
            //let height = onePageCanvas.height;

            if (pageNum > 0) {
                pdf.addPage();
            }

            pdf.addImage(canvasDataURL, 'JPEG', 0, 0);
            //pdf.addImage(canvasDataURL, 'PNG',canvas.width * .015,0,canvas.width/(scale),canvas.height/(scale),null,'SLOW',0);

            pageNum++;
            if (pageNum === count) {
                console.log("componentsPDFButton.jsx:document finished in loop:total pages:" + (pageNum - 1));
                Bert.defaults.hideDelay = 10000;
                Bert.alert(`${name} PDF Generated.`, 'success');
                if (document.querySelectorAll('.bert-content').length > 0) {
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = `<p>${name} PDF Generated</p>`;
                    }
                }
                pdf.save(`${name}.pdf`);
            } else {
                console.log("componentsPDFButton.jsx:page finished:" + pageNum);
                //console.log("componentsPDFButton.jsx:bert alert count:" + document.querySelectorAll('.bert-alert,.show').length);
                if (document.querySelectorAll('.bert-content').length > 0) {
                    let el = document.querySelectorAll('.bert-content')[0];
                    if (el) {
                        el.innerHTML = '<p>Generating PDF: page ' + (pageNum + 1) + ' of ' + count + '</p>';
                    } else {
                        Bert.alert('Generating PDF: page ' + (pageNum + 1) + ' of ' + count, 'info', 'fixed-top');
                    }
                } else {
                    Bert.alert('Generating PDF: page ' + (pageNum + 1) + ' of ' + count, 'info', 'fixed-top');
                }
                html2canvasOnePageCallback(pdf, pageNum, count, name);
            }
        });
    }
}

export default PDFButton;
