import React from 'react';
import { Link } from 'react-router-dom';

import CC from '/imports/api';

import Document, { DocumentType } from '/imports/api/Document';

import container from '/imports/ui/helpers/container';

import { handleError, CCButton } from '/imports/ui/helpers';

export const DocumentTypeNew = ({ onSubmit }) => (
  <form
    className="documentType new"
    onSubmit={(event) => {
      event.preventDefault();
      const documentType = new DocumentType({
        name: document.getElementById('documentType__name').value,
      });
      Meteor.call('/documentType/create', documentType, handleError({ callback: onSubmit }));
    }}
  >
    <label htmlFor="documentType__name">
      <input type="text" id="documentType__name" placeholder="Document Type Name" />
    </label>
    <input type="submit" value="New Document Type" />
  </form>
);

class DocumentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creatingDocumentType: false,
    };
  }

  render() {
    const documents = this.props.documents;
    const documentTypes = this.props.documentTypes;
    return (
      <div className="document list body">
        <ul className="document">
          {documents.map(doc => (
            <li key={doc._id}>
              <Link to={`/document/${doc._id}`}>
                <h2>{doc.title}</h2>
                <ul className="chips">
                  <li>{doc.type().name}</li>
                </ul>
              </Link>
            </li>
          ))}
          {Meteor.user().type === 'Administrator' ? (
            <li className="new">
              <Link to="/document/new">{CC.icon('create')}New Document</Link>
            </li>
          ) : null}
        </ul>
        {Meteor.user().type === 'Administrator' ? (
          <ul className="documentType chips">
            {documentTypes.map(documentType => (
              <li key={documentType._id}>
                {documentType.name}
                <CCButton
                  type="remove"
                  onClick={(event) => {
                    event.preventDefault();
                    Meteor.call('/documentType/delete', documentType, handleError);
                  }}
                />
              </li>
            ))}
            <li className="new">
              {this.state.creatingDocumentType ? (
                <DocumentTypeNew
                  onSubmit={() => {
                    this.setState({ creatingDocumentType: false });
                  }}
                />
              ) : (
                <button onClick={event => this.setState({ creatingDocumentType: true })}>
                  New Document Type
                </button>
              )}
            </li>
          </ul>
        ) : null}
      </div>
    );
  }
}

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/document/list');
  if (subscription.ready()) {
    const documents = Document.find().fetch();
    const documentTypes = DocumentType.find().fetch();
    onData(null, { documents, documentTypes });
  }
}, DocumentList);
