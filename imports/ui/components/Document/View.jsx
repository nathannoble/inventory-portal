import { Meteor } from 'meteor/meteor';
import React from 'react';

import User from '/imports/api/User';
import Document, { DocumentType } from '/imports/api/Document';

import CC from '/imports/api';

import container from '/imports/ui/helpers/container';

import { Select, handleError, SubHeader, Breadcrumbs } from '/imports/ui/helpers';

const DocumentView = ({ doc }) => {
  return <div className="document view body">
    <SubHeader><Breadcrumbs breadcrumbs={[
      {link: '/document', text: 'Documents'},
      {text: doc.name}
    ]}/></SubHeader>
    {Meteor.user().type === 'Administrator'
      ? <form>
        <label htmlFor="name">
          <input
            type="text"
            id="name"
            placeholder="Document Title"
            defaultValue={doc.name}
            onChange={event => {
              doc.name = event.target.value;
              Meteor.call('/document/update', doc, handleError);
            }}
          />
        </label>

        <label htmlFor="content">
          <textarea
            id="content"
            defaultValue={doc.content}
            onChange={event => {
              doc.content = event.target.value;
              Meteor.call('/document/update', doc, handleError);
            }}
          />
        </label>

        <label htmlFor="roles">
          Roles
          <Select
            id="roles"
            multi={true}
            value={doc.roles}
            options={User.children.map(child => Object({ value: child.className, label: child.className }))}
            onChange={selectedOptions => {
              doc.roles = selectedOptions.map(({ value }) => value);
              Meteor.call('/document/update', doc, handleError);
            }}
          />
        </label>

        <label htmlFor="documentTypeId">
          Type
          <Select
            id="documentTypeId"
            options={DocumentType.find().map(documentType => Object({ value: documentType._id, label: documentType.name }))}
            value={doc.documentTypeId}
            onChange={({ value }) => {
              doc.documentTypeId = value;
              Meteor.call('/document/update', doc, handleError);
            }}
          />
        </label>
      </form>
      : <article>
        <h1>{doc.name}</h1>
        <p>{doc.content}</p>
      </article>
    }
  </div>
};

export default container((props, onData) => {
  const documentId = props.match.params.documentId;
  const subscription = Meteor.subscribe('/document/read', documentId);
  if(subscription.ready()) {
    const doc = Document.findOne(documentId);
    onData(null, { doc });
  }
}, DocumentView);
