import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import DocumentNew from './New';
import DocumentView from './View';
import DocumentList from './List';

const DocumentRoutes = props => (
	<Switch className="document">
		<Authenticated exact name="DocumentNew" path={`${props.match.path}/new`} component={DocumentNew} roles={['administrator']} {...props} />
		<Authenticated exact name="DocumentView" path={`${props.match.path}/:documentId`} component={DocumentView} {...props} />
		<Authenticated exact name="DocumentList" path={`${props.match.path}`} component={DocumentList} {...props} />
	</Switch>
);

export default DocumentRoutes;
