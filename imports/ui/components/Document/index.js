import DocumentNew from './New.jsx';
import DocumentList from './List.jsx';
import DocumentView from './View.jsx';

export { DocumentNew, DocumentList, DocumentView };
