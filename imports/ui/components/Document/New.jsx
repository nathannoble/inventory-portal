import { Meteor } from 'meteor/meteor';
import React from 'react';

import User from '/imports/api/User';
import Document, { DocumentType } from '/imports/api/Document';

import container from '/imports/ui/helpers/container';

import { Select, handleError, SubHeader, Breadcrumbs } from '/imports/ui/helpers';

class DocumentNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      documentTypeId: false,
      roles: User.children.map(child => child.className),
    };
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
  }

  onSubmitHandler(event) {
    event.preventDefault();
    const history = this.props.history;
    const doc = new Document({
      name: document.getElementById('name').value,
      title: document.getElementById('name').value,
      documentTypeId: this.state.documentTypeId,
      roles: this.state.roles,
      content: document.getElementById('content').value,
    });
    Meteor.call(
      '/document/create',
      doc,
      handleError({
        callback(documentId) {
          history.push(`/document/${documentId}`);
        },
      }),
    );
  }

  render() {
    return (
      <div className="document new">
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[{ link: '/document', text: 'Documents' }, { text: 'New Document' }]}
          />
        </SubHeader>
        <form onSubmit={this.onSubmitHandler}>
          <label htmlFor="name">
            <input type="text" id="name" placeholder="Document Title" />
          </label>

          <label htmlFor="content">
            <textarea id="content" />
          </label>

          <label htmlFor="roles">
            Roles
            <Select
              id="roles"
              multi
              value={this.state.roles}
              options={User.children.map(child =>
                Object({ value: child.className, label: child.className }))}
              onChange={(selectedOptions) => {
                const roles = selectedOptions.map(({ value }) => value);
                this.setState({ roles });
              }}
            />
          </label>

          <label htmlFor="documentTypeId">
            Type
            <Select
              id="documentTypeId"
              options={DocumentType.find().map(documentType =>
                Object({ value: documentType._id, label: documentType.name }))}
              value={this.state.documentTypeId}
              onChange={({ value }) => this.setState({ documentTypeId: value })}
            />
          </label>
          <input type="submit" value="Add New Document" />
        </form>
      </div>
    );
  }
}

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/documentType/list');
  if (subscription.ready()) {
    onData(null, {});
  }
}, DocumentNew);
