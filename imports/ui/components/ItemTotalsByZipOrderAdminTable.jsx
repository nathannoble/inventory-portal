import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Papa from 'papaparse';
import DOMPurify from 'dompurify';

import DateTime from 'react-datetime';
import moment from 'moment';

import Business from '/imports/api/Organization/Business';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import { Checkbox, Select,  Label, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import saveAs from '/imports/ui/helpers/saveAs';
import Drawer from '/imports/ui/components/helpers/Drawer';

import { OrderStatus,  OrderFilter,OrderItemCategory,
    OrderBagType, OrderClothingType,
    OrderDiaperType,OrderHygieneType,OrderBeddingType,
    OrderEquipmentType, OrderFeedingType,OrderToyType,
    OrderStrollerType,
    OrderClothingSize, OrderGirlPantSize,OrderBoyPantSize,
    OrderShoeSize, ChildAge, ChildSex } from '/imports/api/Order';

function groupBy(array, f) {
    let groups = {};
    array.forEach(function (o) {
        var group = JSON.stringify(f(o));
        groups[group] = groups[group] || [];
        groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
        return groups[group];
    })
}

function sum(items, prop) {
    return items.reduce(function (a, b) {
        return a + b[prop];
    }, 0);
};

class ItemTotalsByZipOrderAdminTable extends React.Component {
    constructor(props) {
        super(props);

        let startDate;
        let endDate;

        let now = new Date();

        // This Week (Until this Friday at 5:00 PM)
        let nextFriday = 5;
        let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
        if (daysUntilPickupLocationDay < 0) {
            daysUntilPickupLocationDay += 7;
        } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
            daysUntilPickupLocationDay += 7;
        }

        const user = Meteor.user();
        
        if (props.match.params.startDate) {
            startDate = new Date(0);
            startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
            endDate = new Date(0);
            endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
        } else if (user.type != 'Caseworker'
            && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
                || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
            startDate = user.queryStartDate;
            endDate = user.queryEndDate;
        } else {
            startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
            endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
        }

        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        startDate.setMilliseconds(0);

        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        endDate.setMilliseconds(999);

        this.state = {
            columnsVisible: _.uniq(this.props.collection.itemTotalsByZipAdminTable.columns.map(({ className, visible }) => (visible ? className : null))),
            daysUntilPickupLocationDay: daysUntilPickupLocationDay,
            startDate: startDate,
            endDate: endDate,
            dateRange: undefined,
            sort: undefined,
            reverse: false,
        };

    }

    render() {
        const collection = this.props.collection;

        let startDate = new Date();
        let endDate = new Date();

        let filteredItems = [];

        const results = ((sort) => {

            let query = {
                $and: [
                    {"pickupDate": {"$gte": 0}},
                    {"pickupDate": {"$lte": 0}},
                ]
            };

            if (this.props.startDate) {

                startDate = this.props.startDate;
                endDate = this.props.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                query = {
                    $and: [
                        {"pickupDate": {"$gte": startDate}},
                        {"pickupDate": {"$lte": endDate}},
                    ]
                };
            } else if (this.state.startDate !== undefined &&
                this.state.endDate !== undefined ) {

                startDate = this.state.startDate;
                endDate = this.state.endDate;

                startDate.setHours(0);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
                startDate.setMilliseconds(0);

                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                endDate.setMilliseconds(999);

                if (startDate < endDate) {

                    query = {
                        $and: [
                            {"pickupDate": {"$gte": startDate}},
                            {"pickupDate": {"$lte": endDate}}
                        ]
                    };
                    console.log("components/ItemTotalsByZipOrderAdminTable.jsx:query:1:" + JSON.stringify(query));

                }

                console.log("components/ItemTotalsByZipOrderAdminTable.jsx:manual:startDate:" + startDate);
                console.log("components/ItemTotalsByZipOrderAdminTable.jsx:manual:endDate:" + endDate);

            }

            const orders = collection.find(query).fetch();

            // Make filteredItems from filteredOrders

            orders.forEach((order) => {

                let cpNumber = 0;
                let name = "";
                if (order.pickupLocation.indexOf("(") > 0) {
                    cpNumber = order.pickupLocation.substr(order.pickupLocation.indexOf("("), order.pickupLocation.indexOf(")"));
                    name = order.pickupLocation.replace(" " + cpNumber,"");
                    cpNumber = cpNumber.replace("(", "").replace(")", "");
                }

                let phone = order.userPhone[0];
                let ordererName = order.userName[0];

                order.items.forEach((item) => {

                    let item2 = item;
                    item2.cpNumber = cpNumber;
                    item2.phone = phone;
                    item2.ordererName = ordererName;
                    item2.pickupLocation = name;
                    item2._id = order._id;
                    item2.childName = order.childName;
                    item2.childAge = order.childAge;
                    item2.childWeight = order.childWeight;
                    item2.childSex = order.childSex;
                    item2.childZip = order.childZip;
                    item2.comments = order.comments;
                    item2.pickupDate = order.pickupDate;
                    item2.status = order.status;
                    filteredItems.push(item2);
                });
            });

            let itemsZipFreqs =  groupBy(filteredItems, function (item) {
                return [item.childZip];
            });

            let itemsCategoryFreqs = [];
            itemsZipFreqs.forEach((itemsZipFreq) => {
                let itemsCategoryFreq = groupBy(itemsZipFreq, function (item) {
                    return [item.itemsCategory, item.itemsType];
                });
                itemsCategoryFreq = itemsCategoryFreq.sort(function (a, b) {
                    return (b.length - a.length);
                });
                itemsCategoryFreqs.push(itemsCategoryFreq);
            });

            if (sort) {
                itemsCategoryFreqs.sort(sort);
                if (this.state.reverse) itemsCategoryFreqs.reverse();
            } else {
                itemsCategoryFreqs = itemsCategoryFreqs.sort(function (a, b) {
                    return (b.length - a.length);
                });
            }


            itemsCategoryFreqs = itemsCategoryFreqs.sort(function (a, b) {
                return (a[0][0].childZip - b[0][0].childZip);
            });


            return itemsCategoryFreqs;

        })(this.state.sort);

        return (
            <>
            <div id="reportRoot" >
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ text: `Order Totals` }]}/>
                    <Drawer>
                        <div className="adminTableFilter">
                            <Label
                                name="startDate"
                                element={DateTime}
                                text="Pick-up Start Date"
                                onChange={(selectedOption) => {
                                 //event.preventDefault();
                                 console.log("components/ItemTotalsByZipOrderAdminTable.jsx:startDate:selectedOption:" + JSON.stringify(selectedOption));
                                 if (typeof selectedOption === "object") {
                                     this.setState({
                                        startDate: selectedOption ? selectedOption.toDate() : undefined,
                                        endDate:undefined,
                                        dateRange: undefined,
                                     });
                                 }
                                 }}
                                timeFormat={false}
                                value={this.state.startDate}
                            />
                            <Label
                                name="endDate"
                                element={DateTime}
                                text="Pick-up End Date"
                                onChange={(selectedOption) => {
                                     //event.preventDefault();
                                     console.log("components/ItemTotalsByZipOrderAdminTable.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                                     if (typeof selectedOption === "object") {
                                         this.setState({
                                            endDate: selectedOption ? selectedOption.toDate() : undefined,
                                            dateRange: undefined,
                                          });
                                     }

                                     if (this.state.startDate !== undefined
                                          && selectedOption.toDate() !== undefined) {

                                            let startDate = this.state.startDate;
                                            let endDate = selectedOption.toDate();

                                            startDate.setHours(0);
                                            startDate.setMinutes(0);
                                            startDate.setSeconds(0);
                                            startDate.setMilliseconds(0);

                                            endDate.setHours(23);
                                            endDate.setMinutes(59);
                                            endDate.setSeconds(59);
                                            endDate.setMilliseconds(999);

                                            if (startDate < endDate) {
                                                this.props.history.push(`/order/itemTotalsByZipReport/${startDate.getTime()}/${endDate.getTime()}`);
                                           }
                                    }
                                 }}
                                timeFormat={false}
                                value={this.state.endDate}
                            />
                        </div>
                    </Drawer>
                </SubHeader>
                <div className="adminTable cbTotal">
                    <div className="headers2">
                    <span>{'Item Totals By Zip Code Report: ' + startDate.toLocaleDateString() + ' - ' + endDate.toLocaleDateString()}</span>
                    </div>
                </div>
                {results.map((resultsZip, indexZip) => (
                    <div className={`adminTable cbTotal`}>
                        <div className="headers2">
                            <span>{resultsZip[0][0].childZip}</span>
                        </div>
                        <div className="headers">
                            {collection.itemTotalsByZipAdminTable.columns.map(({ header, className, sort }) =>
                                (this.state.columnsVisible.indexOf(className) >= 0 ? (
                                    <span className={className}>
                                    {header === "CaseWorker" ? "Users" : header}
                                        {sort ? (
                                            <div className="sort">
                                                <i
                                                    className={`mdi mdi-arrow-up${this.state.sort === sort && this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                                    onClick={event => this.setState({ sort, reverse: true })}
                                                />
                                                <i
                                                    className={`mdi mdi-arrow-down${this.state.sort === sort &&
                                            !this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                                    onClick={event => this.setState({ sort, reverse: false })}
                                                />
                                            </div>
                                        ) : null}
                            </span>
                                ) : null))}
                        </div>
                        <div className="data">
                            {resultsZip.map((item, index) => (
                                (groupBy(item, function (item2) {return [item2.itemsSize]})).sort(function (a, b) {return (parseInt(b.length,0) - parseInt(a.length,0))}).map((item3, index3) => (
                                    <>
                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Clothing Bag' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                <span className={'text'}><b>
                                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                    &nbsp;:&nbsp;{OrderBagType.getIdentifier(OrderBagType.getValues()[item[0].itemsType])}
                                                </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                                            <span className={'text'}></span>
                                            <span className={'text'}>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize])} </span>
                                            <span className={'number2'}>{item3.length}</span>
                                            <span className={'number2'}></span>
                                        </div>
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Other Clothing' ? (
                                        <>
                                        {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Maternity' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pajamas - Girls' ? (
                                            <>
                                            {index3 === 0 ?
                                                <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                    <span className={'text'}>&nbsp;</span>
                                                    <span className={'number2'}>&nbsp;</span>
                                                    <span className={'number2'}><b>{item.length}</b></span>
                                                </div>
                                                : null}
                                            <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                                                <span className={'text'}></span>
                                                <span className={'text'}>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize])} </span>
                                                <span className={'number2'}>{item3.length}</span>
                                                <span className={'number2'}></span>
                                            </div>
                                            </>
                                        ) : null}

                                        {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pajamas - Boys' ? (
                                            <>
                                            {index3 === 0 ?
                                                <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                    <span className={'text'}>&nbsp;</span>
                                                    <span className={'number2'}>&nbsp;</span>
                                                    <span className={'number2'}><b>{item.length}</b></span>
                                                </div>
                                                : null}
                                            <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                                                <span className={'text'}></span>
                                                <span className={'text'}>{OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize])} </span>
                                                <span className={'number2'}>{item3.length}</span>
                                                <span className={'number2'}></span>
                                            </div>
                                            </>
                                        ) : null}

                                        {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pants (2 Pair)' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Underwear (2 Pair)' ? (
                                            <>
                                            {index3 === 0 ?
                                                <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                    <span className={'text'}>&nbsp;</span>
                                                    <span className={'number2'}>&nbsp;</span>
                                                    <span className={'number2'}><b>{item.length}</b></span>
                                                </div>
                                                : null}
                                            <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                                                <span className={'text'}></span>
                                                <span className={'text'}>{OrderBoyPantSize.getIdentifier(OrderBoyPantSize.getValues()[item3[0].pantSize])} </span>
                                                <span className={'number2'}>{item3.length}</span>
                                                <span className={'number2'}></span>
                                            </div>
                                            </>
                                        ) : null}

                                        {OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Shoes' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Socks (2 Pair)' ? (
                                            <>
                                            {index3 === 0 ?
                                                <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                    <span className={'text'}>&nbsp;</span>
                                                    <span className={'number2'}>&nbsp;</span>
                                                    <span className={'number2'}><b>{item.length}</b></span>
                                                </div>
                                                : null}
                                            <div className="datum" index={index} key={item[0]._id} id={item[0]._id}>
                                                <span className={'text'}></span>
                                                <span className={'text'}>{OrderShoeSize.getIdentifier(OrderShoeSize.getValues()[item3[0].shoeSize])} </span>
                                                <span className={'number2'}>{item3.length}</span>
                                                <span className={'number2'}></span>
                                            </div>
                                            </>
                                        ) : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Diapers' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderDiaperType.getIdentifier(OrderDiaperType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Hygiene Items' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderHygieneType.getIdentifier(OrderHygieneType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Wipes' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Bedding' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderBeddingType.getIdentifier(OrderBeddingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Equipment' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderEquipmentType.getIdentifier(OrderEquipmentType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Feeding' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderFeedingType.getIdentifier(OrderFeedingType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Toys' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderToyType.getIdentifier(OrderToyType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}

                                    {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Stroller' ? (
                                        <>
                                        {index3 === 0 ?
                                            <div className="datum-heading" index={index} key={item[0]._id + "00"} id={item[0]._id}>
                                                    <span className={'text'}><b>
                                                        {OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])}
                                                        &nbsp;:&nbsp;{OrderStrollerType.getIdentifier(OrderStrollerType.getValues()[item[0].itemsType])}
                                                    </b></span>
                                                <span className={'text'}>&nbsp;</span>
                                                <span className={'number2'}>&nbsp;</span>
                                                <span className={'number2'}><b>{item.length}</b></span>
                                            </div>
                                            : null}
                                        </>
                                    ) : null}
                                    </>
                                ))
                            ))}
                            <div className="datum-heading">
                                <span className={'text'}><b>Totals:</b></span>
                                <span className={'text'}>&nbsp;</span>
                                <span className={'number2'}>&nbsp;</span>
                                <span className={'number2'}><b>{resultsZip.reduce(((sum2, result) => sum2 + resultsZip.length), 0)}</b></span>
                            </div>
                        </div>
                    </div>
                ))}
                <div className="adminTable cbTotal">
                    <div className="headers2">
                        <span>{'Grand Total: ' + results.reduce(((sum2, result) => sum2 + result.length), 0)}</span>
                    </div>
                    <div className="footers">
                        <CCButton
                            type="download"
                            onClick={(event) => {
                              console.log("components/itemTotalsByZipAdminTable.jsx:download");

                              event.preventDefault();

                              const fields = ["Zip","Item Type","Size","Size Count","Zip Count"];

                              const data = [];

                              results.map((resultsZip, indexZip) => {
                                resultsZip.map((item, index) => (
                                (groupBy(item, function (item2) {return [item2.itemsSize]})).sort(function (a, b) {return (parseInt(b.length,0) - parseInt(a.length,0))}).map((item3, index3) => {
                                    let row;

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Clothing Bag' ) {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderBagType.getIdentifier(OrderBagType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                        row = [];
                                        row.push(item[0].childZip);
                                        row.push("");
                                        row.push(OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize]));
                                        row.push(item3.length);
                                        row.push("");
                                        data.push(row);
                                    }
                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Other Clothing' ) {
                                        if (OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Maternity' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pajamas - Girls' ) {
                                           if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderBagType.getIdentifier(OrderBagType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                        row = [];
                                        row.push(item[0].childZip);
                                        row.push("");
                                        row.push(OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize]));
                                        row.push(item3.length);
                                        row.push("");
                                        data.push(row);
                                    }
                                    if (OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pajamas - Boys') {
                                       if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderBagType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                        row = [];
                                        row.push(item[0].childZip);
                                        row.push("");
                                        row.push(OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize]));
                                        row.push(item3.length);
                                        row.push("");
                                        data.push(row);
                                    }
                                    if (OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Pants (2 Pair)' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Underwear (2 Pair)') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderBagType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                        row = [];
                                        row.push(item[0].childZip);
                                        row.push("");
                                        row.push(OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize]));
                                        row.push(item3.length);
                                        row.push("");
                                        data.push(row);
                                    }

                                    if (OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Shoes' ||
                                        OrderClothingType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]) === 'Socks (2 Pair)') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderBagType.getIdentifier(OrderClothingType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                        row = [];
                                        row.push(item[0].childZip);
                                        row.push("");
                                        row.push(OrderClothingSize.getIdentifier(OrderClothingSize.getValues()[item3[0].itemsSize]));
                                        row.push(item3.length);
                                        row.push("");
                                        data.push(row);
                                    }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Diapers') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                    + " : " + OrderDiaperType.getIdentifier(OrderDiaperType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Hygiene Items') {
                                       if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderHygieneType.getIdentifier(OrderHygieneType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }
                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Wipes') {
                                       if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Bedding') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderBeddingType.getIdentifier(OrderBeddingType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Equipment' ) {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderEquipmentType.getIdentifier(OrderEquipmentType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Feeding') {
                                       if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderFeedingType.getIdentifier(OrderFeedingType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Toys') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderToyType.getIdentifier(OrderToyType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                                    if (OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory]) === 'Stroller') {
                                        if (index3 === 0) {
                                                row = [];
                                                row.push(item[0].childZip);
                                                row.push(OrderItemCategory.getIdentifier(OrderItemCategory.getValues()[item[0].itemsCategory])
                                                + " : " + OrderStrollerType.getIdentifier(OrderStrollerType.getValues()[item[0].itemsType]));
                                                row.push("");
                                                row.push("");
                                                row.push(item.length);
                                                data.push(row);
                                        }
                                    }

                               })));
                                let trow = [];
                                trow.push(resultsZip[0][0].childZip);
                                trow.push("Totals");
                                trow.push("");
                                trow.push("");
                                trow.push(resultsZip.reduce(((sum2, result) => sum2 + resultsZip.length), 0));
                                data.push(trow);

                                trow = [];
                                trow.push("");
                                trow.push("");
                                trow.push("");
                                trow.push("");
                                trow.push("");
                                data.push(trow);

                               });

                               let gtrow = [];
                               gtrow.push("");
                               gtrow.push("Grand Total");
                               gtrow.push("");
                               gtrow.push("");
                               gtrow.push(results.reduce(((sum2, result) => sum2 + result.length), 0));
                               data.push(gtrow);

                               const link = encodeURI(`data:text/csv;charset=utf-8,${Papa.unparse({ fields, data })}`);
                               try {
                                    saveAs(link, `itemTotalsByZipReport_export.csv`);
                               }
                               catch (e) {
                                    console.log("components/itemTotalsByZipAdminTable.jsx:error:4:e" + JSON.stringify(e));
                               }

                            }}
                        />
                    </div>
                </div>
            </div>
            </>
            );
    }
}

export default container((props, onData) => {
    //console.log("components/ItemTotalsByZipOrderAdminTable.jsx" + JSON.stringify(props));
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        onData(null, {});
    }
}, ItemTotalsByZipOrderAdminTable);
