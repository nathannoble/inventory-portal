import { Meteor } from 'meteor/meteor';
import React from 'react';

import container from '/imports/ui/helpers/container';

import Organization from '/imports/api/Organization';

import BusinessView from '/imports/ui/components/Organization/Business/View';

const OrganizationProfileSwitcher = container(
  (props, onData) => {
    const subscription = Meteor.subscribe(
      `/organization/${props.match.params.organizationType}`,
      props.match.params.organizationId,
    );
    if (subscription.ready()) {
      const organization = Organization.findOne(props.match.params.organizationId);
      onData(null, { organization });
    }
  },
  ({ organization }) =>
    Object({
      business: <BusinessView business={organization} />,
    })[organization.type || 'business'],
);

export default OrganizationProfileSwitcher;
