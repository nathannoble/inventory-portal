import React from 'react';

import BusinessCardFields from '/imports/ui/components/Organization/Business/View/CardFields';
//import SchoolCardFields from '/imports/ui/components/Organization/School/View/CardFields';

import Avatar from '/imports/ui/components/Avatar';

const OrganizationCard = ({ organization }) => (
  <div className={`user ${organization.type.toLowerCase()} card`}>
    <h2>{organization.name}</h2>
    <section className="profile">
      <Avatar organization={organization} />
      <span className="name">{organization.name}</span>
      {
        Object({
          business: <BusinessCardFields business={organization} />,
        })[organization.type.toLowerCase()]
      }
    </section>
  </div>
);

export default OrganizationCard;
