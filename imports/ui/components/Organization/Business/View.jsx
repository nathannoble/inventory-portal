import React from 'react';
import { Link } from 'react-router-dom';

import container from '/imports/ui/helpers/container';

import Organization from '/imports/api/Organization';

import BusinessViewInformation from './View/Information';
import BusinessViewDepartments from './View/Departments';
import BusinessViewOpportunities from './View/Opportunities';
import BusinessViewInvite from './View/Invite';

import AvatarUpload from '/imports/ui/components/AvatarUpload';
import CCButton from '/imports/ui/components/helpers/CCButton';

import handleError from '/imports/ui/helpers/handleError';

const BusinessAdministrationLink = ({ business }) =>
  (Meteor.user().type === 'Administrator' || business.adminIds.includes(Meteor.userId()) ? (
    <Link className="admin button" to={`${business.path}/administration`}>
      Employees/Admins
    </Link>
  ) : null);

const BusinessReportsLink = ({ business }) =>
  (Meteor.user().type === 'Administrator' || business.adminIds.includes(Meteor.userId()) ? (
    <Link className="admin button" to={`${business.path}/reports`}>
      Reports
    </Link>
  ) : null);

const BusinessRemoveButton = ({ business, history }) =>
  (Meteor.user().may.delete.organization(business) ? (
      <CCButton
          type="remove"
          onClick={(event) => {
            Meteor.call(
              '/organization/business/delete',
              business,
              handleError({
                callback() {
                  history.push('/business');
                },
              }),
            );
          }}
      >{`Delete ${business.name}`}</CCButton>
  ) : null);

const BusinessAdminActions = ({ business }) =>
  (Meteor.user().type === 'Administrator' && typeof business.approved === 'undefined' ? (
    <div className="adminActions">
      <button className="approve">Approve</button>
      <button className="deny">Deny</button>
    </div>
  ) : null);

const BusinessView = container(
  (props, onData) => {
    if (props.organization) onData(null, { business: props.organization });
    const subscription = Meteor.subscribe(
      '/organization/business',
      props.match.params.organizationId,
    );
    if (subscription.ready()) {
      const organization = Organization.findOne(props.match.params.organizationId);
      onData(null, { business: organization });
    }
  },
  props => (
    <div className="business organization view">
      <BusinessViewInformation {...props} />
      <div className="sidebar">
        <AvatarUpload
          entity={props.business}
          onUpload={(organization) => {
            Meteor.call('/organization/business/update', organization, handleError);
          }}
          onRemove={(organization, callback) => {
            Meteor.call('/organization/business/update', organization, handleError({ callback }));
          }}
        />
        <BusinessRemoveButton {...props} />
      </div>
    </div>
  ),
);

export default BusinessView;
