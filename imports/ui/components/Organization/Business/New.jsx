import React from 'react';
import { Bert } from 'meteor/themeteorchef:bert';

import Business, { CareProviderLocation, CareProviderPickupDay} from '/imports/api/Organization/Business';

import { Label, Select, handleError } from '/imports/ui/helpers';

class BusinessNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            location: null,
            pickupDay: null,
            cpNumber: 0
        };
    }

    render() {
        return (
            <div className="business organization new">
                {Meteor.user().may.create.organization() ? (
                    <form
                        onSubmit={(event) => {
          event.preventDefault();

          const history = this.props.history;
          const name =  this.nameInput.ref.value;
          const email =  this.emailInput.ref.value;
          const pickupDay =  this.state.pickupDay;
          const location =  this.state.location;
          const cpNumber =  parseInt(this.cpNumberInput.ref.value,0);

          const business = new Business({
              name,
              email: email,
              pickupDay: pickupDay,
              location: location,
              cpNumber: cpNumber
          });
          Meteor.call(
            '/organization/create',
            business,
            handleError({
              callback(business) {
                history.push(`/business/${business._id}`);
              },
            }),
          );
        }}
                    >
                        <h2>New Care Provider</h2>
                        <Label name="name"
                               required
                               text="Name"
                               type="text"
                               placeholder="New Care Provider Name"
                               ref={input => (this.nameInput = input)}
                        />

                        <Label
                            name="cpNumber"
                            text="CP Number"
                            type="number"
                            placeholder="CP Number"
                            ref={input => (this.cpNumberInput = input)}
                        />

                        <Label
                            name="email"
                            text="Email of contact"
                            type="email"
                            placeholder="Care Provider Email Contact"
                            ref={input => (this.emailInput = input)}
                        />

                        <Label
                            name="location"
                            text="Location"
                            placeholder="Location"
                            value={this.state.location}
                            element={Select}
                            options={CareProviderLocation.getIdentifiers().map((location, index) => Object({ value: index, label: location }))}
                            onChange={(selectedOption) => {
                               this.setState({location: selectedOption.value});
                             }}
                        />

                        <Label
                            name="pickupDay"
                            text="Pickup Day"
                            placeholder="Pickup Day"
                            value={this.state.pickupDay}
                            element={Select}
                            options={CareProviderPickupDay.getIdentifiers().map((day, index) => Object({ value: index, label: day }))}
                            onChange={(selectedOption) => {
                              this.setState({pickupDay: selectedOption.value});
                              //console.log(JSON.stringify(this.state));
                             }}
                        />
                        <Label name="submit" type="submit" value="Add Care Provider"/>
                    </form>
                ) : null}
            </div>
        );
    }
}

export default BusinessNew;
