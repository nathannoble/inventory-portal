import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import BusinessNew from './New';
import BusinessView from './View';
import BusinessAdministration from './Administration';
import BusinessAdminTable from './AdminTable';

const BusinessRoutes = props => (
  <Switch className="business">
    <Authenticated
      exact
      name="BusinessNew"
      path={`${props.match.path}/new`}
      component={BusinessNew}
      roles={['administrator']}
      {...props}
    />
    <Authenticated
      exact
      name="BusinessAdministration"
      path={`${props.match.path}/:organizationId/administration`}
      component={BusinessAdministration}
      {...props}
    />
    <Authenticated
      exact
      name="BusinessView"
      path={`${props.match.path}/:organizationId`}
      component={BusinessView}
      {...props}
    />
    <Authenticated
      exact
      name="BusinessList"
      path={props.match.path}
      component={BusinessAdminTable}
      roles={['administrator']}
      {...props}
    />
  </Switch>
);

export default BusinessRoutes;
