import React from 'react';

const SignupOrganization = ({ onKeyUp }) => (
  <fieldset name="organization">
    <label htmlFor="signupOrganizationName">Please enter the name of your business.</label>
    <input
      autoComplete="off"
      id="signupOrganizationName"
      name="organizationName"
      onKeyUp={onKeyUp}
      placeholder="Business name"
      type="text"
    />
  </fieldset>
);

export default SignupOrganization;
