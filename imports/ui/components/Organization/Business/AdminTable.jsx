import React from 'react';
import _ from 'underscore';
import { Link } from 'react-router-dom';
import listify from 'listify';

import Business, { CareProviderLocation, CareProviderPickupDay} from '/imports/api/Organization/Business';

import AdminTableDefinition from '/imports/api/helpers/AdminTable';
import helperDate from '/imports/api/helpers/helperDate';
import compare from '/imports/api/helpers/compare';

import Avatar from '/imports/ui/components/Avatar';
import AdminTable from '/imports/ui/components/AdminTable';

import container from '/imports/ui/helpers/container';

import Checkbox from '/imports/ui/components/helpers/Checkbox';

Business.adminTable = new AdminTableDefinition();

Business.adminTable.push({
  transform: business => <Avatar organization={business} />,
  className: 'avatar',
  export: false,
  visible: true,
});

Business.adminTable.push({
  header: 'Care Provider',
  sort: (a, b) => {
    a = a.name;
    b = b.name;
    return compare(a, b);
  },
  transform: (business, willExport = false) => {
    if (willExport) {
      return (
        <a title={business.name} href={business.path}>
          {business.name}
        </a>
      );
    }
    return (
      <Link title={business.name} to={business.path}>
        {business.name}
      </Link>
    );
  },
  className: 'name',
  visible: true,
});

Business.adminTable.push({
  header: 'Date Registered',
  sort: (a, b) => {
    a = a.createdAt;
    b = b.createdAt;
    return compare(a, b);
  },
  transform: business => helperDate(business.createdAt),
  className: 'createdAt',
  visible: true,
});

Business.adminTable.push({
    header: 'CP Number',
    sort: (a, b) => {
        a = a.cpNumber;
        b = b.cpNumber;
        return compare(a, b);
    },
    transform: business => business.cpNumber,
    className: 'cpNumber',
    visible: true,
});

Business.adminTable.push({
    header: 'Location',
    sort: (a, b) => {
        a = a.location;
        b = b.location;
        return compare(a, b);
    },
    transform: business => CareProviderLocation.getIdentifier(business.location),
    className: 'location',
    visible: true,
});

Business.adminTable.push({
    header: 'Pickup Day',
    sort: (a, b) => {
        a = a.pickupDay;
        b = b.pickupDay;
        return compare(a, b);
    },
    transform: business => CareProviderPickupDay.getIdentifier(business.pickupDay),
    className: 'pickupDay',
    visible: true,
});


Business.adminTable.push({
    header: 'Contact Email',
    sort: (a, b) => {
        a = a.email;
        b = b.email;
        return compare(a, b);
    },
    transform: (business, willExport = false) => {
        if (willExport) {
            return (
                <a title={business.email} href={business.path}>
                    {business.email}
                </a>
            );
        }
        return (
            <Link title={business.email} to={business.path}>
                {business.email}
            </Link>
        );
    },
    className: 'email',
    visible: true,
});

const BusinessAdminTable = props => <AdminTable collection={Business} {...props} />;

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/organization/business/list');
  if (subscription.ready()) {
    onData(null, {});
  }
}, BusinessAdminTable);
