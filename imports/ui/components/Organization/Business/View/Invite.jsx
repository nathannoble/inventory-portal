import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Bert } from 'meteor/themeteorchef:bert';

import handleError from '/imports/ui/helpers/handleError';

const BusinessViewInvite = ({ business }) => (
  <div className="business invite">
    <h2>Invite Employees to Sign Up</h2>
    <form
      onSubmit={(event) => {
        event.preventDefault();
        let message = document.getElementById('message').value;
        message += `${message && business.authorizationCode
          ? ' || '
          : ''}${business.authorizationCode
          ? `Use the authorization code ${business.authorizationCode} when signing up`
          : ''}`;
        Meteor.call(
          '/email',
          'user/volunteer/invitation',
          document.getElementById('emails').value.split(','),
          null,
          "We\'re Connecting Educators with Volunteers. Sign up for Northwest Children's Outreach!",
          { message },
          handleError({ confirmationText: 'Email Sent' }),
        );
      }}
    >
      <label htmlFor="emails">
        Employee Emails
        <input type="text" id="emails" placeholder="john@doe.com, jane@doe.com" />
      </label>
      <label htmlFor="message">
        Custom Message
        <textarea id="message" />
      </label>
      <a
        className="preview"
        onClick={(event) => {
          let message = document.getElementById('message').value;
          message += `${message && business.authorizationCode
            ? ' || '
            : ''}${business.authorizationCode
            ? `Use the authorization code ${business.authorizationCode} when signing up`
            : ''}`;
          window.open(
            `/preview/email/user/volunteer/invitation?message=${encodeURIComponent(message)}`,
            'preview',
            'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,width=650,height=800',
          );
        }}
      >
        Click here to see a preview
      </a>
      <input type="submit" value="Send Invitation" />
    </form>
  </div>
);

export default BusinessViewInvite;
