import { Meteor } from 'meteor/meteor';
import React from 'react';

import OrganizationDepartment from '/imports/api/Organization/Business/Department';

import { CCButton, handleError } from '/imports/ui/helpers';

const BusinessViewDepartments = ({ business }) => (
  <form className="business organization departments" onSubmit={event => event.preventDefault()}>
    <h2>Departments</h2>
    <ul>
      {business.departments.map((department, index) => (
        <li key={department.id}>
          <input
            type="text"
            placeholder="e.g. Marketing"
            size="15"
            defaultValue={department.name}
            onChange={(event) => {
              business.departments[index].name = event.target.value;
              Meteor.call('/organization/business/update', business, handleError);
            }}
          />
          <CCButton
            type="remove"
            onClick={(event) => {
              event.preventDefault();
              business.departments.splice(index, 1);
              Meteor.call('/organization/business/update', business, handleError);
            }}
          />
        </li>
      ))}
    </ul>
    <CCButton
      type="add"
      onClick={(event) => {
        event.preventDefault();
        if (
          business.departments.length >= 1 &&
          !business.departments[business.departments.length - 1].name
        ) { return; }
        business.departments.push(new OrganizationDepartment());
        Meteor.call('/organization/business/update', business, handleError);
      }}
    >
      {business.departments.length >= 1 ? null : 'Add New Department'}
    </CCButton>
  </form>
);

export default BusinessViewDepartments;
