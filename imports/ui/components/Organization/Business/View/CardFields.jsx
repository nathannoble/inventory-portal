import React from 'react';
import listify from 'listify';

const BusinessCardFields = ({ business }) => (
  <div>
    {business.address
      ? <span className="address">
        <span className="label">Address</span>
        <span className="value">{business.address}</span>
      </span>
      : null
    }
    {business.location
      ? <span className="website">
        <span className="label">Location</span>
        <span className="value">{business.location}</span>
      </span>
      : null
    }
    <span className="description">
      <span className="label">Description</span>
      <span className="value">{business.description}</span>
    </span>
  </div>
);

export default BusinessCardFields;
