import { Meteor } from 'meteor/meteor';
import React from 'react';

import { handleError, Select, Label } from '/imports/ui/helpers';

import Business, { CareProviderLocation, CareProviderPickupDay} from '/imports/api/Organization/Business';

const BusinessViewInformation = ({ business }) => (
    <form className="business organization information">
        <h2>{business.name} Information</h2>
        <Label
            name="name"
            text="Care Provider Name"
            type="text"
            defaultValue={business.name}
            placeholder="Care Provider Name"
            onChange={(event) => {
                business.name = String(event.target.value);
                Meteor.call('/organization/business/update', business, handleError);
              }}
        />

        <Label
            name="cpNumber"
            text="CP Number"
            type="number"
            value={business.cpNumber}
            required
            onChange={(event) => {
                if (parseInt(event.target.value,0) !== business.cpNumber) {
                    business.cpNumber = parseInt(event.target.value,0);
                    Meteor.call('/organization/business/update', business, handleError);
                }
              }}
        />

        {business.admins().count() === 0 ? (
            <Label
                name="email"
                text="Email of contact"
                type="email"
                defaultValue={business.email}
                placeholder="admin@business.website"
                onChange={(event) => {
          business.email = String(event.target.value);
          Meteor.call('/organization/business/update', business, handleError);
        }}
            />
        ) : null}

        <label htmlFor="address">
            <span className="legend">Address</span>
            <input
                defaultValue={business.addressParts.street}
                name="address__street"
                onChange={(event) => {
          business.addressParts.street = String(event.target.value);
          Meteor.call('/organization/business/update', business, handleError);
        }}
                placeholder="Street Address"
                size="30"
                type="text"
            />
            <input
                defaultValue={business.addressParts.city}
                name="address__city"
                onChange={(event) => {
          business.addressParts.city = String(event.target.value);
          Meteor.call('/organization/business/update', business, handleError);
        }}
                placeholder="City"
                size="15"
                type="text"
            />
            <input
                defaultValue={business.addressParts.state}
                name="address__state"
                onChange={(event) => {
          business.addressParts.state = String(event.target.value);
          Meteor.call('/organization/business/update', business, handleError);
        }}
                placeholder="State"
                size="3"
                type="text"
            />
            <input
                defaultValue={business.addressParts.zip}
                name="address__zip"
                onChange={(event) => {
          business.addressParts.zip = String(event.target.value);
          Meteor.call('/organization/business/update', business, handleError);
        }}
                placeholder="ZIP"
                size="6"
                type="text"
            />
        </label>

        <Label
            name="phone"
            text="Main Office Phone Number"
            type="tel"
            defaultValue={business.phone}
            placeholder="Phone Number"
            onChange={(event) => {
        business.phone = String(event.target.value);
        Meteor.call('/organization/business/update', business, handleError);
      }}
        />

        <Label
            name="location"
            required
            text="Location"
            placeholder="Location"
            value={business.location}
            element={Select}
            options={CareProviderLocation.getIdentifiers().map((location, index) => Object({ value: index, label: location }))}
            onChange={(selectedOption) => {
                 business.location = selectedOption.value;
                Meteor.call('/organization/business/update', business, handleError);
            }}
        />

        <Label
            name="pickupDay"
            required
            text="Pickup Day"
            placeholder="Pickup Day"
            value={business.pickupDay}
            element={Select}
            options={CareProviderPickupDay.getIdentifiers().map((day, index) => Object({ value: index, label: day }))}
            onChange={(selectedOption) => {
                business.pickupDay = selectedOption.value;
                Meteor.call('/organization/business/update', business, handleError);
            }}
        />


        <Label
            name="description"
            text="Description"
            element="textarea"
            defaultValue={business.description}
            placeholder="Care Provider Description"
            onChange={(event) => {
        business.description = String(event.target.value);
        Meteor.call('/organization/business/update', business, handleError);
      }}
        />
    </form>
);

export default BusinessViewInformation;
