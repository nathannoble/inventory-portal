import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';

import CC from '/imports/api';
import Volunteer from '/imports/api/User/Volunteer';

import { Select, handleError, Checkbox, Label } from '/imports/ui/helpers';

import OpportunitiesTable from '/imports/ui/components/User/Volunteer/OpportunitiesTable';

class BusinessViewOpportunities extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { business } = this.props;
    return (
      <form
        className="business organization opportunities"
        onSubmit={event => event.preventDefault()}
      >
        <h2>Volunteer Opportunities through the Business</h2>

        <p>
          Check the boxes next to each type of volunteer opportunity for which you would like to
          make your business available.
        </p>

        {RequestType.find({ requestedEntityTypes: { $in: ['organization'] } }).map(requestType => (
          <Label
            text={requestType.name}
            element={Checkbox}
            name={requestType._id}
            checked={business.requestTypeIds.includes(requestType._id)}
            onChange={(event) => {
              if (event.target.checked && !business.requestTypeIds.includes(requestType._id)) {
                business.requestTypeIds.push(requestType._id);
              } else if (
                !event.target.checked &&
                business.requestTypeIds.includes(requestType._id)
              ) {
                business.requestTypeIds.splice(business.requestTypeIds.indexOf(requestType._id), 1);
              }
              console.log(business);
              Meteor.call('/organization/business/update', business, handleError);
            }}
          />
        ))}
      </form>
    );
  }
}

export default BusinessViewOpportunities;
