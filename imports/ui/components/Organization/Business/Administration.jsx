import React from 'react';

import Business from '/imports/api/Organization/Business';
import Volunteer from '/imports/api/User/Volunteer';

import container from '/imports/ui/helpers/container';
import Avatar from '/imports/ui/components/Avatar';
import Label from '/imports/ui/components/helpers/Label';

import {
  SubHeader,
  Breadcrumbs,
  Modal,
  Select,
  handleError,
  Checkbox,
  CCButton,
  TypeaheadResults,
} from '/imports/ui/helpers';

class BusinessAddMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      volunteerMatches: [],
      volunteerSelected: false,
    };
  }

  render() {
    const { business, closeModal } = this.props;
    return (
      <form className="volunteer add">
        <label htmlFor="volunteerSearch">
          <input
            type="text"
            id="volunteerSearch"
            placeholder="Center Director Name"
            autoComplete="off"
            ref={input => (this.searchInput = input)}
            onKeyUp={(event) => {
              const maxResults = 5;
              this.setState({ volunteerSelected: false });
              const volunteerMatches = Volunteer.find()
                .fetch()
                .filter(volunteer =>
                  volunteer.name.toLowerCase().includes(event.target.value.toLowerCase()))
                .slice(0, maxResults);
              this.setState({ volunteerMatches });
            }}
          />
          <TypeaheadResults
            results={this.state.volunteerMatches}
            onSelect={(volunteerId) => {
              const volunteerSelected = Volunteer.findOne(volunteerId);
              const volunteerMatches = [];
              this.setState({ volunteerSelected, volunteerMatches });
              this.searchInput.value = volunteerSelected.name;
            }}
          />
        </label>

        {this.state.volunteerSelected ? (
          <button
            id="addVolunteer"
            onClick={(event) => {
              event.preventDefault();
              this.state.volunteerSelected.addOrganization(
                business._id,
                business.authorizationCode,
                handleError,
              );
              closeModal();
            }}
          >
            Add {this.state.volunteerSelected.name} to {business.name}
          </button>
        ) : null}
        <button id="cancelAdd" onClick={() => closeModal()}>
          Cancel
        </button>
      </form>
    );
  }
}

BusinessAddMember = container((props, onData) => {
  const subscription = Meteor.subscribe('/user/volunteer/list');
  if (subscription.ready()) {
    onData(null, {});
  }
}, BusinessAddMember);

const BusinessEditMember = ({
  volunteer, business, affiliationIndex, closeModal,
}) => (
  <form className="volunteer edit">
    <label htmlFor="titleAtOrganization">
      Title at Organization
      <input
        type="text"
        id="titleAtOrganization"
        defaultValue={volunteer.organizations[affiliationIndex].titleAtOrganization}
        onChange={(event) => {
          volunteer.organizations[affiliationIndex].titleAtOrganization = event.target.value;
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
      />
    </label>

    <label htmlFor="department">
      Department
      <Select
        id="department"
        value={volunteer.organizations[affiliationIndex].departmentId}
        options={business.departments.map(department =>
          Object({ value: department.id, label: department.name }))}
        onChange={({ value }) => {
          volunteer.organizations[affiliationIndex].departmentId = value;
          Meteor.call('/user/volunteer/update', volunteer, handleError);
        }}
      />
    </label>

    <fieldset className="permissions">
      <legend>Designate Permissions</legend>

      {Meteor.userId() !== volunteer._id ? (
        <label htmlFor="administrator">
          Administrator of {business.name}
          <Checkbox
            id="administrator"
            checked={business.adminIds.indexOf(volunteer._id) >= 0}
            onClick={(event) => {
              event.preventDefault();
              if (event.target.checked) {
                business.addAdmin(volunteer._id);
              } else {
                business.removeAdmin(volunteer._id);
              }
              Meteor.call('/organization/business/update', business, handleError);
            }}
          />
        </label>
      ) : null}

      {business.requestTypeIds.map((requestTypeId) => {
        const requestType = RequestType.findOne(requestTypeId);
        return requestType ? (
          <Label
            name={`${requestType.name}Contact`}
            text={`${requestType.name} Contact`}
            element={Checkbox}
            checked={
              business.requestTypeAdmins[requestTypeId] &&
              business.requestTypeAdmins[requestTypeId].includes(volunteer._id)
            }
            onChange={(event) => {
              event.preventDefault();
              if (event.target.checked) {
                if (!business.requestTypeAdmins[requestTypeId]) {
                  business.requestTypeAdmins[requestTypeId] = [];
                }
                business.requestTypeAdmins[requestTypeId].push(volunteer._id);
              } else {
                business.requestTypeAdmins[requestTypeId].splice(
                  business.requestTypeAdmins[requestTypeId].indexOf(volunteer._id),
                  1,
                );
              }
              Meteor.call('/organization/business/update', business, handleError);
            }}
          />
        ) : null;
      })}
    </fieldset>

    <CCButton
      type="remove"
      onClick={(event) => {
        event.preventDefault();
        volunteer.organizations.splice(affiliationIndex, 1);
        business.removeUser(volunteer._id);
        closeModal();
        Meteor.call('/user/volunteer/update', volunteer, handleError);
      }}
    >
      Remove User
    </CCButton>
  </form>
);

class BusinessAdministration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addingUser: false,
      editingUser: false,
    };
    this.employeeRow = this.employeeRow.bind(this, this.props.business);
  }

  employeeRow(business, volunteer) {
    return (
      <li key={volunteer._id} onClick={event => this.setState({ editingUser: volunteer })}>
        <Avatar user={volunteer} />
        <span className="name">{volunteer.name}</span>
        <span className="title">{volunteer.affiliationWith(business).titleAtOrganization}</span>
        <ul className="chips permissions">
          {volunteer.administer(business._id) ? <li>Administrator</li> : null}
          {business.requestTypeIds.map((requestTypeId) => {
            const requestType = RequestType.findOne(requestTypeId);
            if (
              business.requestTypeIds[requestTypeId] &&
              business.requestTypeIds[requestTypeId].includes(volunteer._id)
            ) {
              return <li>{requestType.name} Contact</li>;
            }
            return null;
          })}
        </ul>
        <div className="actions">
          <CCButton
            type="view"
            onClick={(event) => {
              event.preventDefault();
              this.props.history.push(volunteer.path);
            }}
          />
          <CCButton type="edit" onClick={event => this.setState({ editingUser: volunteer })} />
        </div>
      </li>
    );
  }

  departmentRow(department) {
    const volunteers = department.volunteers().fetch();
    return (
      <li key={department.id}>
        <h3>
          {department.name}
          <small>
            ({volunteers.length || 'no'} volunteer{volunteers.length !== 1 ? 's' : ''})
          </small>
        </h3>
        <ul className="employees">{volunteers.map(volunteer => this.employeeRow(volunteer))}</ul>
      </li>
    );
  }

  render() {
    const { business } = this.props;
    return (
      <div className="business administration">
        <SubHeader>
          <Breadcrumbs
            breadcrumbs={[
              { link: `/business/${business._id}`, text: business.name },
              { text: 'Administration' },
            ]}
          />
        </SubHeader>

        <ul className="departments">
          {business.departments.map(department => this.departmentRow(department))}
          <li>
            <h3>Employees Unassigned to a Department</h3>
            <ul className="employees">
              {business.employees().map((volunteer) => {
                if (!volunteer.affiliationWith(business).departmentId) {
                  return this.employeeRow(volunteer);
                }
              })}
            </ul>
          </li>
        </ul>

        {Meteor.user().type === 'Administrator' ? (
          <button
            id="addVolunteer"
            onClick={(event) => {
              event.preventDefault();
              this.setState({ addingUser: true });
            }}
          >
            Add Volunteer to {business.name}
          </button>
        ) : null}

        {this.state.editingUser ? (
          <Modal
            onClose={event => this.setState({ editingUser: false })}
            header={`Edit ${this.state.editingUser.name}`}
          >
            <BusinessEditMember
              volunteer={this.state.editingUser}
              business={business}
              affiliationIndex={((volunteer, businessId) => {
                let affiliationIndex = -1;
                volunteer.organizations.forEach((affiliation, index) => {
                  if (affiliation.organizationId === businessId) affiliationIndex = index;
                });
                return affiliationIndex;
              })(this.state.editingUser, business._id)}
              closeModal={() => this.setState({ editingUser: false })}
            />
          </Modal>
        ) : null}

        {this.state.addingUser ? (
          <Modal
            onClose={event => this.setState({ addingUser: false })}
            header={`Add User to ${business.name}`}
          >
            <BusinessAddMember
              business={business}
              closeModal={() => this.setState({ addingUser: false })}
            />
          </Modal>
        ) : null}
      </div>
    );
  }
}

export default container((props, onData) => {
  if (props.organization) onData(null, { business: props.organization });
  const subscription = Meteor.subscribe(
    '/organization/business',
    props.match.params.organizationId,
  );
  if (subscription.ready()) {
    const business = Business.findOne(props.match.params.organizationId);
    onData(null, { business, history: props.history });
  }
}, BusinessAdministration);
