import React from 'react';
import { Switch } from 'react-router-dom';

import Authenticated from '/imports/ui/components/helpers/Authenticated';

import BusinessRoutes from '/imports/ui/components/Organization/Business/Routes';
//import SchoolRoutes from '/imports/ui/components/Organization/School/Routes';

import OrganizationProfileSwitcher from '/imports/ui/components/Organization';

const OrganizationRoutes = props => (
  <Switch className="organization">
    <Authenticated path="/business" component={BusinessRoutes} {...props} />
    <Authenticated
      path={`${props.match.path}/:organizationId?`}
      component={OrganizationProfileSwitcher}
      {...props}
    />
  </Switch>
);

export default OrganizationRoutes;
