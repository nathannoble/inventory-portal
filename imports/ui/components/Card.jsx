import React from 'react';

import VolunteerCardFields from '/imports/ui/components/User/Volunteer/View/CardFields';
import CaseworkerCardFields from '/imports/ui/components/User/Caseworker/View/CardFields';

import Avatar from '/imports/ui/components/Avatar';

const UserCard = ({ user }) => (
  <div className={`user ${user.type.toLowerCase()} card`}>
    <h2>{user.name}</h2>
    <section className="profile">
      <Avatar user={user} />
      <span className="name">{user.name}</span>
      {
        Object({
          caseworker: <CaseworkerCardFields student={user} />,
          volunteer: <VolunteerCardFields volunteer={user} />,
        })[user.type.toLowerCase()]
      }
    </section>
  </div>
);

export default UserCard;
