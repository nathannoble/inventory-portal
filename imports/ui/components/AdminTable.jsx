import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Papa from 'papaparse';
import DOMPurify from 'dompurify';

import DateTime from 'react-datetime';
import moment from 'moment';

import Business from '/imports/api/Organization/Business';

import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import { Checkbox, Select,  Label, SubHeader, Breadcrumbs, CCButton } from '/imports/ui/helpers';
import saveAs from '/imports/ui/helpers/saveAs';
import Drawer from '/imports/ui/components/helpers/Drawer';

class AdminTable extends React.Component {
    constructor(props) {
        super(props);

        if (props.collection.className.toLowerCase() === "order" || props.collection.className.toLowerCase() === "orderuserorg") {

            console.log("components/AdminTable.jsx:props" + JSON.stringify(props));

            let startDate;
            let endDate;

            let now = new Date();

            // This Week (Until this Friday at 5:00 PM)
            let nextFriday = 5;
            let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
            if (daysUntilPickupLocationDay < 0) {
                daysUntilPickupLocationDay += 7;
            } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
                daysUntilPickupLocationDay += 7;
            }
            
            const user = Meteor.user();

            if (props.match.params.startDate) {
                startDate = new Date(0);
                startDate.setUTCSeconds(parseInt(props.match.params.startDate, 0) / 1000);
                endDate = new Date(0);
                endDate.setUTCSeconds(parseInt(props.match.params.endDate, 0) / 1000);
            } else if (user.type != 'Caseworker'
                && (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()
                    || user.queryEndDate && user.queryEndDate.toDateString() != now.toDateString())) {
                startDate = user.queryStartDate;
                endDate = user.queryEndDate;
            } else {
                console.log("components/AdminTable.jsx::render:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
                startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
                endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
            }

            startDate.setHours(0);
            startDate.setMinutes(0);
            startDate.setSeconds(0);
            startDate.setMilliseconds(0);

            endDate.setHours(23);
            endDate.setMinutes(59);
            endDate.setSeconds(59);
            endDate.setMilliseconds(999);

            this.state = {
                columnsVisible: _.uniq(this.props.collection.adminTable.columns.map(({ className, visible }) => (visible ? className : null))),
                daysUntilPickupLocationDay: daysUntilPickupLocationDay,
                startDate: startDate,
                endDate: endDate,
                dateRange: undefined,
                sort: undefined,
                reverse: false,
            };
        } else {
            this.state = {
                columnsVisible: _.uniq(this.props.collection.adminTable.columns.map(({ className, visible }) => (visible ? className : null))),
                daysUntilPickupLocationDay: 0,
                startDate: undefined,
                endDate: undefined,
                dateRange: undefined,
                sort: undefined,
                reverse: false,
            };
        }
    }

    render() {
        const collection = this.props.collection;
        const results = ((sort) => {

            let query = {};
            let startDate = new Date();
            let endDate = new Date();

            if (collection.className.toLowerCase() === "order" || collection.className.toLowerCase() === "orderuserorg") {

                query = {
                    $and: [
                        {"pickupDate": {"$gte": 0}},
                        {"pickupDate": {"$lte": 0}},
                    ]
                };

                if (this.props.startDate) {

                    startDate = this.props.startDate;
                    endDate = this.props.endDate;

                    startDate.setHours(0);
                    startDate.setMinutes(0);
                    startDate.setSeconds(0);
                    startDate.setMilliseconds(0);

                    endDate.setHours(23);
                    endDate.setMinutes(59);
                    endDate.setSeconds(59);
                    endDate.setMilliseconds(999);

                    query = {
                        $and: [
                            {"pickupDate": {"$gte": startDate}},
                            {"pickupDate": {"$lte": endDate}},
                        ]
                    };
                }
            }

            const results = collection.find(query).fetch();
            console.log("components/AdminTable.jsx:query:" + JSON.stringify(query));
            console.log("components/AdminTable.jsx:results.length:" + results.length);
            console.log("components/AdminTable.jsx:className:" + collection.className.toLowerCase());
            console.log("components/AdminTable.jsx:this.state.dateRange:" + this.state.dateRange);
            console.log("components/AdminTable.jsx:startDate:" + startDate);
            console.log("components/AdminTable.jsx:endDate:" + endDate);

            if (sort) {
                results.sort(sort);
                if (this.state.reverse) results.reverse();
            } else {
                results.sort((a, b) => compare(a.name || a.title, b.name || b.title));
            }
            return results;

        })(this.state.sort);

        return (
            <div
                className={`adminTable ${collection.className.toLowerCase() === "order" ? "internship" : collection.className.toLowerCase()}`}>
                <SubHeader>
                    <Breadcrumbs
                        breadcrumbs={[{ text: `${collection.className  === "Business" ? "Care Provider" : (collection.className  === "UserOrg" ? "User" : (collection.className  === "Volunteer" ? "This Location's User": (collection.className  === "OrderUserOrg") ? "Order" : collection.className))} List` }]}/>
                    <Drawer>
                        {(collection.className.toLowerCase() === "order" || collection.className.toLowerCase() === "orderuserorg") ? (
                            <div className="adminTableFilter">

                                <Label
                                    name="startDate"
                                    element={DateTime}
                                    text="Pick-up Start Date"
                                    onChange={(selectedOption) => {
                                     console.log("components/AdminTable.jsx:startDate:selectedOption:" + JSON.stringify(selectedOption));
                                     if (typeof selectedOption === "object") {
                                         this.setState({
                                            startDate: selectedOption ? selectedOption.toDate() : undefined,
                                            endDate:undefined,
                                            dateRange: undefined,
                                         });
                                     }
                                     }}
                                    timeFormat={false}
                                    value={this.state.startDate}
                                />
                                <Label
                                    name="endDate"
                                    element={DateTime}
                                    text="Pick-up End Date"
                                    onChange={(selectedOption) => {
                                     console.log("components/AdminTable.jsx:endDate:selectedOption:" + JSON.stringify(selectedOption));
                                     if (typeof selectedOption === "object") {

                                          this.setState({
                                            endDate: selectedOption ? selectedOption.toDate() : undefined,
                                            dateRange: undefined,
                                          });

                                          if (this.state.startDate !== undefined && selectedOption.toDate() !== undefined) {

                                            let startDate = this.state.startDate;
                                            let endDate = selectedOption.toDate();

                                            startDate.setHours(0);
                                            startDate.setMinutes(0);
                                            startDate.setSeconds(0);
                                            startDate.setMilliseconds(0);

                                            endDate.setHours(23);
                                            endDate.setMinutes(59);
                                            endDate.setSeconds(59);
                                            endDate.setMilliseconds(999);

                                            if (startDate < endDate) {
                                                console.log("components/AdminTable.jsx:manual:startDate:" + startDate);
                                                console.log("components/AdminTable.jsx:manual:endDate:" + endDate);
                                                this.props.history.push(`/order/${startDate.getTime()}/${endDate.getTime()}`);

                                           }}
                                           }
                                    }}
                                    timeFormat={false}
                                    value={this.state.endDate}
                                />
                            </div>) : null}
                        <div className="adminTableFilter">
                            {collection.adminTable.columns.map(({ header, className }) =>
                                (header ? (
                                    <label htmlFor={className}>
                                        {header}
                                        <Checkbox
                                            id={className}
                                            checked={this.state.columnsVisible.indexOf(className) >= 0}
                                            onChange={(event) => {
                                              const columnsVisible = this.state.columnsVisible;
                                              if (event.target.checked) {
                                                columnsVisible.push(className);
                                              } else {
                                                columnsVisible.splice(columnsVisible.indexOf(className), 1);
                                              }
                                              this.setState({ columnsVisible });
                                            }}
                                        />
                                    </label>
                                ) : null))
                            }
                        </div>
                    </Drawer>
                </SubHeader>
                <div className="headers">
                    {collection.adminTable.columns.map(({ header, className, sort }) =>
                        (this.state.columnsVisible.indexOf(className) >= 0 ? (
                            <span className={className}>
                                {header === "CaseWorker" ? "Users" : header}
                                {sort ? (
                                    <div className="sort">
                                        <i
                                            className={`mdi mdi-arrow-up${this.state.sort === sort && this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: true })}
                                        />
                                        <i
                                            className={`mdi mdi-arrow-down${this.state.sort === sort &&
                                            !this.state.reverse
                                              ? ' active'
                                              : ''}`}
                                            onClick={event => this.setState({ sort, reverse: false })}
                                        />
                                    </div>
                                ) : null}
                        </span>
                        ) : null))}
                </div>
                <div className="data">
                    {results.map((datum, index) => (
                        <div className="datum" index={index} key={datum._id} id={datum._id}>
                            {collection.adminTable.columns.map(({ transform, className }) =>
                                (this.state.columnsVisible.indexOf(className) >= 0 ? (
                                    <span className={className}>{transform(datum)}</span>
                                ) : null))}
                        </div>
                    ))}
                </div>
                <div className="footers">
                    {collection.adminTable.emails ? (
                        <div className="emailActions">
                            <CCButton
                                type="copy"
                                onClick={(event) => {
                                  event.preventDefault();
                                  const input = document.createElement('INPUT');
                                  input.type = 'text';
                                  input.style = 'display: none';
                                  document.body.appendChild(input);
                                  collection.adminTable.emails().forEach((email) => {
                                    input.innerText += `${email.name} <${email.email}>, `;
                                  });
                                  let range;
                                  if (document.body.createTextRange) {
                                    range = document.body.createTextRange();
                                    range.moveToElementText(input);
                                    range.select();
                                  } else if (window.getSelection) {
                                    const selection = window.getSelection();
                                    range = document.createRange();
                                    range.selectNodeContents(input);
                                    selection.removeAllRanges();
                                    selection.addRange(range);
                                  }
                                  try {
                                    document.execCommand('copy');
                                    sAlert.success('Emails copied to clipboard');
                                    if (document.selection) {
                                      document.selection.empty();
                                    } else if (window.getSelection) {
                                      window.getSelection().removeAllRanges();
                                    }
                                  } catch (err) {
                                    window.prompt('Copy to clipboard: Ctrl+C, Enter', input.innerText);
                                  }
                                  document.body.removeChild(input);
                                }}
                            >
                                Copy Emails to Clipboard
                            </CCButton>
                        </div>
                    ) : null}
                    <CCButton
                        type="download"
                        onClick={(event) => {
                          event.preventDefault();
                          const fields = [];
                          const data = [];
                          collection.adminTable.columns.forEach(column =>
                              (column.export && this.state.columnsVisible.indexOf(column.className) >= 0
                                ? fields.push(column.header)
                                : false));
                              results.forEach((datum) => {
                                    const row = [];
                                    collection.adminTable.columns.forEach((column) => {
                                      if (!column.export ||
                                          this.state.columnsVisible.indexOf(column.className) < 0
                                          || column.className === "delete"
                                          || column.className === "copy") {
                                        return;
                                      }
                                      let text = column.transform(datum, true);
                                      //console.log("components/AdminTable.jsx:text:1:" + text);
                                      if (typeof text === 'object') {
                                         try {
                                            if (text.props.title)
                                                text = text.props.title.replace('#', '');
                                            //text = ReactDOMServer.renderToStaticMarkup(text);
                                         }
                                         catch (e) {
                                            console.log("components/AdminTable.jsx:text:error:" + text + ":" + JSON.stringify(e));
                                         }
                                      }
                                      //console.log("components/AdminTable.jsx:text:2:" + text);
                                      try {
                                            row.push(DOMPurify.sanitize(text, { ALLOWED_TAGS: [] }));
                                      }
                                      catch (e) {
                                        console.log("components/AdminTable.jsx:text:error:2:" + text + ":" + JSON.stringify(e));
                                      }
                                    });
                                    try {
                                       data.push(row);
                                    }
                                    catch (e) {
                                        console.log("components/AdminTable.jsx:row:error:3:" + JSON.stringify(row) + ":" + JSON.stringify(e));
                                    }
                               });
                               const link = encodeURI(`data:text/csv;charset=utf-8,${Papa.unparse({ fields, data })}`);
                               try {
                                    saveAs(link, `${collection.className.toLowerCase()}_export.csv`);
                               }
                               catch (e) {
                                    console.log("components/AdminTable.jsx:error:4:e" + JSON.stringify(e));
                               }
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        onData(null, {history:props.history});
    }
}, AdminTable);
