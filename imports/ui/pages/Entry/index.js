import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { Bert } from 'meteor/themeteorchef:bert';
import { Accounts } from 'meteor/accounts-base';

import Logo from '/imports/ui/components/Logo';
import Avatar from '/imports/ui/components/Avatar';

import handleError from '/imports/ui/helpers/handleError';

import Signup from '/imports/ui/pages/Entry/Signup';
import Login from '/imports/ui/pages/Entry/Login';

class Entry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signingUp: this.props.match.path.includes('signup'),
      signingUpUser: false,
    };
  }

  render() {
    const { ...props } = this.props;
    return (
      <div className="entry">
        <div className="wrapper">
          <Logo />
          {this.state.signingUp ? (
            <Signup
              user={this.state.signingUpUser}
              switchToLogin={() => this.setState({ signingUp: false })}
              {...props}
            />
          ) : (
            <Login
              switchToSignup={user => this.setState({ signingUp: true, signingUpUser: user })}
              {...props}
            />
          )}
          {this.state.signingUp ? (
            <button
              id="login"
              className="button--text-appearance"
              onClick={() => {
                props.history.replace('/login');
                this.setState({ signingUp: false });
              }}
            >
              Login
            </button>
          ) : (
            <a className="button button--text-appearance"
               title="Sign Up" href="https://nwchildrens.org/care-provider/">
            SIGN UP
            </a>
          )}
        </div>
      </div>
    );
  }
}

Entry.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    replace: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    path: PropTypes.string,
  }).isRequired,
};

export default Entry;
