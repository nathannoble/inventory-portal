import { Meteor } from 'meteor/meteor';
import React from 'react';

import Title from '/imports/ui/components/helpers/Title';
import handleLogin from '/imports/ui/helpers/login';

import handleError from '/imports/ui/helpers/handleError';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.oAuthOptions = {
      requestPermissions: [],
      loginUrlParameters: {},
      loginStyle: 'popup',
    };
  }
  componentWillMount() {
    if (Meteor.user()) {
      this.props.history.push('/');
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOAuth = this.handleOAuth.bind(this);
  }

  componentDidMount() {
    handleLogin({ component: this });
    this.oAuthOptions.loginHint = this.emailInput.value;
  }

  handleOAuth(error) {
    if (error && error.error === 412) {
      handleError(error);
      this.props.switchToSignup(error.details.user);
    } else {
      handleError({
        callback: () => {
          this.props.history.push('/');
        },
      })(error);
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    const email = this.emailInput.value.toLowerCase();
    const password = this.passwordInput.value;
    Meteor.loginWithPassword(email, password, (error) => {
      if (error) {
        if (error.reason === 'Incorrect password') {
          let message = 'Incorrect password';
          Bert.alert(message);
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          this.emailInput.value = email;
          this.passwordInput.focus();
        } else {
          handleError(error);
        }
      } else {
        this.props.history.push('/');
      }
    });
  }

  render() {
    return (
      <form id="login" onSubmit={this.handleSubmit}>
        <Title>NCO Login</Title>
        <h2><b>NCO</b> Login</h2>
        <input
          autoComplete="off"
          id="email"
          name="email"
          placeholder="Your email"
          ref={input => (this.emailInput = input)}
          type="email"
        />
        <input
          autoComplete="off"
          id="password"
          name="password"
          placeholder="Password"
          ref={input => (this.passwordInput = input)}
          type="password"
        />
        <div id="social">
          {/* <i
            className="mdi mdi-facebook"
            onClick={(event) => {
              Meteor.loginWithFacebook(this.oAuthOptions, this.handleOAuth);
            }}
          />
          <i
            className="mdi mdi-twitter"
            onClick={(event) => {
              Meteor.loginWithTwitter(this.oAuthOptions, this.handleOAuth);
            }}
          />*/}
        </div>
        <div className="entry-actions">
          <a
            onClick={(event) => {
              event.preventDefault();
              const email = this.emailInput.value;
              if (!email) {
                let message = 'Please enter your email address';
                Bert.alert(message, 'danger');
                if (document.querySelectorAll('.bert-content').length > 0) {
                   let el = document.querySelectorAll('.bert-content')[0];
                   if (el) {
                       el.innerHTML = `<p>${message}</p>`;
                   }
                }
                this.emailInput.focus();
              } else {
                Meteor.call(
                  '/user/resetPassword',
                  this.emailInput.value,
                  handleError({
                    confirmationText: `Password reset link sent to ${this.emailInput.value}`,
                  }),
                );
              }
            }}
            className="button button--text-appearance"
          >
            Reset Password
          </a>
          <button type="submit">Login</button>
        </div>
      </form>
    );

    //<i
    //    className="mdi mdi-google-plus"
    //    onClick={(event) => {
    //          Meteor.loginWithGoogle(this.oAuthOptions, this.handleOAuth);
    //        }}
    ///>
  }
}

export default Login;
