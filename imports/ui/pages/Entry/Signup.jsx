import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Accounts } from 'meteor/accounts-base';
import classNames from 'classnames';
import { Bert } from 'meteor/themeteorchef:bert';

import Title from '/imports/ui/components/helpers/Title';
import CC from '/imports/api';

import User from '/imports/api/User';
import Organization from '/imports/api/Organization';
import Business from '/imports/api/Organization/Business';
//import School from '/imports/api/Organization/School';
import camelcase from '/imports/api/helpers/camelcase';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

import SignupCaseworker from '/imports/ui/components/User/Caseworker/Signup';
import SignupVolunteer from '/imports/ui/components/User/Volunteer/Signup';
//import SignupStudent from '/imports/ui/components/User/Student/Signup';
//import SignupEducator from '/imports/ui/components/User/Educator/Signup';

import SignupOrganization from '/imports/ui/components/Organization/Business/Signup';
//import SignupSchool from '/imports/ui/components/Organization/School/Signup';

const SignupRole = ({
  name, role, description, signupForm,
}) => (
  <li className={camelcase(name)}>
    <h3>{name === "Volunteer" ? "Center Director" : name}</h3>
    <span>{description}</span>
    <button
      onClick={(event) => {
        event.preventDefault();
        signupForm.chooseRole(role);
      }}
    >
      {CC.icon(camelcase(name))}
    </button>
  </li>
);

class Signup extends React.Component {
  constructor(props) {
    super(props);
    // Does some checking to see if the user has been linked to signup for a particular
    // role or organization

    const role = this.props.match.params.role || 'volunteer';

    const step = ((params) => {
      if (params.organizationId) {
        // If an organization ID has been passed via URL,
        // user is at the last step of signup
        return 2;
      } else if (params.role) {
        // If only the role has been passed,
        // they might want to choose an organization
        return 1;
      }
      // Everyone else starts at the beginning
      return 0;
    })(this.props.match.params);

    const organization = ((params) => {
      if (params.organizationId) {
        // If the organization ID is set via params,
        // tell the form to fill out that for the user
        return { _id: organizationId };
      }
      return null;
    })(this.props.match.params);

    // If a whole user object is passed to this form, they
    // are ready to sign up by the time they reach the end
    // (for social logins specifying role)
    const readyToSignUp = !!this.props.user;

    this.state = {
      role,
      step,
      organization,
      readyToSignUp,
      hasAuthorizedService: false,
      advancingBlocked: false,
    };

    this.oAuthOptions = {
      requestPermissions: [],
      loginUrlParameters: {},
      loginStyle: 'popup',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOAuth = this.handleOAuth.bind(this);
  }

  chooseRole(role) {
    this.setState({ role });
    const rolesWithPrompts = ['caseworker', 'organization'];
    this.setState({ advancingBlocked: rolesWithPrompts.includes(role) });
    this.setState({ step: 1 });
    const fieldToFocus = {
      organization: 'signupOrganizationName',
      //school: 'signupSchoolName',
      volunteer: 'organizationSearch',
      caseworker: 'organizationSearch'
      //educator: 'organizationSearch',
      //student: 'organizationSearch',
    }[role];
    this.props.history.replace(`/signup/${role}`);
    Meteor.setTimeout(() => document.getElementById(fieldToFocus).focus(), 500);
  }

  handleOAuth(error) {
    if (error && error.error === 412) {
      this.setState({ hasAuthorizedService: true });
      const user = new User(error.details.user);
      user.profile.role = this.state.role;
      user.createdAt = new Date();
      Meteor.call('/user/create', user, (error) => {
        if (user.services.google) {
          Meteor.loginWithGoogle(this.oAuthOptions, () => this.props.history.push('/'));
        } else if (user.services.facebook) {
          Meteor.loginWithFacebook(this.oAuthOptions, () => this.props.history.push('/'));
        } else if (user.services.twitter) {
          Meteor.loginWithTwitter(this.oAuthOptions, () => this.props.history.push('/'));
        }
      });
    } else {
      handleError({
        callback: () => {
          this.props.history.push('/');
        },
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    if (!this.nameInput.value.includes(' ')) {
      let message = 'Please enter both first and last name';
      Bert.alert(message, 'danger');
      if (document.querySelectorAll('.bert-content').length > 0) {
        let el = document.querySelectorAll('.bert-content')[0];
        if (el) {
          el.innerHTML = `<p>${message}</p>`;
        }
      }
      return;
    }
    if (this.state.readyToSignUp) {
      let user = this.props.user || {
        email: this.emailInput.value,
        password: this.passwordInput.value,
      };

      user.profile = {
        role: this.state.role,
        name: this.nameInput.value,
      };

      if (this.state.organization) {
        if (this.state.organization._id) {
          user.profile.organizationId = this.state.organization._id;
          //user.employer =  this.state.organization._id;
        } else {
          user.profile.organization = this.state.organization.name;
        }
      }
      console.log('Entry/Signup.jsx:handleSubmit:state:', JSON.stringify(this.state));
      console.log('Entry/Signup.jsx:handleSubmit:user:', JSON.stringify(user));
      if (user.services) {
        // Call a special method for user creation if they've already
        // begun the process and been kicked to this form
        user = new User(user);
        user.createdAt = new Date();
        Meteor.call('/user/create', user, (error) => {
          if (error) {
            handleError(error);
          } else {
            this.props.switchToLogin();
          }
        });
      } else {
        Accounts.createUser(
          user,
          handleError({
            callback: () => {
              Meteor.loginWithPassword(user.email.toLowerCase(), user.password, () =>
                this.props.history.push('/'));
            },
          }),
        );
      }
    }
  }

  render() {
    return (
      <form
        id="signup"
        onSubmit={this.handleSubmit}
        onKeyUp={(event) => {
          if (event.keyCode === 13) {
            event.stopPropagation();
          }
        }}
        onKeyDown={(event) => {
          if (event.keyCode === 13) {
            event.preventDefault();
          }
        }}
      >
        <Title>NCO Signup</Title>
        <h2><b>NCO </b>
          {
            // This h2 stays the same but the text inside changes based
            // on the state of the wizard
            {
              0: 'Welcome',
              1: {
                caseworker: '(optional) Choose Location',
                volunteer: '(optional) Choose Location',
                organization: 'Enter Care Provider',
              }[this.state.role],
              2: 'Personal Information',
            }[this.state.step || 0]
          }
        </h2>
        <ol className="wizard">
          <li
            id="signupForm__choose-role"
            className={classNames({ active: this.state.step === 0 })}
          >
            {<p>To continue signing in, please choose your role.</p>}
            <ul>
              <SignupRole
                description="You are Center Director for a Care Provider."
                name="Volunteer"
                role="volunteer"
                signupForm={this}
              />
              <SignupRole
                  description="You volunteer your time as a case worker to help process orders."
                  name="Caseworker"
                  role="caseworker"
                  signupForm={this}
              />
            </ul>
          </li>

          <li
            id="signupForm__role-details"
            className={classNames({ active: this.state.step === 1 })}
          >
            {this.state.role
              ? {
                  volunteer: (
                    <SignupVolunteer
                      onChooseOrganization={(organization) => {
                        this.props.history.replace(`/signup/volunteer/${organization._id}`);
                        this.setState({ organization, step: 2 });
                        document.getElementById('name').focus();
                      }}
                    />
                  ),
                  caseworker: (
                      <SignupCaseworker
                          onChooseOrganization={(organization) => {
                            this.props.history.replace(`/signup/caseworker/${organization._id}`);
                            this.setState({ organization, step: 2 });
                            document.getElementById('name').focus();
                          }}
                      />
                  ),
                  organization: (
                    <SignupOrganization
                      onKeyUp={(event) => {
                        if (event.target.value.length > 0) {
                          this.setState({ organization: { name: event.target.value } });
                          this.setState({ advancingBlocked: false });
                          Meteor.call(
                            '/organization/checkName',
                            event.target.value,
                            handleError({
                              callback: (response) => {
                                if (response.code !== 200) {
                                  this.setState({ advancingBlocked: true });
                                  if (response.code === 409) {
                                    let message = 'An organization with this name has already been entered. Try signing up as a volunteer for it.';
                                    Bert.alert(message, 'danger');
                                    if (document.querySelectorAll('.bert-content').length > 0) {
                                       let el = document.querySelectorAll('.bert-content')[0];
                                       if (el) {
                                           el.innerHTML = `<p>${message}</p>`;
                                       }
                                    }
                                  }
                                }
                              },
                            }),
                          );
                        } else {
                          this.setState({ organization: null });
                        }
                      }}
                    />
                  ),
                }[this.state.role]
              : null}

            <div className="entry-actions">
              <button
                className="signupForm__reverse"
                onClick={(event) => {
                  event.preventDefault();
                  if (event.target === event.currentTarget) this.setState({ step: 0 });
                }}
              >
                Back
              </button>
              {this.state.advancingBlocked ? null : (
                <button
                  className="signupForm__advance"
                  onClick={(event) => {
                    event.preventDefault();
                    this.setState({ step: 2 });
                    document.getElementById('name').focus();
                  }}
                >
                  {['volunteer', 'caseworker'].indexOf(this.state.role) >= 0 ? 'Skip' : 'Next'}
                </button>
              )}
            </div>
          </li>

          <li
            className={classNames({ active: this.state.step === 2 })}
            id="signupForm__personal-info"
          >
            {this.state.organization ? (
              <fieldset name="organization">
                <input type="hidden" name="organizationId" value={this.state.organization._id} />
                <input type="hidden" name="organizationName" value={this.state.organization.name} />
              </fieldset>
            ) : null}
            {this.props.user && this.props.user.services ? (
              <h3>
                Signing Up with Third Party<br />
                <small>You will need to log in again after signing up</small>
              </h3>
            ) : (
              <h3>
                {this.state.hasAuthorizedService
                  ? 'Please authenticate once more to complete signup'
                  : 'Sign Up with Third Party'}
              </h3>
            )}
            {this.props.user && this.props.user.services ? null : (
              <div id="social">
                {/*<i
                  className="mdi mdi-facebook"
                  onClick={event => Meteor.loginWithFacebook(this.oAuthOptions, this.handleOAuth)}
                />
                <i
                  className="mdi mdi-twitter"
                  onClick={event => Meteor.loginWithTwitter(this.oAuthOptions, this.handleOAuth)}
                />
                 <i
                  className="mdi mdi-google-plus"
                  onClick={event => Meteor.loginWithGoogle(this.oAuthOptions, this.handleOAuth)}
                /> */}
              </div>
            )}
            {(this.props.user && this.props.user.services) ||
            this.state.hasAuthorizedService ? null : (
              <h3>Or Enter Your Information</h3>
            )}
            {(this.props.user && this.props.user.services) ||
            this.state.hasAuthorizedService ? null : (
              <input
                type="text"
                name="name"
                placeholder="Your First and Last Name"
                id="name"
                ref={input => (this.nameInput = input)}
                defaultValue={this.props.user ? this.props.user.name : ''}
              />
            )}
            {(this.props.user && this.props.user.services) ||
            this.state.hasAuthorizedService ? null : (
              <input
                type="email"
                name="email"
                id="email"
                placeholder="Your Email"
                ref={input => (this.emailInput = input)}
                defaultValue={this.props.user ? this.props.user.emails[0].address : ''}
              />
            )}
            {(this.props.user && this.props.user.services) ||
            this.state.hasAuthorizedService ? null : (
              <input
                type="text"
                name="password"
                id="password"
                placeholder="Choose a password"
                ref={input => (this.passwordInput = input)}
                onKeyUp={() => {
                  if (this.passwordInput.value.length > 0) {
                    this.passwordInput.type = 'password';
                    this.passwordVerificationInput.type = 'text';
                    this.passwordMeter.style.display = 'block';
                  } else {
                    this.passwordInput.type = 'text';
                    this.passwordVerificationInput.innerText = '';
                    this.passwordVerificationInput.type = 'hidden';
                    this.passwordMeter.style.display = 'none';
                  }
                }}
              />
            )}
            <input
              type="hidden"
              name="passwordVerification"
              id="passwordVerification"
              placeholder="Confirm Password"
              ref={input => (this.passwordVerificationInput = input)}
              onKeyUp={(event) => {
                if (this.passwordVerificationInput.value.length > 0) {
                  this.passwordVerificationInput.type = 'password';
                } else {
                  this.passwordVerificationInput.type = 'text';
                }
                const readyToSignUp =
                  this.passwordVerificationInput.value === this.passwordInput.value;
                this.setState({ readyToSignUp });
              }}
            />
            <div
              id="passwordMeter"
              ref={div => (this.passwordMeter = div)}
              style={{ display: 'none' }}
            >
              <span />
            </div>
            <a id="tos--link">Terms of service</a>

            {(this.props.user && this.props.user.services) ||
            this.state.hasAuthorizedService ? null : (
              <div className="entry-actions">
                <a
                  className="signupForm__reverse button"
                  onClick={(event) => {
                    event.preventDefault();
                    this.setState({ step: 1 });
                  }}
                >
                  Back
                </a>
                {this.state.readyToSignUp ? <button type="submit">Sign Up</button> : null}
              </div>
            )}
          </li>
        </ol>
      </form>
    );
  }
}

export default Signup;
