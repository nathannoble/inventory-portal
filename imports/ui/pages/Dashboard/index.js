import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import CC from '/imports/api';

import DashboardVolunteer from '/imports/ui/components/User/Volunteer/Dashboard';
import DashboardCaseworker from '/imports/ui/components/User/Caseworker/Dashboard';
import DashboardAdministrator from '/imports/ui/components/User/Administrator/Dashboard';

export const DashboardIcon = ({
  icon, label, linkTo, className,
}) => (
  <figure className={`dashboard-icon ${className}`}>
    <Link to={linkTo} href={linkTo} className="icon">
      {CC.icon(icon)}
      <figcaption>{label}</figcaption>
    </Link>
  </figure>
);

DashboardIcon.propTypes = {
  icon: PropTypes.string,
  label: PropTypes.string,
  linkTo: PropTypes.string,
  className: PropTypes.string,
};

DashboardIcon.defaultProps = {
  icon: 'error',
  label: 'Error',
  linkTo: '/404',
  className: 'error',
};

const Dashboard = () =>
  Object({
    Volunteer: <DashboardVolunteer volunteer={Meteor.user()} />,
    Caseworker: <DashboardCaseworker caseworker={Meteor.user()} />,
    Administrator: <DashboardAdministrator administrator={Meteor.user()} />,
  })[Meteor.user().type];

export default Dashboard;
