import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import Order, { OrderStatus, OrderBasicItemCategory } from '/imports/api/Order';

import compare from '/imports/api/helpers/compare';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class MyOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
        };
        this.maxOrders = 10;
        this.getOrders = this.getOrders.bind(this);
    }

    getOrders() {
        const allOrders = this.props.orders;
        return allOrders.slice(this.state.offset, this.state.offset + this.maxOrders);
    }

    render() {
        const orders = this.props.orders;
        return <div className="administratorWidget dashboard-icon newOrders">
            <h2>Recent Orders</h2>
            {orders.length
                ? <ul>
                {this.getOrders().map(order => <li key={order._id}>
                    <Link to={`/order/${order._id}`}>
                        <span className="name">{order.childName}</span>
                        <span className="type">{order.comments}</span>
                        <time>{moment(order.pickupDate).fromNow()}</time>
                    </Link>
                </li>)}
            </ul>
                : <ul><li>No orders</li></ul>
            }
            {this.props.orders.length > this.maxOrders
                ? <div className="pagination">
                <button className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`}
                        onClick={event => this.setState({ offset: this.state.offset - this.maxOrders })}><i
                    className="mdi mdi-arrow-left"></i></button>
                <button
                    className={`next ${this.state.offset + this.maxOrders > this.props.orders.length ? 'disabled' : ''}`}
                    onClick={event => this.setState({ offset: this.state.offset + this.maxOrders })}><i
                    className="mdi mdi-arrow-right"></i></button>
            </div>
                : null
            }
        </div>;
    }
}

export default container((props, onData) => {
    
    // let startDate;
    // let endDate;

    // let now = new Date();

    // let nextFriday = 5;
    // let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
    // if (daysUntilPickupLocationDay < 0) {
    //     daysUntilPickupLocationDay += 7;
    // } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
    //     daysUntilPickupLocationDay += 7;
    // }

    // startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 100));
    // endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay + 100));

    // startDate.setHours(0);
    // startDate.setMinutes(0);
    // startDate.setSeconds(0);
    // startDate.setMilliseconds(0);

    // endDate.setHours(23);
    // endDate.setMinutes(59);
    // endDate.setSeconds(59);
    // endDate.setMilliseconds(999);

    //const subscription = Meteor.subscribe('/orderUserOrg/searchAllByDate', startDate, endDate);
    //const subscription = Meteor.subscribe('/order/searchByDate', startDate, endDate);

    const subscription = Meteor.subscribe('/order/list');
    if (subscription.ready()) {
        //const orders = Order.find(query, {sort: {createdAt: -1}}).fetch();
        //const orders = Order.find(query,{sort: {'createdAt': -1}, limit: 100}).fetch();
        const orders = Order.find({ordererId: props.user._id},{sort: {'createdAt': -1}, limit: 100}).fetch();
        console.log("pages/Dashboard/MyOrders.jsx:orders.length:" + orders.length);
        onData(null, {orders});
    }
}, MyOrders);
