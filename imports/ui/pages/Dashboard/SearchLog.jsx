import { Meteor } from 'meteor/meteor';
import React from 'react';
import _ from 'underscore';
import { Link } from 'react-router-dom';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class SearchLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
    };
    this.maxSearchInstances = 5;
    this.getVisibleSearchInstances = this.getVisibleSearchInstances.bind(this);
  }

  getVisibleSearchInstances() {
    const allSearchInstances = this.props.searchInstances;
    const visibleSearchInstances = allSearchInstances.slice(
      this.state.offset,
      this.state.offset + this.maxSearchInstances,
    );
    return visibleSearchInstances;
  }

  render() {
    const searchInstances = this.props.searchInstances;
    return (
      <div className="administratorWidget dashboard-icon searchLog">
        <h2>Search Instances</h2>
        {searchInstances.length ? (
          <ul>
            {this.getVisibleSearchInstances().map(searchInstance => (
              <li key={`${searchInstance._id}`}>
                <Link to={searchInstance.owner().path || '/'}>
                  {searchInstance.owner().name}
                </Link>{' '}
                made{' '}
                <Link to={searchInstance.path || '/'}>
                  a search{searchInstance.title ? ` called ${searchInstance.title}` : ''}
                </Link>{' '}
                that returned {searchInstance.numberOfResults} results.
              </li>
            ))}
          </ul>
        ) : (
          <p>No Search Instances</p>
        )}
        {this.props.searchInstances.length > this.maxSearchInstances ? (
          <div className="pagination">
            <button
              className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`}
              onClick={event =>
                this.setState({ offset: this.state.offset - this.maxSearchInstances })}
            >
              <i className="mdi mdi-arrow-left" />
            </button>
            <button
              className={`next ${this.state.offset + this.maxSearchInstances >
              this.props.searchInstances.length
                ? 'disabled'
                : ''}`}
              onClick={event =>
                this.setState({ offset: this.state.offset + this.maxSearchInstances })}
            >
              <i className="mdi mdi-arrow-right" />
            </button>
          </div>
        ) : null}
      </div>
    );
  }
}

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/searchInstance/list');
  if (subscription.ready()) {
    const searchInstances = [];
    onData(null, { searchInstances });
  }
}, SearchLog);
