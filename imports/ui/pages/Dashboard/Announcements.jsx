import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import Announcement from '/imports/api/Announcement';
import compare from '/imports/api/helpers/compare';
import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

import Business, { CareProviderLocation, CareProviderPickupDay } from '/imports/api/Organization/Business';

class NewAnnouncements extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
        };
        this.maxAnnouncements = 5;
        this.getAnnouncements = this.getAnnouncements.bind(this);
    }

    getAnnouncements() {
        const allAnnouncements = this.props.announcements;
        return allAnnouncements.slice(this.state.offset, this.state.offset + this.maxAnnouncements);
    }

    render() {
        const announcements = this.props.announcements;
        return <div className="administratorWidget announcementWidget dashboard-icon announcements">
            <h2>{"Announcements for: " + Meteor.user().name + "  from  " + (this.props.business ? this.props.business.name + " (" + this.props.business.cpNumber + ")" : "None") + "  located at  " + this.props.location}</h2>
            <ul>
                {Meteor.user().inactive ?

                    <li key={Meteor.user()._id}>
                        <div>
                            <span className="anouncementName">*** THIS USER IS INACTIVE ***</span>
                            <time>{moment(Meteor.user().updatedAt).fromNow()}</time>
                        </div>
                    </li>
                    : ""
                }
                {announcements.length ?
                    this.getAnnouncements().map(announcement =>
                        <li key={announcement._id}>
                            <div>
                                <span className="anouncementName">{announcement.comments}</span>
                                <time>{moment(announcement.createdAt).fromNow()}</time>
                            </div>
                        </li>)

                    : <p>No announcements</p>
                }
            </ul>
            {this.props.announcements.length > this.maxAnnouncements
                ? <div className="pagination">
                    <button className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`}
                        onClick={event => this.setState({ offset: this.state.offset - this.maxAnnouncements })}><i
                            className="mdi mdi-arrow-left"></i></button>
                    <button
                        className={`next ${this.state.offset + this.maxAnnouncements > this.props.announcements.length ? 'disabled' : ''}`}
                        onClick={event => this.setState({ offset: this.state.offset + this.maxAnnouncements })}><i
                            className="mdi mdi-arrow-right"></i></button>
                </div>
                : null
            }
        </div>;
    }
}

export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        const subscription = Meteor.subscribe('/announcement/list');
        if (subscription.ready()) {
            const query = {};
            const announcements = Announcement.find(query, { sort: { createdAt: -1 } }).fetch();
            const business = Business.findOne({ _id: Meteor.user().profile.organizationId });
            let location = "None";
            if (business && business.location !== null) {
                location = CareProviderLocation.getIdentifier(business.location)
            }
            // console.log('Dashboard/Announcements.jsx:export:business:' + JSON.stringify(business));
            onData(null, { announcements, business, location });
        }
    }
}, NewAnnouncements);
