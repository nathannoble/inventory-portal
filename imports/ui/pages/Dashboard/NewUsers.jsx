import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import User from '/imports/api/User';

import compare from '/imports/api/helpers/compare';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class NewUsers extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      offset: 0,
    };
    this.maxUsers = 5;
    this.getVisibleUsers = this.getVisibleUsers.bind(this);
  }

  getVisibleUsers () {
    const allUsers = this.props.users;
    const visibleUsers = allUsers.slice(this.state.offset, this.state.offset + this.maxUsers);
    return visibleUsers;
  }

  render () {
    const users = this.props.users;
    return <div className="administratorWidget dashboard-icon newUsers">
      <h2>New Users</h2>
      {users.length
        ? <ul>
          {this.getVisibleUsers().map(user => <li key={user._id}>
            <Link to={`/user/${user._id}`}>
            <span className="name">{user.name}</span>
            <span className="type">{user.type === "Volunteer" ? "Center Director" : user.type}</span>
            <time>{moment(user.createdAt).fromNow()}</time>
            </Link>
          </li>)}
        </ul>
        : <p>No users</p>
      }
      {this.props.users.length > this.maxUsers
        ? <div className="pagination">
          <button className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`} onClick={event => this.setState({ offset: this.state.offset - this.maxUsers })}><i className="mdi mdi-arrow-left"></i></button>
          <button className={`next ${this.state.offset + this.maxUsers > this.props.users.length ? 'disabled' : ''}`} onClick={event => this.setState({ offset: this.state.offset + this.maxUsers })}><i className="mdi mdi-arrow-right"></i></button>
        </div>
        : null
      }
    </div>;
  }
}

export default container((props, onData) => {
  const subscription = Meteor.subscribe('/user/basic');
  if(subscription.ready()) {
    const users = User.find().fetch().sort((a, b) => compare(b.createdAt, a.createdAt));
    onData(null, { users });
  }
}, NewUsers);
