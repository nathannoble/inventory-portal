import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import User from '/imports/api/User';

import compare from '/imports/api/helpers/compare';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class MyUser extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      offset: 0,
    };
  }

  render () {
    const myUser = this.props.me;
    return <div className="administratorWidget dashboard-icon newUsers">
      <h2>{myUser.name + "  " + myUser.type}</h2>
    </div>;
  }
}

export default container((props, onData) => {
  //const subscription = Meteor.subscribe('/user/basic');
  //if(subscription.ready()) {
  //  const me = Meteor.user();
    const me = Meteor.user();
    onData(null, { me });
  //}
}, MyUser);
