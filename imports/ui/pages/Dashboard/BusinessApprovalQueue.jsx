import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';

import Business from '/imports/api/Organization/Business';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class BusinessApprovalQueue extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      offset: 0,
    };
    this.maxBusinesses = 3;
    this.getVisibleBusinesses = this.getVisibleBusinesses.bind(this);
  }

  getVisibleBusinesses () {
    const allBusinesses = this.props.businesses;
    const visibleBusinesses = allBusinesses.slice(this.state.offset, this.state.offset + this.maxBusinesses);
    return visibleBusinesses;
  }

  render () {
    const businesses = this.props.businesses;
    return <div className="administratorWidget dashboard-icon businessApprovalQueue">
      <h2>Business Approval Queue</h2>
      {businesses.length
        ? <ul>
          {this.getVisibleBusinesses().map(business => <li key={business._id}>
            <Link to={`/business/${business._id}`}>{business.name}</Link>
            <div className="actions">
              <button className="approve" onClick={event => Meteor.call('/organization/approve', business, handleError)}>Approve</button>
              <button className="deny" onClick={event => Meteor.call('/organization/deny', business, handleError)}>Deny</button>
            </div>
          </li>)}
        </ul>
        : <p>No businesses</p>
      }
      {this.props.businesses.length > this.maxBusinesses
        ? <div className="pagination">
          <button className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`} onClick={event => this.setState({ offset: this.state.offset - this.maxBusinesses })}><i className="mdi mdi-arrow-left"></i></button>
          <button className={`next ${this.state.offset + this.maxBusinesses > this.props.businesses.length ? 'disabled' : ''}`} onClick={event => this.setState({ offset: this.state.offset + this.maxBusinesses })}><i className="mdi mdi-arrow-right"></i></button>
        </div>
        : null
      }
    </div>;
  }
}

export default container((props, onData) => {
  // const subscription = Meteor.subscribe('/organization/business/list');
  if(true) {
    const businesses = Business.find({approved: {$exists: false}}).fetch();
    onData(null, { businesses });
  }
}, BusinessApprovalQueue);
