import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import Order, { OrderStatus, OrderBasicItemCategory } from '/imports/api/Order';

import compare from '/imports/api/helpers/compare';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

class NewOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0,
        };
        this.maxOrders = 5;
        this.getOrders = this.getOrders.bind(this);
    }

    getOrders() {
        const allOrders = this.props.orders;
        return allOrders.slice(this.state.offset, this.state.offset + this.maxOrders);
    }

    render() {
        const orders = this.props.orders;
        return <div className="administratorWidget dashboard-icon newOrders">
            <h2>Last 10 Orders</h2>
            {orders.length
                ? <ul>
                {this.getOrders().map(order => <li key={order._id}>
                    <Link to={`/order/${order._id}`}>
                        <span className="name">{order.childName}</span>
                        <span className="type">{order.comments}</span>
                        <time>{moment(order.pickupDate).fromNow()}</time>
                    </Link>
                </li>)}
            </ul>
                : <ul><li>No orders</li></ul>
            }
            {this.props.orders.length > this.maxOrders
                ? <div className="pagination">
                <button className={`previous ${this.state.offset <= 0 ? 'disabled' : ''}`}
                        onClick={event => this.setState({ offset: this.state.offset - this.maxOrders })}><i
                    className="mdi mdi-arrow-left"></i></button>
                <button
                    className={`next ${this.state.offset + this.maxOrders > this.props.orders.length ? 'disabled' : ''}`}
                    onClick={event => this.setState({ offset: this.state.offset + this.maxOrders })}><i
                    className="mdi mdi-arrow-right"></i></button>
            </div>
                : null
            }
        </div>;
    }
}

export default container((props, onData) => {
    const businessSubscription = Meteor.subscribe('/organization/business/list');
    if (businessSubscription.ready()) {
        const subscription = Meteor.subscribe('/order/list');
        if (subscription.ready()) {

            let dayOfWeekForPickupLocation = 4;  // Assume it's Friday
            let daysUntilPickupLocationDay = dayOfWeekForPickupLocation - (new Date()).getDay();

            if (daysUntilPickupLocationDay <= 0) {
                daysUntilPickupLocationDay += 7;
            }

            let endEditDate = new Date();
            endEditDate.setDate(endEditDate.getDate() + daysUntilPickupLocationDay - 6);

            console.log('NewOrders.jsx:render:endEditDate', endEditDate);
            //{"pickupDate.$date": {"$lte": endEditDate}},
            const query = {
                $and:[
                    {"pickupDate": {"$gte": endEditDate}}
                ]};
            //const orders = Order.find(query, {sort: {createdAt: -1}}).fetch();
            const orders = Order.find(query,{sort: {'createdAt': -1}, limit: 10}).fetch();
            console.log("pages/Dashboard/NewOrders.jsx:orders.length:" + orders.length);
            onData(null, {orders});
        }
    }
}, NewOrders);
