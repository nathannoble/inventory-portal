import { Meteor } from 'meteor/meteor';
import React from 'react';

import { Label } from '/imports/ui/helpers';
import DateTime from 'react-datetime';
import container from '/imports/ui/helpers/container';
import handleError from '/imports/ui/helpers/handleError';

class MyDates extends React.Component {
    constructor(props) {
        super(props);
       
        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            user: this.props.user
        };
    }

    render() {
        return <div className="administratorWidget dashboard-icon myDates2">
            <h2>Report Dates</h2>
            <div className="myDates">
                <Label
                    name="startDate"
                    element={DateTime}
                    text="Start Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("pages/Dashboard/MyDates.jsx:startDate:selectedOption:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                startDate: selectedOption ? selectedOption.toDate() : undefined,
                                endDate: undefined,
                                dateRange: undefined,
                            });
                        }
                    }}
                    timeFormat={false}
                    value={this.state.startDate}
                />
                <Label
                    name="endDate"
                    element={DateTime}
                    text="End Date"
                    onChange={(selectedOption) => {
                        //event.preventDefault();
                        console.log("pages/Dashboard/MyDates.jsx:" + JSON.stringify(selectedOption));
                        if (typeof selectedOption === "object") {
                            this.setState({
                                endDate: selectedOption ? selectedOption.toDate() : undefined,
                                dateRange: undefined,
                            });

                            if (this.state.startDate !== undefined && selectedOption.toDate() !== undefined) {

                                let startDate = this.state.startDate;
                                let endDate = selectedOption.toDate();

                                startDate.setHours(0);
                                startDate.setMinutes(0);
                                startDate.setSeconds(0);
                                startDate.setMilliseconds(0);

                                endDate.setHours(23);
                                endDate.setMinutes(59);
                                endDate.setSeconds(59);
                                endDate.setMilliseconds(999);

                                if (startDate < endDate) {
                                    console.log("pages/Dashboard/MyDates.jsx:manual:startDate:" + startDate);
                                    console.log("pages/Dashboard/MyDates.jsx:" + endDate);

                                    this.state.user.queryStartDate = startDate;
                                    this.state.user.queryEndDate = endDate;
                                    if (this.state.user.type === "Administrator")        
                                        Meteor.call('/user/administrator/update', this.state.user, handleError);
                                    else if (this.state.user.type === "Volunteer")
                                        Meteor.call('/user/volunteer/update', this.state.user, handleError);
                                }
                            }
                        }
                    }}
                    timeFormat={false}
                    value={this.state.endDate}
                />
            </div>
        </div>;
    }
}

export default container((props, onData) => {

    let startDate;
    let endDate;

    let now = new Date();

    var user = props.user;

    // This Week (Until this Friday at 5:00 PM)
    let nextFriday = 5;
    let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
    if (daysUntilPickupLocationDay < 0) {
        daysUntilPickupLocationDay += 7;
    } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
        daysUntilPickupLocationDay += 7;
    }

    if (user.queryStartDate && user.queryStartDate.toDateString() != now.toDateString()) {
        startDate = user.queryStartDate;
        endDate = user.queryEndDate;
    } else {
        startDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay - 7));
        endDate = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    startDate.setMilliseconds(0);

    endDate.setHours(23);
    endDate.setMinutes(59);
    endDate.setSeconds(59);
    endDate.setMilliseconds(999);

    //console.log("pages/Dashboard/MyDates.jsx:user:" + JSON.stringify(user));
    onData(null, { user,startDate, endDate });

}, MyDates);
