import { Meteor } from 'meteor/meteor';
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { withRouter } from 'react-router';
import DOMPurify from 'dompurify';
import queryString from 'query-string';

import container from '/imports/ui/helpers/container';

import handleError from '/imports/ui/helpers/handleError';

const PreviewEmail = container(
  (props, onData) => {
    const data = queryString.parse(props.location.search);
    Meteor.call(
      '/asset',
      `emails/${props.match.params.name}`,
      data,
      handleError({
        callback(text) {
          const html = text;
          // const html = DOMPurify.sanitize(text, {ADD_TAGS: ['style']}); // Wouldn't leave style tags
          onData(null, { html });
        },
      }),
    );
  },
  withRouter(({ html }) => <div id="email" dangerouslySetInnerHTML={{ __html: html }} />),
);

const Preview = props => (
  <Router basename="/preview">
    <Route path="/email/:name+" component={PreviewEmail} {...props} />
  </Router>
);

export default Preview;
