import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route, Redirect, Link } from 'react-router-dom';

import container from '/imports/ui/helpers/container';
import ErrorBoundary, { WrappedRoute } from '/imports/ui/helpers/errorBoundary';
import Authenticated from '/imports/ui/components/helpers/Authenticated';
import Navigation from '/imports/ui/components/helpers/Navigation';
import ScrollToTop from '/imports/ui/components/helpers/ScrollToTop';

import NotFound from '/imports/ui/pages/NotFound';

import User from '/imports/api/User';
import Document from '/imports/api/Document';

import Order from '/imports/api/Order';
import Announcement from '/imports/api/Announcement';
import Organization from '/imports/api/Organization';

import Entry from '/imports/ui/pages/Entry';
import Dashboard from '/imports/ui/pages/Dashboard';
import inIframe from '/imports/ui/helpers/inIframe';

import Title from '/imports/ui/components/helpers/Title';
import Head from '/imports/ui/components/helpers/Head';
import Logo from '/imports/ui/components/Logo';
import Avatar from '/imports/ui/components/Avatar';
import HelpButton from '/imports/ui/components/HelpButton';
import PDFButton from '/imports/ui/components/PDFButton';
import PrintButton from '/imports/ui/components/PrintButton';
import Search from '/imports/ui/components/Search';
import SearchLogView from '/imports/ui/components/Search/Log';
import OrderRoutes from '/imports/ui/components/Order/Routes';
import OrganizationRoutes from '/imports/ui/components/Organization/Routes';
import UserRoutes from '/imports/ui/components/User/Routes';

const Counts = new Mongo.Collection('counts');

const ProfileRedirecter = props => <Redirect to={`/user/${Meteor.userId()}`} {...props} />;

const App = props => (
  <Router>
    <ScrollToTop>
      <div className={`App${props.user ? ' loggedIn' : ''}`}>
        <Head />
        <Title >{location.pathname}</Title>
        {props.user ? (
          <header className="no-print">
            <ErrorBoundary>
              <Navigation {...props} />
            </ErrorBoundary>
            {/*<Logo />
             <Avatar user={props.user._id} />*/}
            <Avatar url="./img/nco-logo.jpg" />
            <HelpButton />
            <PDFButton />
            {/*<PrintButton />*/}
            {process.env.NODE_ENV === 'development' ? (
              <span id="userType" className="development">
                {props.user.type}
              </span>
            ) : null}
          </header>
        ) : null}
        <div className="body">
          <ErrorBoundary>
            <div id="subHeader" />
          </ErrorBoundary>
          <Switch>
            <Authenticated exact name="Dashboard" path="/" component={Dashboard} {...props} />
            <Authenticated exact path="/profile" component={ProfileRedirecter} {...props} />
            <Authenticated path={Order.path} component={OrderRoutes} {...props} />
            <Authenticated path={`${Order.path}UserOrg`} component={OrderRoutes} {...props} />
            <Authenticated
              path={`/:organizationType(organization|${Organization.children
                .map(child => child.className.toLowerCase())
                .join('|')})/:organizationId?`}
              component={OrganizationRoutes}
              {...props}
            />
            <Authenticated
              path={`/:userType(user|${User.getChildren(2)
                .map(child => child.className.toLowerCase().replace(/ /g, ''))
                .join('|')})/:userId?`}
              component={UserRoutes}
              {...props}
            />
            <WrappedRoute path="/login" component={Entry} {...props} />
            <WrappedRoute path="/signup/:role?/:organizationId?" component={Entry} {...props} />
            <WrappedRoute component={Entry} />
            <WrappedRoute component={NotFound} />
          </Switch>
        </div>
        <ErrorBoundary>
          <div id="modalWrapper" />
        </ErrorBoundary>
      </div>
    </ScrollToTop>
  </Router>
);

export default container((props, onData) => {
  //const subscription = Meteor.subscribe('/message/userCount', Meteor.userId());
  //if (subscription.ready()) {
      console.log("layouts/App.jsx:location/pathname:" + JSON.stringify(location.pathname));
      //const messageCount = Meteor.user() && Counts.findOne(Meteor.userId()) ? Counts.findOne(Meteor.userId()).count : 0;
    //if (Meteor.user() && inIframe()) window.parent.location = Meteor.absoluteUrl();
    onData(null, { user: Meteor.user()});
  //}
}, App);
