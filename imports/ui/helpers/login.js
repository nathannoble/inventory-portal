import { Meteor } from 'meteor/meteor';
import 'jquery-validation';
import handleError from '/imports/ui/helpers/handleError';
import $ from 'jquery';

const login = (component) => {
  const email = document.querySelector('[name=email]').value.toLowerCase();
  const password = document.querySelector('[name=password]').value;

  Meteor.loginWithPassword(
    email,
    password,
    handleError({
      callback() {
        component.props.history.push('/');
      },
    }),
  );
};

const validate = (component) => {
  $(component.loginForm).validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
      },
    },
    messages: {
      email: {
        required: 'Need an email address here',
        email: "This doesn't look like an email",
      },
      password: {
        required: 'Need a password here',
      },
    },
    submitHandler() {
      login(component);
    },
  });
};

export default function handleLogin({ component }) {
  validate(component);
}
