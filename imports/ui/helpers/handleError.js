/* eslint no-console: 0 */

import { Bert } from 'meteor/themeteorchef:bert';

const handleError = function () {

    var self = this;
    self.error = false;

    function throwError(error) {
        self.error = true;
        console.error(error);
        const { reason } = error.error && typeof error.error !== 'number' ? error.error : error;
        Bert.alert(reason, 'danger');
        if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
                el.innerHTML = `<p>${reason}</p>`;
            }
        }
        return true;
    }

    if (arguments.length > 0) {
        const args = Array.from(arguments);
        //console.log("1:handleError:args:error:" + JSON.stringify(args) + ":" + self.error);

        args.forEach((argument) => {
            if (argument) {
                //console.log("2:handleError:argument.error:" + argument.error);
            }
            if (argument && argument.error) {
            //if (argument && argument.constructor.name === 'errorClass') {
                self.error = throwError(argument);
                //console.log("3:handleError:args:error:" + JSON.stringify(args) + ":" + self.error);
            }
        });

        const options = args[0];

        //console.log("4:handleError:args:options:error:" + JSON.stringify(args) + ":" + JSON.stringify(options) + ":" + self.error);

        if (self.error === false && options) {
            if (options.filtered) {
                const {
                    confirmationText = false,
                    position = 'growl-bottom-right',
                    callback = function () {
                    },
                    } = options;
                if (confirmationText)
                    Bert.alert(confirmationText, 'success', position);
                    if (document.querySelectorAll('.bert-content').length > 0) {
                        let el = document.querySelectorAll('.bert-content')[0];
                        if (el) {
                            el.innerHTML = `<p>${confirmationText}</p>`;
                        }
                    }
                if (callback) callback.apply(callback, args.slice(2)); // Run the callback with any returned arguments
            } else {
                options.filtered = true;
                return handleError.bind(handleError, options);
            }
        }
    }
};

export default handleError;
