import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Route } from 'react-router-dom';

export const WrappedRoute = props => <ErrorBoundary><Route {...props} /></ErrorBoundary>;

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props);
    this.state = {hasError: false};
  }
  componentDidCatch(error, info) {
    console.log('error', error);
    this.setState({ hasError: error });
  }
  render() {
    if(this.state.hasError) {
      const error = this.state.hasError;
      return (
        <div className="error">
          <h1>Something went wrong</h1>
          <p>Technical information:</p>
          <pre><code>{error.name}: {error.message}</code></pre>
          <pre style={{overflow: 'scroll'}}>
          {error.stack
            .split('\n')
            .map(line => line
              .replace(/\?hash=[\w\d]+/, '')
              .replace(Meteor.absoluteUrl(), '')
            )
            .map(line => <code>{line}</code>)
          }
          </pre>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
