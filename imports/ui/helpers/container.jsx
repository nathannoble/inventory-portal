import React from 'react';
import { compose } from 'react-komposer';
import { Tracker } from 'meteor/tracker';

import Loader from '/imports/ui/components/helpers/Loader';
import CCError from '/imports/ui/components/helpers/CCError';

const getTrackerLoader = reactiveMapper => (props, onData, env) => {
  let trackerCleanup = null;
  const handler = Tracker.nonreactive(() =>
    Tracker.autorun(() => {
      trackerCleanup = reactiveMapper(props, onData, env);
    }));

  return () => {
    if (typeof trackerCleanup === 'function') trackerCleanup();
    return handler.stop();
  };
};

export default function container(composer, Component, options = {}) {
  if (!options.component) options.component = Component.displayName || 'component';
  if (!options.loadingHandler) {
    options.loadingHandler = () => <Loader text={options.component} />;
  }
  if (!options.errorHandler) {
    options.errorHandler = error => <CCError error={error} />;
  }
  return compose(getTrackerLoader(composer), options)(Component);
}
