import moment from 'moment';
import { CareProviderPickupDay } from '/imports/api/Organization/Business';

// 1/3/25 NN

// Start Ordering: 1/6/25
// End Ordering: 3/14/25

// Cutoff	  Pickup
// 1/10	    1/15-17
// 1/17	    1/22-24
// 1/24	    1/29-31
// 1/31	    2/5-7
// 2/7	    2/12-14
// 2/14	    2/19-21
// 2/21	    2/26-28
// 2/28	    3/5-7
// 3/7	    3/12-14
// 3/14	    3/19-21

const checkDate = (date,dayOfWeekForPickupLocation) => {

  const currentDayOfWeek = moment(date).day();
  const now = new Date();

  // This Week (Until this Friday at 5:00 PM)
  let nextFriday = 5;
  let daysUntilPickupLocationDay = nextFriday - (new Date()).getDay();
  if (daysUntilPickupLocationDay < 0) {
      daysUntilPickupLocationDay += 7;
  } else if (daysUntilPickupLocationDay === 0 && now.getHours() > 16) {
      daysUntilPickupLocationDay += 7;
  }

  let thisFridayat5 = new Date(now.getTime() + 86400000 * (daysUntilPickupLocationDay));

  thisFridayat5.setHours(17);
  thisFridayat5.setMinutes(0);
  thisFridayat5.setSeconds(0);
  thisFridayat5.setMilliseconds(0);

  console.log("checkDate.js:pickupDate:daysUntilPickupLocationDay:" + daysUntilPickupLocationDay);
  console.log("checkDate.js:pickupDate:thisFridayat5:" + thisFridayat5);

  let d = null;

  if (moment.isMoment(date)) {
    d = date.toDate();
  } else {
    d = date;
  }

  d.setHours(17);
  d.setMinutes(0);
  d.setSeconds(0);
  d.setMilliseconds(0);

  if (Meteor.user().type !== 'Caseworker') {
    console.log("checkDate.js:setting pickupDate:d:" + d);
    //order.pickupDate = d;
    //Meteor.call('/order/update', order, handleError);
    return true;

  } else {

    if (
      (d < new Date(2025, 0, 15, 0, 0, 0, 0) || d >= new Date(2025, 2, 22, 0, 0, 0, 0))
      || (d >= new Date(2025, 0, 18, 0, 0, 0, 0) && d < new Date(2025, 0, 22, 0, 0, 0, 0))
      || (d >= new Date(2025, 0, 25, 0, 0, 0, 0) && d < new Date(2025, 0, 29, 0, 0, 0, 0))
      || (d >= new Date(2025, 1, 1, 0, 0, 0, 0) && d < new Date(2025, 1, 5, 0, 0, 0, 0))
      || (d >= new Date(2025, 1, 8, 0, 0, 0, 0) && d < new Date(2025, 1, 12, 0, 0, 0, 0))
      || (d >= new Date(2025, 1, 15, 0, 0, 0, 0) && d < new Date(2025, 1, 19, 0, 0, 0, 0))
      || (d >= new Date(2025, 1, 22, 0, 0, 0, 0) && d < new Date(2025, 1, 26, 0, 0, 0, 0))
      || (d >= new Date(2025, 2, 1, 0, 0, 0, 0) && d < new Date(2025, 2, 5, 0, 0, 0, 0))
      || (d >= new Date(2025, 2, 8, 0, 0, 0, 0) && d < new Date(2025, 2, 12, 0, 0, 0, 0))
      || (d >= new Date(2025, 2, 15, 0, 0, 0, 0) && d < new Date(2025, 2, 19, 0, 0, 0, 0))
    ) {

      let message = 'No pickup days outside of 1/15-1/17, 1/22-1/24, 1/29-1/31, 2/5-2/7, 2/12-2/14, 2/19-2/21,2/26-2/28,3/5-3/7, 3/12-3/14, 3/19-3/21 are permitted because the center is closed.';
      Bert.alert(message, 'danger');
      if (document.querySelectorAll('.bert-content').length > 0) {
        let el = document.querySelectorAll('.bert-content')[0];
        if (el) {
          el.innerHTML = `<p>${message}</p>`;
        }
      }
      return false;
    }

    if (d <= thisFridayat5) {
      console.log("checkDate.js:pickupDate less than thisFriday@5:" + d);
      let message = 'You must choose a pickup day after this Friday at 5:00 PM for the Pickup Day';
      Bert.alert(message, 'danger');
      if (document.querySelectorAll('.bert-content').length > 0) {
        let el = document.querySelectorAll('.bert-content')[0];
        if (el) {
          el.innerHTML = `<p>${message}</p>`;
        }
      }
      return false;
    } else if (dayOfWeekForPickupLocation === currentDayOfWeek) {

      if (d >= new Date(2025, 0, 15, 0, 0, 0, 0) && d < new Date(2025, 0, 18, 0, 0, 0, 0)) {
        if (now > new Date(2025, 0, 10, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 1/15-1/17 is 1/6/25-1/10/25 at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 0, 22, 0, 0, 0, 0) && d < new Date(2025, 0, 25, 0, 0, 0, 0)) {
        if (now > new Date(2025, 0, 17, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 1/22-1/24 is 1/6/25-1/17/25 at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 0, 29, 0, 0, 0, 0) && d < new Date(2025, 1, 1, 0, 0, 0, 0)) {
        if (now > new Date(2025, 0, 24, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 1/29-1/31 is 1/6/25-1/24/25 at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 1, 5, 0, 0, 0, 0) && d < new Date(2025, 1, 8, 0, 0, 0, 0)) {
        if (now > new Date(2025, 0, 31, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 2/5-2/8 is 1/6/25-1/31/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 1, 12, 0, 0, 0, 0) && d < new Date(2025, 1, 15, 0, 0, 0, 0)) {
        if (now > new Date(2025, 1, 7, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 2/12-2/14 is 1/6/25-2/7/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 1, 19, 0, 0, 0, 0) && d < new Date(2025, 1, 22, 0, 0, 0, 0)) {
        if (now > new Date(2025, 1, 14, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 2/19-2/21 is 1/6/25-2/14/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 1, 26, 0, 0, 0, 0) && d < new Date(2025, 2, 1, 0, 0, 0, 0)) {
        if (now > new Date(2025, 1, 21, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 2/26-2/28 is 1/6/25-2/21/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 2, 5, 0, 0, 0, 0) && d < new Date(2025, 2, 8, 0, 0, 0, 0)) {
        if (now > new Date(2025, 1, 28, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 3/5-3/7 is 1/6/25-2/28/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 2, 12, 0, 0, 0, 0) && d < new Date(2025, 2, 15, 0, 0, 0, 0)) {
        if (now > new Date(2025, 2, 7, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 3/12-3/14 is 1/6/25-3/7/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      if (d >= new Date(2025, 2, 19, 0, 0, 0, 0) && d < new Date(2025, 2, 22, 0, 0, 0, 0)) {
        if (now > new Date(2025, 2, 14, 17, 0, 0, 0) || now < new Date(2025, 0, 6, 8, 0, 0, 0)) {
          let message = 'The allowed range for orders for the time period 3/19-3/21 is 1/6/25-3/14/25  at 5:00 PM';
          Bert.alert(message, 'danger');
          if (document.querySelectorAll('.bert-content').length > 0) {
            let el = document.querySelectorAll('.bert-content')[0];
            if (el) {
              el.innerHTML = `<p>${message}</p>`;
            }
          }
          return false;
        }
      }
      return true;

    } else {

      let message = 'You must choose a ' + CareProviderPickupDay.getIdentifier(dayOfWeekForPickupLocation) + ' for the Pickup Day';
      Bert.alert(message, 'danger');
      if (document.querySelectorAll('.bert-content').length > 0) {
        let el = document.querySelectorAll('.bert-content')[0];
        if (el) {
          el.innerHTML = `<p>${message}</p>`;
        }
      }
    }
  }
};

export default checkDate;
