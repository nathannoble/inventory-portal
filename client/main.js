import '/imports/startup/client';

Accounts.onResetPasswordLink((token, done) => {
  let newPassword = '';
  let newPasswordConfirmation = 'confirm';
  while (newPassword !== newPasswordConfirmation) {
    newPassword = prompt('Enter your new password');
    newPasswordConfirmation = prompt('Enter the same password again');
  }
  Accounts.resetPassword(token, newPassword, (error) => {
    console.error(error);
  });
});
